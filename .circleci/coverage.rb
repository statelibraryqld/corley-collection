#!/usr/bin/env ruby

require "simplecov"
require "down"

user = ENV.fetch("CIRCLE_PROJECT_USERNAME")
repo = ENV.fetch("CIRCLE_PROJECT_REPONAME")
build = ENV.fetch("CIRCLE_BUILD_NUM")
token = ENV.fetch("CIRCLE_TOKEN")

api_url = "https://circleci.com/api/v1.1/project/github/#{user}/#{repo}/#{build}/artifacts?circle-token=#{token}"

artifacts = JSON.parse(Down.open(api_url).read)

coverage_dir = Pathname(Dir.pwd).join("coverage").realpath

SimpleCov.coverage_dir(coverage_dir)

resultsets = artifacts.map { |a|
  if a["path"].include?("coverage/.resultset.json")
    response = Down.open("#{a['url']}?circle-token=#{token}").read
    JSON.parse(response)
  end
}.compact

resultsets.each do |resultset|
  resultset.each do |command_name, data|
    result = SimpleCov::Result.from_hash(command_name => data)
    SimpleCov::ResultMerger.store_result(result)
  end
end

merged_result = SimpleCov::ResultMerger.merged_result
merged_result.command_name = "RSpec"

html_formatter = SimpleCov::Formatter::HTMLFormatter.new
html_formatter.format(merged_result)

File.open(coverage_dir.join(".last_run.json"), "w") do |f|
  f << JSON.dump(result: {covered_percent: merged_result.covered_percent})
end
