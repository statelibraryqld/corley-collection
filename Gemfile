source "https://rubygems.org"

ruby "2.5.1"

gem "rake", "~> 12.0"

gem "bootsnap"
gem "snowflakes", git: "https://github.com/icelab/snowflakes", branch: "support-max-connections-setting"

# Web framework
gem "dry-system", "~> 0.10"
gem "dry-web", "~> 0.8"
gem "dry-web-roda", "~> 0.7"
gem "puma", "~> 3.10"
gem "rack", "~> 2.0"
gem "rack-attack", "~> 5.0"
gem "rack-cors", require: "rack/cors"
gem "rack-rewrite", "~> 1.5.1"
gem "rack-ssl"
gem "rack_csrf"
gem "roda", "2.29"

# Database persistence
gem "elasticsearch-dsl"
gem "pg", "~> 1.0"
gem "redis", "~> 4.0"
gem "rom", "~> 4.2", ">= 4.2.1"
gem "rom-core", git: "https://github.com/rom-rb/rom.git", branch: "master"
gem "rom-elasticsearch", git: "https://github.com/andrewcroome/rom-elasticsearch.git", branch: "per-page-fix"
gem "rom-http", git: "https://github.com/rom-rb/rom-http.git", branch: "master"
gem "rom-sql", "~> 2.3"
gem "sequel_pg", "~> 1.8"

# Application dependencies
gem "addressable"
gem "aws-sdk-s3"
gem "babosa"
gem "bcrypt"
gem "down", "~> 4.5"
gem "dry-auto_inject", "~> 0.4", ">= 0.4.6"
gem "dry-core", "~> 0.4", ">= 0.4.5"
gem "dry-matcher", "~> 0.6"
gem "dry-monads", "= 1.0.0.rc1"
gem "dry-struct", "~> 0.5"
gem "dry-transaction", "~> 0.12"
gem "dry-types", "~> 0.13"
gem "dry-validation", "~> 0.12"
gem "dry-view", "~> 0.5"
gem "formalist", "~> 0.5.1"
gem "html_truncator", "~> 0.4"
gem "http"
gem "i18n", "~> 0.9"
gem "icy-content_buffer", "~> 0.1"
gem "postmark"
gem "premailer", "~> 1.11"
gem "que", "= 1.0.0.beta3"
gem "rainbow"
gem "redcarpet"
gem "ruby-thumbor"
gem "sanitize", git: "https://github.com/icelab/sanitize", branch: "remove-nokogumbo" # Using our own fork here due to memory usage issues with the original source
gem "slim", "~> 3.0"
gem "time_math2", "~> 0.1"
gem "uuid", "~> 2.3"
gem "yajl-ruby", "~> 1.4"

# 3rd party services
gem "bugsnag"

group :development do
  gem "foreman"
  gem "icelab-style", git: "https://github.com/icelab/icelab-style", branch: "master"
end

group :development, :test do
  gem "bundler-audit", "~> 0.6"
  gem "danger", "~> 5.5"
  gem "danger-rubocop", "~> 0.6"
  gem "guard"
  gem "guard-rack"
  gem "pry-byebug", "~> 3.5"
  gem "rom-factory", git: "https://github.com/rom-rb/rom-factory", branch: "master"
end

group :test do
  gem "capybara", "~> 3.0"
  gem "capybara-chromedriver-logger", "~> 0.2"
  gem "capybara-screenshot", "~> 1.0"
  gem "chromedriver-helper", "~> 1.2"
  gem "database_cleaner", "~> 1.6"
  gem "rspec", "~> 3.7"
  gem "rspec_junit_formatter", "~> 0.3"
  gem "selenium-webdriver", "~> 3.11"
  gem "simplecov", "~> 0.15"
  gem "waitutil"
  gem "webmock", "~> 3.4"
end
