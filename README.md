# Corley Collection

The Corley Collection web application.

## Requirements

The app uses system services managed by [asdf](https://github.com/asdf-vm/asdf). Install this first.

## Installation

After cloning, run:

```
./bin/bootstrap
```

## Running system services

Make sure you have overmind installed then use `bin/services`

```
./bin/services
```

## Setup

After the system services have started, run:

```
./bin/setup
```

## After pulling changes

After pulling changes from the remote, run:

```
./bin/update
```

This will install any new dependencies and run any migrations.

## Running the app

After updating, run:

```
./bin/server
```

## Hosting

Production application:

https://api.corley.slq.qld.gov.au, (corley-collection.herokuapp.com)

Staging application:

https://staging-api.corley.slq.qld.gov.au, (corley-collection-staging.herokuapp.com)

## Documentation

Find api documentation here: https://api.corley.slq.qld.gov.au/api-docs/index.html#introduction

These docs are generated with: https://github.com/icelab/corley-collection-api-docs

