require "bundler/setup"
require "rake"
require "snowflakes/test_tasks"
require "time"

require_relative "system/corley_collection/container"
CorleyCollection::Container.start :exception_notifier

namespace :db do
  desc "Remove databases created for topic branches that no longer exist"
  task drop_topic_dbs: :setup do
    if CorleyCollection::Container.env != :development
      puts "=> this task is only available in development"
      exit
    end

    db = CorleyCollection::Container['persistence.db']

    sql = <<-SQL
      SELECT datname AS name FROM pg_database
      WHERE
        datistemplate = false AND
        datname LIKE 'corley_collection_%' AND
        datname != 'corley_collection_test' AND
        datname != 'corley_collection_development' AND
        datname != 'corley_collection_production' AND
        datname != '#{db.opts[:database]}'
    SQL

    db[sql].each do |tuple|
      puts "=> dropping #{tuple[:name]}"
      db.execute(%(DROP DATABASE "#{tuple[:name]}"))
    end
  end

  desc "Init persistence"
  task :setup do
    require_relative "system/corley_collection/container"
    CorleyCollection::Container.init(:persistence)
  end
end

namespace :assets do
  desc "Compile assets with webpack"
  task :precompile do
    Rake::Task["assets:clobber"].invoke
    system "bin/run assets precompile"
  end

  desc "Remove compiled assets"
  task :clobber do
    system "bin/run assets clobber"
  end
end

namespace :search do
  desc "Start search system"
  task :system do
    require "./backend/search/system/boot"
  end

  desc "Check environment - prevents us resetting production"
  task :check_environment do
    if CorleyCollection::Container.env == :production
      raise "Task disabled in production. Please reset entities one at a time via `rake search:reset_entity[entity_name]`"
    end
  end

  desc "Create indices"
  task create_indices: :system do
    es = Search::Container['es.rom']

    es.relations.each do |name, relation|
      next if es.gateways[:default].dataset?(name)
      puts "=> creating index for #{name.inspect}"
      relation.create_index unless relation.class.multi_index_types
    end
  end

  desc "Delete indices"
  task delete_indices: :system do
    es = Search::Container['es.rom']

    es.relations.each do |name, relation|
      next unless es.gateways[:default].dataset?(name)
      puts "=> deleting index for #{name.inspect}"
      relation.delete_index
    end
  end

  desc "Refresh indices"
  task refresh_indices: :system do
    Search::Container['es.rom'].relations.each do |name, relation|
      puts "=> refreshing index for #{name.inspect}"
      relation.refresh
    end
  end

  desc "Re-index data"
  task reindex: :system do
    require "./system/corley_collection/container"

    CorleyCollection::Container.init(:persistence)

    es = Search::Container['es.rom']
    relations = es.relations

    CorleyCollection::Container['persistence.rom'].relations.each do |name, relation|
      if relations.key?(name.to_sym)
        puts "=> re-indexing #{name.inspect} via background jobs"

        relation.pluck(:id).each do |id|
          Search::Container["#{name}.operations.index"].enqueue(id)
        end
      end
    end
  end

  desc "Reset a particular entity - rake reset_entity[articles]"
  task :reset_entity, [:entity_names] => :system do |t, args|
    entity_names = args.entity_names

    raise "must provide an entity name(s)" unless entity_names

    # support multiple entities via rake search:reset_entity[assets:articles:news_items]
    entity_names.split(":").each do |entity_name|
      es = Search::Container['es.rom']

      puts "=> resetting #{entity_name}"

      es_relation = es.relations[entity_name.to_sym]

      if es.gateways[:default].dataset?(es_relation.name)
        puts "=> deleting #{entity_name} index"
        es_relation.delete_index
      end

      puts "=> creating index for #{entity_name}"
      es_relation.create_index unless es_relation.class.multi_index_types

      require "./system/corley_collection/container"
      CorleyCollection::Container.init(:persistence)

      relation = CorleyCollection::Container['persistence.rom'].relations[entity_name.to_sym]

      puts "=> re-indexing #{entity_name} via background jobs"

      relation.pluck(:id).each do |id|
        Search::Container["#{entity_name}.operations.index"].enqueue(id)
      end
    end
  end

  desc "Reset indices and re-index"
  task reset: %i[check_environment delete_indices create_indices reindex]

  desc "Reset indices and re-index in production"
  task reset__production: %i[delete_indices create_indices reindex]
end

namespace :exception_notifier do
  desc "Send a test to the exception notifier"
  task :test do
    raise "test error raised at #{Time.now}"
  end
end

namespace :scheduled do
  task :system do
    require "./apps/admin/system/boot"
  end

  task :main do
    require "./apps/main/system/boot"
  end

  desc "Notify admins of recently created stories"
  task :new_story_summary => [:system] do
    Admin::Container["notifications.recent_stories"].call
  end

  desc "Send weekly locality digest to contributors"
  task :send_weekly_locality_digest => [:main] do
    # Heroku schedules this task once a day, but we only send on Saturdays
    if Time.now.saturday?
      Main::Container["locality_notifications.operations.enqueue"].call
    end
  end
end
