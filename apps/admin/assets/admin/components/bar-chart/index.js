import Chart from "chart.js";

/**
 * Display a bar chart
 * @param  {Node} el Element
 * @return {Void}
 */

export default function barChart(el, props) {
  let context = el.getContext("2d");

  let data = {
    labels: props.labels,
    datasets: props.datasets
  };

  new Chart(context, {
    type: "bar",
    data: data,
    options: {
      responsive: true,
      scales: {
        xAxes: [{ gridLines: { display: false } }],
        yAxes: [
          {
            ticks: {
              beginAtZero: true
            }
          }
        ]
      }
    }
  });
}
