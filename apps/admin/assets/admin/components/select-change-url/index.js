/**
 * Change URL when the select `el` changes
 * @param  {Node} el Element
 * @return {Void}
 */

export default function selectChangeUrl(el) {
  el.addEventListener("change", e => {
    const url = el.value;
    window.location = url;
  });
}
