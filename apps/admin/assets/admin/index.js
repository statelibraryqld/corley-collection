import roneo from "roneo";
import domready from "domready";
import viewloader from "viewloader";
import views from "./views";

let viewloaderManager = viewloader(views);

/**
 * Kick roneo immediately
 */

roneo();

/**
 * Kick things off on domReady
 */
domready(function onDomready() {
  viewloaderManager.callViews();
});

document.addEventListener("turbolinks:load", function onTurboLinksLoad() {
  viewloaderManager.callViews();
});

document.addEventListener(
  "turbolinks:before-visit",
  function onTurboLinksBeforeVisit() {
    viewloaderManager.destroyViews();
  }
);
