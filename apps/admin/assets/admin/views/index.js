/**
 * Singleton object we bind use to bind DOM elements to functions through
 * the viewloader library.
 *
 * snakeCase-named functions are matched to dasher-ize-d DOM nodes:
 *
 *   <div class="foo" data-view-foo-bar="hello">
 *   views.fooBar = function(el, props) {
 *     el.classList.has("foo") // true
 *     props === "hello" // true
 *   }
 *
 * @type {Object}
 */

import barChart from "../components/bar-chart";
import selectChangeUrl from "../components/select-change-url";

const views = {
  selectChangeUrl: selectChangeUrl,
  barChart: barChart
};

export default views;
