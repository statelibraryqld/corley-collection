// Import the base JS, basically our entry point for JS. If your entry
// doesn't require JS, you can comment this out or remove it.
import "./index.css";

// This will inspect all subdirectories from the context (this file) and
// require files matching the regex.
// https://webpack.js.org/guides/dependency-management/#require-context
require.context(".", true, /^\.\/.*\.(jpe?g|png|gif|svg|woff2?|ttf|otf|eot)$/);
