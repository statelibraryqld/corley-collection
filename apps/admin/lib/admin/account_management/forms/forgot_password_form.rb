require "admin/view/form"

module Admin
  module AccountManagement
    module Forms
      class ForgotPasswordForm < Admin::View::Form
        prefix :user

        define do
          text_field :email, label: "Email", placeholder: "your@email.com", validation: {filled: true}
        end
      end
    end
  end
end
