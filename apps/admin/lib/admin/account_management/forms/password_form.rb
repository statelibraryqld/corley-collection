require "admin/view/form"

module Admin
  module AccountManagement
    module Forms
      class PasswordForm < Admin::View::Form
        prefix :user

        define do
          text_field :password,
            password: true,
            label: "Password",
            placeholder: "Minimum 8 characters",
            validation: {filled: true}

          text_field :password_confirmation,
            password: true,
            label: "Confirm password",
            validation: {filled: true}
        end
      end
    end
  end
end
