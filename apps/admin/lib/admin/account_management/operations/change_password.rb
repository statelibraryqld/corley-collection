require "corley_collection/operation"
require "admin/import"
require "admin/account_management/validation/password_form"

module Admin
  module AccountManagement
    module Operations
      class ChangePassword < CorleyCollection::Operation
        include Import[
          "user_repo",
          "core.authentication.encrypt_password"
        ]

        def call(id, params)
          validation = Validation::PasswordForm.(params)

          if validation.success?
            result = user_repo.update(id, prepare_attributes(validation.to_h))

            Success(result)
          else
            Failure(validation)
          end
        end

        private

        def prepare_attributes(params)
          # Encrypt the new password and clear the password reset token
          params.merge(
            encrypted_password: encrypt_password.(params[:password]),
            password_reset_token: nil,
            password_reset_token_expires_at: nil
          )
        end
      end
    end
  end
end
