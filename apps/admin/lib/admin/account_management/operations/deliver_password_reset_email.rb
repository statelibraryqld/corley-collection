require "corley_collection/operation"
require "admin/import"

module Admin
  module AccountManagement
    module Operations
      class DeliverPasswordResetEmail < CorleyCollection::Operation
        include Import[
          "mail.admin.password_reset_success",
          "mail.admin.password_reset_failure",
        ]

        def call(input)
          result =
            if input[:user]
              password_reset_success.enqueue(input[:user])
            else
              password_reset_failure.enqueue(input[:email])
            end

          Success(result)
        end
      end
    end
  end
end
