require "corley_collection/operation"
require "admin/import"
require "admin/account_management/validation/forgot_password_form"

module Admin
  module AccountManagement
    module Operations
      class InitiatePasswordReset < CorleyCollection::Operation
        include Import[
          "user_repo",
          "core.authentication.generate_secret_token"
        ]

        def call(params)
          validation = Validation::ForgotPasswordForm.(params)

          if validation.success?
            email = validation[:email]
            user = user_repo.by_email(email)
            secret_token = generate_secret_token.(user_repo.method(:password_reset_token_exists?))

            if user
              user_repo.update(
                user.id,
                password_reset_token: secret_token.value,
                password_reset_token_expires_at: secret_token.expires_at
              )

              Success(user: user)
            else
              Success(email: email)
            end
          else
            Failure(validation)
          end
        end
      end
    end
  end
end
