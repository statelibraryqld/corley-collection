require "admin/transaction"

module Admin
  module AccountManagement
    module Transactions
      class ResetPassword < Admin::Transaction
        step :initiate_password_reset, with: "account_management.operations.initiate_password_reset"
        step :deliver_password_reset_email, with: "account_management.operations.deliver_password_reset_email"
      end
    end
  end
end
