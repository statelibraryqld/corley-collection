# auto_register: false

require "corley_collection/validation/form"

module Admin
  module AccountManagement
    module Validation
      ForgotPasswordForm = CorleyCollection::Validation.Form do
        required(:email, :string).filled(:email?)
      end
    end
  end
end
