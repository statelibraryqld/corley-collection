# auto_register: false

require "corley_collection/validation/form"

module Admin
  module AccountManagement
    module Validation
      # We can't use type specs here due to an issue with using the confirmation macro with type specs
      # See: https://github.com/dry-rb/dry-validation/issues/367
      PasswordForm = CorleyCollection::Validation.Form do
        configure do
          config.type_specs = false
        end

        required(:password).filled(min_size?: 8).confirmation
      end
    end
  end
end
