require "singleton"
require "dry/events/publisher"

module Admin
  class ApplicationEvents
    include Singleton
    include Dry::Events::Publisher[:admin]

    %w[
      photograph
      tag
      user
    ].each do |entity|
      register_event("#{entity}.created")
      register_event("#{entity}.updated")
      register_event("#{entity}.deleted")
    end
  end
end
