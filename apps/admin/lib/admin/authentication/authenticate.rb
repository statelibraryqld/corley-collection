require "admin/import"
require "corley_collection/authentication/authenticate"

module Admin
  module Authentication
    class Authenticate < CorleyCollection::Authentication::Authenticate
      include Admin::Import["core.authentication.encrypt_password", "user_repo"]

      def fetch(email)
        user_repo.by_email(email)
      end
    end
  end
end
