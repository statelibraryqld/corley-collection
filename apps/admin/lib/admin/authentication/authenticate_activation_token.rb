require "corley_collection/operation"
require "admin/import"

module Admin
  module Authentication
    class AuthenticateActivationToken < CorleyCollection::Operation
      include Import["user_repo"]

      def call(activation_token)
        user = user_repo.by_activation_token(activation_token)

        if user
          Success(user)
        else
          Failure(:activation_token_invalid)
        end
      end
    end
  end
end
