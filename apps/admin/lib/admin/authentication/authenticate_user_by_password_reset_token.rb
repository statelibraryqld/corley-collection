require "corley_collection/operation"
require "admin/import"

module Admin
  module Authentication
    class AuthenticateUserByPasswordResetToken < CorleyCollection::Operation
      include Import["user_repo"]

      def call(password_reset_token)
        user = user_repo.by_current_password_reset_token(password_reset_token)

        if user
          Success(user)
        else
          Failure(:password_reset_token_invalid)
        end
      end
    end
  end
end
