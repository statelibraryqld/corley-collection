require "corley_collection/operation"
require "admin/import"

module Admin
  module Authentication
    class Authorize < CorleyCollection::Operation
      include Admin::Import["core.authentication.encrypt_password", "user_repo"]

      def call(session)
        id = session[:user_id]
        user = id && user_repo[id]

        if user
          Success(user)
        else
          Failure(:unauthorized)
        end
      end
    end
  end
end
