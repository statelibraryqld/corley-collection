# auto_register: false

require "rom/changeset"

module Admin
  module Changesets
    class NewTags < ROM::Changeset::Create
      map do |name|
        tags.fetch(name)
      end

      private

      def tags
        @tags ||= relation.tags.dataset.to_hash(:name)
      end
    end
  end
end
