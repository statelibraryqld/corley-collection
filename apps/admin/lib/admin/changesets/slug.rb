# auto_register: false

require "admin/import"

module Admin
  module Changesets
    module Slug
      def self.included(base)
        base.include Import["support.generate_slug"]
        super
      end

      private

      def tuple_with_slug(tuple, uniqueness_check: nil, scope: nil, candidate: nil)
        if (slug_candidate = slug_candidate(tuple, candidate: candidate))
          new_slug = generate_slug.(
            slug_candidate,
            uniqueness_check: uniqueness_check,
            scope: scope
          )
          tuple = tuple.merge(slug: new_slug)
        end

        tuple
      end

      def slug_candidate(tuple, candidate: nil)
        if slug_empty?(tuple)
          candidate || tuple[:title]
        elsif slug_changed?(tuple)
          tuple[:slug]
        end
      end

      def slug_empty?(tuple)
        command_type == :create ? tuple[:slug].nil? : tuple.key?(:slug) && tuple[:slug].nil?
      end

      def slug_changed?(tuple)
        command_type == :update ? original[:slug] != tuple[:slug] : true
      end
    end
  end
end
