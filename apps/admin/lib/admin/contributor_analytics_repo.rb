require "admin/repository"

module Admin
  class ContributorAnalyticsRepo < Repository[:contributors]
    def total_contributors_count
      contributors.count
    end

    def created_contributor_stats
      created_contributors_by_month.to_a
    end
  end
end
