require "admin/repository"

module Admin
  class ContributorImageAnalyticsRepo < Repository[:contributor_images]
    def total_contributor_images_count
      contributor_images.count
    end

    def created_contributor_image_stats
      created_contributor_images_by_month.to_a
    end
  end
end
