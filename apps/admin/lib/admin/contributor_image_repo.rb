require "admin/repository"

module Admin
  class ContributorImageRepo < Repository[:contributor_images]
    commands update: :by_pk, delete: :by_pk

    def index(page = 1)
      contributor_images.combine(:contributor).order { created_at.desc }.page(page)
    end

    def for_editing(id)
      contributor_images.by_pk(id).one!
    end

    def for_export
      contributor_images.combine(:contributor, story: :photograph).order(:id)
    end
  end
end
