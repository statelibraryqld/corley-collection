require "admin/view/form"
require "admin/import"

module Admin
  module ContributorImages
    class Form < Admin::View::Form
      prefix :contributor_image

      define do
        text_field :caption, label: "Caption"
        check_box :published, label: "Published", hint: "Whether this image will appear in the contributor's story"
      end
    end
  end
end
