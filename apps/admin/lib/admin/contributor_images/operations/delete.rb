require "corley_collection/operation"
require "admin/import"

module Admin
  module ContributorImages
    module Operations
      class Delete < CorleyCollection::Operation
        include Import["contributor_image_repo", "story_repo", "photograph_repo", "application_events"]

        def call(id)
          contributor_image = contributor_image_repo.delete(id)

          story = story_repo[contributor_image.story_id]
          photograph = photograph_repo[story.photograph_id]

          application_events.publish("photograph.updated", photograph: photograph)

          Success(contributor_image)
        end
      end
    end
  end
end
