require "corley_collection/operation"
require "admin/import"
require "admin/contributor_images/validation/schema"

module Admin
  module ContributorImages
    module Operations
      class Update < CorleyCollection::Operation
        include Import["contributor_image_repo", "story_repo", "photograph_repo", "application_events"]

        def call(id, params)
          validation = Validation::Schema.(params)

          if validation.success?
            contributor_image = contributor_image_repo.update(id, validation.to_h)

            story = story_repo[contributor_image.story_id]
            photograph = photograph_repo[story.photograph_id]

            application_events.publish("photograph.updated", photograph: photograph)

            Success(story)
          else
            Failure(validation)
          end
        end
      end
    end
  end
end
