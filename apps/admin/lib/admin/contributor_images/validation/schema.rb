# auto_register: false

require "corley_collection/validation/form"

module Admin
  module ContributorImages
    module Validation
      Schema = CorleyCollection::Validation.Form(:contributor_images) do
        configure do
          config.type_specs = false
        end

        required(:caption).filled
        required(:published).value(:bool?)
      end
    end
  end
end
