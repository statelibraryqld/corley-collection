require "admin/repository"

module Admin
  class ContributorRepo < Repository[:contributors]
    commands update: :by_pk, delete: :by_pk

    def for_editing(id)
      contributors.by_pk(id).one!
    end

    def index(page = 1)
      contributors.combine(:stories).order(:email).page(page)
    end
  end
end
