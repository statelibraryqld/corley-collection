require "admin/view/form"
require "admin/import"

module Admin
  module Contributors
    class Form < Admin::View::Form
      prefix :contributor

      define do
        text_field :email, label: "Email"
        text_field :first_name, label: "First name"
        text_field :last_name, label: "Last name"
        check_box :suspended, label: "Suspended?", hint: "Suspended contributor accounts can no longer sign in"
      end
    end
  end
end
