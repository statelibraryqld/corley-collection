require "corley_collection/operation"
require "admin/import"

module Admin
  module Contributors
    module Operations
      class Delete < CorleyCollection::Operation
        include Import["contributor_repo"]

        def call(id)
          contributor = contributor_repo.delete(id)
          Success(contributor)
        end
      end
    end
  end
end
