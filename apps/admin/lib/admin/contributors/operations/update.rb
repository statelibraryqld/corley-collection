require "corley_collection/operation"
require "admin/import"
require "admin/contributors/validation/schema"

module Admin
  module Contributors
    module Operations
      class Update < CorleyCollection::Operation
        include Import["contributor_repo"]

        def call(id, params)
          validation = Validation::Schema.with(contributor_id: id).(params)

          if validation.success?
            contributor = contributor_repo.update(id, validation.to_h)

            Success(contributor)
          else
            Failure(validation)
          end
        end
      end
    end
  end
end
