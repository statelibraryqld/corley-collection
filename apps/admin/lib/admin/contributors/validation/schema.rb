# auto_register: false

require "admin/validation/form"

module Admin
  module Contributors
    module Validation
      Schema = Admin::Validation.Form(:contributors) do
        configure do
          option :contributor_id

          def email_unique?(value)
            relations[:contributors].exclude(id: contributor_id).unique?(email: value)
          end

          config.type_specs = false
        end

        required(:email).value(:email?, :email_unique?)
        required(:first_name).filled
        required(:last_name).maybe(:str?)
        required(:suspended).value(:bool?)
      end
    end
  end
end
