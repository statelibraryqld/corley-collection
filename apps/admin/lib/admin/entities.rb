# auto_register: false

require_relative "entities/contributor"
require_relative "entities/contributor_image"
require_relative "entities/photograph"
require_relative "entities/user"
require_relative "entities/story"
