# auto_register: false

require "corley_collection/entities/contributor"

module Admin
  module Entities
    class Contributor < CorleyCollection::Entities::Contributor
    end
  end
end
