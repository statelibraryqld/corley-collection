# auto_register: false

require "admin/container"
require "addressable/uri"

module Admin
  module Entities
    class ContributorImage < ROM::Struct
      CSV_HEADERS = %w(
        corley_collection_api_id
        photograph_slq_identifier
        corley_collection_story_id
        corley_collection_api_contributor_id
        contributor_full_name
        contributor_public_name
        contributor_email
        caption
        published
        content_type
        size_kilobytes
        created_at
        created_at_year
        url
      ).freeze

      def to_csv_row
        [
          id,
          story.photograph.slq_identifier,
          story_id,
          contributor_id,
          contributor.full_name,
          contributor.public_name,
          contributor.email,
          caption,
          published,
          content_type,
          size_kilobytes,
          created_at.iso8601,
          created_at.year,
          export_url
        ]
      end

      def size_kilobytes
        (size / 1_000).floor if size
      end

      def export_url
        [Container["core.s3.buckets.assets"].url, Addressable::URI.escape(path)].join("/")
      end
    end
  end
end
