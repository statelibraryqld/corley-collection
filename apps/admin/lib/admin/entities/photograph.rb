# auto_register: false

require "corley_collection/entities/photograph"

module Admin
  module Entities
    class Photograph < CorleyCollection::Entities::Photograph
      CSV_HEADERS = %w(
        slq_identifier
        corley_collection_api_id
        spool_slq_identifer
        street_address
        postcode
        locality_name
        latitude
        longitude
        house_name
        annotations
        wall_material_tags
        roof_material_tags
        roof_shape_tags
        height_tags
        fence_tags
        details_tags
        extras_tags
      ).freeze

      def to_csv_row
        [
          slq_identifier,
          id,
          spool&.slq_identifier,
          street_address,
          postcode,
          locality&.name,
          latitude,
          longitude,
          house_name,
          annotations,
          tag_names_for_type("wallMaterial"),
          tag_names_for_type("roofMaterial"),
          tag_names_for_type("roofShape"),
          tag_names_for_type("height"),
          tag_names_for_type("fence"),
          tag_names_for_type("details"),
          tag_names_for_type("extras")
        ]
      end

      private

      def tag_names_for_type(type)
        tags_of_type = tags.select { |t| t.type == type }

        if tags_of_type.any?
          tags_of_type.map(&:name).join(", ")
        end
      end
    end
  end
end
