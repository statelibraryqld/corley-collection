# auto_register: false

module Admin
  module Entities
    class Story < ROM::Struct
      CSV_HEADERS = %w(
        corley_collection_api_id
        photograph_slq_identifier
        corley_collection_api_contributor_id
        contributor_full_name
        contributor_public_name
        contributor_email
        body
        image_count
        published
        created_at
        created_at_year
      ).freeze

      def to_csv_row
        [
          id,
          photograph.slq_identifier,
          contributor_id,
          contributor.full_name,
          contributor.public_name,
          contributor.email,
          body,
          published,
          contributor_images.size,
          created_at.iso8601,
          created_at.year
        ]
      end
    end
  end
end
