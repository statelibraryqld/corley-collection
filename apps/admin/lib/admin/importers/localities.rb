require "csv"
require "down"

module Admin
  module Importers
    class Localities
      include Import["localities.operations.create_or_update_from_csv"]

      def call(remote_csv_url)
        CSV.new(Down.open(remote_csv_url), headers: true).each do |row|
          create_or_update_from_csv.(name: row["Town"], latitude: row["Lat"], longitude: row["Lng"])
        end
      end
    end
  end
end
