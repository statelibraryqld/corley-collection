require "csv"
require "down"

module Admin
  module Importers
    class Spools
      include Import[
        "spool_repo",
        "locality_repo",
        "locality_spool_repo",
        "photograph_repo",
        create_or_update_spool_from_csv: "spools.operations.create_or_update_from_csv"
      ]

      def call(remote_csv_url) # rubocop:disable Metrics/AbcSize
        CSV.new(Down.open(remote_csv_url), headers: true).each do |row|
          # create a spool identifier.
          # unitids are format "6169/1". We want "6169-0001"
          first_segment, second_segment = row["unitid"].split("/")

          slq_identifier = [first_segment, second_segment.rjust(4, "0")].join("-")

          spool_params = {
            slq_identifier: slq_identifier,
            content_description: row["Content"],
            photograph_count: (row["photos"] ? row["photos"].split(" ").count : 0)
          }

          create_or_update_spool_from_csv.(spool_params)

          spool = spool_repo.by_slq_identifier!(spool_params[:slq_identifier])

          # Update localities for this spool.
          locality_spool_repo.transaction do
            locality_spool_repo.delete_by_spool_id(spool.id)

            if row["SpoolLocation"]
              locality_names = row["SpoolLocation"].split(",").map(&:strip).uniq

              locality_names.each do |locality_name|
                locality = locality_repo.by_name(locality_name)

                if locality
                  locality_spool_repo.create(locality_id: locality.id, spool_id: spool.id)
                end
              end
            end
          end

          # Create or update photographs for this spool
          spool_params[:photograph_count].times do |i|
            photograph_number = i + 1

            photo_slq_identifier = "#{spool.slq_identifier}-#{photograph_number.to_s.rjust(4, '0')}"

            photograph_attrs = {slq_identifier: photo_slq_identifier, spool_id: spool.id}

            photograph_repo.create(photograph_attrs) unless photograph_repo.by_slq_identifier(photo_slq_identifier)
          end
        end
      end
    end
  end
end
