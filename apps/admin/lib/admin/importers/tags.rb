require "csv"
require "down"

module Admin
  module Importers
    class Tags
      include Import[
        "tag_repo",
        "photograph_repo",
        "photograph_tag_repo",
        "application_events",
        create_tag: "tags.operations.create"
      ]

      def call(remote_csv_url) # rubocop:disable Metrics/AbcSize
        CSV.new(Down.open(remote_csv_url), headers: true).each do |row|
          next unless row["imgID"].to_s.length.positive?

          tag_types = row.headers.compact - ["imgID"]

          # Prepare tags data
          tag_ids = []

          # Ensure the tags exist
          tag_types.each do |tag_type|
            tag_names = row[tag_type].split(",").map(&:strip) if row[tag_type]

            Array(tag_names).each do |tag_name|
              existing_tag = tag_repo.by_name_type_and_source(tag_name, tag_type)

              if existing_tag
                tag_ids << existing_tag.id
              else
                result = create_tag.(name: tag_name, type: tag_type, source: "human")
                result.bind { |r| tag_ids << r.id }
              end
            end
          end

          photograph_slq_identifer = row["imgID"].split("/").last.split(".").first

          photograph = photograph_repo.by_slq_identifier(photograph_slq_identifer)

          # If the photograph exists in our database, tag it with the tags.
          if photograph # rubocop:disable Style/Next
            puts "Tagging photograph: #{photograph.slq_identifier}"

            tag_ids.each do |tag_id|
              unless photograph_tag_repo.exist?(photograph.id, tag_id)
                photograph_tag_repo.create(photograph_id: photograph.id, tag_id: tag_id)
              end
            end

            photograph_repo.mark_as_human_tagged(photograph.id)

            application_events.publish("photograph.updated", photograph: photograph)
          end
        end
      end
    end
  end
end
