require "corley_collection/operation"
require "dry/validation"

module Admin
  module Localities
    module Operations
      class CreateOrUpdateFromCsv < CorleyCollection::Operation
        include Import["locality_repo"]

        LocalitySchema = Dry::Validation.Params do
          required(:name).filled
          optional(:latitude).maybe(:float?)
          optional(:longitude).maybe(:float?)
        end

        def call(params)
          validation = LocalitySchema.call(params)

          if validation.success?
            locality =
              locality_repo.create_or_update_by_name(
                validation[:name],
                latitude: validation[:latitude],
                longitude: validation[:longitude]
              )

            Success(locality)
          else
            Failure(validation)
          end
        end
      end
    end
  end
end
