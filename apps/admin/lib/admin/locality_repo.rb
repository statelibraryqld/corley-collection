require "admin/repository"

module Admin
  class LocalityRepo < Repository[:localities]
    commands :create, update: :by_pk, delete: :by_pk

    def by_name(name)
      localities.by_name(name).one
    end

    def create_or_update_by_name(name, attrs)
      locality = localities.by_name(name).one

      if locality
        update(locality.id, attrs)
      else
        create(attrs.merge(name: name))
      end
    end

    def select_map
      localities
        .order(:name)
        .select(:id, :name)
        .map { |tuple| {id: tuple[:id], label: tuple[:name]} }
    end
  end
end
