require "admin/repository"

module Admin
  class LocalitySpoolRepo < Repository[:localities_spools]
    commands :create

    def delete_by_spool_id(spool_id)
      localities_spools.by_spool_id(spool_id).delete
    end
  end
end
