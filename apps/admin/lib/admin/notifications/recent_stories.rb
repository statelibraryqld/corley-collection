require "corley_collection/operation"
require "admin/import"

module Admin
  module Notifications
    class RecentStories < CorleyCollection::Operation
      include Import[
        "story_repo",
        "mail.admin.recent_stories_notification"
      ]

      def call
        stories = story_repo.recently_created.to_a

        if stories.any?
          recent_stories_notification.enqueue(stories.map(&:id))
        end

        Success(stories)
      end
    end
  end
end
