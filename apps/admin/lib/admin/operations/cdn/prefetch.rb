require "corley_collection/operation"
require "admin/import"
require "corley_collection/que_job_manager"
require "time_math"

module Admin
  module Operations
    module Cdn
      class Prefetch < CorleyCollection::Operation
        include Import[
          "pending_cdn_action_repo",
          "cdn.operations.execute_pending_purges",
          "core.settings"
        ]

        def call(entity)
          paths = target_paths(entity)

          paths.each do |path|
            pending_cdn_action_repo.create(path: path, type: "prefetch")
          end

          unless CorleyCollection::QueJobManager.job_queued_with_job_class?(execute_pending_purges.class)
            execute_pending_purges.enqueue(
              run_at: TimeMath.sec.advance(Time.now, settings.cdn_purge_delay_in_seconds)
            )
          end
        end
      end
    end
  end
end
