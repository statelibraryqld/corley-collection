require "admin/repository"

module Admin
  class PendingCdnActionRepo < Repository[:pending_cdn_actions]
    commands :create
  end
end
