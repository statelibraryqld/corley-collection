require "admin/repository"

module Admin
  class PhotographAnalyticsRepo < Repository[:photographs]
    def total_photographs_count
      photographs.count
    end

    def tagged_photographs_count
      photographs.human_tagged.count
    end

    def total_taggings_count
      photographs_tags.count
    end

    def geo_located_photographs_count
      photographs.geo_located.count
    end

    def storied_photographs_count
      photographs.storied.count
    end

    def tagged_photographs_stats
      tagged_photographs_by_month.to_a
    end

    def geo_located_photographs_stats
      geo_located_photographs_by_month.to_a
    end

    def storied_photographs_stats
      storied_photographs_by_month.to_a
    end
  end
end
