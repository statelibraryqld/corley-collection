require "admin/repository"

module Admin
  class PhotographRepo < Repository[:photographs]
    commands delete: :by_pk

    def [](id)
      photographs.by_pk(id).one
    end

    def by_slq_identifier(slq_identifier)
      photographs.by_slq_identifier(slq_identifier).one
    end

    def mark_as_human_tagged(id)
      photographs.by_pk(id).update(human_tagged_at: Time.now)
    end

    def index(page = 1)
      photographs.order(:slq_identifier).page(page)
    end

    def for_editing(id)
      photographs.combine(:spool).by_pk(id).with_tag_ids.one!
    end

    def for_search(pks)
      photographs.by_pk(pks).order_by_id_values(pks)
    end

    def for_export
      photographs
        .combine(:spool, :locality, :tags)
        .order(:slq_identifier)
    end

    def create(attrs)
      transaction do
        photograph = photographs.changeset(:create, attrs).commit

        attrs[:tag_ids].to_a.each do |tag_id|
          photographs_tags.changeset(:create, tag_id: tag_id).associate(photograph).commit
        end

        photograph
      end
    end

    def update(id, attrs)
      transaction do
        photograph = photographs.by_pk(id).changeset(:update, attrs).commit

        if attrs.key?(:tag_ids)
          photographs_tags.by_photograph_id(id).delete

          attrs[:tag_ids].to_a.each do |tag_id|
            photographs_tags.changeset(:create, tag_id: tag_id).associate(photograph).commit
          end
        end

        photograph
      end
    end
  end
end
