require "admin/repository"

module Admin
  class PhotographTagRepo < Repository[:photographs_tags]
    commands :create

    def exist?(photograph_id, tag_id)
      photographs_tags.exist?(photograph_id: photograph_id, tag_id: tag_id)
    end
  end
end
