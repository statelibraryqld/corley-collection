require "admin/view/form"
require "admin/import"

module Admin
  module Photographs
    class Form < Admin::View::Form
      include Import["spool_repo", "locality_repo"]

      prefix :photograph

      define do
        text_field :slq_identifier,
          label: "SLQ Identifier",
          validation: {filled: true}

        selection_field :spool_id,
          label: "Spool",
          options: spool_list

        selection_field :locality_id,
          label: "Locality",
          options: locality_list

        text_field :street_number, label: "Street number"
        text_field :street_name, label: "Street name"
        text_field :street_type, label: "Street type"
        text_field :street_address, label: "Street address", hint: "The full street address, comma separated"
        text_field :postcode, label: "Postcode"
        text_field :latitude, label: "Latitude"
        text_field :longitude, label: "Longitude"
        text_field :house_name, label: "House name"
        text_area :annotations, label: "Annotations"
        check_box :shop, label: "Shop?"
        check_box :damaged, label: "Damaged?"

        search_multi_selection_field :tag_ids,
          label: "Tags",
          search_url: "/admin/tags/search.json",
          search_per_page: 10,
          search_threshold: 0
      end

      def spool_list
        spool_repo.select_map
      end

      def locality_list
        locality_repo.select_map
      end
    end
  end
end
