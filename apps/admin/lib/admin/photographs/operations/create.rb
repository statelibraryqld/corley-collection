require "corley_collection/operation"
require "admin/import"
require "admin/photographs/validation/new_schema"

module Admin
  module Photographs
    module Operations
      class Create < CorleyCollection::Operation
        include Import["photograph_repo", "application_events"]

        def call(params)
          validation = Validation::NewSchema.(params)

          if validation.success?
            photograph = photograph_repo.create(validation.to_h)

            application_events.publish("photograph.created", photograph: photograph)

            Success(photograph)
          else
            Failure(validation)
          end
        end
      end
    end
  end
end
