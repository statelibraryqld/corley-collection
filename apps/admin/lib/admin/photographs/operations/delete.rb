require "corley_collection/operation"
require "admin/import"

module Admin
  module Photographs
    module Operations
      class Delete < CorleyCollection::Operation
        include Import["photograph_repo", "application_events"]

        def call(id)
          photograph = photograph_repo.delete(id)
          application_events.publish("photograph.deleted", photograph: photograph)
          Success(photograph)
        end
      end
    end
  end
end
