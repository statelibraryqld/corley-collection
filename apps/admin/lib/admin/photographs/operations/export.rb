require "corley_collection/operation"
require "admin/import"
require "admin/entities/photograph"
require "csv"

module Admin
  module Photographs
    module Operations
      class Export < CorleyCollection::Operation
        include Import[
          "photograph_repo",
          "core.settings",
          bucket: "core.s3.buckets.assets"
        ]

        def call(recipient_email_address)
          csv_body =
            CSV.generate do |csv|
              csv << Admin::Entities::Photograph::CSV_HEADERS

              photograph_repo.for_export.each do |photograph|
                csv << photograph.to_csv_row
              end
            end

          export_filename = "corley-collection-#{Time.now.strftime('%Y-%m-%d-%H-%M-%S')}.csv"

          export_key = [settings.s3_assets_prefix, "exports", export_filename].join("/")

          s3_object = bucket.object(export_key)

          s3_object.put(
            acl: "public-read",
            body: csv_body,
            content_disposition: "attachment; filename=\"#{export_filename}\""
          )

          result = {
            recipient_email_address: recipient_email_address,
            export_url: s3_object.public_url
          }

          Success(result)
        end
      end
    end
  end
end
