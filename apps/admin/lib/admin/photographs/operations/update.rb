require "corley_collection/operation"
require "admin/import"
require "admin/photographs/validation/edit_schema"

module Admin
  module Photographs
    module Operations
      class Update < CorleyCollection::Operation
        include Import["photograph_repo", "application_events"]

        def call(id, params)
          validation = Validation::EditSchema.with(photograph_id: id).(params)

          if validation.success?
            photograph = photograph_repo.update(id, validation.to_h)

            application_events.publish("photograph.updated", photograph: photograph)

            Success(photograph)
          else
            Failure(validation)
          end
        end
      end
    end
  end
end
