require "admin/search/record_search"
require "admin/import"

module Admin
  module Photographs
    class Search < Admin::Search::RecordSearch
      configure do |config|
        config.es_relation = :photographs
      end

      include Admin::Import[repository: :photograph_repo]
    end
  end
end
