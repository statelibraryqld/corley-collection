# auto_register: false

require "admin/validation/form"

module Admin
  module Photographs
    module Validation
      EditSchema = Admin::Validation.Form(:photographs) do
        configure do
          option :photograph_id

          def slq_identifier_unique?(value)
            relations[:photographs].exclude(id: photograph_id).unique?(slq_identifier: value)
          end

          config.type_specs = false
        end

        required(:slq_identifier).filled(:str?, :slq_identifier_unique?)
        required(:spool_id).maybe(:int?)
        required(:locality_id).maybe(:int?)
        required(:street_name).maybe
        required(:street_number).maybe
        required(:street_type).maybe
        required(:street_address).maybe
        required(:postcode).maybe
        required(:latitude).maybe(:float?)
        required(:longitude).maybe(:float?)
        required(:house_name).maybe
        required(:annotations).maybe
        required(:shop).value(:bool?)
        required(:damaged).value(:bool?)
        required(:tag_ids).each(:int?)

        rule(location_filled: [:latitude, :longitude]) do |latitude, longitude|
          (latitude.filled? | longitude.filled?) > (latitude.filled? & longitude.filled?)
        end
      end
    end
  end
end
