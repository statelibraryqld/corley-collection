require "corley_collection/repository"
require "admin/entities"

module Admin
  class Repository < CorleyCollection::Repository
    struct_namespace Admin::Entities

    # FIXME: I guess we have our first case where we need auto-inject to work with repos
    #        I'll make this possible in rom 5.0
    def application_events
      Container[:application_events]
    end
  end
end
