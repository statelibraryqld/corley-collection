require "corley_collection/operation"
require "admin/import"
require "search/results"

module Admin
  module Search
    class RecordSearch < CorleyCollection::Operation
      extend Dry::Configurable

      setting :es_relation

      include Import[es: "search.es.rom"]

      def call(query)
        if query[:ids]
          return repository.for_search(query[:ids])
        end

        es_query =
          if query[:q]
            es_relation.by_simple_query_string(query)
          else
            es_relation.default_listing
          end

        es_results = es_query.page(query[:page] || 1).per_page(query[:per_page] || 10).call

        ids = es_results.pluck(:id)

        db_results = repository.for_search(ids)

        ::Search::Results.new(es_results, db_results || [])
      end

      private

      def es_relation
        es.relations[self.class.config.es_relation]
      end

      def repository
        raise NotImplementedError
      end
    end
  end
end
