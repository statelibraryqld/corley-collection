# auto_register: false

require "corley_collection/search_query"

module Admin
  class SearchQuery < CorleyCollection::SearchQuery
    params do
      optional(:q).maybe(:str?)
      optional(:ids).each(:int?)
      optional(:page).maybe(:int?)
      optional(:per_page).maybe(:int?)
    end
  end
end
