# auto_register: false

require "admin/import"
require "admin/container"

module Admin
  module Services
    class Indexer
      include Import[:application_events, es: "search.es.rom"]

      def self.index(opts)
        opts.each do |entity_name, entity|
          define_method("on_#{entity}_created") do |event|
            index(entity_name, event[entity])
          end

          define_method("on_#{entity}_updated") do |event|
            index(entity_name, event[entity])
          end

          define_method("on_#{entity}_deleted") do |event|
            delete(entity_name, event[entity])
          end
        end
      end

      index photographs: :photograph,
            tags: :tag

      def index(entity_name, entity)
        Container["search.#{entity_name}.operations.index"].enqueue(entity)
      end

      def delete(entity_name, entity)
        Container["search.#{entity_name}.operations.delete"].enqueue(entity)
      end

      def start
        return self if @started
        application_events.subscribe(self)
        @started = true
        self
      end

      def stop
        application_events.unsubscribe(self)
        @started = false
        self
      end
    end
  end
end
