require "admin/repository"

module Admin
  class SpoolRepo < Repository[:spools]
    commands :create, update: :by_pk, delete: :by_pk

    def by_slq_identifier!(slq_identifier)
      spools.by_slq_identifier(slq_identifier).one!
    end

    def create_or_update_by_slq_identifier(slq_identifier, attrs)
      spool = spools.by_slq_identifier(slq_identifier).one

      if spool
        update(spool.id, attrs)
      else
        create(attrs.merge(slq_identifier: slq_identifier))
      end
    end

    def index(page = 1)
      spools.order(:slq_identifier).page(page)
    end

    def for_editing(id)
      spools.by_pk(id).one!
    end

    def select_map
      spools
        .order(:slq_identifier)
        .select(:id, :slq_identifier)
        .map { |tuple| {id: tuple[:id], label: tuple[:slq_identifier]} }
    end
  end
end
