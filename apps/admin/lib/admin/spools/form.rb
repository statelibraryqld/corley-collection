require "admin/view/form"
require "admin/import"

module Admin
  module Spools
    class Form < Admin::View::Form
      prefix :spool

      define do
        text_field :slq_identifier,
          label: "SLQ Identifier",
          validation: {filled: true}

        text_area :content_description,
          label: "Content description"

        number_field :photograph_count,
          label: "Photograph count"
      end
    end
  end
end
