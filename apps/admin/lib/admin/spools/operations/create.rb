require "corley_collection/operation"
require "admin/import"
require "admin/spools/validation/schema"

module Admin
  module Spools
    module Operations
      class Create < CorleyCollection::Operation
        include Import["spool_repo", "application_events"]

        def call(params)
          validation = Validation::Schema.(params)

          if validation.success?
            spool = spool_repo.create(validation.to_h)

            application_events.publish("spool.created", spool: spool)

            Success(spool)
          else
            Failure(validation)
          end
        end
      end
    end
  end
end
