require "corley_collection/operation"
require "dry/validation"

module Admin
  module Spools
    module Operations
      class CreateOrUpdateFromCsv < CorleyCollection::Operation
        include Import["spool_repo"]

        SpoolSchema = Dry::Validation.Params do
          required(:slq_identifier).filled
          required(:photograph_count).value(:int?)
          required(:content_description).filled
        end

        def call(params)
          validation = SpoolSchema.call(params)

          if validation.success?
            spool =
              spool_repo.create_or_update_by_slq_identifier(
                validation[:slq_identifier],
                photograph_count: validation[:photograph_count],
                content_description: validation[:content_description]
              )

            Success(spool)
          else
            Failure(validation)
          end
        end
      end
    end
  end
end
