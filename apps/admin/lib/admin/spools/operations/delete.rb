require "corley_collection/operation"
require "admin/import"

module Admin
  module Spools
    module Operations
      class Delete < CorleyCollection::Operation
        include Import["spool_repo", "application_events"]

        def call(id)
          spool = spool_repo.delete(id)
          application_events.publish("spool.deleted", spool: spool)
          Success(spool)
        end
      end
    end
  end
end
