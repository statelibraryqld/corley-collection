require "corley_collection/operation"
require "admin/import"
require "admin/spools/validation/schema"

module Admin
  module Spools
    module Operations
      class Update < CorleyCollection::Operation
        include Import["spool_repo", "application_events"]

        def call(id, params)
          validation = Validation::Schema.with(spool_id: id).(params)

          if validation.success?
            spool = spool_repo.update(id, validation.to_h)

            application_events.publish("spool.updated", spool: spool)

            Success(spool)
          else
            Failure(validation)
          end
        end
      end
    end
  end
end
