# auto_register: false

require "admin/validation/form"

module Admin
  module Spools
    module Validation
      Schema = Admin::Validation.Form(:spools) do
        configure do
          option :spool_id

          def slq_identifier_unique?(value)
            relations[:spools].exclude(id: spool_id).unique?(slq_identifier: value)
          end

          config.type_specs = false
        end

        required(:slq_identifier).filled(:str?, :slq_identifier_unique?)
        required(:content_description).maybe(:str?)
        required(:photograph_count).maybe(:int?)
      end
    end
  end
end
