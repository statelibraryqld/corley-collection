require "admin/view/form"
require "admin/import"

module Admin
  module Stories
    class Form < Admin::View::Form
      prefix :story

      define do
        text_area :body, label: "Body"
        check_box :published, label: "Published?", hint: "Uncheck to unpublish inappropriate material"
      end
    end
  end
end
