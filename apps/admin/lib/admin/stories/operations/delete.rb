require "corley_collection/operation"
require "admin/import"

module Admin
  module Stories
    module Operations
      class Delete < CorleyCollection::Operation
        include Import["story_repo", "photograph_repo", "application_events"]

        def call(id)
          story = story_repo.delete(id)

          photograph = photograph_repo[story.photograph_id]

          application_events.publish("photograph.updated", photograph: photograph)

          Success(story)
        end
      end
    end
  end
end
