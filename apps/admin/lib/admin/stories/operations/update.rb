require "corley_collection/operation"
require "admin/import"
require "admin/stories/validation/schema"

module Admin
  module Stories
    module Operations
      class Update < CorleyCollection::Operation
        include Import["story_repo", "photograph_repo", "application_events"]

        def call(id, params)
          validation = Validation::Schema.(params)

          if validation.success?
            story = story_repo.update(id, validation.to_h)

            photograph = photograph_repo[story.photograph_id]

            application_events.publish("photograph.updated", photograph: photograph)

            Success(story)
          else
            Failure(validation)
          end
        end
      end
    end
  end
end
