require "admin/transaction"

module Admin
  module Stories
    module Transactions
      class Export < Admin::Transaction
        step :export, with: "stories.operations.export"
        enqueue :notification_email, with: "mail.admin.export_completed"

        def to_queue(recipient_email)
          [recipient_email]
        end

        def call_from_queue(recipient_email)
          call(recipient_email)
        end
      end
    end
  end
end
