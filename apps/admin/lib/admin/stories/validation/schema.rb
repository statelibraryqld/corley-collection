# auto_register: false

require "corley_collection/validation/form"

module Admin
  module Stories
    module Validation
      Schema = CorleyCollection::Validation.Form(:stories) do
        configure do
          config.type_specs = false
        end

        required(:body).maybe
        required(:published, :string).value(:bool?)
      end
    end
  end
end
