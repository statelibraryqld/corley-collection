require "admin/repository"

module Admin
  class StoryAnalyticsRepo < Repository[:stories]
    def total_stories_count
      stories.count
    end

    def created_story_stats
      created_stories_by_month.to_a
    end
  end
end
