require "admin/repository"

module Admin
  class StoryRepo < Repository[:stories]
    commands update: :by_pk, delete: :by_pk

    def [](id)
      stories.by_pk(id).one
    end

    def index(page = 1)
      stories.combine(:contributor, :photograph, :contributor_images).order { created_at.desc }.page(page)
    end

    def for_editing(id)
      stories.combine(:contributor, :photograph, :contributor_images).by_pk(id).one!
    end

    def for_export
      stories.combine(:contributor, :photograph, :contributor_images).order(:id)
    end

    def recently_created
      stories.recently_created.order(:created_at)
    end
  end
end
