require "babosa"

module Admin
  module Support
    class GenerateSlug
      TRAILING_NUM_REGEXP = /--\d+$/

      def call(slug, uniqueness_check: nil, scope: nil)
        slug = Babosa::Identifier.new(slug.to_s).normalize.to_s
        return slug unless uniqueness_check

        number = 1
        # Adds an incremental number to the end of the slug if it already exists.
        attrs = [slug]
        attrs << scope if scope
        until uniqueness_check.call(*attrs)
          slug = attrs[0] = append_number(slug, number)
          number += 1
        end
        slug
      end

      private

      def append_number(slug, number)
        if slug.match?(TRAILING_NUM_REGEXP)
          slug.sub(TRAILING_NUM_REGEXP, "--#{number}")
        else
          "#{slug}--#{number}"
        end
      end
    end
  end
end
