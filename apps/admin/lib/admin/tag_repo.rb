require "admin/repository"

module Admin
  class TagRepo < Repository[:tags]
    commands :create, update: :by_pk, delete: :by_pk

    def [](id)
      tags.by_pk(id).one
    end

    def index(page = 1)
      tags.order(:name).page(page)
    end

    def contributor_index(page = 1)
      tags.contributor.order(:name).page(page)
    end

    def for_search(pks)
      tags.by_pk(pks).order_by_id_values(pks)
    end

    def by_name_type_and_source(name, type, source = "human")
      tags.by_name_type_and_source(name, type, source).one
    end

    def total_tag_count
      tags.count
    end

    def contributor_tag_count
      tags.contributor.count
    end
  end
end
