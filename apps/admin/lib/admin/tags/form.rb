require "types"
require "admin/view/form"
require "admin/import"

module Admin
  module Tags
    class Form < Admin::View::Form
      prefix :tag

      define do
        text_field :name, label: "Name", validation: {filled: true}
        text_field :type, label: "Type", validation: {filled: true}
        select_box :source,
          label: "Source",
          options:  %w(Human Machine).map { |alignment| [alignment.downcase, alignment] }
      end
    end
  end
end
