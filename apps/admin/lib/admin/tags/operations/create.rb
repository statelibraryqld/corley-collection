require "corley_collection/operation"
require "admin/import"
require "admin/tags/validation/schema"

module Admin
  module Tags
    module Operations
      class Create < CorleyCollection::Operation
        include Import["tag_repo", "application_events"]

        def call(params)
          validation = Validation::Schema.(params)

          if validation.success?
            tag = tag_repo.create(validation.to_h)

            application_events.publish("tag.created", tag: tag)

            Success(tag)
          else
            Failure(validation)
          end
        end
      end
    end
  end
end
