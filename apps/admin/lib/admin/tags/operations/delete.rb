require "corley_collection/operation"
require "admin/import"
require "admin/tags/validation/schema"

module Admin
  module Tags
    module Operations
      class Delete < CorleyCollection::Operation
        include Import["tag_repo", "application_events"]

        def call(id)
          tag = tag_repo.delete(id)
          application_events.publish("tag.deleted", tag: tag)
        end
      end
    end
  end
end
