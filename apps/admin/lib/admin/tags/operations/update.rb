require "corley_collection/operation"
require "admin/import"
require "admin/tags/validation/schema"

module Admin
  module Tags
    module Operations
      class Update < CorleyCollection::Operation
        include Import["tag_repo", "application_events"]

        def call(id, params)
          validation = Validation::Schema.with(tag_id: id).(params)

          if validation.success?
            tag = tag_repo.update(id, validation.to_h)

            application_events.publish("tag.updated", tag: tag)

            Success(tag)
          else
            Failure(validation)
          end
        end
      end
    end
  end
end
