require "admin/search/record_search"
require "admin/import"

module Admin
  module Tags
    class Search < Admin::Search::RecordSearch
      configure do |config|
        config.es_relation = :tags
      end

      include Admin::Import[repository: :tag_repo]
    end
  end
end
