# auto_register: false

require "corley_collection/validation/form"

module Admin
  module Tags
    module Validation
      Schema = CorleyCollection::Validation.Form(:tags) do
        configure do
          config.type_specs = true
        end

        required(:name, :string).filled
        required(:type, :string).filled
        required(:source, :string).filled(included_in?: %w[human machine])
      end
    end
  end
end
