require "corley_collection/validation/form"

module Admin
  module Tags
    module Validation
      SearchSchema = CorleyCollection::Validation.Form do
        configure do
          config.type_specs = false
        end

        optional(:q).maybe(:str?)
        optional(:ids).each(:filled?, :int?)
      end
    end
  end
end
