require "admin/view/form"

module Admin
  module User
    class EditForm < Admin::View::Form
      prefix :user

      define do
        text_field :name, label: "Name", validation: {filled: true}
        text_field :email, label: "Email", validation: {filled: true}
        text_field :password, password: true, label: "Password", hint: "Only required if changing your password"
        text_field :password_confirmation, password: true, label: "Password confirmation"
      end
    end
  end
end
