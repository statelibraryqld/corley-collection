require "corley_collection/operation"
require "admin/import"
require "admin/user/validation/edit_schema"

module Admin
  module User
    module Operations
      class Update < CorleyCollection::Operation
        include Import[
          "user_repo",
          "core.authentication.encrypt_password",
          "application_events"
        ]

        def call(id, params)
          validation = Validation::EditSchema.with(user_id: id).(params)

          if validation.success?
            user = user_repo.update(id, prepare_attributes(validation.to_h))
            application_events.publish("user.updated", user: user)

            Success(user)
          else
            Failure(validation)
          end
        end

        private

        def prepare_attributes(params)
          # Only update the encrypted_password if a new password is provided
          return params unless params[:password]
          params.merge(encrypted_password: encrypt_password.(params[:password])) if params[:password]
        end
      end
    end
  end
end
