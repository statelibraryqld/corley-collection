# auto_register: false

require "corley_collection/validation/form"
require "admin/container"

module Admin
  module User
    module Validation
      # We can't use type specs here due to an issue with using the confirmation macro with type specs
      # See: https://github.com/dry-rb/dry-validation/issues/367
      EditSchema = CorleyCollection::Validation.Form do
        configure do
          config.type_specs = false

          option :user_id

          def email_unique?(value)
            relations[:users].exclude(id: user_id).unique?(email: value)
          end
        end

        required(:name).filled
        required(:email).filled(:email?, :email_unique?)

        optional(:password).maybe(min_size?: 8).confirmation
      end
    end
  end
end
