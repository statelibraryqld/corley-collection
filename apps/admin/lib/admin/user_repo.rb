require "admin/repository"
require "admin/users/changesets/create"
require "admin/users/changesets/update"
require "corley_collection/repos/account_management"

module Admin
  class UserRepo < Repository[:users]
    include CorleyCollection::Repos::AccountManagement

    commands :create, update: :by_pk, delete: :by_pk

    def [](id)
      users.by_pk(id).one
    end

    def index(page = 1)
      users.page(page)
    end

    def for_editing(id)
      users.by_pk(id).one
    end

    def by_email(email)
      users.by_email(email).one
    end
  end
end
