# FIXME: not sure why require "rom/changeset/create" wasn't enough here
require "rom/changeset"

module Admin
  module Users
    module Changesets
      class Create < ROM::Changeset::Create
        command_type :create
      end
    end
  end
end
