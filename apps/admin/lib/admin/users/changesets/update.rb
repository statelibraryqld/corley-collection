require "rom/changeset/update"

module Admin
  module Users
    module Changesets
      class Update < ROM::Changeset::Update
        command_type :update
      end
    end
  end
end
