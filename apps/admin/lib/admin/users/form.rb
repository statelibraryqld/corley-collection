require "types"
require "admin/view/form"
require "admin/import"

module Admin
  module Users
    class Form < Admin::View::Form
      prefix :user
    end
  end
end
