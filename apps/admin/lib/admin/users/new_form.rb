require "admin/users/form"
require "admin/import"

module Admin
  module Users
    class NewForm < Users::Form
      define do
        text_field :name, label: "Name", validation: {filled: true}
        text_field :email, label: "Email", validation: {filled: true}
      end
    end
  end
end
