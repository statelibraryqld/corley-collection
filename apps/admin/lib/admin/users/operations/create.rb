require "corley_collection/operation"
require "admin/import"
require "admin/users/validation/new_schema"

module Admin
  module Users
    module Operations
      class Create < CorleyCollection::Operation
        include Import[
          "user_repo",
          "core.authentication.generate_secret_token",
          "application_events"
        ]

        def call(params)
          validation = Validation::NewSchema.(params)

          if validation.success?
            user = user_repo.create(prepare_attributes(validation.to_h))
            application_events.publish("user.created", user: user)

            Success(user)
          else
            Failure(validation)
          end
        end

        private

        def prepare_attributes(params)
          secret_token = generate_secret_token.(user_repo.method(:activation_token_exists?))
          params.merge(activation_token: secret_token.value)
        end
      end
    end
  end
end
