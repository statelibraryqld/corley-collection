require "corley_collection/operation"
require "admin/import"

module Admin
  module Users
    module Operations
      class Delete < CorleyCollection::Operation
        include Import[:user_repo, :application_events]

        def call(id)
          user = user_repo.delete(id)
          application_events.publish("user.deleted", user: user)
        end
      end
    end
  end
end
