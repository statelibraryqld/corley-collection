require "corley_collection/operation"
require "admin/import"
require "admin/users/validation/edit_schema"

module Admin
  module Users
    module Operations
      class Update < CorleyCollection::Operation
        include Import[:user_repo, :application_events]

        def call(id, params)
          validation = Validation::EditSchema.with(user_id: id).(params)

          if validation.success?
            user = user_repo.update(id, validation.to_h)
            application_events.publish("user.updated", user: user)

            Success(user)
          else
            Failure(validation)
          end
        end
      end
    end
  end
end
