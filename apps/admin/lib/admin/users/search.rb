require "admin/search/record_search"
require "admin/import"

module Admin
  module Users
    class Search < Admin::Search::RecordSearch
      configure do |config|
        config.es_relation = :users
      end

      include Admin::Import[repository: :user_repo]
    end
  end
end
