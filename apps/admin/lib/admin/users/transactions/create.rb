require "admin/transaction"

module Admin
  module Users
    module Transactions
      class Create < Admin::Transaction
        step :create_user, with: "users.operations.create"
        enqueue :account_activation_email, with: "mail.admin.account_activation"
      end
    end
  end
end
