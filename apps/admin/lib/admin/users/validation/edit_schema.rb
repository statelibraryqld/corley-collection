# auto_register: false

require "corley_collection/validation/form"

module Admin
  module Users
    module Validation
      EditSchema = CorleyCollection::Validation.Form do
        configure do
          option :user_id

          def email_unique?(value)
            relations[:users].exclude(id: user_id).unique?(email: value)
          end

          config.type_specs = false
        end

        required(:name).value(:filled?)
        required(:email).filled(:email?, :email_unique?)
      end
    end
  end
end
