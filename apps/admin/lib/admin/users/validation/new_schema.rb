# auto_register: false

require "corley_collection/validation/form"

module Admin
  module Users
    module Validation
      NewSchema = CorleyCollection::Validation.Form do
        configure do
          def email_unique?(value)
            relations[:users].unique?(email: value)
          end

          config.type_specs = false
        end

        required(:name).value(:filled?)
        required(:email).filled(:email?, :email_unique?)
        required(:email, :string).filled(:email?, :email_unique?)
      end
    end
  end
end
