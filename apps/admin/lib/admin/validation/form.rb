require "corley_collection/validation/form"
require "admin/container"
require "formalist/rich_text/embedded_form_compiler"
require "formalist/rich_text/validity_check"
require "dry/validation/messages/i18n"

module Admin
  module Validation
    class Form < CorleyCollection::Validation::Form
    end

    def self.Form(relation = nil, &block)
      Dry::Validation.Schema(Form, &block).with(relation: relation)
    end
  end
end
