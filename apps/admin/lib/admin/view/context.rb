# auto_register: false

require "admin/import"
require "corley_collection/view/context"
require "admin/import"

module Admin
  module View
    class Context < CorleyCollection::View::Context
      include Import["thumbor"]

      def current_user
        attrs[:current_user]
      end
    end
  end
end
