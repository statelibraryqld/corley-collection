# auto_register: false

require "slim"
require "yajl"
require "dry/view/controller"
require "admin/import"
require "admin/container"

module Admin
  module View
    class Controller < Dry::View::Controller
      configure do |config|
        config.paths = [Container.root.join("web/templates")]
        config.layout = "application"
        config.context = Container["view.context"]
        config.decorator = Container["view.decorator"]
      end
    end
  end
end
