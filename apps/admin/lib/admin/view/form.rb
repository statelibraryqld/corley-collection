# auto_register: false

require "snowflakes/web/form"

module Admin
  module View
    class Form < Snowflakes::Web::Form
      def fill(from: nil, **args)
        if from
          input = from.respond_to?(:to_h) ? from.to_h : {}
          errors = from.respond_to?(:errors) ? from.errors : {}

          super({input: input, errors: errors}.merge(args))
        elsif args.any?
          super(args)
        else
          self
        end
      end
    end
  end
end
