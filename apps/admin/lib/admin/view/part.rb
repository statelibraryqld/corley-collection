require "dry/core/memoizable"
require "dry/view/part"

require "admin/import"

module Admin
  module View
    class Part < Dry::View::Part
      include Dry::Core::Memoizable
      include Import[i: "core.inflector"]
    end
  end
end
