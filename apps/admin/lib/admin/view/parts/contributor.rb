# auto_register: false

require "admin/view/part"
require_relative "helpers/time"

module Admin
  module View
    module Parts
      class Contributor < Admin::View::Part
        include Helpers::Time

        def display_created_at
          format_time(created_at)
        end
      end
    end
  end
end
