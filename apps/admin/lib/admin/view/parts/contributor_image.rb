# auto_register: false

require "dry/view/part"
require "addressable/uri"
require_relative "helpers/time"

module Admin
  module View
    module Parts
      class ContributorImage < Dry::View::Part
        include Helpers::Time

        def url
          Addressable::URI.escape(path)
        end

        def preview_url
          context.thumbor.preview(url)
        end

        def thumbnail_url
          context.thumbor.thumbnail(url)
        end

        def display_created_at
          format_time(created_at)
        end
      end
    end
  end
end
