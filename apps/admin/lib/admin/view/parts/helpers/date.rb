# auto_register: false

module Admin
  module View
    module Parts
      module Helpers
        module Date
          private

          def format_date(date)
            date&.strftime("%-d %b %Y")
          end
        end
      end
    end
  end
end
