# auto_register: false

require "time_math"

module Admin
  module View
    module Parts
      module Helpers
        module Time
          private

          def format_time(time)
            time&.strftime("%-d %b %Y, %l:%M%P")
          end

          def format_date(date)
            date&.strftime("%-d %b %Y")
          end

          def format_concised(time)
            if time
              if TimeMath.day.measure(time, ::Time.now).abs < 2
                format_time(time)
              else
                format_date(time)
              end
            end
          end
        end
      end
    end
  end
end
