# auto_register: false

require "admin/view/part"
require_relative "helpers/time"
require_relative "contributor_image"

module Admin
  module View
    module Parts
      class Story < Admin::View::Part
        include Helpers::Time

        decorate :contributor_images, as: ContributorImage

        def display_created_at
          format_time(created_at)
        end
      end
    end
  end
end
