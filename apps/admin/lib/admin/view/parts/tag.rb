# auto_register: false

require "admin/view/part"

module Admin
  module View
    module Parts
      class Tag < Admin::View::Part
        def display_name
          "#{name} - #{type} (#{source})"
        end

        def selection_attributes
          {
            id: id,
            label: display_name,
            value: display_name
          }
        end
      end
    end
  end
end
