# auto_register: false

require "admin/view/part"

module Admin
  module View
    module Parts
      class User < Admin::View::Part
        def display_name
          email
        end
      end
    end
  end
end
