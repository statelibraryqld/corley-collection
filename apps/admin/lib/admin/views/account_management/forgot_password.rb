require "admin/view/controller"
require "admin/import"

module Admin
  module Views
    module AccountManagement
      class ForgotPassword < Admin::View::Controller
        include Import[form: "account_management.forms.forgot_password_form"]

        configure do |config|
          config.template = "account_management/forgot_password"
        end

        expose :form do |validation: nil|
          form.fill(from: validation)
        end
      end
    end
  end
end
