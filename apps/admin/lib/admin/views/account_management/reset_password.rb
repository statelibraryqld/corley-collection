require "admin/view/controller"
require "admin/import"

module Admin
  module Views
    module AccountManagement
      class ResetPassword < Admin::View::Controller
        include Import[
          "user_repo",
          form: "account_management.forms.password_form"
        ]

        configure do |config|
          config.template = "account_management/reset_password"
        end

        expose :user do |user_id:|
          user_repo[user_id]
        end

        expose :form do |validation: nil|
          form.fill(from: validation)
        end
      end
    end
  end
end
