require "admin/views/index"
require "admin/import"

module Admin
  module Views
    module Analytics
      module Contributors
        class Index < Admin::Views::Index
          include Admin::Import["contributor_analytics_repo"]

          configure do |config|
            config.template = "analytics/contributors/index"
          end

          expose :total_contributors_count do
            contributor_analytics_repo.total_contributors_count
          end

          expose :created_contributors_by_month do
            contributor_analytics_repo.created_contributor_stats
          end
        end
      end
    end
  end
end
