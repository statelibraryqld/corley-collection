require "admin/views/index"
require "admin/import"

module Admin
  module Views
    module Analytics
      module Photographs
        class Index < Admin::Views::Index
          include Admin::Import["photograph_analytics_repo"]

          configure do |config|
            config.template = "analytics/photographs/index"
          end

          expose :total_photographs_count do
            photograph_analytics_repo.total_photographs_count
          end

          expose :tagged_photographs_count do
            photograph_analytics_repo.tagged_photographs_count
          end

          expose :total_taggings_count do
            photograph_analytics_repo.total_taggings_count
          end

          expose :tagged_photographs_by_month do
            photograph_analytics_repo.tagged_photographs_stats
          end

          expose :geo_located_photographs_count do
            photograph_analytics_repo.geo_located_photographs_count
          end

          expose :geo_located_photographs_by_month do
            photograph_analytics_repo.geo_located_photographs_stats
          end

          expose :storied_photographs_count do
            photograph_analytics_repo.storied_photographs_count
          end

          expose :storied_photographs_by_month do
            photograph_analytics_repo.storied_photographs_stats
          end

          expose :percentage_tagged do |tagged_photographs_count, total_photographs_count|
            calculate_percentage(tagged_photographs_count, total_photographs_count)
          end

          expose :percentage_geo_located do |geo_located_photographs_count, total_photographs_count|
            calculate_percentage(geo_located_photographs_count, total_photographs_count)
          end

          expose :percentage_storied do |storied_photographs_count, total_photographs_count|
            calculate_percentage(storied_photographs_count, total_photographs_count)
          end

          private

          def calculate_percentage(count, total)
            if total == 0
               0
            else
              ((count.to_f / total.to_f) * 100).round(2)
            end
          end
        end
      end
    end
  end
end
