require "admin/views/index"
require "admin/import"

module Admin
  module Views
    module Analytics
      module Stories
        class Index < Admin::Views::Index
          include Admin::Import[
            "story_analytics_repo",
            "contributor_image_analytics_repo"
          ]

          configure do |config|
            config.template = "analytics/stories/index"
          end

          expose :total_stories_count do
            story_analytics_repo.total_stories_count
          end

          expose :created_stories_by_month do
            story_analytics_repo.created_story_stats
          end

          expose :total_contributor_images_count do
            contributor_image_analytics_repo.total_contributor_images_count
          end

          expose :created_contributor_images_by_month do
            contributor_image_analytics_repo.created_contributor_image_stats
          end
        end
      end
    end
  end
end
