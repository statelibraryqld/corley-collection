require "admin/view/controller"
require "admin/import"

module Admin
  module Views
    module ContributorImages
      class Edit < Admin::View::Controller
        include Admin::Import["contributor_image_repo", form: "contributor_images.form"]

        configure do |config|
          config.template = "contributor_images/edit"
        end

        expose :form do |contributor_image, validation: nil|
          form.fill(from: validation || contributor_image)
        end

        expose :contributor_image do |id:|
          contributor_image_repo.for_editing(id)
        end
      end
    end
  end
end
