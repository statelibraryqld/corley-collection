require "admin/views/index"
require "admin/import"

module Admin
  module Views
    module ContributorImages
      class Index < Admin::Views::Index
        include Admin::Import["contributor_image_repo"]

        configure do |config|
          config.template = "contributor_images/index"
        end

        expose :contributor_images do |page:|
          contributor_image_repo.index(page)
        end
      end
    end
  end
end
