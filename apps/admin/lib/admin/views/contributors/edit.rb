require "admin/view/controller"
require "admin/import"

module Admin
  module Views
    module Contributors
      class Edit < Admin::View::Controller
        include Admin::Import["contributor_repo", form: "contributors.form"]

        configure do |config|
          config.template = "contributors/edit"
        end

        expose :form do |contributor, validation: nil|
          form.fill(from: validation || contributor)
        end

        expose :contributor do |id:|
          contributor_repo.for_editing(id)
        end
      end
    end
  end
end
