require "admin/views/index"
require "admin/import"

module Admin
  module Views
    module Contributors
      class Index < Admin::Views::Index
        include Admin::Import["contributor_repo"]

        configure do |config|
          config.template = "contributors/index"
        end

        expose :contributors do |page:|
          contributor_repo.index(page)
        end
      end
    end
  end
end
