require "dry/core/class_attributes"

require "admin/view/controller"
require "admin/search_query"

module Admin
  module Views
    class Index < Admin::View::Controller
      extend Dry::Core::ClassAttributes

      defines :search_query_type

      search_query_type Admin::SearchQuery

      expose :search_query do |params:|
        self.class.search_query_type.new(params)
      end
    end
  end
end
