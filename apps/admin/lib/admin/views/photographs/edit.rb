require "admin/view/controller"
require "admin/import"

module Admin
  module Views
    module Photographs
      class Edit < Admin::View::Controller
        include Admin::Import[
          "photograph_repo",
          form: "photographs.form"
        ]

        configure do |config|
          config.template = "photographs/edit"
        end

        expose :form do |photograph, validation: nil|
          form.fill(from: validation || photograph)
        end

        expose :photograph do |id:|
          photograph_repo.for_editing(id)
        end
      end
    end
  end
end
