require "admin/views/index"
require "admin/import"

module Admin
  module Views
    module Photographs
      class Index < Admin::Views::Index
        include Admin::Import[
          "photograph_repo",
          "photographs.search"
        ]

        configure do |config|
          config.template = "photographs/index"
        end

        expose :photographs do |search_query, page:|
          if search_query.present?
            search.(search_query)
          else
            photograph_repo.index(page)
          end
        end

        expose :placeholder_text do
          "Search photographs"
        end
      end
    end
  end
end
