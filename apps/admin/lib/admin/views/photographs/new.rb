require "admin/import"
require "admin/view/controller"

module Admin
  module Views
    module Photographs
      class New < Admin::View::Controller
        include Admin::Import["photographs.form"]

        configure do |config|
          config.template = "photographs/new"
        end

        expose :form do |validation: nil|
          form.fill(from: validation)
        end
      end
    end
  end
end
