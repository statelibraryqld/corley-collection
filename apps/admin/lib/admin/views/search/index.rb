require "admin/view/controller"

module Admin
  module Views
    module Search
      class Index < View::Controller
        configure do |config|
          config.template = "search/index"
          config.default_format = :json
        end

        expose :results
      end
    end
  end
end
