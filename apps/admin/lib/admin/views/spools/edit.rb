require "admin/view/controller"
require "admin/import"

module Admin
  module Views
    module Spools
      class Edit < Admin::View::Controller
        include Admin::Import[
          "spool_repo",
          form: "spools.form"
        ]

        configure do |config|
          config.template = "spools/edit"
        end

        expose :form do |spool, validation: nil|
          form.fill(from: validation || spool)
        end

        expose :spool do |id:|
          spool_repo.for_editing(id)
        end
      end
    end
  end
end
