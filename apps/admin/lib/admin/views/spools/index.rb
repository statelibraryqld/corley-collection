require "admin/views/index"
require "admin/import"

module Admin
  module Views
    module Spools
      class Index < Admin::Views::Index
        include Admin::Import["spool_repo"]

        configure do |config|
          config.template = "spools/index"
        end

        expose :spools do |page:|
          spool_repo.index(page)
        end
      end
    end
  end
end
