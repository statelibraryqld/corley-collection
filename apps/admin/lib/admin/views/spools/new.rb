require "admin/import"
require "admin/view/controller"

module Admin
  module Views
    module Spools
      class New < Admin::View::Controller
        include Admin::Import["spools.form"]

        configure do |config|
          config.template = "spools/new"
        end

        expose :form do |validation: nil|
          form.fill(from: validation)
        end
      end
    end
  end
end
