require "admin/view/controller"
require "admin/import"

module Admin
  module Views
    module Stories
      class Edit < Admin::View::Controller
        include Admin::Import["story_repo", form: "stories.form"]

        configure do |config|
          config.template = "stories/edit"
        end

        expose :form do |story, validation: nil|
          form.fill(from: validation || story)
        end

        expose :story do |id:|
          story_repo.for_editing(id)
        end
      end
    end
  end
end
