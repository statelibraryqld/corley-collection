require "admin/views/index"
require "admin/import"

module Admin
  module Views
    module Stories
      class Index < Admin::Views::Index
        include Admin::Import["story_repo"]

        configure do |config|
          config.template = "stories/index"
        end

        expose :stories do |page:|
          story_repo.index(page)
        end
      end
    end
  end
end
