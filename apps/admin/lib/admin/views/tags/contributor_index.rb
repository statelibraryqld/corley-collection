require "admin/views/index"
require "admin/import"

module Admin
  module Views
    module Tags
      class ContributorIndex < Admin::Views::Index
        include Admin::Import["tag_repo"]

        configure do |config|
          config.template = "tags/contributor_index"
        end

        expose :tags do |page:|
          tag_repo.contributor_index(page)
        end

        expose :total_tag_count do
          tag_repo.total_tag_count
        end

        expose :contributor_tag_count do
          tag_repo.contributor_tag_count
        end
      end
    end
  end
end
