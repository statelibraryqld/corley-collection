require "admin/import"
require "admin/view/controller"

module Admin
  module Views
    module Tags
      class Edit < Admin::View::Controller
        include Admin::Import["tags.form", "tag_repo"]

        configure do |config|
          config.template = "tags/edit"
        end

        expose :form do |tag, validation: nil|
          form.fill(from: validation || tag)
        end

        expose :tag do |id:|
          tag_repo[id]
        end
      end
    end
  end
end
