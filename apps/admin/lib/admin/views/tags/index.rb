require "admin/views/index"
require "admin/import"

module Admin
  module Views
    module Tags
      class Index < Admin::Views::Index
        include Admin::Import[
          "tag_repo",
          "tags.search"
        ]

        configure do |config|
          config.template = "tags/index"
        end

        expose :tags do |search_query, page:|
          if search_query.present?
            search.(search_query)
          else
            tag_repo.index(page)
          end
        end

        expose :placeholder_text do
          "Search tags"
        end

        expose :total_tag_count do
          tag_repo.total_tag_count
        end

        expose :contributor_tag_count do
          tag_repo.contributor_tag_count
        end
      end
    end
  end
end
