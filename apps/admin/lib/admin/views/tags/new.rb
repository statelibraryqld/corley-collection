require "admin/import"
require "admin/view/controller"

module Admin
  module Views
    module Tags
      class New < Admin::View::Controller
        include Admin::Import["tags.form"]

        configure do |config|
          config.template = "tags/new"
        end

        expose :form do |validation: nil|
          form.fill(from: validation)
        end
      end
    end
  end
end
