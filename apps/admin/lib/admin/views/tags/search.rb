require "admin/views/index"
require "admin/import"

module Admin
  module Views
    module Tags
      class Search < Admin::Views::Index
        include Admin::Import["tags.search"]

        configure do |config|
          config.template = "tags/search"
        end

        expose :tags do |search_query|
          search.(search_query)
        end

        expose :pager do |tags|
          tags.pager
        end
      end
    end
  end
end
