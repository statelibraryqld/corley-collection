require "admin/view/controller"
require "admin/import"

module Admin
  module Views
    module User
      class Dashboard < Admin::View::Controller
        include Admin::Import[:user_repo]

        configure do |config|
          config.template = "user/dashboard"
        end

        expose :user do |id:|
          user_repo[id]
        end
      end
    end
  end
end
