require "admin/view/controller"
require "admin/import"

module Admin
  module Views
    module User
      class Edit < Admin::View::Controller
        include Admin::Import[
          "user_repo",
          form: "user.edit_form"
        ]

        configure do |config|
          config.template = "user/edit"
        end

        expose :form do |user, validation: nil|
          form.fill(from: validation || user)
        end

        expose :user do |id:|
          user_repo.for_editing(id)
        end
      end
    end
  end
end
