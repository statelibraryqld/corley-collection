require "admin/import"
require "admin/view/controller"

module Admin
  module Views
    module Users
      class Edit < Admin::View::Controller
        include Admin::Import["user_repo", form: "users.edit_form"]

        configure do |config|
          config.template = "users/edit"
        end

        expose :form do |user, validation: nil|
          form.fill(from: validation || user)
        end

        expose :user do |id:|
          user_repo.for_editing(id)
        end
      end
    end
  end
end
