require "admin/views/index"
require "admin/import"

module Admin
  module Views
    module Users
      class Index < Admin::Views::Index
        include Admin::Import[
          "user_repo",
          "users.search"
        ]

        configure do |config|
          config.template = "users/index"
        end

        expose :users do |search_query, page:|
          if search_query.present?
            search.(search_query)
          else
            user_repo.index(page)
          end
        end

        expose :placeholder_text do
          "Search users"
        end
      end
    end
  end
end
