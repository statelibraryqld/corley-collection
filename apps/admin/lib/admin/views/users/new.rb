require "admin/entities"
require "admin/import"
require "admin/view/controller"

module Admin
  module Views
    module Users
      class New < Admin::View::Controller
        include Admin::Import[form: "users.new_form"]

        configure do |config|
          config.template = "users/new"
        end

        expose :form do |user, validation: nil|
          form.fill(from: validation || user)
        end

        expose :user do
          Entities::User.new
        end
      end
    end
  end
end
