require "dry/web/roda/application"
require_relative "container"
require "rack/csrf"
require "rack_attack"
require "roda_plugins"

module Admin
  class Application < Dry::Web::Roda::Application
    configure do |config|
      config.container = Container
      config.routes = "web/routes".freeze
    end

    opts[:root] = Pathname(__FILE__).join("../..").realpath.dirname

    use Rack::Session::Cookie, key: "corley-collection.session", secret: self["core.settings"].session_secret

    use Rack::MethodOverride
    use Rack::Csrf, raise: true
    use Rack::Attack

    # Vendor plugins
    plugin :all_verbs
    plugin :flash
    plugin :halt
    plugin :header_matchers # TODO: make this a dependency of our own plugin
    plugin :json
    plugin :json_parser
    plugin :not_found
    plugin :type_routing
    plugin :multi_route

    # Our plugins
    plugin :view
    plugin :auth

    # Set default_headers
    plugin :default_headers, "Cache-Control" => "private, no-cache, max-age=0, must-revalidate, no-store"

    route do |r|
      r.multi_route

      r.authorize do
        r.is do
          r.view("user.dashboard", id: current_user.id)
        end
      end
    end

    load_routes!

    plugin :error_handler, classes: [] do |_e|
      response.status = 401

      view = request.xhr? ? "views.errors.xhr.access_denied" : "views.errors.401"

      self.class.container[view].(context: current_view_context)
    end

    def t
      self.class["core.i18n.t"]
    end

    def current_user
      env["admin.current_user"]
    end
  end
end
