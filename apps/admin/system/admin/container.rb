require "pathname"
require "dry/web/container"
require "corley_collection/resolver"

module Admin
  class Container < Dry::Web::Container
    use :monitoring

    require root.join("system/corley_collection/container")
    import core: CorleyCollection::Container

    require root.join("backend/mail/system/mail/container")
    import mail: Mail::Container

    require root.join("backend/search/system/search/container")
    import search: Search::Container

    require root.join("backend/cdn/system/cdn/container")
    import cdn: Cdn::Container

    register :rack_monitor, CorleyCollection::Container[:rack_monitor]

    configure do |config|
      config.resolver = CorleyCollection::Resolver.new(config.env)
      config.root = Pathname(__FILE__).join("../..").realpath.dirname.freeze
      config.logger = CorleyCollection::Container[:logger]
      config.default_namespace = "admin"
      config.auto_register = %w[lib/admin]
    end

    boot(:view, from: :snowflakes, namespace: "view") do
      before(:init) do
        require "types"
      end

      configure do |config|
        config.part_globs = [container.root.join("lib/admin/view/parts")]
      end

      after(:start) do
        require "admin/view/context"
        register("view.context", Admin::View::Context.new)
      end
    end

    boot(:auto_view) do |admin|
      init do
        require "dry/web/auto_view"
        require "admin/view/controller"
      end

      start do
        Dry::Web::AutoView.new(Admin::View::Controller, admin).load!
      end
    end

    load_paths! "lib"
  end
end
