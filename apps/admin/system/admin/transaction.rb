require "dry/monads/result"
require "dry/transaction"
require "corley_collection/backgroundable"

require_relative "container"
require_relative "transaction/step_adapters"

module Admin
  class Transaction
    include Dry::Monads::Result::Mixin
    include Dry::Transaction(container: Container, step_adapters: Transaction::StepAdapters)

    def self.inherited(subclass)
      subclass.include CorleyCollection::Backgroundable.new(for_method: :call_from_queue)
    end
  end
end
