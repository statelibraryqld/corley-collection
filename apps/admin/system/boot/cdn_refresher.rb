Admin::Container.boot :cdn_refresher do |container|
  init do
    require "admin/services/cdn_refresher"

    register :cdn_refresher, Admin::Services::CdnRefresher.new
  end

  start do
    container[:cdn_refresher].start if container.env == :production
  end

  stop do
    container[:cdn_refresher].stop
  end
end
