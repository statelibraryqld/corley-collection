Admin::Container.boot :indexer do |admin|
  init do
    require "admin/services/indexer"

    register(:indexer, Admin::Services::Indexer.new)
  end

  start do
    admin[:indexer].start
  end

  stop do
    admin[:indexer].stop
  end
end
