Admin::Container.boot(:thumbor) do |container|
  init do
    require "corley_collection/thumbor/client"
  end

  start do
    client = CorleyCollection::Thumbor::Client.new(
      host: container["core.settings"].thumbor_host,
      security_key: container["core.settings"].thumbor_security_key,
      default_image_host: container["core.s3.buckets.assets"].url
    ) do
      preset :thumbnail, width: 50, height: 50, fit_in: true
      preset :form_thumbnail, width: 75, height: 75, fit_in: true
      preset :preview, width: 300, height: 300, fit_in: true
    end

    register :thumbor, client
  end
end
