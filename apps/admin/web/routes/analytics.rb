module Admin
  class Application
    route "analytics" do |r|
      r.authorize do
        r.on "photographs" do
          r.is do
            r.get do
              r.view "analytics.photographs.index", params: r.params
            end
          end
        end

        r.on "contributors" do
          r.is do
            r.get do
              r.view "analytics.contributors.index", params: r.params
            end
          end
        end

        r.on "stories" do
          r.is do
            r.get do
              r.view "analytics.stories.index", params: r.params
            end
          end
        end
      end
    end
  end
end
