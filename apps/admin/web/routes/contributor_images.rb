module Admin
  class Application
    route "contributor-images" do |r|
      r.authorize do
        r.is do
          r.get do
            r.view "contributor_images.index", params: r.params, page: r[:page] || 1
          end
        end

        r.post "export" do
          r.resolve("contributor_images.transactions.export") do |export|
            export.enqueue(current_user.email)
            flash[:notice] = "A CSV export will be sent to #{current_user.email}. Please check your email"
            r.redirect "/admin/contributor-images"
          end
        end

        r.on :id do |id|
          r.put do
            r.resolve "contributor_images.operations.update" do |update|
              update.(id, r[:contributor_image]) do |m|
                m.success do
                  flash[:notice] = "Contributor image has been updated"
                  r.redirect "/admin/contributor-images/#{id}/edit"
                end

                m.failure do |validation|
                  r.view "contributor_images.edit", id: id, validation: validation
                end
              end
            end
          end

          r.delete do
            r.resolve "contributor_images.operations.delete" do |delete|
              delete.(id)
              flash[:notice] = "Contributor image has been deleted"
              r.redirect "/admin/contributor-images"
            end
          end

          r.on "edit" do
            r.get do
              r.view "contributor_images.edit", id: id
            end
          end
        end
      end
    end
  end
end
