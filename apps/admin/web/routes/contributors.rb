module Admin
  class Application
    route "contributors" do |r|
      r.authorize do
        r.is do
          r.get do
            r.view "contributors.index", params: r.params, page: r[:page] || 1
          end
        end

        r.on :id do |id|
          r.put do
            r.resolve "contributors.operations.update" do |update|
              update.(id, r[:contributor]) do |m|
                m.success do
                  flash[:notice] = "Contributor has been updated"
                  r.redirect "/admin/contributors/#{id}/edit"
                end

                m.failure do |validation|
                  r.view "contributors.edit", id: id, validation: validation
                end
              end
            end
          end

          r.delete do
            r.resolve "contributors.operations.delete" do |delete|
              delete.(id)
              flash[:notice] = "Contributor has been deleted"
              r.redirect "/admin/contributors"
            end
          end

          r.on "edit" do
            r.get do
              r.view "contributors.edit", id: id
            end
          end
        end
      end
    end
  end
end
