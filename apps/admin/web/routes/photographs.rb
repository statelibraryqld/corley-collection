module Admin
  class Application
    route "photographs" do |r|
      r.authorize do
        r.is do
          r.get do
            r.view "photographs.index", params: r.params, page: r[:page] || 1
          end

          r.post do
            r.resolve "photographs.operations.create" do |create|
              create.(r[:photograph]) do |m|
                m.success do
                  flash[:notice] = "Photograph has been created"
                  r.redirect "/admin/photographs"
                end

                m.failure do |validation|
                  r.view "photographs.new", validation: validation
                end
              end
            end
          end
        end

        r.get "new" do
          r.view "photographs.new"
        end

        r.post "export" do
          r.resolve("photographs.transactions.export") do |export|
            export.enqueue(current_user.email)
            flash[:notice] = "A CSV export will be sent to #{current_user.email}. Please check your email"
            r.redirect "/admin/photographs"
          end
        end

        r.on :id do |id|
          r.put do
            r.resolve "photographs.operations.update" do |update|
              update.(id, r[:photograph]) do |m|
                m.success do
                  flash[:notice] = "Photograph has been updated"
                  r.redirect "/admin/photographs/#{id}/edit"
                end

                m.failure do |validation|
                  r.view "photographs.edit", id: id, validation: validation
                end
              end
            end
          end

          r.delete do
            r.resolve "photographs.operations.delete" do |delete|
              delete.(id)
              flash[:notice] = "Photograph has been deleted"
              r.redirect "/admin/photographs"
            end
          end

          r.on "edit" do
            r.get do
              r.view "photographs.edit", id: id
            end
          end
        end
      end
    end
  end
end
