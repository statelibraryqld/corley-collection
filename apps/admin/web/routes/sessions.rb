class Admin::Application
  route "sign-in" do |r|
    r.get do
      # route[sign_in]: GET /admin/sign-in
      r.view "sign_in"
    end

    # route[sign_in]: POST /admin/sign-in
    r.post do
      r.resolve "authentication.authenticate" do |authenticate|
        authenticate.(r[:user]) do |m|
          m.success do |user|
            flash[:notice] = "You are now signed in"
            session[:user_id] = user.id
            r.redirect "/admin"
          end

          m.failure do |error|
            flash[:alert] = t["admin.auth.errors.#{error}"]
            r.redirect "/admin/sign-in"
          end
        end
      end
    end
  end

  # route[sign_out]: GET /admin/sign-out
  route "sign-out" do |r|
    flash[:notice] = "You are now signed out"
    session[:user_id] = env["admin.current_user"] = nil
    r.redirect "/admin/sign-in"
  end
end
