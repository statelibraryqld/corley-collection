module Admin
  class Application
    route "spools" do |r|
      r.authorize do
        r.is do
          r.get do
            r.view "spools.index", params: r.params, page: r[:page] || 1
          end

          r.post do
            r.resolve "spools.operations.create" do |create|
              create.(r[:spool]) do |m|
                m.success do
                  flash[:notice] = "Spool has been created"
                  r.redirect "/admin/spools"
                end

                m.failure do |validation|
                  r.view "spools.new", validation: validation
                end
              end
            end
          end
        end

        r.get "new" do
          r.view "spools.new"
        end

        r.on :id do |id|
          r.put do
            r.resolve "spools.operations.update" do |update|
              update.(id, r[:spool]) do |m|
                m.success do
                  flash[:notice] = "Spool has been updated"
                  r.redirect "/admin/spools/#{id}/edit"
                end

                m.failure do |validation|
                  r.view "spools.edit", id: id, validation: validation
                end
              end
            end
          end

          r.delete do
            r.resolve "spools.operations.delete" do |delete|
              delete.(id)
              flash[:notice] = "Spool has been deleted"
              r.redirect "/admin/spools"
            end
          end

          r.on "edit" do
            r.get do
              r.view "spools.edit", id: id
            end
          end
        end
      end
    end
  end
end
