module Admin
  class Application
    route "stories" do |r|
      r.authorize do
        r.is do
          r.get do
            r.view "stories.index", params: r.params, page: r[:page] || 1
          end
        end

        r.post "export" do
          r.resolve("stories.transactions.export") do |export|
            export.enqueue(current_user.email)
            flash[:notice] = "A CSV export will be sent to #{current_user.email}. Please check your email"
            r.redirect "/admin/stories"
          end
        end

        r.on :id do |id|
          r.put do
            r.resolve "stories.operations.update" do |update|
              update.(id, r[:story]) do |m|
                m.success do
                  flash[:notice] = "Story has been updated"
                  r.redirect "/admin/stories/#{id}/edit"
                end

                m.failure do |validation|
                  r.view "stories.edit", id: id, validation: validation
                end
              end
            end
          end

          r.delete do
            r.resolve "stories.operations.delete" do |delete|
              delete.(id)
              flash[:notice] = "Story has been deleted"
              r.redirect "/admin/stories"
            end
          end

          r.on "edit" do
            r.get do
              r.view "stories.edit", id: id
            end
          end
        end
      end
    end
  end
end
