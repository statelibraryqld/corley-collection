class Admin::Application
  route "tags" do |r|
    r.authorize do
      r.is do
        # route[tags]: GET /admin/tags
        r.get do
          r.view "tags.index", params: r.params, page: r[:page] || 1
        end

        # route[tags]: POST /admin/tags
        r.post do
          r.resolve("tags.operations.create") do |create|
            create.(r[:tag]) do |m|
              m.success do
                flash[:notice] = "Tag has been created"
                r.redirect "/admin/tags"
              end

              m.failure do |validation|
                r.view "tags.new", validation: validation
              end
            end
          end
        end
      end

      # route[new_tag]: GET /admin/tags/new
      r.get "new" do
        r.view "tags.new"
      end

      # Search endpoint for JSON requests (from Formalist tag fields)
      r.get "search" do
        r.json do
          r.view "tags.search", params: r.params, q: r[:q], ids: r[:ids], page: r[:page] || 1, format: :json
        end
      end

      r.get "contributor" do
        r.view "tags.contributor_index", params: r.params, page: r[:page] || 1
      end

      r.on :id do |id|
        # route[tag]: PUT /admin/tags/:id
        r.put do
          r.resolve "tags.operations.update" do |update_tag|
            update_tag.(id, r[:tag]) do |m|
              m.success do
                flash[:notice] = "Tag has been updated"
                r.redirect "/admin/tags/#{id}/edit"
              end

              m.failure do |validation|
                r.view "tags.edit", id: id, validation: validation
              end
            end
          end
        end

        # route[tag]: DELETE /admin/tags/:id
        r.delete do
          r.resolve "tags.operations.delete" do |delete_tag|
            delete_tag.(id)
            flash[:notice] = "Tag has been deleted"
            r.redirect "/admin/tags"
          end
        end

        # route[edit_tag]: GET /admin/tags/:id/edit
        r.on "edit" do
          r.get do
            r.view "tags.edit", id: id
          end
        end
      end
    end
  end
end
