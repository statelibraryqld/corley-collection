module Admin
  class Application
    route "user" do |r|
      r.authorize do
        # route[edit_current_user] GET /admin/user
        r.on "edit" do
          r.get do
            r.view "user.edit", id: current_user.id
          end

          # route[update_current_user] PUT /admin/user
          r.put do
            r.resolve("user.operations.update") do |update|
              update.(current_user.id, r[:user]) do |m|
                m.success do
                  flash[:notice] = t["admin.user.account_updated"]
                  r.redirect "/admin/user/edit"
                end

                m.failure do |validation|
                  r.view "user.edit", id: current_user.id, validation: validation
                end
              end
            end
          end
        end
      end
    end
  end
end
