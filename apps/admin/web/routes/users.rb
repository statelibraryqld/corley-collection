class Admin::Application
  route "users" do |r|
    # route[forgot_password] GET /admin/forgot-password
    r.on "forgot-password" do
      r.get do
        r.view "account_management.forgot_password"
      end

      # route[forgot_password] POST /admin/forgot-password
      r.post do
        r.resolve "account_management.transactions.reset_password" do |reset_password|
          reset_password.(r[:user]) do |m|
            m.success do
              flash[:notice] = t["admin.auth.password_reset"]
              r.redirect "/admin/sign-in"
            end

            m.failure do |validation|
              r.view "account_management.forgot_password", validation: validation
            end
          end
        end
      end
    end

    r.on "reset-password" do
      # route[password_reset_token] GET /admin/reset-password/:password_reset_token
      r.on :password_reset_token do |password_reset_token|
        r.resolve "authentication.authenticate_user_by_password_reset_token" do |auth_by|
          auth_by.(password_reset_token) do |m|
            m.success do |user|
              r.get do
                r.view "account_management.reset_password", user_id: user.id
              end

              r.post do
                r.resolve "account_management.operations.change_password" do |change_password|
                  change_password.(user.id, r[:user]) do |n|
                    n.success do
                      flash[:notice] = t["admin.auth.password_set"]
                      session[:user_id] = user.id
                      r.redirect "/admin"
                    end

                    n.failure do |validation|
                      r.view "account_management.reset_password", user_id: user.id, validation: validation
                    end
                  end
                end
              end
            end

            m.failure do |error|
              flash[:notice] = t["admin.auth.errors.#{error}"]
              r.redirect "/admin/sign-in"
            end
          end
        end
      end
    end

    r.on "activate" do
      r.on :activation_token do |activation_token|
        r.resolve "authentication.authenticate_activation_token" do |authenticate_activation_token|
          authenticate_activation_token.(activation_token) do |m|
            m.success do |user|
              # route[activate_account] GET /admin/activate/:activation_token
              r.get do
                r.view "account_management.activate", user_id: user.id
              end

              # route[activate_account] POST /admin/activate/:activation_token
              r.post do
                r.resolve "account_management.operations.change_password" do |change_password|
                  change_password.(user.id, r[:user]) do |n|
                    n.success do
                      flash[:notice] = t["admin.auth.password_set"]
                      session[:user_id] = user.id
                      r.redirect "/admin"
                    end

                    n.failure do |validation|
                      r.view "account_management.activate", user_id: user.id, validation: validation
                    end
                  end
                end
              end
            end

            m.failure do |error|
              flash[:notice] = t["admin.auth.errors.#{error}"]
              r.redirect "/admin/sign-in"
            end
          end
        end
      end
    end

    r.authorize do
      r.is do
        # route[users]: GET /admin/users
        r.get do
          r.view "users.index", params: r.params, page: r[:page] || 1
        end

        # route[users]: POST /admin/users
        r.post do
          r.resolve "users.transactions.create" do |create|
            create.(r[:user]) do |m|
              m.success do
                flash[:notice] = "User has been created"
                r.redirect "/admin/users"
              end

              m.failure do |validation|
                r.view "users.new", validation: validation
              end
            end
          end
        end
      end

      # route[new_user]: GET /admin/users/new
      r.get "new" do
        r.view "users.new"
      end

      r.on :id do |id|
        # route[user]: PUT /admin/users/:id
        r.put do
          r.resolve "users.operations.update" do |update_user|
            update_user.(id, r[:user]) do |m|
              m.success do
                flash[:notice] = "User has been updated"
                r.redirect "/admin/users/#{id}/edit"
              end

              m.failure do |validation|
                r.view "users.edit", id: id, validation: validation
              end
            end
          end
        end

        # route[user]: DELETE /admin/users/:id
        r.delete do
          r.resolve "users.operations.delete" do |delete_user|
            delete_user.(id)
            flash[:notice] = "User has been deleted"
            r.redirect "/admin/users"
          end
        end

        # route[edit_user]: GET /admin/users/:id/edit
        r.on "edit" do
          r.get do
            r.view "users.edit", id: id
          end
        end
      end
    end
  end
end
