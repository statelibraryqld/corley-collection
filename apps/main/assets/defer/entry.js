// Require the base CSS, basically our entry point for CSS
// If your target doesn't require CSS, you can comment this out
import "./index.css";

// Require all images and CSS by default
// This will inspect all subdirectories from the context (first param) and
// require files matching the regex.
require.context(".", true, /^\.\/.*\.(jpe?g|png|gif|svg)$/);
