import FontFaceObserver from "fontfaceobserver";
var Promise = require("es6-promise").Promise;

/**
 * List of fonts we care about loading
 * @type {Array}
 */
var fontsToCheck = [
  {
    family: "Fira Mono",
    style: "normal",
    weight: 400
  },
  {
    family: "Fira Mono",
    style: "normal",
    weight: 700
  }
];

/**
 * Font loading
 * Implements deferred font-loading
 * https://www.filamentgroup.com/lab/font-events.html
 */

function fontsLoaded(fontsToCheck) {
  var timeout = 1500;
  var fontsLoaded = window.localStorage.getItem("fontsLoaded");

  if (!fontsLoaded) {
    var loading = fontsToCheck.map(function(font) {
      const observer = new FontFaceObserver(font.family, {
        weight: font.weight,
        style: font.style
      });
      return observer.load(null, timeout);
    });
    Promise.all(loading).then(
      () => {
        window.document.documentElement.className += " fonts-loaded";
        window.localStorage.setItem("fontsLoaded", true);
      },
      () => {
        window.document.documentElement.className += " fonts-loaded";
      }
    );
  }
}

fontsLoaded(fontsToCheck);
