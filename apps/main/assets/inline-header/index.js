/**
 * Inlined and injected in to the <head> of the page
 */

// Needs to be inlined for special modes check
import "url-polyfill";
import "fg-loadcss";
import "fg-loadcss/src/cssrelpreload.js";
import checkFontsPreloaded from "./check-fonts-preloaded";
checkFontsPreloaded();
