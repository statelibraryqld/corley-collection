import "es6-weak-map/implement";
import "mdn-polyfills/Array.prototype.find";
import "formdata-polyfill";
import URLSearchParams from "url-search-params";
window.URLSearchParams = URLSearchParams;
