// Normal deps
import delegate from "delegate";
import domready from "domready";
import viewloader from "viewloader";

import views from "./views";

let viewloaderManager = viewloader(views);

domready(() => {
  viewloaderManager.callViews();
});
