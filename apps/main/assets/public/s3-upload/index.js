import request from "superagent";

/**
 * noOp
 * Default param value
 * @return {Function}
 */

const noOp = _ => {};

/**
 * reqs
 * a hash of existing XHR requests
 */

let reqs = {};

/**
 * getXHRRequests
 * Get the current XHR processing XHR requests
 * @return {Object}
 */

function getXHRRequests() {
  return reqs;
}

/**
 * setXHRRequests
 * Assign an object to `reqs`
 * @param {Object} object
 * @return {Object}
 */

function setXHRRequests(object) {
  Object.assign(reqs, object);
  return reqs;
}

/**
 * abortXHRRequest
 * Abort a XHR request by 'uid'
 * @param {String} uid
 * @param {Function} optional function
 * @return {Object}
 */

function abortXHRRequest(uid, fn) {
  if (reqs.hasOwnProperty(uid)) {
    if (!reqs[uid]) return;

    if (fn) {
      fn();
    } else {
      reqs[uid].abort();
    }
    delete reqs[uid];
    return reqs;
  }
}

/**
 * customError
 * return an object forming a custom error message
 * @param  {String} name
 * @param  {Object} error
 * @return {Object}
 */

function customError(name, error) {
  return {
    error,
    message: error.message,
    name
  };
}

/**
 * responseStatus
 * take a response and check the response `status` property
 * if between 200-300 return the response object
 * else throw a custom error
 * @param  {Object} res
 * @return {Object}
 */

function responseStatus(res) {
  if (res.status >= 200 && res.status < 300) {
    return res;
  } else {
    let error = new Error(res.statusText);
    error.response = res;
    throw customError("responseStatus", error);
  }
}

/**
 * parseJSON
 * Take a response object and return the parsed res.text
 * @param  {String} response
 * @return {Object}
 */

function parseJSON(res) {
  return JSON.parse(res.text);
}

/**
 * formData
 * Build up and return a FormData object
 *
 * @param {String} as: Define the type of form data
 * @param {Object} file: a file object
 * @param {Object} fields: key/value from preSign response
 * @return {Object} FormData
 */

function formData(as, file, fields) {
  var data = new window.FormData();

  if (fields) {
    Object.keys(fields).forEach(function(key) {
      data.append(key, fields[key]);
    });
  }
  const name = file.name;
  data.append(as, file, name);
  return data;
}

/**
 * Parse file metadata
 * @param {FileObject} file
 */
function parseMetadata(file) {
  return new Promise(function(resolve, reject) {
    resolve({
      size: file.size,
      content_type: file.type
    });
  });
}

/**
 * Assemble output data into persistable format
 * @param {FileObject} fileObject File object from uploader
 * @param {Object} metadata Extracted metadata
 */
function assembleOutputData(res, fileObject, metadata) {
  const { file } = fileObject;
  const { name } = file;
  const { fields } = res;
  const { key } = fields;
  const path = key.replace(/\${filename}/, name);

  return Object.assign({ path: path }, metadata);
}

/**
 * uploadRequest
 * Assign an XHR request to the `reqs` hash using the `uid`.
 * @param  {Object} res - the response from presignRequest()
 * @param  {File Object} file
 * @param  {Function} on progress event handler
 * @return  {Promise}
 */

function uploadRequest(res, fileObject, showProgress) {
  const { url, fields } = res;
  const { file, uid } = fileObject;
  const data = formData("file", file, fields);

  return new Promise((resolve, reject) => {
    reqs[uid] = request
      .post(url)
      .send(data)
      .set({
        Accept: "application/json"
      })
      .on("progress", e => {
        showProgress(e, fileObject);
      })
      .end((err, res) => {
        delete reqs[uid];

        // throw a custom error message
        if (err) return reject(customError("uploadRequest", err));

        resolve(res);
      });
  });
}

/**
 * upload
 * Take a response object (from presignRequest) a file and a token
 * and return a Promise that makes an uploadRequest()
 * @param  {Object} res - the response from presignRequest()
 * @param  {File Object} fileObject
 * @param  {Function} showProgress - progress event handler
 * @param  {Function} fn - defaults to uploadRequest()
 * @return {Promise}
 */

function upload(res, fileObject, showProgress = noOp, fn = uploadRequest) {
  return new Promise((resolve, reject) => {
    const metadata = parseMetadata(fileObject.file);
    const req = fn(res, fileObject, showProgress).then(responseStatus);

    Promise.all([req, metadata])
      .then(args => {
        // Merge metadata with base attributes
        const metadata = args[1];
        const output = assembleOutputData(res, fileObject, metadata);
        resolve(output);
      })
      .catch(err => {
        reject(err);
      });
  });
}

/**
 * presignRequest
 * Perform an XHR request and Resolve or Reject
 * @param  {String} presignUrl
 * @param  {String} token
 * @param  {Promise}
 */

function presignRequest(presignUrl, token, presignOptions) {
  return new Promise((resolve, reject) => {
    request
      .post(presignUrl)
      .send(presignOptions)
      .set({
        Accept: "application/json",
        "Content-Type": "application/json",
        "X-CSRF-Token": token
      })
      .end((err, res) => {
        // throw a custom error message
        if (err) return reject(customError("presignRequest", err));
        resolve(res);
      });
  });
}

/**
 * presign
 * Take a url and optional token
 * return a Promise
 * @param  {String} presignUrl
 * @param  {String} token
 * @param  {Function} defaults to presignRequest()
 * @param  {Promise}
 */

function presign(presignUrl, token, presignOptions = {}, fn = presignRequest) {
  return new Promise((resolve, reject) => {
    fn(presignUrl, token, presignOptions)
      .then(responseStatus)
      .then(parseJSON)
      .then(res => {
        resolve(res);
      })
      .catch(err => {
        reject(err);
      });
  });
}

export default {
  presign: presign,
  upload: upload,
  abortXHRRequest: abortXHRRequest,
  getXHRRequests: getXHRRequests,
  setXHRRequests: setXHRRequests
};
