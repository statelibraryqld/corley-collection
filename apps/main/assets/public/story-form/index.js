import S3Upload from "../s3-upload";
import uid from "uid";
import addClass from "../utilities/add-class";
import removeClass from "../utilities/remove-class";

export default function storyForm(el, props) {
  const form = el;
  const bodyField = el.querySelector(".story-body");
  const submitButton = el.querySelector("button");
  const formErrorDisplay = el.querySelector(".error-display");
  const fileField = el.querySelector(".file-input");
  const fileUploadingDisplay = el.querySelector(".file-loading");
  const uploadedImagesDisplay = el.querySelector(".uploaded-images");

  form.addEventListener("submit", onSubmit);

  fileField.addEventListener("change", handleFiles, false);

  function onSubmit(e) {
    e.preventDefault();

    clearErrors();

    disableSubmit("Submitting");

    // Validate required fields here
    const formIsValid = validateForm();

    enableSubmit();

    if (!formIsValid) {
      return false;
    } else {
      form.submit();
    }
  }

  function validateForm() {
    // Create a switch to toggle when validation fails
    let formValid = true;

    const uploadedImages = Array.prototype.slice.call(
      uploadedImagesDisplay.querySelectorAll(".uploaded-image-container")
    );

    // If there are no images, ensure that there's a story
    if (uploadedImages.length === 0) {
      if (bodyField.value.length < 1) {
        formValid = false;
        addClass(bodyField, "form-input--error");
        formErrorDisplay.textContent = "Please fill in your story";
        formErrorDisplay.style.display = "block";
      }
    }

    // Ensure each image has a caption
    uploadedImages.forEach(el => {
      const captionInput = el.querySelector(".image-caption");

      if (captionInput.value.length === 0) {
        formValid = false;
        addClass(captionInput, "form-input--error");
        formErrorDisplay.textContent = "Please caption all images";
        formErrorDisplay.style.display = "block";
      }
    });

    return formValid;
  }

  function enableSubmit() {
    submitButton.innerHTML = "Submit";
    submitButton.removeAttribute("disabled");
  }

  function disableSubmit(text) {
    submitButton.innerHTML = props.onSubmitButtonText || text + " &hellip;";
    submitButton.setAttribute("disabled", "disabled");
  }

  function clearErrors() {
    formErrorDisplay.style.display = "none";
    removeClass(bodyField, "form-input--error");
  }

  function handleFiles() {
    const fileList = this.files;

    // Check that we have a file
    if (!fileList.length) return;

    // Hide any errors that might be showing from a previous attempt
    formErrorDisplay.style.display = "none";

    // Show the uploading animation
    fileField.style.display = "none";
    fileUploadingDisplay.style.display = "block";

    disableSubmit("Uploading");

    // Create a 'file object' of the file and assign to `file`
    for (let i = 0; i < fileList.length; i++) {
      const file = createFileObject(fileList[i]);
      uploadFile(file);
    }
  }

  function createFileObject(file) {
    const name = file.name;
    return {
      file: file,
      name: name,
      uid: (0, generateUniqueID)()
    };
  }

  function generateUniqueID() {
    return "IMG_" + uid(10);
  }

  function uploadFile(fileObject) {
    if (!fileObject) return;
    let presign_url = props.presign_url;
    let token = props.token;
    let presign_options = { acl: "public-read" };

    S3Upload.presign(presign_url, token, presign_options)
      .then(function(presignResponse) {
        return S3Upload.upload(presignResponse, fileObject);
      })
      .then(function(uploadResponse) {
        return addUploadedImage({
          fileObject: fileObject,
          path: uploadResponse.path
        });
      })
      .catch(err => {
        // Display the file uploading error
        hideUploadingDisplay();
        throw new Error(err.message);
      });
  }

  function addUploadedImage(image) {
    let uploadedImageContainer = document.createElement("div");
    uploadedImageContainer.className = "uploaded-image-container";

    // Create an img
    let uploadedImage = document.createElement("img");
    uploadedImage.setAttribute("height", "100");

    // Show the file name
    let nameDisplay = document.createElement("div");
    nameDisplay.className = "uploaded-image-name";
    nameDisplay.textContent = image.fileObject.name;

    // Create a caption input
    let captionInput = document.createElement("input");
    captionInput.type = "text";
    captionInput.className = "form-input image-caption";
    captionInput.id = image.fileObject.uid;
    captionInput.placeholder = "Add a caption";
    captionInput.setAttribute("name", "story[contributor_images][][caption]");

    // Create a remove button
    let removeButton = document.createElement("button");
    removeButton.textContent = "Remove image";
    removeButton.className = "remove-button";
    removeButton.addEventListener("click", e => {
      e.preventDefault();
      uploadedImageContainer.remove();
    });

    // Show a preview of the image
    let reader = new FileReader();

    reader.onload = function(e) {
      uploadedImage.setAttribute("src", e.target.result);
    };

    reader.readAsDataURL(image.fileObject.file);

    // Create a hidden input to carry the file path
    let filePathInput = document.createElement("input");
    filePathInput.setAttribute("type", "hidden");
    filePathInput.setAttribute("name", "story[contributor_images][][path]");
    filePathInput.value = image.path;

    // Create a hidden input to carry the content type
    let contentTypeInput = document.createElement("input");
    contentTypeInput.setAttribute("type", "hidden");
    contentTypeInput.setAttribute(
      "name",
      "story[contributor_images][][content_type]"
    );
    contentTypeInput.value = image.fileObject.file.type;

    // Create a hidden input to carry the size
    let fileSizeInput = document.createElement("input");
    fileSizeInput.setAttribute("type", "hidden");
    fileSizeInput.setAttribute("name", "story[contributor_images][][size]");
    fileSizeInput.value = image.fileObject.file.size;

    uploadedImageContainer.appendChild(uploadedImage);
    uploadedImageContainer.appendChild(nameDisplay);
    uploadedImageContainer.appendChild(captionInput);
    uploadedImageContainer.appendChild(removeButton);
    uploadedImageContainer.appendChild(filePathInput);
    uploadedImageContainer.appendChild(contentTypeInput);
    uploadedImageContainer.appendChild(fileSizeInput);

    uploadedImagesDisplay.appendChild(uploadedImageContainer);

    hideUploadingDisplay();

    enableSubmit();
  }

  function hideUploadingDisplay() {
    // Hide the uploading animation
    fileField.style.display = "block";
    fileUploadingDisplay.style.display = "none";
    fileField.value = "";
  }
}
