export { default as mergeDefaults } from "./merge-defaults";

export function addClass(el, className) {
  if (el.classList) {
    el.classList.add(className);
  } else {
    el.className += " " + className;
  }
}

export function removeClass(el, className) {
  if (el.classList) {
    el.classList.remove(className);
  } else {
    el.className = el.className.replace(
      new RegExp("(^|\\b)" + className.split(" ").join("|") + "(\\b|$)", "gi"),
      " "
    );
  }
}

/**
 * Empty the passed `node` of all children
 * @param  {Element} node Node to empty
 * @return {Void}
 */
export function emptyNode(node) {
  while (node.firstChild) {
    node.removeChild(node.firstChild);
  }
}

/**
 * Is a number or not
 * @param {Mixed} n Maybe a number?
 */
export function isNumber(n) {
  return Number(n) === n && n % 1 !== 0;
}

/**
 * Unwrap an element, leaving its children
 * @param {DOMNode} node Node to unwrap
 */
export function unwrapNode(node) {
  const parent = node.parentNode;
  if (parent) {
    while (node.firstChild) parent.insertBefore(node.firstChild, node);
    parent.removeChild(node);
  }
}
