import storyForm from "../story-form";

/**
 * Viewloader-compatible view functions
 * https://github.com/icelab/viewloader
 */

export default {
  storyForm: storyForm
};
