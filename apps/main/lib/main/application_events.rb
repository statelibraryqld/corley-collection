require "singleton"
require "dry/events/publisher"

module Main
  class ApplicationEvents
    include Singleton
    include Dry::Events::Publisher[:main]

    %w[
      photograph
      story
      tag
    ].each do |entity|
      register_event("#{entity}.created")
      register_event("#{entity}.updated")
      register_event("#{entity}.deleted")
    end
  end
end
