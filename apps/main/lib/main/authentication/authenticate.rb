require "main/import"
require "corley_collection/operation"

module Main
  module Authentication
    class Authenticate < CorleyCollection::Operation
      include Main::Import["core.authentication.encrypt_password", "contributor_repo"]

      def call(attributes)
        email, password = attributes.values_at("email", "password")

        contributor = contributor_repo.by_email(email)

        password_correct = contributor&.encrypted_password && encrypt_password.same?(contributor.encrypted_password, password)

        return Failure(:contributor_not_found) unless password_correct

        if contributor.active?
          Success(contributor)
        else
          Failure(:suspended)
        end
      end
    end
  end
end
