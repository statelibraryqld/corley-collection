require "corley_collection/operation"
require "main/import"

module Main
  module Authentication
    class AuthenticateContributorByPasswordResetToken < CorleyCollection::Operation
      include Import["contributor_repo"]

      def call(password_reset_token)
        contributor = contributor_repo.by_current_password_reset_token(password_reset_token)

        if contributor
          Success(contributor)
        else
          Failure(:password_reset_token_invalid)
        end
      end
    end
  end
end
