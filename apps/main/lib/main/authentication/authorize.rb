require "corley_collection/operation"
require "main/import"

module Main
  module Authentication
    class Authorize < CorleyCollection::Operation
      include Main::Import["contributor_repo"]

      def call(session)
        id = session[:contributor_id]
        contributor = id && contributor_repo[id]

        return Failure(:unauthorized) unless contributor

        if contributor.active?
          Success(contributor)
        else
          Failure(:suspended)
        end
      end
    end
  end
end
