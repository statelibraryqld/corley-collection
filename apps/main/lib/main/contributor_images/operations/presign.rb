require "corley_collection/operation"
require "aws-sdk-s3"
require "dry-validation"
require "securerandom"
require "main/import"

module Main
  module ContributorImages
    module Operations
      class Presign < CorleyCollection::Operation
        include Main::Import["core.settings", bucket: "core.s3.buckets.assets"]

        OptionsSchema = Dry::Validation.JSON do
          optional(:prefix).filled
          optional(:acl).filled(included_in?: %w[private public-read])
        end

        def call(options = {})
          validation = OptionsSchema.(options)

          return Failure(validation) if validation.failure?

          presign_options = validation.to_h
          prefix = presign_options.delete(:prefix)

          presign_options[:key] = file_key(prefix: prefix)

          Success(bucket.presigned_post(**presign_options))
        end

        private

        def file_key(prefix:)
          hash = SecureRandom.hex(20)
          hash_partition = hash.scan(/.{3}/).first(3).join("/")

          [
            settings.s3_assets_prefix,
            prefix,
            hash_partition,
            hash, "${filename}",
          ].compact.join("/")
        end
      end
    end
  end
end
