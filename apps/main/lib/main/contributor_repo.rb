require "main/repository"
require "corley_collection/repos/account_management"

module Main
  class ContributorRepo < Repository[:contributors]
    include CorleyCollection::Repos::AccountManagement

    commands :create, update: :by_pk

    def [](id)
      contributors.by_pk(id).one
    end

    def by_email(email)
      contributors.by_email(email).one
    end

    def by_public_token(token)
      contributors.by_public_token(token).one
    end

    def subscribed_to_localities
      contributors.join(:locality_notification_subscriptions).distinct(:id)
    end

    def with_localities(id)
      contributors
        .combine(:localities)
        .by_pk(id)
        .one
    end
  end
end
