require "corley_collection/operation"
require "main/import"
require "main/contributors/validation/password_form_schema"

module Main
  module Contributors
    module Operations
      class ChangePassword < CorleyCollection::Operation
        include Import[
          "contributor_repo",
          "core.authentication.encrypt_password"
        ]

        def call(id, params)
          validation = Validation::PasswordFormSchema.(params)

          if validation.success?
            contributor = contributor_repo.update(id, prepare_attributes(validation.to_h))

            Success(contributor)
          else
            Failure(validation)
          end
        end

        private

        def prepare_attributes(params)
          # Encrypt the new password and clear the password reset token
          params.merge(
            encrypted_password: encrypt_password.(params[:password]),
            password_reset_token: nil,
            password_reset_token_expires_at: nil
          )
        end
      end
    end
  end
end
