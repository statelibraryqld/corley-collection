require "corley_collection/operation"
require "main/contributors/validation/sign_up_schema"
require "main/import"

module Main
  module Contributors
    module Operations
      class Create < CorleyCollection::Operation
        include Import["contributor_repo", "core.authentication.encrypt_password"]

        def call(params)
          validation = Validation::SignUpSchema.(params)

          if validation.success?
            contributor = contributor_repo.create(
              validation.to_h.merge(
                encrypted_password: encrypt_password.(validation[:password])
              )
            )

            Success(contributor)
          else
            Failure(validation)
          end
        end
      end
    end
  end
end
