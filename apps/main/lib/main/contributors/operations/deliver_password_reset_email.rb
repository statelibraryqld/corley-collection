require "corley_collection/operation"
require "main/import"

module Main
  module Contributors
    module Operations
      class DeliverPasswordResetEmail < CorleyCollection::Operation
        include Import[
          "mail.main.password_reset_success",
          "mail.main.password_reset_failure",
        ]

        def call(input)
          result =
            if input[:contributor]
              password_reset_success.enqueue(input[:contributor])
            else
              password_reset_failure.enqueue(input[:email])
            end

          Success(result)
        end
      end
    end
  end
end
