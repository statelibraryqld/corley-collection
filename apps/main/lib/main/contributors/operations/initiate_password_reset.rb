require "corley_collection/operation"
require "main/import"
require "main/contributors/validation/forgot_password_schema"

module Main
  module Contributors
    module Operations
      class InitiatePasswordReset < CorleyCollection::Operation
        include Import[
          "contributor_repo",
          "core.authentication.generate_secret_token"
        ]

        def call(params)
          validation = Validation::ForgotPasswordSchema.(params)

          if validation.success?
            email = validation[:email]
            contributor = contributor_repo.by_email(email)
            secret_token = generate_secret_token.(contributor_repo.method(:password_reset_token_exists?))

            if contributor
              contributor_repo.update(
                contributor.id,
                password_reset_token: secret_token.value,
                password_reset_token_expires_at: secret_token.expires_at
              )

              Success(contributor: contributor)
            else
              Success(email: email)
            end
          else
            Failure(validation)
          end
        end
      end
    end
  end
end
