require "corley_collection/operation"
require "main/import"
require "main/contributors/validation/update_schema"

module Main
  module Contributors
    module Operations
      class Update < CorleyCollection::Operation
        include Import[
          "contributor_repo",
          "core.authentication.encrypt_password"
        ]

        def call(id, params)
          validation = Validation::UpdateSchema.with(contributor_id: id).(params)

          if validation.success?
            contributor = contributor_repo.update(id, prepare_attributes(validation.to_h))
            Success(contributor)
          else
            Failure(validation)
          end
        end

        private

        def prepare_attributes(params)
          # Only update the encrypted_password if a new password is provided
          if params[:password]
            params.merge(encrypted_password: encrypt_password.(params[:password]))
          else
            params
          end
        end
      end
    end
  end
end
