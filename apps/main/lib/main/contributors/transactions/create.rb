require "main/transaction"

module Main
  module Contributors
    module Transactions
      class Create < Main::Transaction
        step :create, with: "contributors.operations.create"
        enqueue :deliver_welcome_email, with: "mail.main.welcome"
      end
    end
  end
end
