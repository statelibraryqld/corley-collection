require "main/transaction"

module Main
  module Contributors
    module Transactions
      class ResetPassword < Main::Transaction
        step :initiate_password_reset, with: "contributors.operations.initiate_password_reset"
        step :deliver_password_reset_email, with: "contributors.operations.deliver_password_reset_email"
      end
    end
  end
end
