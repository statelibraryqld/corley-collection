# auto_register: false

require "corley_collection/validation/form"

module Main
  module Contributors
    module Validation
      ForgotPasswordSchema = CorleyCollection::Validation.Form do
        configure do
          config.type_specs = false
        end

        required(:email).filled(:email?)
      end
    end
  end
end
