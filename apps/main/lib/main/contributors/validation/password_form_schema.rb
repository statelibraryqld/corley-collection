# auto_register: false

require "corley_collection/validation/form"

module Main
  module Contributors
    module Validation
      PasswordFormSchema = CorleyCollection::Validation.Form do
        configure do
          config.type_specs = false
        end

        required(:password).filled(min_size?: 8).confirmation
      end
    end
  end
end
