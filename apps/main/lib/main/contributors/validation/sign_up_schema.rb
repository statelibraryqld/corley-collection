# auto_register: false

require "corley_collection/validation/form"

module Main
  module Contributors
    module Validation
      SignUpSchema = CorleyCollection::Validation.Form do
        configure do
          config.type_specs = false

          def email_unique?(value)
            relations[:contributors].unique?(email: value)
          end
        end

        required(:first_name).filled
        required(:last_name).maybe(:str?)
        required(:email).filled(:email?, :email_unique?)
        required(:password).filled(min_size?: 8).confirmation
        required(:accept_terms).value(:bool?, :true?)
      end
    end
  end
end
