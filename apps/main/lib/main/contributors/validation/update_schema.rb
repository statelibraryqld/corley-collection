# auto_register: false

require "corley_collection/validation/form"

module Main
  module Contributors
    module Validation
      UpdateSchema = CorleyCollection::Validation.Form do
        configure do
          config.type_specs = false

          option :contributor_id

          def email_unique?(value)
            relations[:contributors].exclude(id: contributor_id).unique?(email: value)
          end
        end

        required(:first_name).filled
        required(:last_name).maybe(:str?)
        required(:email).filled(:email?, :email_unique?)
        required(:password).maybe(min_size?: 8).confirmation
      end
    end
  end
end
