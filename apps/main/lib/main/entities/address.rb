# auto_register: false

require "rom/struct"

module Main
  module Entities
    class Address < ROM::Struct
      def to_h
        {
          streetAddress: street_address,
          geoLocation: geo_location_attributes
        }
      end

      def geo_location_attributes
        if geo_location?
          {latitude: latitude, longitude: longitude}
        end
      end

      def geo_location?
        !latitude.nil? && !longitude.nil?
      end
    end
  end
end
