# auto_register: false

require "corley_collection/entities/locality"

module Main
  module Entities
    class Locality < CorleyCollection::Entities::Locality
      def to_h
        {
          id: id,
          name: name,
          geoLocation: geo_location_attributes
        }
      end

      def geo_location_attributes
        if geo_location?
          {latitude: latitude, longitude: longitude}
        end
      end
    end
  end
end
