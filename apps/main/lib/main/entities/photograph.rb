# auto_register: false

require "corley_collection/entities/photograph"

module Main
  module Entities
    class Photograph < CorleyCollection::Entities::Photograph
      def address?
        street_address? || locality?
      end

      def locality?
        !locality.nil?
      end

      def address_attributes
        if address?
          {
            streetNumber: street_number,
            streetName: street_name,
            streetType: street_type,
            postcode: postcode,
            streetAddress: street_address,
            localityName: locality&.name
          }
        end
      end

      def geo_location_attributes
        if geo_location?
          {latitude: latitude, longitude: longitude}
        end
      end

      def human_tagged?
        !human_tagged_at.nil?
      end

      def tag_collection
        @tag_collection ||=
          tags.group_by { |tag| tag.type }.inject({}) do |tag_collection, (type, tags)|
            tag_collection.tap { |collection|
              collection[type] = tags.map { |tag| {id: tag.id, name: tag.name} }
            }
          end
      end
    end
  end
end
