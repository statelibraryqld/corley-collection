# auto_register: false

require "rom/struct"

module Main
  module Entities
    class Tag < ROM::Struct
      def to_h
        {id: id, name: name, type: type}
      end
    end
  end
end
