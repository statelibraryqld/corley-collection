require "main/repository"

module Main
  class FavouriteRepo < Repository[:favourites]
    commands :create, delete: :by_pk

    def for_contributor_id(id)
      favourites.combine(photograph: [:spool, :locality]).by_contributor_id(id).order { created_at.desc }
    end

    def by_photograph_and_contributor_id!(photograph_id, contributor_id)
      favourites.by_photograph_and_contributor_id(photograph_id, contributor_id).one!
    end

    def find_or_create(attrs)
      return create(attrs) if attrs[:contributor_id].nil?

      existing_favourite =
        favourites.by_photograph_and_contributor_id(attrs[:photograph_id], attrs[:contributor_id]).one

      if existing_favourite
        existing_favourite
      else
        create(attrs)
      end
    end
  end
end
