require "corley_collection/operation"
require "main/favourites/validation/schema"
require "main/import"

module Main
  module Favourites
    module Operations
      class Create < CorleyCollection::Operation
        include Import["favourite_repo", "photograph_repo"]

        def call(params)
          validation = Validation::Schema.(params)

          if validation.success?
            photograph = photograph_repo.by_slq_identifier!(validation[:slq_photograph_identifier])

            attrs = {
              photograph_id: photograph.id,
              contributor_id: validation[:contributor_id],
              set_names: (validation.to_h[:set_names] || [])
            }

            favourite = favourite_repo.find_or_create(attrs)

            Success(favourite)
          else
            Failure(validation)
          end
        end
      end
    end
  end
end
