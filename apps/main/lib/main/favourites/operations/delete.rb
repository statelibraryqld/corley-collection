require "corley_collection/operation"
require "main/favourites/validation/delete_schema"
require "main/import"

module Main
  module Favourites
    module Operations
      class Delete < CorleyCollection::Operation
        include Import["favourite_repo", "photograph_repo"]

        def call(params)
          validation = Validation::DeleteSchema.(params)

          if validation.success?
            photograph = photograph_repo.by_slq_identifier!(validation[:slq_photograph_identifier])

            favourite = favourite_repo.by_photograph_and_contributor_id!(photograph.id, validation[:contributor_id])

            favourite_repo.delete(favourite.id)

            Success(favourite)
          else
            Failure(validation)
          end
        end
      end
    end
  end
end
