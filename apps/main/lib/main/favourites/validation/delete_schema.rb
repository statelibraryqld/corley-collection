# auto_register: false

require "corley_collection/validation/form"

module Main
  module Favourites
    module Validation
      DeleteSchema = CorleyCollection::Validation.Form do
        configure do
          def photograph_exists?(identifier)
            relations[:photographs].where(slq_identifier: identifier).exist?
          end
        end

        required(:slq_photograph_identifier).filled(:photograph_exists?)
        required(:contributor_id).filled(:int?)
      end
    end
  end
end
