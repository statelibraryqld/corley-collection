# auto_register: false

require "corley_collection/validation/form"

module Main
  module Favourites
    module Validation
      Schema = CorleyCollection::Validation.Form do
        configure do
          def photograph_exists?(identifier)
            relations[:photographs].where(slq_identifier: identifier).exist?
          end
        end

        required(:slq_photograph_identifier).filled(:photograph_exists?)
        required(:contributor_id).maybe(:int?)
        optional(:set_names).maybe(:array?).each(:str?)
      end
    end
  end
end
