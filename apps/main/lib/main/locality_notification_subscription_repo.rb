require "main/repository"
require "main/locality_notification_subscriptions/changesets/create"

module Main
  class LocalityNotificationSubscriptionRepo < Repository[:locality_notification_subscriptions]
    commands delete: :by_pk

    def by_token(token)
      locality_notification_subscriptions.by_token(token).one
    end

    def for_locality_and_contributor(locality_id, contributor_id)
      locality_notification_subscriptions
        .by_locality_and_contributor(locality_id, contributor_id)
        .one
    end

    def for_contributor(contributor_id)
      locality_notification_subscriptions.by_contributor_id(contributor_id).to_a
    end

    def find_or_create(locality_id, contributor_id)
      subscription = for_locality_and_contributor(locality_id, contributor_id)

      if subscription
        subscription
      else
        create(locality_id: locality_id, contributor_id: contributor_id)
      end
    end

    def create(params)
      locality_notification_subscriptions
        .changeset(Main::LocalityNotificationSubscriptions::Changesets::Create, params)
        .commit
    end

    def delete_by_contributor_id(id)
      locality_notification_subscriptions.by_contributor_id(id).delete
    end
  end
end
