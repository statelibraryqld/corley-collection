require "corley_collection/operation"
require "main/import"

module Main
  module LocalityNotificationSubscriptions
    module Operations
      class CreateForPhotograph < CorleyCollection::Operation
        include Import[
          "locality_notification_subscription_repo",
          "photograph_repo",
          "locality_repo"
        ]

        def call(contributor_id, photograph_id)
          photograph = photograph_repo.get!(photograph_id)

          localities =
            if photograph.locality
              Array(photograph.locality)
            elsif photograph.spool_id
              locality_repo.for_spool(photograph.spool_id)
            else
              []
            end

          locality_notification_subscriptions = localities.map do |locality|
            locality_notification_subscription_repo.find_or_create(locality.id, contributor_id)
          end

          Success(locality_notification_subscriptions)
        end
      end
    end
  end
end
