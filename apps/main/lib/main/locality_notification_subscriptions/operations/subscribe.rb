require "corley_collection/operation"
require "main/import"
require_relative "../validation/subscribe_schema"

module Main
  module LocalityNotificationSubscriptions
    module Operations
      class Subscribe < CorleyCollection::Operation
        include Import[
          "locality_repo",
          "locality_notification_subscription_repo",
        ]

        def call(contributor_id, locality_id, params)
          locality = locality_repo.by_id!(locality_id)

          validation = Validation::SubscribeSchema.(params)

          if validation.success?
            subscription = locality_notification_subscription_repo.find_or_create(locality.id, contributor_id)

            Success(subscription)
          else
            Failure(validation)
          end
        end
      end
    end
  end
end
