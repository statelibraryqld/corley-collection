require "corley_collection/operation"
require "main/import"
require_relative "../validation/unsubscribe_schema"

module Main
  module LocalityNotificationSubscriptions
    module Operations
      class Unsubscribe < CorleyCollection::Operation
        include Import["locality_notification_subscription_repo"]

        def call(params)
          validation = Validation::UnsubscribeSchema.(params)

          if validation.success?
            subscription = locality_notification_subscription_repo.by_token(validation[:token])

            if subscription
              locality_notification_subscription_repo.delete(subscription.id)
              Success(subscription)
            else
              Failure(:subscription_not_found)
            end
          else
            Failure(validation)
          end
        end
      end
    end
  end
end
