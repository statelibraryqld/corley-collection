require "corley_collection/operation"
require "main/import"
require_relative "../validation/unsubscribe_contributor_schema"

module Main
  module LocalityNotificationSubscriptions
    module Operations
      class UnsubscribeContributor < CorleyCollection::Operation
        include Import["contributor_repo", "locality_notification_subscription_repo"]

        def call(params)
          validation = Validation::UnsubscribeContributorSchema.(params)

          if validation.success?
            contributor = contributor_repo.by_public_token(validation[:public_token])

            if contributor
              locality_notification_subscription_repo.delete_by_contributor_id(contributor.id)

              Success(contributor)
            else
              Failure(:contributor_not_found)
            end
          else
            Failure(validation)
          end
        end
      end
    end
  end
end
