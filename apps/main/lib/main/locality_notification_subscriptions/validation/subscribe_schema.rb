# auto_register: false

require "corley_collection/validation/form"

module Main
  module LocalityNotificationSubscriptions
    module Validation
      SubscribeSchema = CorleyCollection::Validation.Form do
        required(:locality_notifications).value(:bool?, :true?)
      end
    end
  end
end
