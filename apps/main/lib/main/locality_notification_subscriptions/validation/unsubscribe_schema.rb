# auto_register: false

require "corley_collection/validation/form"

module Main
  module LocalityNotificationSubscriptions
    module Validation
      UnsubscribeSchema = CorleyCollection::Validation.Form do
        configure do
          config.type_specs = false
        end

        required(:token).filled
      end
    end
  end
end
