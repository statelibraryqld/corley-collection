require "corley_collection/operation"
require "main/import"
require "time_math"

module Main
  module LocalityNotifications
    module Operations
      class Enqueue < CorleyCollection::Operation
        include Import[
          "contributor_repo",
          "locality_repo",
          "locality_notifications.operations.send"
        ]

        def call
          time = Time.now

          period_start = TimeMath.week.decrease(time)
          period_end = time

          most_active_localities = locality_repo.recently_active

          contributor_repo.subscribed_to_localities.each { |contributor|
            send.enqueue(
              contributor_id: contributor.id,
              period_start: period_start,
              period_end: period_end,
              most_active_localities: most_active_localities.map { |active_locality|
                {locality_name: active_locality.locality.name, story_count: active_locality.story_count}
              }
            )
          }
        end
      end
    end
  end
end
