require "corley_collection/operation"
require "main/import"

module Main
  module LocalityNotifications
    module Operations
      class Send < CorleyCollection::Operation
        include Import[
          "contributor_repo",
          "story_repo",
          "photograph_repo",
          "locality_notification_subscription_repo",
          "mail.main.weekly_locality_digest"
        ]

        def call(contributor, period_start, period_end, most_active_localities = [])
          active_localities = []

          stories = story_repo.created_between(period_start, period_end)

          # Find active localities - ones with either a new story or geolocation
          contributor.localities.each do |locality|
            locality_stories = stories.select { |story| story.photograph.locality&.id == locality.id }

            geo_located_photographs =
              photograph_repo.recently_geo_located_in_locality(
                locality.id,
                period_start: period_start,
                period_end: period_end
              )

            next unless locality_stories.any? || geo_located_photographs.any?

            active_localities << {
              locality: locality,
              stories: locality_stories,
              geo_located_photographs: geo_located_photographs
            }
          end

          unless active_localities.none? && most_active_localities.none?
            weekly_locality_digest.deliver(
              contributor: contributor,
              active_localities: active_localities,
              most_active_localities: most_active_localities,
              subscriptions: locality_notification_subscription_repo.for_contributor(contributor.id)
            )
          end
        end

        private

        def to_queue(input)
          [input]
        end

        def call_from_queue(input)
          contributor = contributor_repo.with_localities(input[:contributor_id])

          if contributor
            call(
              contributor,
              Time.parse(input[:period_start]),
              Time.parse(input[:period_end]),
              input[:most_active_localities]
            )
          end
        end
      end
    end
  end
end
