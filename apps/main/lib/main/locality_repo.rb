require "main/repository"

module Main
  class LocalityRepo < Repository[:localities]
    commands :create

    def index
      localities.combine(spools: :spools_metadata).order(:name)
    end

    def by_pk(id)
      localities.by_pk(id)
    end

    def by_id!(id)
      localities.by_pk(id).one!
    end

    def for_spool(id)
      localities.join(:localities_spools).where(spool_id: id).to_a
    end

    def find_or_create_by_name(name)
      locality = localities.by_lower_name(name).one

      if locality
        locality
      else
        create(name: name)
      end
    end

    def recently_active(limit: 3)
      recent_active_localities
        .combine(:locality)
        .limit(limit)
        .to_a
    end
  end
end
