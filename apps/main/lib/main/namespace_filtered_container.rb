# auto_register: false

module Main
  class NamespaceFilteredContainer
    attr_reader :container
    attr_reader :namespace

    def initialize(container, namespace)
      @container = container
      @namespace = namespace
    end

    def resolve(key)
      container[resolve_key(key)]
    end

    alias_method :[], :resolve

    def key?(key)
      container.key?(resolve_key(key))
    end

    private

    def resolve_key(key)
      "#{namespace}.#{key}"
    end
  end
end
