require "main/repository"
require "main/photograph_notification_subscriptions/changesets/create"

module Main
  class PhotographNotificationSubscriptionRepo < Repository[:photograph_notification_subscriptions]
    commands delete: :by_pk

    def by_photograph_id(id)
      photograph_notification_subscriptions.by_photograph_id(id)
    end

    def by_token(token)
      photograph_notification_subscriptions.by_token(token).one
    end

    def for_photograph_and_contributor(photograph_id, contributor_id)
      photograph_notification_subscriptions
        .by_photograph_and_contributor(photograph_id, contributor_id)
        .one
    end

    def find_or_create(photograph_id, contributor_id)
      subscription = for_photograph_and_contributor(photograph_id, contributor_id)

      if subscription
        subscription
      else
        create(photograph_id: photograph_id, contributor_id: contributor_id)
      end
    end

    def create(params)
      photograph_notification_subscriptions
        .changeset(Main::PhotographNotificationSubscriptions::Changesets::Create, params)
        .commit
    end

    def delete_by_contributor_id(id)
      photograph_notification_subscriptions.by_contributor_id(id).delete
    end
  end
end
