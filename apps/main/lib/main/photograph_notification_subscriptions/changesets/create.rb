require "rom/changeset/create"
require "securerandom"

module Main
  module PhotographNotificationSubscriptions
    module Changesets
      class Create < ROM::Changeset::Create
        map do |tuple|
          if tuple[:token]
            tuple
          else
            tuple.merge(token: SecureRandom.hex)
          end
        end
      end
    end
  end
end
