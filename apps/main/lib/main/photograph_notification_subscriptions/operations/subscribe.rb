require "corley_collection/operation"
require "main/import"
require_relative "../validation/subscribe_schema"

module Main
  module PhotographNotificationSubscriptions
    module Operations
      class Subscribe < CorleyCollection::Operation
        include Import[
          "photograph_repo",
          "photograph_notification_subscription_repo",
          create_locality_subscriptions: "locality_notification_subscriptions.operations.create_for_photograph"
        ]

        def call(contributor_id, photograph_slq_identifier, params)
          photograph = photograph_repo.by_slq_identifier!(photograph_slq_identifier)

          validation = Validation::SubscribeSchema.(params)

          if validation.success?
            if validation.to_h[:photograph_notifications]
              photograph_notification_subscription_repo.find_or_create(photograph.id, contributor_id)
            end

            if validation.to_h[:locality_notifications]
              create_locality_subscriptions.(contributor_id, photograph.id)
            end

            Success(photograph)
          else
            Failure(validation)
          end
        end
      end
    end
  end
end
