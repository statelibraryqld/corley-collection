require "corley_collection/operation"
require "main/import"
require_relative "../validation/unsubscribe_contributor_schema"

module Main
  module PhotographNotificationSubscriptions
    module Operations
      class UnsubscribeContributor < CorleyCollection::Operation
        include Import["contributor_repo", "photograph_notification_subscription_repo"]

        def call(params)
          validation = Validation::UnsubscribeContributorSchema.(params)

          if validation.success?
            contributor = contributor_repo.by_public_token(validation[:public_token])

            if contributor
              photograph_notification_subscription_repo.delete_by_contributor_id(contributor.id)

              Success(contributor)
            else
              Failure(:contributor_not_found)
            end
          else
            Failure(validation)
          end
        end
      end
    end
  end
end
