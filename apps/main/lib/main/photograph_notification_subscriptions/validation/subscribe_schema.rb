# auto_register: false

require "corley_collection/validation/form"

module Main
  module PhotographNotificationSubscriptions
    module Validation
      SubscribeSchema = CorleyCollection::Validation.Form do
        optional(:photograph_notifications).value(:bool?)
        optional(:locality_notifications).value(:bool?)

        rule(photograph_or_locality: [:photograph_notifications, :locality_notifications]) do |photograph_notifications, locality_notifications|
          photograph_notifications.true? | locality_notifications.true?
        end
      end
    end
  end
end
