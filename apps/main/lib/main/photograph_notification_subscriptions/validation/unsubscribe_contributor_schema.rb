# auto_register: false

require "corley_collection/validation/form"

module Main
  module PhotographNotificationSubscriptions
    module Validation
      UnsubscribeContributorSchema = CorleyCollection::Validation.Form do
        configure do
          config.type_specs = false
        end

        required(:public_token).filled
      end
    end
  end
end
