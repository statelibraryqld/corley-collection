require "main/repository"
require "time_math"

module Main
  class PhotographRepo < Repository[:photographs]
    commands update: :by_pk

    def get!(id)
      photographs.combine(:spool, :locality).by_pk(id).one!
    end

    def by_ids_with_associations(ids)
      photographs
        .combine(:spool, :locality, :tags, stories: [:contributor, :contributor_images])
        .node(:tags) { |tags_relation| tags_relation.public }
        .node(:stories) { |stories_relation| stories_relation.published }
        .by_pk(ids)
        .order_by_id_values(ids)
    end

    def by_slq_identifier!(slq_identifier)
      photographs
        .combine(:spool, :locality, :tags, stories: :contributor)
        .node(:tags) { |tags_relation| tags_relation.human }
        .node(:stories) { |stories_relation| stories_relation.published }
        .by_slq_identifier(slq_identifier)
        .one!
    end

    def append_tag_ids(id, tag_ids)
      transaction do
        missing_taggings = tag_ids - photographs_tags.where(photograph_id: id, tag_id: tag_ids).pluck(:tag_id)

        tuples = missing_taggings.map { |tag_id|
          {photograph_id: id, tag_id: tag_id}
        }

        photographs_tags.changeset(:create, tuples).commit if tuples.any?
      end

      photographs.combine(:tags).by_pk(id).one
    end

    def mark_as_human_tagged(id)
      photographs.by_pk(id).update(human_tagged_at: Time.now)
    end

    def mark_as_geo_located(id)
      photographs.by_pk(id).update(geo_located_at: Time.now)
    end

    def mark_as_storied(id)
      photographs.by_pk(id).update(storied_at: Time.now)
    end

    def random_untagged
      untagged_photograph = photographs.random_untagged.limit(1).one
      by_slq_identifier!(untagged_photograph.slq_identifier) if untagged_photograph
    end

    def tagged_count
      photographs.human_tagged.count
    end

    def untagged_count
      photographs.human_untagged.count
    end

    def unlocated?(slq_identifier)
      photographs.by_slq_identifier(slq_identifier).one!.unlocated?
    end

    def recently_geo_located_in_locality(locality_id, period_start: TimeMath.week.decrease(Time.now), period_end: Time.now)
      photographs
        .by_locality_id(locality_id)
        .geo_located_between(period_start, period_end)
        .to_a
    end
  end
end
