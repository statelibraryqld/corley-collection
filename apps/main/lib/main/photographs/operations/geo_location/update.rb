require "corley_collection/operation"
require "main/photographs/validation/geo_location_schema"
require "main/import"

module Main
  module Photographs
    module Operations
      module GeoLocation
        class Update < CorleyCollection::Operation
          include Import[
            "photograph_repo",
            "locality_repo",
            "application_events"
          ]

          def call(photograph_slq_identifier, params)
            validation = Validation::GeoLocationSchema
              .with(photograph_repo: photograph_repo)
              .(params.merge(photograph_slq_identifier: photograph_slq_identifier))

            if validation.success?
              photograph = photograph_repo.by_slq_identifier!(photograph_slq_identifier)

              locality = locality_repo.find_or_create_by_name(validation[:locality_name])

              attrs = {
                geo_located_at: Time.now,
                latitude: validation[:latitude],
                locality_id: locality.id,
                longitude: validation[:longitude],
                postcode: validation[:postcode],
                street_address: validation[:street_address],
                street_name: validation[:street_name],
                street_number: validation[:street_number],
                street_type: validation[:street_type]
              }

              photograph_repo.update(photograph.id, attrs)

              application_events.publish("photograph.updated", photograph: photograph)

              Success(photograph)
            else
              Failure(validation)
            end
          end
        end
      end
    end
  end
end
