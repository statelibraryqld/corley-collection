require "corley_collection/operation"
require "main/photographs/validation/query_schema"

module Main
  module Photographs
    module Operations
      class Query < CorleyCollection::Operation
        def call(params)
          validation = Validation::QuerySchema.(params)

          if validation.success?
            Success(validation.to_h)
          else
            Failure(validation)
          end
        end
      end
    end
  end
end
