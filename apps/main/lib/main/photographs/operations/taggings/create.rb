require "corley_collection/operation"
require "main/photographs/validation/taggings_schema"
require "main/import"

module Main
  module Photographs
    module Operations
      module Taggings
        class Create < CorleyCollection::Operation
          include Import[
            "photograph_repo",
            "tag_repo",
            "application_events"
          ]

          def call(photograph_slq_identifier, params)
            validation = Validation::TaggingsSchema.(params)

            if validation.success?
              photograph = photograph_repo.by_slq_identifier!(photograph_slq_identifier)

              # We first find or create any contributor tags, then append
              # those tags and any regular tags to the photograph
              if validation.output[:contributor_tags].to_a.any?
                tag_attrs = validation[:contributor_tags].to_a.map { |tag_name|
                  {name: tag_name, type: "Contributor", source: "human"}
                }

                contributor_tags = tag_repo.find_or_create(tag_attrs)
              end

              tag_ids = contributor_tags.to_a.map(&:id).concat(validation.output[:tag_ids].to_a).uniq

              photograph = photograph_repo.append_tag_ids(photograph.id, tag_ids)

              photograph_repo.mark_as_human_tagged(photograph.id)

              application_events.publish("photograph.updated", photograph: photograph)

              Success(photograph)
            else
              Failure(validation)
            end
          end
        end
      end
    end
  end
end
