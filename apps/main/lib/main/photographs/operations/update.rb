require "corley_collection/operation"
require "main/import"
require "main/photographs/validation/schema"

module Main
  module Photographs
    module Operations
      class Update < CorleyCollection::Operation
        include Import[
          "photograph_repo",
          "application_events"
        ]

        def call(photograph_slq_identifier, params)
          validation = Validation::Schema.(params)

          if validation.success?
            photograph = photograph_repo.by_slq_identifier!(photograph_slq_identifier)

            photograph = photograph_repo.update(photograph.id, validation.to_h)

            application_events.publish("photograph.updated", photograph: photograph)

            Success(photograph)
          else
            Failure(validation)
          end
        end
      end
    end
  end
end
