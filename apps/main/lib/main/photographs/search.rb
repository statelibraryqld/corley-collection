require "main/import"
require_relative "search_results"

module Main
  module Photographs
    class Search
      include Import[
        "photograph_repo",
        es: "search.es.rom"
      ]

      # Accepts a Main::Photographs::SearchQuery object
      def call(search_query)
        es_query = es_relation.for_index(
          query: search_query[:query],
          tag_ids: search_query[:tag_ids],
          locality_ids: search_query[:locality_ids],
          slq_identifiers: search_query[:slq_identifiers],
          spool_slq_identifiers: search_query[:spool_slq_identifiers],
          human_tagged: search_query[:human_tagged],
          storied: search_query[:storied],
          has_geo_location: search_query[:has_geo_location],
          is_shop: search_query[:is_shop],
          is_damaged: search_query[:is_damaged],
          has_house_name: search_query[:has_house_name],
          has_annotations: search_query[:has_annotations],
          order_by: search_query[:order_by],
          order_by_direction: search_query[:order_by_direction]
        )

        es_results =
          es_query
            .page(search_query[:page] || 1)
            .per_page(search_query[:per_page] || 20)
            .call

        photograph_ids = es_results.pluck(:id)

        db_results = photograph_repo.by_ids_with_associations(photograph_ids)

        SearchResults.new(es_results, db_results || [])
      end

      def es_relation
        es.relations[:photographs]
      end
    end
  end
end
