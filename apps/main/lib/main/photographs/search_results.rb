# auto_register: false

require "search/results"

module Main
  module Photographs
    class SearchResults < Search::Results
      def photographs
        to_a
      end

      def aggregations
        source.response["aggregations"]
      end
    end
  end
end
