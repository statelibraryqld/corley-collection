require "main/import"
require_relative "search_results"

module Main
  module Photographs
    class TagCloud
      include Import[
        "photograph_repo",
        "tag_repo",
        es: "search.es.rom"
      ]

      # This fetches tag_ids with photograph counts from Elasticsearch,
      # zips them together with their tag from rom, then groups the tags
      # by their type to produce the 'tag cloud'.
      def call
        es_result = es_relation.tag_cloud.call
        tags = tag_repo.public_by_id.to_a

        es_buckets = es_result.response["aggregations"]["tag_ids"]["buckets"]

        results = es_buckets.map { |bucket|
          {
            tag: tags.bsearch { |t| bucket["key"] <=> t.id }&.to_h,
            count: bucket["doc_count"]
          }
        }.reject { |result| result[:tag].nil? }

        results_by_tag_type = results.group_by { |item| item[:tag][:type] }

        {
          cloud: results_by_tag_type,
          tagged_count: photograph_repo.tagged_count,
          untagged_count: photograph_repo.untagged_count
        }
      end

      def es_relation
        es.relations[:photographs]
      end
    end
  end
end
