# auto_register: false

require "corley_collection/validation/form"
require "types"

module Main
  module Photographs
    module Validation
      GeoLocationSchema = CorleyCollection::Validation.Form do
        configure do
          option :photograph_repo

          config.namespace = :photographs

          def unlocated?(photograph_slq_identifier)
            photograph_repo.unlocated?(photograph_slq_identifier)
          end
        end

        required(:photograph_slq_identifier, :string).filled(:unlocated?)
        required(:street_number, :string).filled
        required(:street_name, :string).filled
        required(:street_type, :string).filled
        required(:postcode, :string).filled
        required(:locality_name, Types::CapitalizedLocalityName).filled
        required(:street_address, :string).filled
        required(:latitude, :float).value(:float?)
        required(:longitude, :float).value(:float?)
        required(:state, :string).value(eql?: "QLD")
      end
    end
  end
end
