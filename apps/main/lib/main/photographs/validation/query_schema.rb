# auto_register: false

require "corley_collection/validation/form"

module Main
  module Photographs
    module Validation
      QuerySchema = CorleyCollection::Validation.Form do
        optional(:query).maybe(:str?)
        optional(:page).maybe(:int?)
        optional(:per_page).maybe(:int?, lteq?: 100)
        optional(:tag_ids).each(:int?)
        optional(:locality_ids).each(:int?)
        optional(:slq_identifiers).each(:str?)
        optional(:spool_slq_identifiers).each(:str?)
        optional(:human_tagged).maybe(:bool?)
        optional(:storied).maybe(:bool?)
        optional(:has_geo_location).maybe(:bool?)
        optional(:is_damaged).maybe(:bool?)
        optional(:is_shop).maybe(:bool?)
        optional(:has_annotations).maybe(:bool?)
        optional(:has_house_name).maybe(:bool?)
        optional(:order_by).maybe(included_in?: %w(human_tagged_at storied_at geo_located_at))
        optional(:order_by_direction).maybe(included_in?: %w(asc desc))
      end
    end
  end
end
