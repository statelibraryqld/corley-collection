# auto_register: false

require "corley_collection/validation/form"

module Main
  module Photographs
    module Validation
      Schema = CorleyCollection::Validation.Form do
        optional(:shop).maybe(:bool?)
        optional(:damaged).maybe(:bool?)
        optional(:house_name).maybe(:str?)
        optional(:annotations).maybe(:str?)
      end
    end
  end
end
