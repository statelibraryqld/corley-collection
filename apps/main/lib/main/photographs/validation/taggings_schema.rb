# auto_register: false

require "corley_collection/validation/form"
require "types"

module Main
  module Photographs
    module Validation
      TaggingsSchema = CorleyCollection::Validation.Form do
        optional(:tag_ids, [:integer]).each(:int?)
        optional(:contributor_tags, Types::CommaSeparatedArray).each(:str?)

        rule(tag_ids_or_contributor_tags_presence: [:tag_ids, :contributor_tags]) do |tag_ids, contributor_tags|
          tag_ids.filled? | contributor_tags.filled?
        end

        validate(tags_exist: :tag_ids) do |tag_ids|
          relations[:tags].where(id: tag_ids).count == tag_ids.to_a.size
        end
      end
    end
  end
end
