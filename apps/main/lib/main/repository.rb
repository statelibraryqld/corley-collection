require "corley_collection/repository"
require "main/entities"

module Main
  class Repository < CorleyCollection::Repository
    struct_namespace Main::Entities
  end
end
