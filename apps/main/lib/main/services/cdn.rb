# auto_register: false

require "main/import"
require "main/container"

module Main
  module Services
    class Cdn
      include Import["application_events"]

      def self.update_cdn(opts)
        opts.each do |entity_name, entity|
          define_method("on_#{entity}_updated") do |event|
            purge(entity_name, event[entity])
          end
        end
      end

      update_cdn photographs: :photograph

      def purge(entity_name, entity)
        Container["cdn.#{entity_name}.operations.purge"].(entity)
      end

      def start
        return self if @started
        application_events.subscribe(self)
        @started = true
        self
      end

      def stop
        application_events.unsubscribe(self)
        @started = false
        self
      end
    end
  end
end
