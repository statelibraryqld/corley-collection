# auto_register: false

require "main/import"
require "main/container"

module Main
  module Services
    class ContributorNotifier
      include Import[
        "application_events",
        "stories.operations.notify_contributors"
      ]

      def on_story_created(event)
        notify_contributors.enqueue(event[:story].id)
      end

      def start
        return self if @started
        application_events.subscribe(self)
        @started = true
        self
      end

      def stop
        application_events.unsubscribe(self)
        @started = false
        self
      end
    end
  end
end
