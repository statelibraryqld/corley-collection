require "main/repository"

module Main
  class SpoolRepo < Repository[:spools]
    def index
      spools.with_photograph_slq_identifiers.combine(:localities)
    end

    def by_slq_identifier!(slq_identifier)
      spools
        .combine(:localities, :addresses, photographs: [:locality, :stories])
        .node(:photographs) { |photographs_relation|
          photographs_relation
            .order(:id)
            .node(:stories) { |story_relation|
              story_relation.select(:id, :photograph_id)
            }
        }
        .by_slq_identifier(slq_identifier)
        .one!
    end
  end
end
