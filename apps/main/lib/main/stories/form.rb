require "types"
require "main/view/form"
require "main/import"

module Main
  module Stories
    class Form < Main::View::Form
      prefix :story

      define do
        text_area :body,
          label: "Your story",
          hint: "Tell your story about this house",
          box_size: "large",
          validation: {filled: true}
      end
    end
  end
end
