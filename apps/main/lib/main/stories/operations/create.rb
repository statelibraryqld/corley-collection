require "corley_collection/operation"
require "main/stories/validation/schema"
require "main/import"

module Main
  module Stories
    module Operations
      class Create < CorleyCollection::Operation
        include Import[
          "story_repo",
          "photograph_repo",
          "photograph_notification_subscription_repo",
          "application_events",
          "core.settings",
          create_locality_subscriptions: "locality_notification_subscriptions.operations.create_for_photograph"
        ]

        def call(contributor_id, photograph_slq_identifier, params)
          photograph = photograph_repo.by_slq_identifier!(photograph_slq_identifier)

          story_params =
            params.merge(
              contributor_id: contributor_id,
              photograph_id: photograph.id,
              published: settings.immediate_story_publication
            )

          validation = Validation::Schema.(story_params)

          if validation.success?
            story = story_repo.create(validation.to_h)

            if validation.to_h[:photograph_notifications]
              photograph_notification_subscription_repo.find_or_create(photograph.id, contributor_id)
            end

            if validation.to_h[:locality_notifications]
              create_locality_subscriptions.(contributor_id, photograph.id)
            end

            photograph_repo.mark_as_storied(photograph.id)

            application_events.publish("story.created", story: story)
            application_events.publish("photograph.updated", photograph: photograph)

            Success(story)
          else
            Failure(validation)
          end
        end
      end
    end
  end
end
