require "corley_collection/operation"
require "main/import"

module Main
  module Stories
    module Operations
      class NotifyContributors < CorleyCollection::Operation
        include Import[
          "story_repo",
          "photograph_notification_subscription_repo",
          "mail.main.new_story_notification"
        ]

        def call(story_id)
          story = story_repo.get!(story_id)

          subscriptions = photograph_notification_subscription_repo.by_photograph_id(story.photograph_id)

          subscriptions.each do |subscription|
            next if story.contributor_id == subscription.contributor_id

            new_story_notification.enqueue(
              contributor_id: subscription.contributor_id,
              story_id: story_id,
              photograph_notification_subscription_id: subscription.id
            )
          end

          Success(story)
        end

        def to_queue(story_id)
          [story_id]
        end

        def call_from_queue(story_id)
          call(story_id)
        end
      end
    end
  end
end
