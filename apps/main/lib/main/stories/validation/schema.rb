# auto_register: false

require "corley_collection/validation/form"

module Main
  module Stories
    module Validation
      Schema = CorleyCollection::Validation.Form do
        required(:contributor_id).value(:int?)
        required(:photograph_id).value(:int?)
        required(:body).maybe(:str?)
        required(:published).value(:bool?)
        optional(:contributor_images).each do
          required(:path).filled
          required(:size).value(:int?)
          required(:content_type).value(included_in?: %[image/jpeg image/png])
          required(:caption).filled
        end
        optional(:photograph_notifications).value(:bool?)
        optional(:locality_notifications).value(:bool?)
      end
    end
  end
end
