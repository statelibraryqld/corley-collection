require "main/repository"

module Main
  class StoryRepo < Repository[:stories]
    def get!(id)
      stories.combine(:contributor, :contributor_images).by_pk(id).one!
    end

    def for_photograph_id(id)
      stories
        .combine(:contributor, :contributor_images)
        .node(:contributor_images) { |rel| rel.where { published.is(true) } }
        .published
        .by_photograph_id(id)
        .order(:created_at)
    end

    def created_between(start_time, end_time)
      stories
        .combine(:contributor, photograph: :locality)
        .created_between(start_time, end_time)
        .to_a
    end

    def create(attrs)
      transaction do
        story = stories.changeset(:create, attrs).commit

        if attrs.key?(:contributor_images)
          attrs[:contributor_images].each do |contributor_image_attrs|
            contributor_images
              .changeset(
                :create,
                contributor_image_attrs.merge(
                  contributor_id: attrs[:contributor_id]
                )
              )
              .associate(story)
              .commit
          end
        end

        story
      end
    end
  end
end
