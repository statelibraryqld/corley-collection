require "main/repository"
require "main/container"

module Main
  class TagRepo < Repository[:tags]
    commands :create

    def index
      tags.public.order(:type)
    end

    def public_by_id
      tags.public.order(:id)
    end

    def tag_types
      tags.public.for_tag_types.to_a.map { |tag| tag[:type] }
    end

    def by_pk(id)
      tags.by_pk(id)
    end

    # Creat a tag and emit an event
    def create_with_event(attrs)
      tag = create(attrs)

      Main::Container["application_events"].publish("tag.created", tag: tag)

      tag
    end

    def find_or_create(attrs) # rubocop:disable Metrics/PerceivedComplexity
      if attrs.is_a?(Array)
        result = []

        attrs.each do |tag_attrs|
          tag =
            if tags.exist?(tag_attrs)
              tags.where(tag_attrs).one
            else
              create_with_event(tag_attrs)
            end

          result << tag
        end
      elsif attrs.is_a?(Hash)
        result =
          if tags.exist?(attrs)
            tags.where(attrs).one
          else
            create_with_event(attrs)
          end
      end

      result
    end
  end
end
