# auto_register: false

require "corley_collection/view/context"
require "main/view/parts/helpers/context"
require "main/import"

module Main
  module View
    class Context < CorleyCollection::View::Context
      include Parts::Helpers::Context

      include Import["core.markdown_renderer", "thumbor"]

      def persisted_flash
        self[:persisted_flash]
      end
    end
  end
end
