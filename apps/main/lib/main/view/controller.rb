# auto_register: false

require "slim"
require "yajl"
require "dry/view/controller"

require "main/container"

module Main
  module View
    class Controller < Dry::View::Controller
      configure do |config|
        config.paths = [Container.root.join("web/templates")]
        config.context = Container["view.context"]
        config.decorator = Container["view.decorator"]
        config.layout = "application"
      end
    end
  end
end
