# auto_register: false

require "json"
require "dry/view/part"
require "addressable/uri"

module Main
  module View
    module Parts
      class ContributorImage < Dry::View::Part
        def url
          Addressable::URI.escape(path)
        end

        def thumbnail_url
          context.thumbor.thumbnail(url)
        end

        def feature_url
          context.thumbor.feature(url)
        end
      end
    end
  end
end
