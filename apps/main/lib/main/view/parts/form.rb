# auto_register: false

require "json"
require "dry/view/part"

module Main
  module View
    module Parts
      class Form < Dry::View::Part
        def to_h
          _value.to_h(csrfToken: _context.csrf_token)
        end

        def to_json
          JSON.generate(to_h)
        end

        def formalist_ast
          render :formalist_ast
        end

        def error_messages
          render :error_messages
        end
      end
    end
  end
end
