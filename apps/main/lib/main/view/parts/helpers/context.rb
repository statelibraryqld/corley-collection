module Main
  module View
    module Parts
      module Helpers
        module Context
          def to_sentence(items, last_word_connector: " and ")
            items = Array(items)

            case items.length
            when 0
              ""
            when 1
              items[0].to_s.dup
            when 2
              "#{items[0]}#{last_word_connector}#{items[1]}"
            else
              "#{items[0...-1].join(', ')}#{last_word_connector}#{items[-1]}"
            end
          end
        end
      end
    end
  end
end
