# auto_register: false

require "dry/view/part"
require_relative "story"

module Main
  module View
    module Parts
      class Photograph < Dry::View::Part
        decorate :stories, as: Story
      end
    end
  end
end
