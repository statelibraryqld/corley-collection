# auto_register: false

require "dry/view/part"
require_relative "contributor_image"

module Main
  module View
    module Parts
      class Story < Dry::View::Part
        decorate :contributor_images, as: ContributorImage
      end
    end
  end
end
