require "main/import"
require "main/view/controller"

module Main
  module Views
    module Api
      module Contributors
        class Current < View::Controller
          include Import["contributor_repo"]

          configure do |config|
            config.template = "api/contributors/current"
          end

          expose :current_contributor do |contributor_id:|
            contributor_repo[contributor_id] if contributor_id
          end
        end
      end
    end
  end
end
