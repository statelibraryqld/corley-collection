require "main/import"
require "main/view/controller"

module Main
  module Views
    module Api
      module Favourites
        class Index < View::Controller
          include Import["favourite_repo"]

          configure do |config|
            config.template = "api/favourites/index"
          end

          expose :favourites do |contributor_id: nil|
            contributor_id ? favourite_repo.for_contributor_id(contributor_id).to_a : []
          end
        end
      end
    end
  end
end
