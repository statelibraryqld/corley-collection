require "main/import"
require "main/view/controller"

module Main
  module Views
    module Api
      module Localities
        class Index < View::Controller
          include Import["locality_repo"]

          configure do |config|
            config.template = "api/localities/index"
          end

          expose :localities do
            locality_repo.index.to_a
          end
        end
      end
    end
  end
end
