require "main/import"
require "main/view/controller"
require "main/photographs/search_query"

module Main
  module Views
    module Api
      module Photographs
        class Index < View::Controller
          include Import["photographs.search", "tag_repo", "locality_repo"]

          configure do |config|
            config.template = "api/photographs/index"
          end

          expose :search_query do |params:|
            Main::Photographs::SearchQuery.new(params)
          end

          expose :photographs do |search_result|
            search_result.photographs
          end

          expose :search_result do |search_query|
            search.(search_query)
          end

          expose :pager do |search_result|
            search_result.pager
          end

          expose :tags_aggregation_buckets do |search_result|
            buckets = search_result.aggregations["tag_ids"]["buckets"]

            tag_ids = buckets.map { |i| i["key"] }
            tags = tag_repo.by_pk(tag_ids).to_a

            buckets.map { |bucket|
              bucket.merge("tag" => tags.detect { |tag| tag.id == bucket["key"] })
            }.reject { |bucket|
              bucket["tag"].nil?
            }
          end

          expose :spools_aggregation_buckets do |search_result|
            search_result.aggregations["spool_slq_identifiers"]["buckets"]
          end

          expose :localities_aggregation_buckets do |search_result|
            buckets = search_result.aggregations["locality_ids"]["buckets"]

            locality_ids = buckets.map { |i| i["key"] }
            localities = locality_repo.by_pk(locality_ids).to_a

            buckets.map { |bucket|
              bucket.merge("locality" => localities.detect { |locality| locality.id == bucket["key"] })
            }.reject { |bucket|
              bucket["locality"].nil?
            }
          end
        end
      end
    end
  end
end
