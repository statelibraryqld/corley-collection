require "main/import"
require "main/view/controller"

module Main
  module Views
    module Api
      module Photographs
        class NextUntagged < View::Controller
          include Import["photograph_repo"]

          configure do |config|
            config.template = "api/photographs/next_untagged"
          end

          expose :photograph do
            photograph_repo.random_untagged
          end

          expose :untagged_count do
            photograph_repo.untagged_count
          end
        end
      end
    end
  end
end
