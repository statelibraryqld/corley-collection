require "main/import"
require "main/view/controller"
require "main/view/parts/story"

module Main
  module Views
    module Api
      module Photographs
        class Show < View::Controller
          include Import["photograph_repo", "story_repo"]

          configure do |config|
            config.template = "api/photographs/show"
          end

          expose :photograph do |slq_identifier:|
            photograph_repo.by_slq_identifier!(slq_identifier)
          end

          expose :stories, as: View::Parts::Story do |photograph|
            story_repo.for_photograph_id(photograph.id).to_a
          end
        end
      end
    end
  end
end
