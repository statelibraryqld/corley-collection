require "main/import"
require "main/view/controller"

module Main
  module Views
    module Api
      module Photographs
        class TagCloud < View::Controller
          include Import[
            "photographs.tag_cloud"
          ]

          configure do |config|
            config.template = "api/photographs/tag_cloud"
          end

          expose :tag_cloud do
            tag_cloud.()
          end
        end
      end
    end
  end
end
