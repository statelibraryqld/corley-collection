require "main/import"
require "main/view/controller"

module Main
  module Views
    module Api
      module Spools
        class Index < View::Controller
          include Import["spool_repo"]

          configure do |config|
            config.template = "api/spools/index"
          end

          expose :spools do
            spool_repo.index.to_a
          end
        end
      end
    end
  end
end
