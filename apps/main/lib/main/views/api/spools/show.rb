require "main/import"
require "main/view/controller"

module Main
  module Views
    module Api
      module Spools
        class Show < View::Controller
          include Import["spool_repo"]

          configure do |config|
            config.template = "api/spools/show"
          end

          expose :spool do |slq_identifier:|
            spool_repo.by_slq_identifier!(slq_identifier)
          end
        end
      end
    end
  end
end
