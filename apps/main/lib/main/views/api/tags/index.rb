require "main/import"
require "main/view/controller"

module Main
  module Views
    module Api
      module Tags
        class Index < View::Controller
          include Import["tag_repo"]

          configure do |config|
            config.template = "api/tags/index"
          end

          expose :tag_options do |tags|
            tags.group_by { |tag| tag.type }.inject({}) do |tag_options, (type, type_tags)|
              tag_options.tap { |options|
                options[type] = type_tags.map { |tag| {id: tag.id, name: tag.name} }
              }
            end
          end

          expose :tag_types do
            tag_repo.tag_types
          end

          private_expose :tags do
            tag_repo.index.to_a
          end
        end
      end
    end
  end
end
