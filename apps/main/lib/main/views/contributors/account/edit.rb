require "main/import"
require "main/view/controller"

module Main
  module Views
    module Contributors
      module Account
        class Edit < View::Controller
          configure do |config|
            config.template = "contributors/account/edit"
          end

          expose :current_contributor

          expose :input do |current_contributor, validation: nil|
            validation ? validation.to_h : current_contributor.to_h
          end

          expose :errors do |validation: nil|
            validation ? validation.errors : {}
          end
        end
      end
    end
  end
end
