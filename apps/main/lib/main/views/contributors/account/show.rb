require "main/import"
require "main/view/controller"

module Main
  module Views
    module Contributors
      module Account
        class Show < View::Controller
          configure do |config|
            config.template = "contributors/account/show"
          end

          expose :current_contributor
        end
      end
    end
  end
end
