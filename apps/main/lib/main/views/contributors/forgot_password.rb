require "main/view/controller"
require "main/import"

module Main
  module Views
    module Contributors
      class ForgotPassword < Main::View::Controller
        configure do |config|
          config.template = "contributors/forgot_password"
        end

        expose :input do |validation: nil|
          validation.to_h
        end

        expose :errors do |validation: nil|
          validation ? validation.errors : {}
        end
      end
    end
  end
end
