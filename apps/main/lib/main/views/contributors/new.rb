require "main/view/controller"

module Main
  module Views
    module Contributors
      class New < View::Controller
        configure do |config|
          config.template = "contributors/new"
        end

        expose :input do |validation: nil|
          validation.to_h
        end

        expose :errors do |validation: nil|
          validation ? validation.errors : {}
        end
      end
    end
  end
end
