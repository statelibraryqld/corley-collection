require "main/view/controller"
require "main/import"

module Main
  module Views
    module Contributors
      class ResetPassword < Main::View::Controller
        include Import["contributor_repo"]

        configure do |config|
          config.template = "contributors/reset_password"
        end

        expose :input do |validation: nil|
          validation.to_h
        end

        expose :contributor do |contributor_id:|
          contributor_repo[contributor_id]
        end

        expose :errors do |validation: nil|
          validation ? validation.errors : {}
        end
      end
    end
  end
end
