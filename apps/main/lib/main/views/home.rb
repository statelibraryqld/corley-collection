require "main/import"
require "main/view/controller"

module Main
  module Views
    class Home < View::Controller
      configure do |config|
        config.template = "home"
      end

      expose :contributor_signed_in
    end
  end
end
