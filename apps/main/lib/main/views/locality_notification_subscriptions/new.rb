require "main/import"
require "main/view/controller"

module Main
  module Views
    module LocalityNotificationSubscriptions
      class New < Main::View::Controller
        include Main::Import["locality_repo"]

        configure do |config|
          config.template = "locality_notification_subscriptions/new"
        end

        expose :locality do |locality_id:|
          locality_repo.by_id!(locality_id)
        end

        expose :errors do |validation: nil|
          validation ? validation.errors : {}
        end
      end
    end
  end
end
