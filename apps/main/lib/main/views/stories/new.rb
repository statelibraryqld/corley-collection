require "main/import"
require "main/view/controller"

module Main
  module Views
    module Stories
      class New < Main::View::Controller
        include Main::Import["stories.form", "photograph_repo", "locality_repo"]

        configure do |config|
          config.template = "stories/new"
        end

        expose :photograph do |photograph_slq_identifier:|
          photograph_repo.by_slq_identifier!(photograph_slq_identifier)
        end

        expose :notification_localities do |photograph|
          if photograph.locality
            Array(photograph.locality)
          elsif photograph.spool_id
            locality_repo.for_spool(photograph.spool_id)
          else
            []
          end
        end

        expose :input do |validation: nil|
          validation.to_h
        end

        expose :errors do |validation: nil|
          validation ? validation.errors : {}
        end
      end
    end
  end
end
