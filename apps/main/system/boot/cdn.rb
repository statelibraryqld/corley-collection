Main::Container.boot :cdn do |container|
  init do
    require "main/services/cdn"

    register :cdn, Main::Services::Cdn.new
  end

  start do
    container[:cdn].start if container.env == :production
  end

  stop do
    container[:cdn].stop
  end
end
