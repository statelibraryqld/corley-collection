Main::Container.boot :contributor_notifier do |container|
  init do
    require "main/services/contributor_notifier"

    register :contributor_notifier, Main::Services::ContributorNotifier.new
  end

  start do
    container[:contributor_notifier].start
  end

  stop do
    container[:contributor_notifier].stop
  end
end
