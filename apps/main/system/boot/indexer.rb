Main::Container.boot :indexer do |main|
  init do
    require "main/services/indexer"

    register(:indexer, Main::Services::Indexer.new)
  end

  start do
    main[:indexer].start
  end

  stop do
    main[:indexer].stop
  end
end
