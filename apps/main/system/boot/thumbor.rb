Main::Container.boot(:thumbor) do |container|
  init do
    require "corley_collection/thumbor/client"
  end

  start do
    client = CorleyCollection::Thumbor::Client.new(
      host: container["core.settings"].thumbor_host,
      security_key: container["core.settings"].thumbor_security_key,
      default_image_host: container["core.s3.buckets.assets"].url
    ) do
      preset :thumbnail, width: 600, height: 600, fit_in: true
      preset :feature, width: 1600, height: 1600, fit_in: true
    end

    register :thumbor, client
  end
end
