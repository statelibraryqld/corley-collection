require "rack/csrf"
require "rack/cors"
require "dry/web/roda/application"
require "corley_collection/cookie_filter"
require "roda_plugins"
require_relative "container"

module Main
  class Application < Dry::Web::Roda::Application
    configure do |config|
      config.container = Container
      config.routes = "web/routes".freeze
    end

    opts[:root] = Pathname(__FILE__).join("../..").realpath.dirname

    use Rack::Cors do
      allow do
        origins "localhost:3000", "localhost:9292", "explorer.corley.slq.qld.gov.au", "anudesign.com.au", "anu-dev.corley.icelab.com.au"
        resource "/api/v1/favourites.json",
          headers: :any,
          credentials: true,
          methods: [:get, :post, :put, :patch, :head, :delete, :options]

        resource "/api/v1/current-contributor.json",
          headers: :any,
          credentials: true,
          methods: [:get, :post, :put, :patch, :head, :delete, :options]
      end

      allow do
        origins "*"
        resource "/api/*",
          headers: :any,
          methods: [:get, :post, :put, :patch, :head, :options]
      end
    end

    use CorleyCollection::CookieFilter

    use Rack::MethodOverride

    use Rack::Session::Cookie,
      key: "corley-collection.session",
      secret: self["core.settings"].session_secret,
      expire_after: 10 * 365 * 24 * 60 * 60 # 10 years

    # Avoid CSRF protection for now.
    # if ENV["RACK_ENV"] != "test"
    #   use Rack::Csrf, raise: true, skip_if: lambda { |request|
    #     request.path.to_s.match?(%r{^\/api\/})
    #   }
    # end

    plugin :all_verbs
    plugin :caching
    plugin :flash
    plugin :json
    plugin :json_parser
    plugin :header_matchers
    plugin :multi_route
    plugin :type_routing

    # Our plugins
    plugin :main_view
    plugin :contributor_auth
    plugin :error_handler
    plugin :cookies

    # Set default_headers
    # s-maxage = CDN expiry
    # max-age = client expiry
    plugin :default_headers, "Cache-Control" => "public, s-maxage=1036800, max-age=3600"

    route do |r|
      r.root do
        response.cache_control private: true, no_cache: true

        r.view "home", contributor_signed_in: contributor_signed_in?
      end

      r.multi_route
    end

    error do |e|
      if %i[production test].include?(self.class.container.env)
        if e.is_a?(ROM::TupleCountMismatchError)
          response.status = 404
          response.cache_control private: true, no_cache: true

          self.class["views.errors.404"].(context: current_view_context)
        elsif %i[production].include?(self.class.container.env)
          response.cache_control private: true, no_cache: true

          self.class["core.rack_monitor"].instrument(:error, exception: e, env: env)
          self.class["views.errors.500"].(context: current_view_context)
        else
          raise e
        end
      else
        raise e
      end
    end

    load_routes!

    def t
      self.class["core.i18n.t"]
    end

    def contributor_signed_in?
      !session[:contributor_id].nil?
    end

    def current_contributor
      env["main.current_contributor"]
    end
  end
end
