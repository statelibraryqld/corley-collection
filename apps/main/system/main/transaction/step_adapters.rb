require "dry/transaction"
require "dry/monads/result"

require "main/import"
require "main/container"

module Main
  class Transaction
    class StepAdapters < Dry::Transaction::StepAdapters
      class Enqueue
        include Dry::Monads::Result::Mixin

        def call(_operation, options, args)
          Container[options.fetch(:operation_name)].enqueue(*args)
          Success(args[0])
        end
      end

      register :enqueue, Enqueue.new
    end
  end
end
