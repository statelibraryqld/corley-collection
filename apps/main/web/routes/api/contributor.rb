class Main::Application
  route "api/v1/current-contributor" do |r|
    response.cache_control private: true, no_cache: true

    r.is do
      r.json do
        r.get do
          r.view "api.contributors.current", contributor_id: session[:contributor_id], format: :json
        end
      end
    end
  end
end
