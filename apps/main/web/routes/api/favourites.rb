class Main::Application
  route "api/v1/favourites" do |r|
    response.cache_control private: true, no_cache: true

    r.is do
      r.json do
        r.get do
          r.view "api.favourites.index", contributor_id: session[:contributor_id], format: :json
        end

        r.post do
          r.resolve "favourites.operations.create" do |create|
            create.(r.params.merge(contributor_id: session[:contributor_id])) do |m|
              m.success do
                response.status = 200
                {status: "ok"}
              end

              m.failure do |error|
                response.status = 422
                {errors: error.errors}
              end
            end
          end
        end

        r.delete do
          r.resolve "favourites.operations.delete" do |delete|
            delete.(slq_photograph_identifier: r[:slq_photograph_identifier], contributor_id: session[:contributor_id]) do |m|
              m.success do
                response.status = 200
                {status: "ok"}
              end

              m.failure do |error|
                response.status = 422
                {errors: error.errors}
              end
            end
          end
        end
      end
    end
  end
end
