class Main::Application
  route "api/v1/photographs" do |r|
    response.cache_control public: true, max_age: 60, s_maxage: 300

    r.on "next-untagged" do
      response.cache_control private: true, no_cache: true

      r.json do
        r.get do
          r.view "api.photographs.next_untagged", format: :json
        end
      end
    end

    r.on "tag-cloud" do
      r.json do
        r.get do
          r.enqueue_cache_check
          r.view "api.photographs.tag_cloud", format: :json
        end
      end
    end

    r.on :slq_identifier do |slq_identifier|
      r.on "taggings" do
        r.json do
          r.post do
            r.resolve "photographs.operations.taggings.create" do |create|
              create.(slq_identifier, r.params) do |m|
                m.success do
                  response.status = 200
                  r.view "api.photographs.show", slq_identifier: slq_identifier, format: :json
                end

                m.failure do |error|
                  response.status = 422
                  {errors: error.errors}
                end
              end
            end
          end
        end
      end

      r.on "geo-location" do
        r.json do
          r.put do
            r.resolve "photographs.operations.geo_location.update" do |update|
              update.(slq_identifier, r.params) do |m|
                m.success do
                  response.status = 200
                  r.view "api.photographs.show", slq_identifier: slq_identifier, format: :json
                end

                m.failure do |error|
                  response.status = 422
                  {errors: error.errors}
                end
              end
            end
          end
        end
      end

      r.is do
        response.cache_control public: true, max_age: 1, s_maxage: 300

        r.json do
          r.get do
            r.enqueue_cache_check
            r.view "api.photographs.show", slq_identifier: slq_identifier, format: :json
          end

          r.put do
            r.resolve "photographs.operations.update" do |update|
              update.(slq_identifier, r.params) do |m|
                m.success do
                  response.status = 200
                  r.view "api.photographs.show", slq_identifier: slq_identifier, format: :json
                end

                m.failure do |error|
                  response.status = 422
                  {errors: error.errors}
                end
              end
            end
          end
        end
      end
    end

    r.is do
      # Do not cache search results for long
      response.cache_control public: true, max_age: 1, s_maxage: 60

      r.json do
        r.get do
          r.resolve "photographs.operations.query" do |query|
            query.(r.params) do |m|
              m.success do |input|
                r.view "api.photographs.index", params: input, format: :json
              end

              m.failure do |error|
                response.status = 422
                {errors: error.errors}
              end
            end
          end
        end
      end
    end
  end
end
