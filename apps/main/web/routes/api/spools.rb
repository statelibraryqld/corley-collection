class Main::Application
  route "api/v1/spools" do |r|
    r.on :slq_identifier do |slq_identifier|
      response.cache_control public: true, max_age: 1, s_maxage: 60

      r.json do
        r.get do
          r.enqueue_cache_check
          r.view "api.spools.show", slq_identifier: slq_identifier, format: :json
        end
      end
    end

    r.is do
      response.cache_control public: true, max_age: 60, s_maxage: 300

      r.json do
        r.get do
          r.enqueue_cache_check
          r.view "api.spools.index", format: :json
        end
      end
    end
  end
end
