class Main::Application
  route "api/v1/tags" do |r|
    response.cache_control public: true, max_age: 60, s_maxage: 300

    r.is do
      r.json do
        r.get do
          r.enqueue_cache_check
          r.view "api.tags.index", format: :json
        end
      end
    end
  end
end
