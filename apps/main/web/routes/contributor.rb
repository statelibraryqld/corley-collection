class Main::Application
  route "account" do |r|
    response.cache_control private: true, no_cache: true

    r.authorize do
      r.on "edit" do
        r.view "contributors.account.edit", current_contributor: current_contributor
      end

      r.is do
        r.get do
          r.view "contributors.account.show", current_contributor: current_contributor
        end

        r.put do
          r.resolve("contributors.operations.update") do |update|
            update.(current_contributor.id, r[:contributor]) do |m|
              m.success do
                flash[:notice] = "Your account has been updated"
                r.redirect "/account"
              end

              m.failure do |validation|
                r.view "contributors.account.edit", current_contributor: current_contributor, validation: validation
              end
            end
          end
        end
      end
    end
  end
end
