class Main::Application
  route "contributor-images" do |r|
    response.cache_control private: true, no_cache: true

    r.post "presign" do
      r.resolve "contributor_images.operations.presign" do |presign|
        presign.(r.POST) do |m|
          m.success do |presigned_post|
            {url: presigned_post.url, fields: presigned_post.fields}
          end

          m.failure do |validation|
            r.status = 422
            validation.messages
          end
        end
      end
    end
  end
end
