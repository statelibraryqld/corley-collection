class Main::Application
  route "contributors" do |r|
    response.cache_control private: true, no_cache: true

    r.on "sign-up" do
      r.get do
        if contributor_signed_in?
          flash[:notice] = "You are already signed in"
          r.redirect "/account"
        else
          r.view "contributors.new"
        end
      end

      r.post do
        r.resolve "contributors.transactions.create" do |create|
          create.(r[:contributor]) do |m|
            m.success do |contributor|
              flash[:notice] = "Welcome. You are now a Corley Collection contributor"
              session[:contributor_id] = contributor.id

              return_to = session[:return_to]

              if return_to
                session[:return_to] = nil
                r.redirect(return_to)
              else
                r.redirect("/")
              end
            end

            m.failure do |validation|
              flash.now[:alert] = "There was a problem creating your account"
              r.view "contributors.new", validation: validation
            end
          end
        end
      end
    end

    r.on "forgot-password" do
      r.get do
        r.view "contributors.forgot_password"
      end

      r.post do
        r.resolve "contributors.transactions.reset_password" do |reset_password|
          reset_password.(r[:contributor]) do |m|
            m.success do
              flash[:notice] = t["main.auth.password_reset"]
              r.redirect "/contributors/sign-in"
            end

            m.failure do |validation|
              r.view "contributors.forgot_password", validation: validation
            end
          end
        end
      end
    end

    r.on "reset-password" do
      r.on :password_reset_token do |password_reset_token|
        r.resolve "authentication.authenticate_contributor_by_password_reset_token" do |auth_by|
          auth_by.(password_reset_token) do |m|
            m.success do |contributor|
              r.get do
                r.view "contributors.reset_password", contributor_id: contributor.id
              end

              r.post do
                r.resolve "contributors.operations.change_password" do |change_password|
                  change_password.(contributor.id, r[:contributor]) do |n|
                    n.success do
                      flash[:notice] = t["main.auth.password_set"]
                      session[:contributor_id] = contributor.id

                      return_to = session[:return_to]

                      if return_to
                        session[:return_to] = nil
                        r.redirect(return_to)
                      else
                        r.redirect("/")
                      end
                    end

                    n.failure do |validation|
                      r.view "contributors.reset_password", contributor_id: contributor.id, validation: validation
                    end
                  end
                end
              end
            end

            m.failure do |error|
              flash[:notice] = t["main.auth.errors.#{error}"]
              r.redirect "/contributors/sign-in"
            end
          end
        end
      end
    end
  end
end
