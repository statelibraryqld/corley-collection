class Main::Application
  route "localities" do |r|
    response.cache_control private: true, no_cache: true

    r.authorize do
      r.on :id do |locality_id|
        r.on "notification-subscriptions" do
          r.get "new" do
            r.view "locality_notification_subscriptions.new", locality_id: locality_id
          end

          r.post do
            r.resolve "locality_notification_subscriptions.operations.subscribe" do |subscribe|
              subscribe.(current_contributor.id, locality_id, r[:locality_notification_subscription] || {}) do |m|
                m.success do
                  r.view "locality_notification_subscriptions.thank_you"
                end

                m.failure do |validation|
                  r.view "locality_notification_subscriptions.new", locality_id: locality_id, validation: validation
                end
              end
            end
          end
        end
      end
    end
  end
end
