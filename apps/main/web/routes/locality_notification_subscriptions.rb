class Main::Application
  route "locality-notification-subscriptions" do |r|
    response.cache_control private: true, no_cache: true

    r.on "contributor" do
      r.on :public_token do |public_token|
        r.is "unsubscribe" do
          r.get do
            r.resolve "locality_notification_subscriptions.operations.unsubscribe_contributor" do |unsubscribe_contributor|
              unsubscribe_contributor.(public_token: public_token) do |m|
                m.success do
                  r.view "locality_notification_subscriptions.unsubscribe_contributor"
                end

                m.failure do
                  r.view "locality_notification_subscriptions.unsubscribe_contributor"
                end
              end
            end
          end
        end
      end
    end

    r.on :token do |token|
      r.is "unsubscribe" do
        r.get do
          r.resolve "locality_notification_subscriptions.operations.unsubscribe" do |unsubscribe|
            unsubscribe.(token: token) do |m|
              m.success do
                r.view "locality_notification_subscriptions.unsubscribe_success"
              end

              m.failure do
                r.view "locality_notification_subscriptions.unsubscribe_failure"
              end
            end
          end
        end
      end
    end
  end
end
