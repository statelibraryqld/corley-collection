class Main::Application
  route "photographs" do |r|
    response.cache_control private: true, no_cache: true

    r.authorize do
      r.on :slq_identifier do |slq_identifier|
        r.on "stories" do
          r.get "new" do
            r.view "stories.new", photograph_slq_identifier: slq_identifier
          end

          r.post do
            r.resolve "stories.operations.create" do |create|
              create.(current_contributor.id, slq_identifier, r[:story]) do |m|
                m.success do
                  r.view "stories.thank_you"
                end

                m.failure do |validation|
                  r.view "stories.new", photograph_slq_identifier: slq_identifier, validation: validation
                end
              end
            end
          end
        end

        r.on "notification-subscriptions" do
          r.get "new" do
            r.view "photograph_notification_subscriptions.new", photograph_slq_identifier: slq_identifier
          end

          r.post do
            r.resolve "photograph_notification_subscriptions.operations.subscribe" do |subscribe|
              subscribe.(current_contributor.id, slq_identifier, r[:photograph_notification_subscription] || {}) do |m|
                m.success do
                  r.view "photograph_notification_subscriptions.thank_you"
                end

                m.failure do |validation|
                  r.view "photograph_notification_subscriptions.new", photograph_slq_identifier: slq_identifier, validation: validation
                end
              end
            end
          end
        end
      end
    end
  end
end
