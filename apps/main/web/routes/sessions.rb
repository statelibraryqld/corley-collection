class Main::Application
  route "contributors/sign-in" do |r|
    response.cache_control private: true, no_cache: true

    r.get do
      if contributor_signed_in?
        flash[:notice] = "You are already signed in"
        r.redirect "/account"
      else
        r.view "contributors.sign_in"
      end
    end

    r.post do
      r.resolve "authentication.authenticate" do |authenticate|
        authenticate.(r[:contributor]) do |m|
          m.success do |contributor|
            flash[:notice] = "Welcome. You are now signed in"
            session[:contributor_id] = contributor.id

            return_to = session[:return_to]

            if return_to
              session[:return_to] = nil
              r.redirect(return_to)
            else
              r.redirect("/")
            end
          end

          m.failure do |error|
            flash[:alert] = t["main.auth.errors.#{error}"]
            r.redirect "/contributors/sign-in"
          end
        end
      end
    end
  end

  route "contributors/sign-out" do |r|
    response.cache_control private: true, no_cache: true

    flash[:notice] = "You are now signed out"
    session[:contributor_id] = env["main.current_contributor"] = nil
    r.redirect "/contributors/sign-in"
  end
end
