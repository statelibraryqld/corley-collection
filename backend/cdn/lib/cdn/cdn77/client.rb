# auto_register: false

require "json"
require "dry/monads/result"

module Cdn
  module Cdn77
    class Client
      include Dry::Monads::Result::Mixin

      def initialize(email, password, cdn_id, prefetch_endpoint, purge_endpoint)
        @email = email
        @password = password
        @cdn_id = cdn_id
        @prefetch_endpoint = prefetch_endpoint
        @purge_endpoint = purge_endpoint
      end

      def prefetch_paths(paths)
        response = Net::HTTP.post_form(
          URI(@prefetch_endpoint),
          [
            ["login", @email],
            ["passwd", @password],
            ["cdn_id", @cdn_id]
          ] + prepare_paths(paths)
        )

        parsed_response = JSON.parse(response.body)

        if parsed_response["status"] == "error"
          Failure(parsed_response)
        else
          Success(parsed_response)
        end
      end

      def purge_paths(paths)
        response = Net::HTTP.post_form(
          URI(@purge_endpoint),
          [
            ["login", @email],
            ["passwd", @password],
            ["cdn_id", @cdn_id]
          ] + prepare_paths(paths)
        )

        parsed_response = JSON.parse(response.body)

        if parsed_response["status"] == "error"
          Failure(parsed_response)
        else
          Success(parsed_response)
        end
      end

      private

      def prepare_paths(paths)
        paths.map do |path|
          ["url[]", path]
        end
      end
    end
  end
end
