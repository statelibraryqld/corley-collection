# auto_register: false

module Cdn
  module Cdn77
    class TestClient
      attr_accessor :paths_to_prefetch
      attr_accessor :paths_to_purge

      def initialize
        @paths_to_prefetch = []
        @paths_to_purge = []
      end

      def prefetch_paths(paths)
        paths_to_prefetch << paths
      end

      def purge_paths(paths)
        paths_to_purge << paths
      end
    end
  end
end
