require "rom/struct"

module Cdn
  module Entities
    class PageCacheCheck < ROM::Struct
      CHECK_INTERVAL = 60

      def requires_check_now?
        Time.now - updated_at > CHECK_INTERVAL
      end
    end
  end
end
