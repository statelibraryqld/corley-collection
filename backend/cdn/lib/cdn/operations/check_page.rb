require "corley_collection/operation"
require "cdn/import"

module Cdn
  module Operations
    class CheckPage < CorleyCollection::Operation
      include Import[
        "page_cache_check_repo",
        "operations.get_checksum",
        "operations.purge_path",
      ]

      def call(page_cache_check)
        get_checksum.(page_cache_check.path)
          .bind { |checksum|
            updated_check = update_check(page_cache_check, checksum)

            if updated_check.checksum != page_cache_check.checksum
              purge_path.(updated_check.path).bind {
                Success(updated_check)
              }
            else
              Success(updated_check)
            end
          }
          .or {
            delete_check(page_cache_check)
            Success(page_cache_check)
          }
      end

      def to_queue(page_cache_check)
        [page_cache_check.path]
      end

      def call_from_queue(path)
        page_cache_check = page_cache_check_repo.by_path!(path)

        call(page_cache_check).or { |check| raise "Check failed", check }
      end

      private

      def update_check(page_cache_check, checksum)
        page_cache_check_repo.update(
          page_cache_check.id,
          checksum: checksum,
          updated_at: Time.now,
        )
      end

      def delete_check(page_cache_check)
        page_cache_check_repo.delete(page_cache_check.id)
      end
    end
  end
end
