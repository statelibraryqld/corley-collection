require "corley_collection/operation"
require "cdn/import"

module Cdn
  module Operations
    class EnqueueCheck < CorleyCollection::Operation
      include Import[
        "page_cache_check_repo",
        "operations.check_page"
      ]

      def call(path)
        check = page_cache_check_repo.find_or_create_for_path(path)

        if check.requires_check_now? && !page_cache_check_repo.check_enqueued?(path)
          check_page.enqueue(check)
        end

        Success(check)
      end
    end
  end
end
