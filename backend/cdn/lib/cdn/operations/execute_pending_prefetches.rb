require "corley_collection/operation"
require "cdn/import"

module Cdn
  module Operations
    class ExecutePendingPrefetches < CorleyCollection::Operation
      include Import[
        "pending_cdn_action_repo",
        "cdn"
      ]

      # Sends all pending 'prefetch' actions (up to CDN's limit of 2000), to CDN77 in a single request
      # - If the request succeeds, all actioned prefetches are deleted from the PendingCdnActions table
      # Unlike the ExecutePendingPurges operation, we don't handle the failure case here so as to keep the queue 'clear'
      # for purge requests (as these are the highest priority)

      def call
        prefetch_actions = pending_cdn_action_repo.prefetches(limit: 2000).to_a

        paths_to_prefetch = prefetch_actions.map(&:path).flatten.uniq

        return unless paths_to_prefetch.any?
        # First perform all prefetches
        cdn.prefetch_paths(paths_to_prefetch)

        # Then delete the actioned records from the DB
        pending_cdn_action_repo.delete(prefetch_actions.map(&:id))
      end

      def to_queue
        []
      end

      def call_from_queue
        call
      end
    end
  end
end
