require "corley_collection/operation"
require "cdn/import"
require "dry/monads/unit"
require "corley_collection/job"

module Cdn
  module Operations
    class ExecutePendingPurges < CorleyCollection::Operation
      include Import[
        "pending_cdn_action_repo",
        "operations.execute_pending_prefetches",
        "cdn"
      ]

      # Sends all pending 'purge' actions (up to CDN's limit of 2000), to CDN77 in a single request
      # - If the request succeeds, all actioned purges are deleted from the PendingCdnActions table, and a job to action
      #   the pending 'prefetch' actions is enqueued
      # - If the request fails, an exception is raised and the job runs again in 30 seconds

      def call
        purge_actions = pending_cdn_action_repo.purges(limit: 2000).to_a

        paths_to_purge = purge_actions.map(&:path).flatten.uniq

        # If there are paths to be purged, action these first
        if paths_to_purge.any?
          cdn.purge_paths(paths_to_purge).bind { |_result|
            # Then delete the actioned records from the DB
            pending_cdn_action_repo.delete(purge_actions.map(&:id))
            # Then enqueue the job to perform any pending 'prefetch' actions
            execute_pending_prefetches.enqueue

            Success(Dry::Monads::Unit)
          }
        else
          Success(Dry::Monads::Unit)
        end
      end

      def to_queue; end

      def call_from_queue
        call
      end

      class Job < CorleyCollection::Job[self, :call_from_queue]
        # Retry the job every 15 seconds
        self.retry_interval = 15

        def run(*)
          super.or do |failure_response|
            raise "Purging paths failed: #{failure_response.inspect}"
          end
        end
      end
    end
  end
end
