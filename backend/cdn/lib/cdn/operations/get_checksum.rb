require "corley_collection/operation"
require "cdn/import"
require "digest/md5"
require "http"

module Cdn
  module Operations
    class GetChecksum < CorleyCollection::Operation
      include Import["settings"]

      def call(path)
        response = HTTP[accept: "application/json"] # We only use this for json endpoints, so we set an accept header here
          .yield_self { |http|
            if settings.basic_auth_username || settings.basic_auth_password
              http.basic_auth(user: settings.basic_auth_username, pass: settings.basic_auth_password)
            else
              http
            end
          }
          .get("#{settings.origin_url}#{path}")

        if response.status == 200
          checksum = Digest::MD5.hexdigest(response.to_s)
          Success(checksum)
        else
          Failure(response)
        end
      end
    end
  end
end
