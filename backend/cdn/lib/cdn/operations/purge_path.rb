require "corley_collection/operation"
require "cdn/import"
require "corley_collection/que_job_manager"
require "time_math"

module Cdn
  module Operations
    class PurgePath < CorleyCollection::Operation
      include Import[
        "pending_cdn_action_repo",
        "operations.execute_pending_purges",
        "settings"
      ]

      def call(path)
        pending_cdn_action_repo.create(path: path, type: "purge")

        unless CorleyCollection::QueJobManager.job_queued_with_job_class?(execute_pending_purges.job_class)
          execute_pending_purges.enqueue(
            run_at: TimeMath.sec.advance(Time.now, settings.cdn_purge_delay_in_seconds)
          )
        end

        Success(path)
      end
    end
  end
end
