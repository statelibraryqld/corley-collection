require "cdn/repository"
require_relative "entities"

module Cdn
  class PageCacheCheckRepo < Repository[:page_cache_checks]
    struct_namespace Entities

    def find_or_create_for_path(path)
      transaction do
        check = page_cache_checks.by_path(path).one

        check || page_cache_checks.changeset(:create, path: path).commit
      end
    end

    def update(id, attrs)
      page_cache_checks.by_pk(id).changeset(:update, attrs).commit
    end

    def delete(id)
      page_cache_checks.by_pk(id).changeset(:delete).commit
    end

    def by_path!(path)
      page_cache_checks.by_path(path).one!
    end

    def check_enqueued?(path)
      enqueued_page_cache_checks.exist?(path: path)
    end
  end
end
