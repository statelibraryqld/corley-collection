require "cdn/repository"

module Cdn
  class PendingCdnActionRepo < Repository[:pending_cdn_actions]
    commands :create, delete: :by_pk

    def purges(limit: nil)
      pending_cdn_actions
        .by_type("purge")
        .order { created_at.asc }
        .limit(limit)
    end

    def prefetches(limit: nil)
      pending_cdn_actions
        .by_type("prefetch")
        .order { created_at.asc }
        .limit(limit)
    end
  end
end
