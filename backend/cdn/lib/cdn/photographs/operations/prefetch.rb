require "cdn/operations/prefetch"

module Cdn
  module Photographs
    module Operations
      class Prefetch < Cdn::Operations::Prefetch
        def target_paths(photograph)
          [
            "/api/v1/photographs/#{photograph.slq_identifier}",
            "/api/v1/photographs/#{photograph.slq_identifier}.json",
          ]
        end
      end
    end
  end
end
