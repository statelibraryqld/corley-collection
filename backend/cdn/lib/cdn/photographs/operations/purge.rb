require "cdn/operations/purge"

module Cdn
  module Photographs
    module Operations
      class Purge < Cdn::Operations::Purge
        def target_paths(photograph)
          [
            "/api/v1/photographs/#{photograph.slq_identifier}",
            "/api/v1/photographs/#{photograph.slq_identifier}.json",
            "/api/v1/photographs/tag-cloud",
            "/api/v1/photographs/tag-cloud.json",
          ]
        end
      end
    end
  end
end
