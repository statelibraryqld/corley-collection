require "corley_collection/repository"
require "cdn/entities"

module Cdn
  class Repository < CorleyCollection::Repository
    struct_namespace Cdn::Entities
  end
end
