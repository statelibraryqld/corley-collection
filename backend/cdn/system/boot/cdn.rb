Cdn::Container.boot :cdn do |container|
  init do
    require "cdn/cdn77/client"
  end

  start do
    use :settings

    client = Cdn::Cdn77::Client.new(
      container[:settings].cdn77_email,
      container[:settings].cdn77_password,
      container[:settings].cdn77_cdn_id,
      container[:settings].cdn77_prefetch_endpoint,
      container[:settings].cdn77_purge_endpoint
    )

    if container.env == :test
      require "cdn/cdn77/test_client"
      test_client = Cdn::Cdn77::TestClient.new

      container.register "cdn", test_client
    else
      container.register "cdn", client
    end
  end
end
