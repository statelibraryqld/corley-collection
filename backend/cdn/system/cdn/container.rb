require "dry/web/container"
require "corley_collection/resolver"
require "pathname"

module Cdn
  class Container < Dry::Web::Container
    configure do |config|
      config.resolver = CorleyCollection::Resolver.new(config.env)
      config.root = Pathname(__FILE__).join("../..").realpath.dirname.freeze
      config.logger = CorleyCollection::Container[:logger]
      config.default_namespace = "cdn"
      config.auto_register = %w[lib/cdn]
    end

    load_paths! "lib"

    boot(:settings, from: :system) do
      before(:init) do
        require "types"
      end

      settings do
        key :cdn77_cdn_id, Types::Strict::String.constrained(filled: true)
        key :cdn77_email, Types::Strict::String.constrained(filled: true)
        key :cdn77_password, Types::Strict::String.constrained(filled: true)
        key :cdn77_prefetch_endpoint, Types::Strict::String.constrained(filled: true)
        key :cdn77_purge_endpoint, Types::Strict::String.constrained(filled: true)
        key :cdn_purge_delay_in_seconds, Types::Coercible::Integer.constrained(filled: true)
        key :origin_url, Types::Strict::String.constrained(filled: true)
        key :basic_auth_username, Types::Strict::String.constrained(filled: true).optional.default(nil)
        key :basic_auth_password, Types::Strict::String.constrained(filled: true).optional.default(nil)
      end
    end
  end
end
