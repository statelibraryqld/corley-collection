require "mail/view/admin"
require "mail/import"

module Mail
  module Admin
    class ExportCompleted < Mail::View::Admin
      configure do |config|
        config.template = "admin/export_completed"
      end

      expose :email
      expose :export_url

      expose :to do |email|
        email
      end

      expose :subject do
        "Corley Collection export ready for download"
      end

      def to_queue(data)
        [data[:recipient_email_address], data[:export_url]]
      end

      def deliver_from_queue(email, export_url)
        deliver(email: email, export_url: export_url)
      end
    end
  end
end
