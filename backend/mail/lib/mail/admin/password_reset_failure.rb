require "mail/view/admin"
require "mail/import"

module Mail
  module Admin
    class PasswordResetFailure < Mail::View::Admin
      include Import["core.i18n.t"]

      configure do |config|
        config.template = "admin/password_reset_failure"
      end

      expose :email

      expose :to do |email|
        email
      end

      expose :subject do
        t["admin.emails.users.password_reset.subject"]
      end

      def to_queue(email)
        [email]
      end

      def deliver_from_queue(email)
        deliver(email: email)
      end
    end
  end
end
