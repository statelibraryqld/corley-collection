require "mail/view/admin"
require "mail/import"

module Mail
  module Admin
    class PasswordResetSuccess < Mail::View::Admin
      include Import[
        "core.i18n.t",
        "user_repo",
      ]

      configure do |config|
        config.template = "admin/password_reset_success"
      end

      expose :user

      expose :to do |user|
        user.email
      end

      expose :subject do
        t["admin.emails.users.password_reset.subject"]
      end

      expose :password_reset_url do |user|
        "#{settings.canonical_url}/admin/users/reset-password/#{user.password_reset_token}"
      end

      def to_queue(user)
        [user.id]
      end

      def deliver_from_queue(user_id)
        user = user_repo[user_id]
        deliver(user: user)
      end
    end
  end
end
