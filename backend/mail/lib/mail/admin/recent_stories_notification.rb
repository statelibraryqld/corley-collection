require "mail/view/admin"
require "mail/import"

module Mail
  module Admin
    class RecentStoriesNotification < Mail::View::Admin
      include Import[
        "core.i18n.t",
        "story_repo",
        "core.s3.buckets.assets"
      ]

      configure do |config|
        config.template = "admin/recent_stories_notification"
      end

      expose :stories

      expose :to do
        settings.admin_notifications_email
      end

      expose :canonical_url do
        settings.canonical_url
      end

      expose :subject do |stories|
        "#{stories.count} new Corley Collection stories"
      end

      expose :image_host do
        assets.url
      end

      def to_queue(story_ids)
        [story_ids]
      end

      def deliver_from_queue(story_ids)
        stories = story_repo.by_ids_with_associations(story_ids)
        deliver(stories: stories)
      end
    end
  end
end
