require "mail/repository"

module Mail
  class ContributorRepo < Repository[:contributors]
    def [](id)
      contributors.by_pk(id).one
    end
  end
end
