# auto_register: false

require "corley_collection/entities/contributor"

module Mail
  module Entities
    class Contributor < CorleyCollection::Entities::Contributor
    end
  end
end
