# auto_register: false

require "postmark"

module Mail
  class Mailer
    def initialize(api_key)
      @mailer = ::Postmark::ApiClient.new(api_key)
    end

    def deliver(delivery_hash)
      @mailer.deliver(delivery_hash)
    end
  end
end
