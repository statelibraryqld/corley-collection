require "mail/view/main"
require "mail/import"

module Mail
  module Main
    class NewStoryNotification < Mail::View::Main
      include Import[
        "contributor_repo",
        "core.i18n.t",
        "photograph_notification_subscription_repo",
        "story_repo"
      ]

      configure do |config|
        config.template = "main/new_story_notification"
      end

      expose :contributor
      expose :photograph_notification_subscription
      expose :story

      expose :to do |contributor|
        contributor.email
      end

      expose :photograph do |story|
        story.photograph
      end

      expose :photograph_description do |photograph|
        if photograph.street_address
          "The photograph's address is #{photograph.street_address}."
        else
          "The photograph's identifier is #{photograph.slq_identifier}."
        end
      end

      expose :subject do
        t["main.emails.notifications.new_story.subject"]
      end

      expose :photograph_url do |photograph|
        "#{settings.corley_explorer_url}/#!/photograph/#{photograph.slq_identifier}"
      end

      expose :unsubscribe_url do |photograph_notification_subscription|
        "#{settings.canonical_url}/photograph-notification-subscriptions/#{photograph_notification_subscription.token}/unsubscribe"
      end

      expose :global_unsubscribe_url do |contributor|
        "#{settings.canonical_url}/photograph-notification-subscriptions/contributor/#{contributor.public_token}/unsubscribe"
      end

      def to_queue(input)
        [input]
      end

      def deliver_from_queue(input)
        contributor = contributor_repo[input[:contributor_id]]
        photograph_notification_subscription = photograph_notification_subscription_repo[input[:photograph_notification_subscription_id]]
        story = story_repo[input[:story_id]]

        if contributor && photograph_notification_subscription && story
          deliver(
            contributor: contributor,
            photograph_notification_subscription: photograph_notification_subscription,
            story: story
          )
        end
      end
    end
  end
end
