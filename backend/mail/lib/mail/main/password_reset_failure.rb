require "mail/view/main"
require "mail/import"

module Mail
  module Main
    class PasswordResetFailure < Mail::View::Main
      include Import["core.i18n.t"]

      configure do |config|
        config.template = "main/password_reset_failure"
      end

      expose :email

      expose :to do |email|
        email
      end

      expose :subject do
        t["main.emails.contributors.password_reset.subject"]
      end

      def to_queue(email)
        [email]
      end

      def deliver_from_queue(email)
        deliver(email: email)
      end
    end
  end
end
