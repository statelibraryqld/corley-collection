require "mail/view/main"
require "mail/import"

module Mail
  module Main
    class PasswordResetSuccess < Mail::View::Main
      include Import[
        "core.i18n.t",
        "contributor_repo",
      ]

      configure do |config|
        config.template = "main/password_reset_success"
      end

      expose :contributor

      expose :to do |contributor|
        contributor.email
      end

      expose :subject do
        t["main.emails.contributors.password_reset.subject"]
      end

      expose :password_reset_url do |contributor|
        "#{settings.canonical_url}/contributors/reset-password/#{contributor.password_reset_token}"
      end

      def to_queue(contributor)
        [contributor.id]
      end

      def deliver_from_queue(contributor_id)
        contributor = contributor_repo[contributor_id]
        deliver(contributor: contributor)
      end
    end
  end
end
