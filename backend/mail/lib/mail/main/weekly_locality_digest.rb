require "mail/view/main"
require "mail/import"

module Mail
  module Main
    class WeeklyLocalityDigest < Mail::View::Main
      include Import["core.i18n.t"]

      configure do |config|
        config.template = "main/weekly_locality_digest"
      end

      expose :contributor
      expose :active_localities
      expose :most_active_localities
      expose :subscriptions

      expose :to do |contributor|
        contributor.email
      end

      expose :subject do
        t["main.emails.notifications.weekly_locality_digest.subject"]
      end

      expose :corley_explorer_url do
        settings.corley_explorer_url
      end

      expose :canonical_url do
        settings.canonical_url
      end

      expose :global_unsubscribe_url do |contributor|
        "#{settings.canonical_url}/locality-notification-subscriptions/contributor/#{contributor.public_token}/unsubscribe"
      end

      expose :item_limit do |active_localities|
        if active_localities.size > 2
          2
        else
          4
        end
      end
    end
  end
end
