require "mail/view/main"
require "mail/import"

module Mail
  module Main
    class Welcome < Mail::View::Main
      include Import[
        "core.i18n.t",
        "contributor_repo",
      ]

      configure do |config|
        config.template = "main/welcome"
      end

      expose :contributor

      expose :to do |contributor|
        contributor.email
      end

      expose :subject do
        t["main.emails.contributors.welcome.subject"]
      end

      def to_queue(contributor)
        [contributor.id]
      end

      def deliver_from_queue(contributor_id)
        contributor = contributor_repo[contributor_id]
        deliver(contributor: contributor)
      end
    end
  end
end
