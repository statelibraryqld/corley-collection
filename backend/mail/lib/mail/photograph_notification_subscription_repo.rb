require "mail/repository"

module Mail
  class PhotographNotificationSubscriptionRepo < Repository[:photograph_notification_subscriptions]
    def [](id)
      photograph_notification_subscriptions.combine(:photograph, :contributor).by_pk(id).one
    end
  end
end
