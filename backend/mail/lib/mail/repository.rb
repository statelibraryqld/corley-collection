require "corley_collection/repository"
require "mail/entities"

module Mail
  class Repository < CorleyCollection::Repository
    struct_namespace Mail::Entities
  end
end
