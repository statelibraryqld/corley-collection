require "mail/repository"

module Mail
  class StoryRepo < Repository[:stories]
    def [](id)
      stories.combine(:photograph, :contributor, :contributor_images).by_pk(id).one
    end

    def by_ids_with_associations(ids)
      stories
        .combine(:photograph, :contributor, :contributor_images)
        .by_pk(ids)
        .order_by_id_values(ids)
    end
  end
end
