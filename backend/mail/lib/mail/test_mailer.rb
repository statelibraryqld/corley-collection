# auto_register: false

module Mail
  class TestMailer
    attr_accessor :messages

    def initialize
      @messages = []
    end

    def deliver(delivery_hash)
      @messages << delivery_hash
    end
  end
end
