require "mail/repository"

module Mail
  class UserRepo < Repository[:users]
    def [](id)
      users.by_pk(id).one
    end
  end
end
