require "mail/view/controller"
require "mail/import"

module Mail
  module View
    class Admin < Mail::View::Controller
      include Import["settings"]

      configure do |config|
        config.layout = "admin"
      end

      def from
        settings.admin_mailer_from_email
      end
    end
  end
end
