# auto_register: false

require "mail/import"

module Mail
  module View
    class Context
      include Import["core.assets"]
    end
  end
end
