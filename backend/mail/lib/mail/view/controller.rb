# auto_register: false

require "dry/view/controller"
require "mail/import"
require "dry/monads/result"
require "mail/view/context"
require "mail/container"
require "slim"
require "tilt/erb"
require "corley_collection/backgroundable"

module Mail
  module View
    class Controller < Dry::View::Controller
      include Import[
        "core.logger",
        "mailer"
      ]

      include Dry::Monads::Result::Mixin

      configure do |config|
        config.context = Context.new
        config.paths = [Container.root.join("templates")]
      end

      def self.inherited(klass)
        super
        klass.include CorleyCollection::Backgroundable.new(for_method: :deliver_from_queue)
      end

      def deliver(**input)
        logger.debug("[Mail] delivering #{self.class.inspect}: #{input.inspect}")
        mailer.deliver(delivery_hash(input))
      end

      def to_queue(**input)
        [input]
      end

      def deliver_from_queue(*args)
        deliver(*args)
      end

      def delivery_hash(**input)
        locals = self.locals(input)

        {
          subject: locals.fetch(:subject),
          from: locals.fetch(:from) { respond_to?(:from) && from },
          to: locals.fetch(:to),
          bcc: locals[:bcc],
          html_body: html(input),
          text_body: text(input)
        }
      end

      def html(**input)
        call(input.merge(format: :html))
      end

      def text(**input)
        call(input.merge(format: :txt))
      end
    end
  end
end
