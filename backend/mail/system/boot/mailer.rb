Mail::Container.boot(:mailer) do |container|
  start do
    if container.env == :test
      require "mail/test_mailer"
      register :mailer, Mail::TestMailer.new
    else
      require "mail/mailer"
      register :mailer, Mail::Mailer.new(container.settings.postmark_api_key)
    end
  end
end
