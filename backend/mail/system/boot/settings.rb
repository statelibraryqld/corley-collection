Mail::Container.boot :settings, from: :system do
  before :init do
    require "types"
  end

  settings do
    key :admin_mailer_from_email, Types::Strict::String.constrained(filled: true)
    key :canonical_url, Types::Strict::String.constrained(filled: true)
    key :postmark_api_key, Types::Strict::String.constrained(filled: true)
    key :admin_notifications_email, Types::Strict::String.constrained(filled: true)
    key :corley_explorer_url, Types::Strict::String.constrained(filled: true)
  end
end
