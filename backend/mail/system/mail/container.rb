require "dry/web/container"
require "pathname"

module Mail
  class Container < Dry::Web::Container
    require root.join("system/corley_collection/container")
    import core: CorleyCollection::Container

    require "corley_collection/resolver"

    configure do |config|
      config.resolver = CorleyCollection::Resolver.new(config.env)
      config.root = Pathname(__FILE__).join("../..").realpath.dirname.freeze
      config.logger = CorleyCollection::Container[:logger]
      config.default_namespace = "mail"
      config.auto_register = %w[lib/mail]
    end

    load_paths! "lib"

    def self.settings
      self[:settings]
    end
  end
end
