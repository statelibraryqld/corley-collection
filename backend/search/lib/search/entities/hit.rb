require "rom/struct"

module Search
  module Entities
    class Hit < ROM::Struct
      attribute :id, Types::Integer
      attribute :_metadata, Types::Hash

      def index_name
        _metadata["_index"].to_sym
      end

      def index_type
        _metadata["_type"]
      end

      def highlight?
        _metadata.key?("highlight")
      end

      def highlight
        _metadata["highlight"].map { |_k, v| v }.join(" … ") if highlight?
      end

      def eql?(other)
        other.id == id && other.type == index_type
      end
    end
  end
end
