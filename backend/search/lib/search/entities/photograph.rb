require "corley_collection/entities/photograph"

module Search
  module Entities
    class Photograph < CorleyCollection::Entities::Photograph
      def to_h
        attrs = {}

        if spool
          attrs[:spool_slq_identifier] = spool.slq_identifier
          attrs[:spool_content_description] = spool.content_description
        end

        if locality
          attrs[:locality_name] = locality.name
        end

        if geo_location?
          attrs[:geo_location] = {lat: latitude, lon: longitude}
        end

        if tags.any?
          attrs[:tag_ids] = tags.map(&:id)
          attrs[:tag_names] = tags.map(&:name)
        end

        # photographs either have their own assigned locality,
        # or they inherit the localities of their spool.
        unless locality
          attrs[:locality_id] = spool.localities.map(&:id) if spool
        end

        super.merge(attrs)
      end
    end
  end
end
