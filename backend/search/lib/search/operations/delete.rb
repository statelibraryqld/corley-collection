# auto_register: false

require "corley_collection/operation"
require "search/import"
require "dry/configurable"

module Search
  module Operations
    class Delete < CorleyCollection::Operation
      extend Dry::Configurable

      setting :es_relation

      include Import[es: "es.rom"]

      def call(entity_id)
        es_relation.get(entity_id).delete
      end

      def to_queue(entity_or_id)
        id = entity_or_id.respond_to?(:id) ? entity_or_id.id : entity_or_id
        [id]
      end

      def call_from_queue(entity_id)
        call(entity_id)
      end

      private

      def es_relation
        es.relations[self.class.config.es_relation]
      end
    end
  end
end
