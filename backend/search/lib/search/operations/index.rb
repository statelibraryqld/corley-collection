# auto_register: false

require "corley_collection/operation"
require "search/import"
require "dry/configurable"

module Search
  module Operations
    class Index < CorleyCollection::Operation
      extend Dry::Configurable

      setting :es_relation

      include Import[es: "es.rom"]

      def call(entity)
        es_relation.index(entity.to_h) if entity
      end

      def to_queue(entity_or_id)
        id = entity_or_id.respond_to?(:id) ? entity_or_id.id : entity_or_id
        [id]
      end

      def call_from_queue(entity_id)
        call(repository[entity_id])
      end

      private

      def es_relation
        es.relations[self.class.config.es_relation]
      end

      def repository
        raise NotImplementedError
      end
    end
  end
end
