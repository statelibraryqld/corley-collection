require "search/repository"

module Search
  class PhotographRepo < Repository[:photographs]
    def [](id)
      photographs
        .combine(:locality, :tags, spool: :localities)
        .by_pk(id)
        .one!
    end
  end
end
