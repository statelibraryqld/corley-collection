require "search/operations/delete"
require "search/import"

module Search
  module Photographs
    module Operations
      class Delete < Search::Operations::Delete
        configure do |config|
          config.es_relation = :photographs
        end
      end
    end
  end
end
