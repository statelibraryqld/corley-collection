require "search/operations/index"
require "search/import"

module Search
  module Photographs
    module Operations
      class Index < Search::Operations::Index
        configure do |config|
          config.es_relation = :photographs
        end

        include Import[repository: "photograph_repo"]
      end
    end
  end
end
