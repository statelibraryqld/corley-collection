require "dry/core/class_attributes"
require "search/import"
require "search/entities/search_result"
require "search/entities/hit"

module Search
  class Query
    extend Dry::Configurable
    include Import[es: "es.rom", db: "core.persistence.rom"]

    setting :sources, reader: true

    setting :multi_search_relation, reader: true

    attr_reader :sources

    attr_reader :multi_search

    def initialize(*)
      super
      @multi_search =
        if self.class.multi_search_relation
          es.relations[self.class.multi_search_relation]
        else
          es.relations[:multi]
        end
      @sources = self.class.sources.each_with_object({}) { |name, h|
        h[name] = db.relations[name].with(auto_struct: true).struct_namespace(Search::Entities).as_search_results
      }
    end

    def call(query, page: 1, per_page: 10)
      multi_search.
        by_simple_query_string(query).
        map_to(Search::Entities::Hit).
        per_page(per_page).
        page(page).
        load_results(sources)
    end
  end
end
