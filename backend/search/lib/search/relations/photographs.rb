require "search/search_types"
require "search/field_configurations"
require "search/index_settings"

module Search
  module Relations
    class Photographs < ROM::Relation[:elasticsearch]
      include IndexSettings

      schema(:photographs) do
        attribute :annotations, Types::String.meta(FieldConfigurations::Text)
        attribute :damaged, Types::Bool
        attribute :geo_located_at, Search::SearchTypes::Time.meta(FieldConfigurations::Time)
        attribute :geo_location, Types::String.meta(FieldConfigurations::GeoLocation)
        attribute :house_name, Types::String.meta(FieldConfigurations::Text)
        attribute :human_tagged_at, Search::SearchTypes::Time.meta(FieldConfigurations::Time)
        attribute :id, Types::ID
        attribute :locality_id, Types::Integer
        attribute :locality_name, Types::String.meta(FieldConfigurations::KeywordText)
        attribute :postcode, Types::String.meta(FieldConfigurations::KeywordText)
        attribute :shop, Types::Bool
        attribute :slq_identifier, Types::String.meta(FieldConfigurations::KeywordText)
        attribute :spool_content_description, Types::String.meta(FieldConfigurations::Text)
        attribute :spool_id, Types::Integer
        attribute :spool_slq_identifier, Types::String.meta(FieldConfigurations::KeywordText)
        attribute :storied_at, Search::SearchTypes::Time.meta(FieldConfigurations::Time)
        attribute :street_address, Types::String.meta(FieldConfigurations::Text)
        attribute :street_name, Types::String.meta(FieldConfigurations::KeywordText)
        attribute :street_number, Types::String.meta(FieldConfigurations::KeywordText)
        attribute :tag_ids, Types::Integer
        attribute :tag_names, Types::Array.meta(FieldConfigurations::KeywordText)
      end

      use :query_dsl

      STANDARD_FIELDS = %w[
        locality_name^3
        postcode
        postcode.keyword
        slq_identifier^3
        slq_identifier.keyword
        spool_content_description
        spool_slq_identifier
        spool_slq_identifier.keyword
        street_address^3
        street_name^3
        street_number^3
        tag_names^3
        annotations^3
        house_name^3
      ].freeze

      def by_simple_query_string(search_query)
        search do
          query do
            simple_query_string do
              query(search_query[:q])
            end
          end
        end
      end

      def for_index(options = {}) # rubocop:disable Metrics/AbcSize, Metrics/PerceivedComplexity, Metrics/CyclomaticComplexity
        search do
          query do
            bool do
              must do
                if options[:query]
                  simple_query_string do
                    query(options[:query])
                    fields(STANDARD_FIELDS)
                  end
                else
                  match_all
                end
              end

              filter do
                bool do
                  Array(options[:tag_ids]).each do |tag_id|
                    must do
                      term tag_ids: tag_id
                    end
                  end

                  Array(options[:locality_ids]).each do |locality_id|
                    should do
                      term locality_id: locality_id
                    end
                  end

                  Array(options[:slq_identifiers]).each do |slq_identifier|
                    should do
                      term "slq_identifier.keyword" => slq_identifier
                    end
                  end

                  Array(options[:spool_slq_identifiers]).each do |spool_slq_identifier|
                    should do
                      term "spool_slq_identifier.keyword" => spool_slq_identifier
                    end
                  end

                  if !options[:human_tagged].nil?
                    if options[:human_tagged]
                      must do
                        exists field: :human_tagged_at
                      end
                    else
                      must_not do
                        exists field: :human_tagged_at
                      end
                    end
                  end

                  if !options[:has_geo_location].nil?
                    if options[:has_geo_location]
                      must do
                        exists field: :geo_location
                      end
                    else
                      must_not do
                        exists field: :geo_location
                      end
                    end
                  end

                  if !options[:storied].nil?
                    if options[:storied]
                      must do
                        exists field: :storied_at
                      end
                    else
                      must_not do
                        exists field: :storied_at
                      end
                    end
                  end

                  if !options[:is_shop].nil?
                    must do
                      term shop: options[:is_shop]
                    end
                  end

                  if !options[:is_damaged].nil?
                    must do
                      term damaged: options[:is_damaged]
                    end
                  end

                  if !options[:has_house_name].nil?
                    if options[:has_house_name]
                      must do
                        exists field: :house_name
                      end
                    else
                      must_not do
                        exists field: :house_name
                      end
                    end
                  end

                  if !options[:has_annotations].nil?
                    if options[:has_annotations]
                      must do
                        exists field: :annotations
                      end
                    else
                      must_not do
                        exists field: :annotations
                      end
                    end
                  end
                end
              end
            end
          end

          aggregation :tag_ids do
            terms field: "tag_ids", size: 20, exclude: Array(options[:tag_ids])
          end

          aggregation :locality_ids do
            terms field: "locality_id", exclude: Array(options[:locality_ids])
          end

          aggregation :spool_slq_identifiers do
            terms field: "spool_slq_identifier.keyword", exclude: Array(options[:spool_slq_identifiers])
          end

          if options[:order_by].nil?
            sort do
              by "_score"
              by :id
            end
          else
            sort do
              by options[:order_by], order: options[:order_by_direction] || "asc"
            end
          end
        end
      end

      def tag_cloud
        search do
          query do
            match_all
          end

          size 0

          aggregation :tag_ids do
            terms field: "tag_ids", size: 100_000
          end
        end
      end

      def index(photograph)
        command(:create).call(photograph.to_h)
      end
    end
  end
end
