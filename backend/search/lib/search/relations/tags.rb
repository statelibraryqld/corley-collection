require "search/search_types"
require "search/field_configurations"
require "search/index_settings"

module Search
  module Relations
    class Tags < ROM::Relation[:elasticsearch]
      include IndexSettings

      schema(:tags) do
        attribute :id, Types::ID
        attribute :name, Types::String.meta(FieldConfigurations::KeywordText)
        attribute :type, Types::String.meta(FieldConfigurations::KeywordText)
        attribute :source, Types::String.meta(FieldConfigurations::KeywordText)
      end

      use :query_dsl

      def by_simple_query_string(search_query)
        search do
          query do
            simple_query_string do
              query(search_query[:q])
            end
          end
        end
      end

      def default_listing
        search do
          query do
            match_all
          end

          sort do
            by "name.keyword"
          end
        end
      end

      def index(tag)
        command(:create).call(tag.to_h)
      end
    end
  end
end
