require "corley_collection/repository"
require "search/entities"

module Search
  class Repository < CorleyCollection::Repository
    struct_namespace Search::Entities
  end
end
