require "rom/relation/loaded"
require "corley_collection/pager"

module Search
  class Results < ROM::Relation::Loaded
    attr_reader :hits

    attr_reader :pager

    def initialize(*)
      super
      @hits = source.to_a
      @collection = sort_by { |item| hits.index(hits.detect { |e| e[:id] == item.id }) }
      @pager = CorleyCollection::Pager.new(total: total, per_page: per_page, current_page: current_page)
    end

    def total_hits
      source.total_hits.to_i
    end
    alias_method :total, :total_hits

    def total_pages
      pages = total_hits.to_f / per_page

      if (pages % 1).zero?
        pages
      else
        pages + 1
      end.to_i
    end

    def next_page
      num = current_page + 1
      num if total_pages >= num
    end

    def prev_page
      num = current_page - 1
      num if num > 0
    end

    def per_page
      source.source.per_page
    end

    def current_page
      source.source.current_page
    end
  end
end
