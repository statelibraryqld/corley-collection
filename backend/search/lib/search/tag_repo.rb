require "search/repository"

module Search
  class TagRepo < Repository[:tags]
    def [](id)
      tags.by_pk(id).one
    end
  end
end
