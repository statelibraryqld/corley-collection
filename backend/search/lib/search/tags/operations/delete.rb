require "search/operations/delete"
require "search/import"

module Search
  module Tags
    module Operations
      class Delete < Search::Operations::Delete
        configure do |config|
          config.es_relation = :tags
        end
      end
    end
  end
end
