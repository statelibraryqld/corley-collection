require "search/operations/index"
require "search/import"

module Search
  module Tags
    module Operations
      class Index < Search::Operations::Index
        configure do |config|
          config.es_relation = :tags
        end

        include Import[repository: "tag_repo"]
      end
    end
  end
end
