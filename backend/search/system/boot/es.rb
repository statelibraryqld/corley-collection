Search::Container.boot :es, namespace: true do |container|
  init do
    require "rom"
    require "rom/elasticsearch/plugins/relation/query_dsl"

    rom_config = ROM::Configuration.new(:elasticsearch, container[:settings].es_url)

    register(:config, rom_config)
  end

  start do
    config = container["es.config"]
    config.auto_registration(container.root.join("lib/search"))

    register :rom, ROM.container(config)
  end
end
