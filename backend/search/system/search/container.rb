require "pathname"
require "dry/web/container"

module Search
  class Container < Dry::Web::Container
    require root.join("system/corley_collection/container")
    import core: CorleyCollection::Container

    require "corley_collection/resolver"

    configure do |config|
      config.resolver = CorleyCollection::Resolver.new(config.env)
      config.root = Pathname(__FILE__).join("../..").realpath.dirname.freeze
      config.logger = CorleyCollection::Container[:logger]
      config.default_namespace = "search"
      config.auto_register = %w[lib/search]
    end

    load_paths! "lib"

    boot(:settings, from: :system) do
      before(:init) do
        require "types"
      end

      settings do
        key :es_url, Types::Strict::String.constrained(filled: true)
      end
    end
  end
end
