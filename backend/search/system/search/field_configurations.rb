module Search
  # rubocop:disable Naming/ConstantName
  module FieldConfigurations
    Text = {
      type: "text",
      analyzer: "standard_english",
      fields: {
        exact: {type: "text", analyzer: "exact_english"}
      }
    }.freeze

    KeywordText = {
      type: "text",
      analyzer: "standard_english",
      fields: {
        exact: {type: "text", analyzer: "exact_english"},
        keyword: {type: "keyword"}
      }
    }.freeze

    Time = {
      type: "date"
    }.freeze

    Tags = {
      type: "keyword"
    }.freeze

    GeoLocation = {
      type: "geo_point"
    }.freeze
  end
  # rubocop:enable Naming/ConstantName
end
