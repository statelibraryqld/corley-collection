module Search
  module IndexSettings
    INDEX_SETTINGS = {
      number_of_shards: 1,
      index: {
        analysis: {
          analyzer: {
            standard_english: {
              type: "custom",
              tokenizer: "standard",
              char_filter: %w[
                strip_dots
              ],
              filter: %w[
                standard
                lowercase
                apostrophe
                asciifolding_with_original
                stopwords_english
                snowball_english
              ],
            },
            exact_english: {
              type: "custom",
              tokenizer: "standard",
              filter: %w[
                standard
                lowercase
              ]
            },
            ngram: {
              tokenizer: "ngram_tokenizer",
              filter: ["lowercase"]
            }
          },
          char_filter: {
            strip_dots: {
              type: "mapping",
              mappings: [".=>"],
            }
          },
          filter: {
            stopwords_english: {
              type: "stop",
              stopwords: "_english_",
            },
            snowball_english: {
              type: "snowball",
              language: "English",
            },
            asciifolding_with_original: {
              type: "asciifolding",
              preserve_original: true,
            },
          },
          tokenizer: {
            ngram_tokenizer: {
              type: "ngram",
              min_gram: 3,
              max_gram: 3,
              token_chars: [
                "letter",
                "digit"
              ]
            }
          }
        }
      }
    }.freeze

    def self.included(base)
      base.index_settings(INDEX_SETTINGS)
    end
  end
end
