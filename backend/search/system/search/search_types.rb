require "types"
require "search/container"

module Search
  module SearchTypes
    include ::Types

    Time = Types::Strict::String.constructor { |input| input.iso8601 if input.respond_to?(:iso8601) }.optional
  end
end
