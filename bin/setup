#!/usr/bin/env ruby

# setup script - sets the project up in its initial state. Run after initial clone.

# This script:
#  - calls bootstrap to acquire any new dependencies
#  - creates a .env file based on .env.example
#  - creates development and test databases
#  - seeds the development database
#  - reindexes elasticsearch

require_relative "script_helpers"

in_app_root setup: :bootstrap do
  FileUtils.cp ".example.env", ".env" unless File.exist?(".env")

  run "db create"
  run "db create", env: :test

  run "db reset"
  run "db reset", env: :test

  run "db seed"

  task "search:reset"
end
