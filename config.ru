require "rack/ssl"
require "rack/rewrite"

require_relative "system/boot"
run CorleyCollection::Application.freeze.app

use Rack::SSL if ENV["RACK_ENV"] == "production"
