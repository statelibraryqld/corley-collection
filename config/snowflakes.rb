require "snowflakes/config"

Snowflakes.configure do |c|
  c.system = :corley_collection
  c.system_dir = "system"
end
