require_relative "../../system/corley_collection/import"
require_relative "../../spec/support/db/factory"
require_relative "../../apps/admin/lib/admin/entities"

class Seed
  include CorleyCollection::Import["persistence.rom"]

  def create(factory, attrs) # rubocop:disable Metrics/PerceivedComplexity
    relation = Factory.registry[factory].relation

    existing = unique_indices(relation).reduce(relation) { |a, e|
      e.reduce(a) { |r, attr| attrs.key?(attr) ? r.where(r[attr].is(attrs[attr])) : r }
    }

    if existing.count.zero? || existing.dataset.opts[:where].nil?
      result = Factory.struct_namespace(Admin::Entities)[factory, attrs]
      puts "=> created #{factory.inspect} #{result.id if result.respond_to?(:id)}"
    else
      changeset = existing.changeset(:update, attrs)

      if changeset.diff?
        result = changeset.commit
        puts "=> updated #{factory.inspect} #{result.id if result.respond_to?(:id)}"
      else
        result = changeset.original
      end
    end

    result
  end

  def relations
    rom.relations
  end

  def unique_indices(relation)
    relation.schema.indexes.select(&:unique?).map { |idx| idx.attributes.map(&:name) } +
      [relation.schema.select(&:foreign_key?).map(&:name)]
  end
end
