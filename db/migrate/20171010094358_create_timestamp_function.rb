ROM::SQL.migration do
  up do
    execute <<-SQL
     CREATE OR REPLACE FUNCTION update_updated_at_column()
       RETURNS TRIGGER AS $$
         BEGIN
          NEW.updated_at = (now() at time zone 'utc');
          RETURN NEW;
         END;
     $$ language 'plpgsql';
    SQL
  end

  down do
    execute("DROP FUNCTION IF EXISTS update_updated_at_column();")
  end
end
