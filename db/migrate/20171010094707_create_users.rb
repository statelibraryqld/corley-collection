ROM::SQL.migration do
  up do
    create_table(:users) do
      primary_key :id
      column :name, String, null: false
      column :email, String, null: false, unique: true, index: true
      column :encrypted_password, String, null: false
      column :password_reset_token, String, unique: true, index: true
      column :password_reset_token_expires_at, DateTime
      column :created_at, DateTime, null: false, default: Sequel.lit("(now() at time zone 'utc')")
      column :updated_at, DateTime, null: false, default: Sequel.lit("(now() at time zone 'utc')")
    end

    sql = <<-SQL
      CREATE TRIGGER update_users_updated_at BEFORE UPDATE
      ON users FOR EACH ROW EXECUTE PROCEDURE
      update_updated_at_column();
    SQL

    run sql
  end

  down do
    sql = <<-SQL
      ALTER TABLE users
      ALTER COLUMN created_at
      DROP DEFAULT;

      DROP TRIGGER IF EXISTS update_users_updated_at ON users;
    SQL

    run sql

    drop_table :users
  end
end
