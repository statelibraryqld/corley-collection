ROM::SQL.migration do
  change do
    CorleyCollection::Container.start :que
    Que.migrate! version: 3
  end
end
