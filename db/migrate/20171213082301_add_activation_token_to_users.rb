ROM::SQL.migration do
  change do
    alter_table :users do
      add_column :activation_token, String, unique: true, index: true
    end
  end
end
