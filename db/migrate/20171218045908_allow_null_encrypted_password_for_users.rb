ROM::SQL.migration do
  change do
    alter_table :users do
      set_column_allow_null :encrypted_password
    end
  end
end
