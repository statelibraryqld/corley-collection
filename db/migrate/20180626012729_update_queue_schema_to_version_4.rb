ROM::SQL.migration do
  up do
    CorleyCollection::Container.start :que
    Que.migrate! version: 4
  end

  down do
    CorleyCollection::Container.start :que
    Que.migrate! version: 3
  end
end
