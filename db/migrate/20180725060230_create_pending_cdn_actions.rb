ROM::SQL.migration do
  up do
    create_table(:pending_cdn_actions) do
      primary_key :id
      column :path, String, null: false
      column :type, String, null: false, index: true
      column :created_at, DateTime, null: false, default: Sequel.lit("(now() at time zone 'utc')")

      constraint :check_type, type: %w[prefetch purge]
    end
  end

  down do
    drop_table :pending_cdn_actions
  end
end
