ROM::SQL.migration do
  up do
    create_table :spools do
      primary_key :id
      column :slq_identifier, String, null: false, unique: true, index: true
      column :content_description, String
      column :photograph_count, Integer
      column :created_at, DateTime, null: false, default: Sequel.lit("(now() at time zone 'utc')")
      column :updated_at, DateTime, null: false, default: Sequel.lit("(now() at time zone 'utc')")
    end

    sql = <<-SQL
      CREATE TRIGGER update_spools_updated_at BEFORE UPDATE
      ON spools FOR EACH ROW EXECUTE PROCEDURE
      update_updated_at_column();
    SQL

    run sql
  end

  down do
    sql = <<-SQL
      ALTER TABLE spools
      ALTER COLUMN created_at
      DROP DEFAULT;

      DROP TRIGGER IF EXISTS update_spools_updated_at ON spools;
    SQL

    run sql

    drop_table :spools
  end
end
