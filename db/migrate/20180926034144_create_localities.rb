ROM::SQL.migration do
  change do
    create_table :localities do
      primary_key :id
      column :name, String, null: false
      column :latitude, :float
      column :longitude, :float
    end
  end
end
