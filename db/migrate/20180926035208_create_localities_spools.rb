ROM::SQL.migration do
  change do
    create_table :localities_spools do
      foreign_key :locality_id, :localities, null: false, on_delete: :cascade
      foreign_key :spool_id, :spools, null: false, on_delete: :cascade
      primary_key [:locality_id, :spool_id]
    end
  end
end
