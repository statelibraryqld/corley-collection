ROM::SQL.migration do
  up do
    create_table :photographs do
      primary_key :id
      column :slq_identifier, String, null: false, unique: true, index: true
      foreign_key :spool_id, :spools, on_delete: :set_null, index: true
      column :street_number, String
      column :street_name, String
      foreign_key :locality_id, :localities, on_delete: :set_null
      column :latitude, :float
      column :longitude, :float
      column :created_at, DateTime, null: false, default: Sequel.lit("(now() at time zone 'utc')")
      column :updated_at, DateTime, null: false, default: Sequel.lit("(now() at time zone 'utc')")
    end

    sql = <<-SQL
      CREATE TRIGGER update_photographs_updated_at BEFORE UPDATE
      ON photographs FOR EACH ROW EXECUTE PROCEDURE
      update_updated_at_column();
    SQL

    run sql
  end

  down do
    sql = <<-SQL
      ALTER TABLE photographs
      ALTER COLUMN created_at
      DROP DEFAULT;

      DROP TRIGGER IF EXISTS update_photographs_updated_at ON photographs;
    SQL

    run sql

    drop_table :photographs
  end
end
