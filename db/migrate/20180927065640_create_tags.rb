ROM::SQL.migration do
  up do
    create_enum(:tag_source, %w(human machine))

    create_table :tags do
      primary_key :id
      column :name, String, null: false
      column :type, String, index: true
      column :source, :tag_source, null: false, index: true, default: "human"
      column :created_at, DateTime, null: false, index: true, default: Sequel.lit("(now() at time zone 'utc')")
      index [:name, :type, :source], unique: true
    end
  end

  down do
    sql = <<-SQL
      ALTER TABLE tags
      ALTER COLUMN created_at
      DROP DEFAULT;
    SQL

    run sql

    drop_column :tags, :source
    drop_enum :tag_source
    drop_table :tags
  end
end
