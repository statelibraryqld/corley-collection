ROM::SQL.migration do
  change do
    create_table :photographs_tags do
      foreign_key :photograph_id, :photographs, on_delete: :cascade, null: false, index: true
      foreign_key :tag_id, :tags, on_delete: :cascade, null: false, index: true
      primary_key [:photograph_id, :tag_id]
    end
  end
end
