ROM::SQL.migration do
  change do
    alter_table :photographs do
      add_column :human_tagged_at, DateTime, index: true
    end
  end
end
