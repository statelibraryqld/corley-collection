ROM::SQL.migration do
  up do
    create_table(:contributors) do
      primary_key :id
      column :name, String, null: false
      column :email, String, null: false, unique: true, index: true
      column :encrypted_password, String, null: false
      column :password_reset_token, String, unique: true, index: true
      column :password_reset_token_expires_at, DateTime
      column :suspended, :boolean, null: false, default: false
      column :created_at, DateTime, null: false, default: Sequel.lit("(now() at time zone 'utc')")
      column :updated_at, DateTime, null: false, default: Sequel.lit("(now() at time zone 'utc')")
    end

    sql = <<-SQL
      CREATE TRIGGER update_contributors_updated_at BEFORE UPDATE
      ON contributors FOR EACH ROW EXECUTE PROCEDURE
      update_updated_at_column();
    SQL

    run sql
  end

  down do
    sql = <<-SQL
      ALTER TABLE contributors
      ALTER COLUMN created_at
      DROP DEFAULT;

      DROP TRIGGER IF EXISTS update_contributors_updated_at ON contributors;
    SQL

    run sql

    drop_table :contributors
  end
end
