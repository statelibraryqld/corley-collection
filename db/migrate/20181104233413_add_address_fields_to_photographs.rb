ROM::SQL.migration do
  change do
    alter_table :photographs do
      add_column :street_type, :text
      add_column :street_address, :text
      add_column :postcode, :text
    end
  end
end
