ROM::SQL.migration do
  change do
    alter_table :photographs do
      add_column :geo_located_at, DateTime, index: true
    end
  end
end
