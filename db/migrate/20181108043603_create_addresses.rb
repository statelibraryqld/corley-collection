ROM::SQL.migration do
  up do
    create_table(:addresses) do
      primary_key :id
      column :street_address, :text, null: false
      column :latitude, :float
      column :longitude, :float
      foreign_key :spool_id, :spools, on_delete: :cascade, index: true
      column :created_at, DateTime, null: false, default: Sequel.lit("(now() at time zone 'utc')")
      column :updated_at, DateTime, null: false, default: Sequel.lit("(now() at time zone 'utc')")
    end

    sql = <<-SQL
      CREATE TRIGGER update_addresses_updated_at BEFORE UPDATE
      ON addresses FOR EACH ROW EXECUTE PROCEDURE
      update_updated_at_column();
    SQL

    run sql
  end

  down do
    sql = <<-SQL
      ALTER TABLE addresses
      ALTER COLUMN created_at
      DROP DEFAULT;

      DROP TRIGGER IF EXISTS update_addresses_updated_at ON addresses;
    SQL

    run sql

    drop_table :addresses
  end
end
