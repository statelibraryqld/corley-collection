ROM::SQL.migration do
  change do
    alter_table :photographs do
      add_column :house_name, :text
      add_column :annotations, :text
      add_column :damaged, :boolean, default: false
      add_column :shop, :boolean, default: false
    end
  end
end
