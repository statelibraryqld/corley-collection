ROM::SQL.migration do
  change do
    create_table(:favourites) do
      primary_key :id
      foreign_key :photograph_id, :photographs, on_delete: :cascade, index: true, null: false
      foreign_key :contributor_id, :contributors, on_delete: :set_null, index: true
      column :created_at, DateTime, null: false, default: Sequel.lit("(now() at time zone 'utc')")
      index [:photograph_id, :contributor_id], unique: true
    end
  end
end
