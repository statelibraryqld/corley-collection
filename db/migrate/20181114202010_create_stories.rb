ROM::SQL.migration do
  up do
    create_table(:stories) do
      primary_key :id
      foreign_key :photograph_id, :photographs, on_delete: :cascade, index: true, null: false
      foreign_key :contributor_id, :contributors, on_delete: :cascade, index: true, null: false
      column :body, :text, null: false
      column :published, :boolean, null: false, default: true
      column :created_at, DateTime, null: false, default: Sequel.lit("(now() at time zone 'utc')")
      column :updated_at, DateTime, null: false, default: Sequel.lit("(now() at time zone 'utc')")
    end

    sql = <<-SQL
      CREATE TRIGGER update_stories_updated_at BEFORE UPDATE
      ON stories FOR EACH ROW EXECUTE PROCEDURE
      update_updated_at_column();
    SQL

    run sql
  end

  down do
    sql = <<-SQL
      ALTER TABLE stories
      ALTER COLUMN created_at
      DROP DEFAULT;

      DROP TRIGGER IF EXISTS update_stories_updated_at ON stories;
    SQL

    run sql

    drop_table :stories
  end
end
