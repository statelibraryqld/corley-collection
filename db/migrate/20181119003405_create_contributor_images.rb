ROM::SQL.migration do
  up do
    create_table(:contributor_images) do
      primary_key :id
      foreign_key :story_id, :stories, on_delete: :cascade, index: true, null: false
      foreign_key :contributor_id, :contributors, on_delete: :cascade, index: true, null: false
      column :caption, :text, null: false
      column :published, :boolean, null: false, default: true
      column :path, :text, null: false
      column :content_type, :text
      column :size, BigDecimal
      column :created_at, DateTime, null: false, default: Sequel.lit("(now() at time zone 'utc')")
      column :updated_at, DateTime, null: false, default: Sequel.lit("(now() at time zone 'utc')")
    end

    sql = <<-SQL
      CREATE TRIGGER update_contributor_images_updated_at BEFORE UPDATE
      ON contributor_images FOR EACH ROW EXECUTE PROCEDURE
      update_updated_at_column();
    SQL

    run sql
  end

  down do
    sql = <<-SQL
      ALTER TABLE contributor_images
      ALTER COLUMN created_at
      DROP DEFAULT;

      DROP TRIGGER IF EXISTS update_contributor_images_updated_at ON contributor_images;
    SQL

    run sql

    drop_table :contributor_images
  end
end
