ROM::SQL.migration do
  change do
    alter_table :photographs do
      add_column :similar_photographs, :json, null: false, default: "[]"
    end
  end
end
