ROM::SQL.migration do
  change do
    alter_table :contributors do
      rename_column :name, :first_name
      add_column :last_name, :text
    end
  end
end
