ROM::SQL.migration do
  up do
    create_table :page_cache_checks do
      primary_key :id
      column :path, :text, null: false, unique: true, index: true
      column :checksum, :text
      column :created_at, :timestamp, null: false, default: Sequel.lit("(now() at time zone 'utc')")
      column :updated_at, :timestamp, null: false, index: true, default: Sequel.lit("(now() at time zone 'utc')")
    end

    sql = <<-SQL
      CREATE TRIGGER touch_updated_at BEFORE UPDATE
      ON page_cache_checks FOR EACH ROW EXECUTE PROCEDURE
      update_updated_at_column();
    SQL

    run sql
  end

  down do
    sql = <<-SQL
      DROP TRIGGER IF EXISTS touch_updated_at ON page_cache_checks;
    SQL

    run sql

    drop_table :page_cache_checks
  end
end
