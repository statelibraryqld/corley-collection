ROM::SQL.migration do
  up do
    sql = <<~SQL
      CREATE VIEW enqueued_page_cache_checks AS
        SELECT args->>0 AS path
        FROM que_jobs
        WHERE job_class = 'Cdn::Operations::CheckPage::Job'
    SQL

    run sql

    sql = <<~SQL
      COMMENT ON VIEW enqueued_page_cache_checks
        IS 'A simple list of paths with enqueued page cache check jobs'
    SQL

    run sql
  end

  down do
    sql = <<~SQL
      DROP VIEW enqueued_page_cache_checks
    SQL

    run sql
  end
end
