ROM::SQL.migration do
  up do
    sql = <<~SQL
      CREATE VIEW created_contributor_images_by_month AS
        WITH
          range_values AS (
            SELECT date_trunc('month', min(created_at)) as minval,
                  date_trunc('month', now()) as maxval
            FROM contributor_images
          ),
          month_range AS (
            SELECT generate_series(minval, maxval, '1 month'::interval) as month
            FROM range_values
          ),
          monthly_counts AS (
            SELECT date_trunc('month', created_at) as month,
                   count(*) as count
            FROM contributor_images
            GROUP BY month
          )

        SELECT month_range.month as month,
               to_char(month_range.month, 'YYYY Mon') as formatted_month,
               coalesce(monthly_counts.count, 0) as count
        FROM month_range
        LEFT OUTER JOIN monthly_counts on month_range.month = monthly_counts.month
        ORDER BY month DESC;
    SQL

    run sql
  end

  down do
    sql = <<~SQL
      DROP VIEW created_contributor_images_by_month
    SQL

    run sql
  end
end
