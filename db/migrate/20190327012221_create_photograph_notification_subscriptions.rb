ROM::SQL.migration do
  change do
    create_table :photograph_notification_subscriptions do
      primary_key :id
      foreign_key :photograph_id, :photographs, null: false, on_delete: :cascade
      foreign_key :contributor_id, :contributors, null: false, on_delete: :cascade
      column :token, :text, null: false, unique: true
      column :created_at, DateTime, null: false, default: Sequel.lit("(now() at time zone 'utc')")
      index [:photograph_id, :contributor_id], unique: true
    end
  end
end
