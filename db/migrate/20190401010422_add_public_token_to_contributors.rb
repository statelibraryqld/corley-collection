ROM::SQL.migration do
  change do
    alter_table :contributors do
      add_column :public_token, :text, null: false, unique: true, default: Sequel.lit("(md5(random()::text))")
    end
  end
end
