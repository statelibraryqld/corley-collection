ROM::SQL.migration do
  up do
    sql = <<~SQL
      CREATE VIEW recent_active_localities AS
        SELECT locality_id, count(*) as story_count
        FROM stories
        JOIN photographs ON stories.photograph_id = photographs.id
        WHERE stories.created_at >= CURRENT_DATE - 7
          AND photographs.locality_id is not NULL
        GROUP BY locality_id
        ORDER BY story_count DESC;
    SQL

    run sql
  end

  down do
    sql = <<~SQL
      DROP VIEW recent_active_localities;
    SQL

    run sql
  end
end
