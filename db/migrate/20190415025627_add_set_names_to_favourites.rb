ROM::SQL.migration do
  change do
    alter_table :favourites do
      add_column :set_names, :json, null: false, default: "[]"
    end
  end
end
