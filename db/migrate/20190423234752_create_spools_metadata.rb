ROM::SQL.migration do
  up do
    sql = <<~SQL
      CREATE VIEW spools_metadata AS
        WITH
          spools_geo_located_count AS (
            SELECT spool_id, count(*) as geo_located_photographs_count
            FROM photographs
            WHERE photographs.geo_located_at is not null
            GROUP BY spool_id
          )

        SELECT spools.id as spool_id, COALESCE(geo_located_photographs_count, 0) as geo_located_photograph_count
        FROM spools
        LEFT JOIN spools_geo_located_count
          ON spools.id = spools_geo_located_count.spool_id
        ORDER BY spool_id
    SQL

    run sql
  end

  down do
    sql = <<~SQL
      DROP VIEW spools_metadata
    SQL

    run sql
  end
end
