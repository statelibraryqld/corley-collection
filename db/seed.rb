require "date"
require_relative "./lib/seed"
require_relative "../system/corley_collection/container"
require_relative "../apps/admin/system/admin/container"
require "time_math"
require "babosa"

seed = Seed.new
settings = CorleyCollection::Container.settings

# Admin user
admin_user = seed.create(:user, email: "admin@#{settings.app_domain}")

# Contributor
contributor = seed.create(:contributor, email: "contributor@#{settings.app_domain}")

# Localities
localities = []

20.times do |i|
  localities << seed.create(:locality, name: "Locality #{i}")
end

# Spools
spools = []

30.times do |i|
  spools << seed.create(:spool, slq_identifier: "1-#{i}")

  seed.create(:locality_spool, locality: localities.sample, spool: spools.last)
end

640.times do |i|
  spool = spools.sample

  seed.create(:photograph, slq_identifier: "#{spool.slq_identifier}-#{i}", spool_id: spool.id, locality: nil)
end
