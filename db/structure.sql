--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.9
-- Dumped by pg_dump version 9.6.9

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: pg_stat_statements; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS pg_stat_statements WITH SCHEMA public;


--
-- Name: EXTENSION pg_stat_statements; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION pg_stat_statements IS 'track execution statistics of all SQL statements executed';


--
-- Name: tag_source; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.tag_source AS ENUM (
    'human',
    'machine'
);


--
-- Name: que_validate_tags(jsonb); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.que_validate_tags(tags_array jsonb) RETURNS boolean
    LANGUAGE sql
    AS $$
  SELECT bool_and(
    jsonb_typeof(value) = 'string'
    AND
    char_length(value::text) <= 100
  )
  FROM jsonb_array_elements(tags_array)
$$;


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: que_jobs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.que_jobs (
    priority smallint DEFAULT 100 NOT NULL,
    run_at timestamp with time zone DEFAULT now() NOT NULL,
    id bigint NOT NULL,
    job_class text NOT NULL,
    error_count integer DEFAULT 0 NOT NULL,
    last_error_message text,
    queue text DEFAULT 'default'::text NOT NULL,
    last_error_backtrace text,
    finished_at timestamp with time zone,
    expired_at timestamp with time zone,
    args jsonb DEFAULT '[]'::jsonb NOT NULL,
    data jsonb DEFAULT '{}'::jsonb NOT NULL,
    CONSTRAINT error_length CHECK (((char_length(last_error_message) <= 500) AND (char_length(last_error_backtrace) <= 10000))),
    CONSTRAINT job_class_length CHECK ((char_length(
CASE job_class
    WHEN 'ActiveJob::QueueAdapters::QueAdapter::JobWrapper'::text THEN ((args -> 0) ->> 'job_class'::text)
    ELSE job_class
END) <= 200)),
    CONSTRAINT queue_length CHECK ((char_length(queue) <= 100)),
    CONSTRAINT valid_args CHECK ((jsonb_typeof(args) = 'array'::text)),
    CONSTRAINT valid_data CHECK (((jsonb_typeof(data) = 'object'::text) AND ((NOT (data ? 'tags'::text)) OR ((jsonb_typeof((data -> 'tags'::text)) = 'array'::text) AND (jsonb_array_length((data -> 'tags'::text)) <= 5) AND public.que_validate_tags((data -> 'tags'::text))))))
)
WITH (fillfactor='90');


--
-- Name: TABLE que_jobs; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.que_jobs IS '4';


--
-- Name: que_determine_job_state(public.que_jobs); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.que_determine_job_state(job public.que_jobs) RETURNS text
    LANGUAGE sql
    AS $$
  SELECT
    CASE
    WHEN job.expired_at  IS NOT NULL    THEN 'expired'
    WHEN job.finished_at IS NOT NULL    THEN 'finished'
    WHEN job.error_count > 0            THEN 'errored'
    WHEN job.run_at > CURRENT_TIMESTAMP THEN 'scheduled'
    ELSE                                     'ready'
    END
$$;


--
-- Name: que_job_notify(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.que_job_notify() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
  DECLARE
    locker_pid integer;
    sort_key json;
  BEGIN
    -- Don't do anything if the job is scheduled for a future time.
    IF NEW.run_at IS NOT NULL AND NEW.run_at > now() THEN
      RETURN null;
    END IF;

    -- Pick a locker to notify of the job's insertion, weighted by their number
    -- of workers. Should bounce pseudorandomly between lockers on each
    -- invocation, hence the md5-ordering, but still touch each one equally,
    -- hence the modulo using the job_id.
    SELECT pid
    INTO locker_pid
    FROM (
      SELECT *, last_value(row_number) OVER () + 1 AS count
      FROM (
        SELECT *, row_number() OVER () - 1 AS row_number
        FROM (
          SELECT *
          FROM public.que_lockers ql, generate_series(1, ql.worker_count) AS id
          WHERE listening AND queues @> ARRAY[NEW.queue]
          ORDER BY md5(pid::text || id::text)
        ) t1
      ) t2
    ) t3
    WHERE NEW.id % count = row_number;

    IF locker_pid IS NOT NULL THEN
      -- There's a size limit to what can be broadcast via LISTEN/NOTIFY, so
      -- rather than throw errors when someone enqueues a big job, just
      -- broadcast the most pertinent information, and let the locker query for
      -- the record after it's taken the lock. The worker will have to hit the
      -- DB in order to make sure the job is still visible anyway.
      SELECT row_to_json(t)
      INTO sort_key
      FROM (
        SELECT
          'job_available' AS message_type,
          NEW.queue       AS queue,
          NEW.priority    AS priority,
          NEW.id          AS id,
          -- Make sure we output timestamps as UTC ISO 8601
          to_char(NEW.run_at AT TIME ZONE 'UTC', 'YYYY-MM-DD"T"HH24:MI:SS.US"Z"') AS run_at
      ) t;

      PERFORM pg_notify('que_listener_' || locker_pid::text, sort_key::text);
    END IF;

    RETURN null;
  END
$$;


--
-- Name: que_state_notify(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.que_state_notify() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
  DECLARE
    row record;
    message json;
    previous_state text;
    current_state text;
  BEGIN
    IF TG_OP = 'INSERT' THEN
      previous_state := 'nonexistent';
      current_state  := public.que_determine_job_state(NEW);
      row            := NEW;
    ELSIF TG_OP = 'DELETE' THEN
      previous_state := public.que_determine_job_state(OLD);
      current_state  := 'nonexistent';
      row            := OLD;
    ELSIF TG_OP = 'UPDATE' THEN
      previous_state := public.que_determine_job_state(OLD);
      current_state  := public.que_determine_job_state(NEW);

      -- If the state didn't change, short-circuit.
      IF previous_state = current_state THEN
        RETURN null;
      END IF;

      row := NEW;
    ELSE
      RAISE EXCEPTION 'Unrecognized TG_OP: %', TG_OP;
    END IF;

    SELECT row_to_json(t)
    INTO message
    FROM (
      SELECT
        'job_change' AS message_type,
        row.id       AS id,
        row.queue    AS queue,

        coalesce(row.data->'tags', '[]'::jsonb) AS tags,

        to_char(row.run_at AT TIME ZONE 'UTC', 'YYYY-MM-DD"T"HH24:MI:SS.US"Z"') AS run_at,
        to_char(now()      AT TIME ZONE 'UTC', 'YYYY-MM-DD"T"HH24:MI:SS.US"Z"') AS time,

        CASE row.job_class
        WHEN 'ActiveJob::QueueAdapters::QueAdapter::JobWrapper' THEN
          coalesce(
            row.args->0->>'job_class',
            'ActiveJob::QueueAdapters::QueAdapter::JobWrapper'
          )
        ELSE
          row.job_class
        END AS job_class,

        previous_state AS previous_state,
        current_state  AS current_state
    ) t;

    PERFORM pg_notify('que_state', message::text);

    RETURN null;
  END
$$;


--
-- Name: update_updated_at_column(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.update_updated_at_column() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
         BEGIN
          NEW.updated_at = (now() at time zone 'utc');
          RETURN NEW;
         END;
     $$;


--
-- Name: addresses; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.addresses (
    id integer NOT NULL,
    street_address text NOT NULL,
    latitude double precision,
    longitude double precision,
    spool_id integer,
    created_at timestamp without time zone DEFAULT timezone('utc'::text, now()) NOT NULL,
    updated_at timestamp without time zone DEFAULT timezone('utc'::text, now()) NOT NULL
);


--
-- Name: contributor_images; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.contributor_images (
    id integer NOT NULL,
    story_id integer NOT NULL,
    contributor_id integer NOT NULL,
    caption text NOT NULL,
    published boolean DEFAULT true NOT NULL,
    path text NOT NULL,
    content_type text,
    size numeric,
    created_at timestamp without time zone DEFAULT timezone('utc'::text, now()) NOT NULL,
    updated_at timestamp without time zone DEFAULT timezone('utc'::text, now()) NOT NULL
);


--
-- Name: contributors; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.contributors (
    id integer NOT NULL,
    first_name text NOT NULL,
    email text NOT NULL,
    encrypted_password text NOT NULL,
    password_reset_token text,
    password_reset_token_expires_at timestamp without time zone,
    suspended boolean DEFAULT false NOT NULL,
    created_at timestamp without time zone DEFAULT timezone('utc'::text, now()) NOT NULL,
    updated_at timestamp without time zone DEFAULT timezone('utc'::text, now()) NOT NULL,
    last_name text,
    public_token text DEFAULT md5((random())::text) NOT NULL
);


--
-- Name: created_contributor_images_by_month; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.created_contributor_images_by_month AS
 WITH range_values AS (
         SELECT date_trunc('month'::text, min(contributor_images.created_at)) AS minval,
            date_trunc('month'::text, now()) AS maxval
           FROM public.contributor_images
        ), month_range AS (
         SELECT generate_series((range_values.minval)::timestamp with time zone, range_values.maxval, '1 mon'::interval) AS month
           FROM range_values
        ), monthly_counts AS (
         SELECT date_trunc('month'::text, contributor_images.created_at) AS month,
            count(*) AS count
           FROM public.contributor_images
          GROUP BY (date_trunc('month'::text, contributor_images.created_at))
        )
 SELECT month_range.month,
    to_char(month_range.month, 'YYYY Mon'::text) AS formatted_month,
    COALESCE(monthly_counts.count, (0)::bigint) AS count
   FROM (month_range
     LEFT JOIN monthly_counts ON ((month_range.month = monthly_counts.month)))
  ORDER BY month_range.month DESC;


--
-- Name: created_contributors_by_month; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.created_contributors_by_month AS
 WITH range_values AS (
         SELECT date_trunc('month'::text, min(contributors.created_at)) AS minval,
            date_trunc('month'::text, now()) AS maxval
           FROM public.contributors
        ), month_range AS (
         SELECT generate_series((range_values.minval)::timestamp with time zone, range_values.maxval, '1 mon'::interval) AS month
           FROM range_values
        ), monthly_counts AS (
         SELECT date_trunc('month'::text, contributors.created_at) AS month,
            count(*) AS count
           FROM public.contributors
          GROUP BY (date_trunc('month'::text, contributors.created_at))
        )
 SELECT month_range.month,
    to_char(month_range.month, 'YYYY Mon'::text) AS formatted_month,
    COALESCE(monthly_counts.count, (0)::bigint) AS count
   FROM (month_range
     LEFT JOIN monthly_counts ON ((month_range.month = monthly_counts.month)))
  ORDER BY month_range.month DESC;


--
-- Name: stories; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.stories (
    id integer NOT NULL,
    photograph_id integer NOT NULL,
    contributor_id integer NOT NULL,
    body text,
    published boolean DEFAULT true NOT NULL,
    created_at timestamp without time zone DEFAULT timezone('utc'::text, now()) NOT NULL,
    updated_at timestamp without time zone DEFAULT timezone('utc'::text, now()) NOT NULL
);


--
-- Name: created_stories_by_month; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.created_stories_by_month AS
 WITH range_values AS (
         SELECT date_trunc('month'::text, min(stories.created_at)) AS minval,
            date_trunc('month'::text, now()) AS maxval
           FROM public.stories
        ), month_range AS (
         SELECT generate_series((range_values.minval)::timestamp with time zone, range_values.maxval, '1 mon'::interval) AS month
           FROM range_values
        ), monthly_counts AS (
         SELECT date_trunc('month'::text, stories.created_at) AS month,
            count(*) AS count
           FROM public.stories
          GROUP BY (date_trunc('month'::text, stories.created_at))
        )
 SELECT month_range.month,
    to_char(month_range.month, 'YYYY Mon'::text) AS formatted_month,
    COALESCE(monthly_counts.count, (0)::bigint) AS count
   FROM (month_range
     LEFT JOIN monthly_counts ON ((month_range.month = monthly_counts.month)))
  ORDER BY month_range.month DESC;


--
-- Name: enqueued_page_cache_checks; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.enqueued_page_cache_checks AS
 SELECT (que_jobs.args ->> 0) AS path
   FROM public.que_jobs
  WHERE (que_jobs.job_class = 'Cdn::Operations::CheckPage::Job'::text);


--
-- Name: VIEW enqueued_page_cache_checks; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON VIEW public.enqueued_page_cache_checks IS 'A simple list of paths with enqueued page cache check jobs';


--
-- Name: favourites; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.favourites (
    id integer NOT NULL,
    photograph_id integer NOT NULL,
    contributor_id integer,
    created_at timestamp without time zone DEFAULT timezone('utc'::text, now()) NOT NULL,
    set_names json DEFAULT '[]'::json NOT NULL
);


--
-- Name: photographs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.photographs (
    id integer NOT NULL,
    slq_identifier text NOT NULL,
    spool_id integer,
    street_number text,
    street_name text,
    locality_id integer,
    latitude double precision,
    longitude double precision,
    created_at timestamp without time zone DEFAULT timezone('utc'::text, now()) NOT NULL,
    updated_at timestamp without time zone DEFAULT timezone('utc'::text, now()) NOT NULL,
    human_tagged_at timestamp without time zone,
    street_type text,
    street_address text,
    postcode text,
    geo_located_at timestamp without time zone,
    house_name text,
    annotations text,
    damaged boolean DEFAULT false,
    shop boolean DEFAULT false,
    storied_at timestamp without time zone,
    similar_photographs json DEFAULT '[]'::json NOT NULL
);


--
-- Name: geo_located_photographs_by_month; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.geo_located_photographs_by_month AS
 WITH range_values AS (
         SELECT date_trunc('month'::text, min(photographs.geo_located_at)) AS minval,
            date_trunc('month'::text, now()) AS maxval
           FROM public.photographs
        ), month_range AS (
         SELECT generate_series((range_values.minval)::timestamp with time zone, range_values.maxval, '1 mon'::interval) AS month
           FROM range_values
        ), monthly_counts AS (
         SELECT date_trunc('month'::text, photographs.geo_located_at) AS month,
            count(*) AS count
           FROM public.photographs
          GROUP BY (date_trunc('month'::text, photographs.geo_located_at))
        )
 SELECT month_range.month,
    to_char(month_range.month, 'YYYY Mon'::text) AS formatted_month,
    COALESCE(monthly_counts.count, (0)::bigint) AS count
   FROM (month_range
     LEFT JOIN monthly_counts ON ((month_range.month = monthly_counts.month)))
  ORDER BY month_range.month DESC;


--
-- Name: localities; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.localities (
    id integer NOT NULL,
    name text NOT NULL,
    latitude double precision,
    longitude double precision
);


--
-- Name: localities_spools; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.localities_spools (
    locality_id integer NOT NULL,
    spool_id integer NOT NULL
);


--
-- Name: locality_notification_subscriptions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.locality_notification_subscriptions (
    id integer NOT NULL,
    locality_id integer NOT NULL,
    contributor_id integer NOT NULL,
    token text NOT NULL,
    created_at timestamp without time zone DEFAULT timezone('utc'::text, now()) NOT NULL
);


--
-- Name: page_cache_checks; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.page_cache_checks (
    id integer NOT NULL,
    path text NOT NULL,
    checksum text,
    created_at timestamp without time zone DEFAULT timezone('utc'::text, now()) NOT NULL,
    updated_at timestamp without time zone DEFAULT timezone('utc'::text, now()) NOT NULL
);


--
-- Name: pending_cdn_actions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.pending_cdn_actions (
    id integer NOT NULL,
    path text NOT NULL,
    type text NOT NULL,
    created_at timestamp without time zone DEFAULT timezone('utc'::text, now()) NOT NULL,
    CONSTRAINT check_type CHECK ((type = ANY (ARRAY['prefetch'::text, 'purge'::text])))
);


--
-- Name: photograph_notification_subscriptions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.photograph_notification_subscriptions (
    id integer NOT NULL,
    photograph_id integer NOT NULL,
    contributor_id integer NOT NULL,
    token text NOT NULL,
    created_at timestamp without time zone DEFAULT timezone('utc'::text, now()) NOT NULL
);


--
-- Name: photographs_tags; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.photographs_tags (
    photograph_id integer NOT NULL,
    tag_id integer NOT NULL
);


--
-- Name: que_jobs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.que_jobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: que_jobs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.que_jobs_id_seq OWNED BY public.que_jobs.id;


--
-- Name: que_lockers; Type: TABLE; Schema: public; Owner: -
--

CREATE UNLOGGED TABLE public.que_lockers (
    pid integer NOT NULL,
    worker_count integer NOT NULL,
    worker_priorities integer[] NOT NULL,
    ruby_pid integer NOT NULL,
    ruby_hostname text NOT NULL,
    queues text[] NOT NULL,
    listening boolean NOT NULL,
    CONSTRAINT valid_queues CHECK (((array_ndims(queues) = 1) AND (array_length(queues, 1) IS NOT NULL))),
    CONSTRAINT valid_worker_priorities CHECK (((array_ndims(worker_priorities) = 1) AND (array_length(worker_priorities, 1) IS NOT NULL)))
);


--
-- Name: que_values; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.que_values (
    key text NOT NULL,
    value jsonb DEFAULT '{}'::jsonb NOT NULL,
    CONSTRAINT valid_value CHECK ((jsonb_typeof(value) = 'object'::text))
)
WITH (fillfactor='90');


--
-- Name: recent_active_localities; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.recent_active_localities AS
 SELECT photographs.locality_id,
    count(*) AS story_count
   FROM (public.stories
     JOIN public.photographs ON ((stories.photograph_id = photographs.id)))
  WHERE ((stories.created_at >= (('now'::text)::date - 7)) AND (photographs.locality_id IS NOT NULL))
  GROUP BY photographs.locality_id
  ORDER BY (count(*)) DESC;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.schema_migrations (
    filename text NOT NULL
);


--
-- Name: spools; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.spools (
    id integer NOT NULL,
    slq_identifier text NOT NULL,
    content_description text,
    photograph_count integer,
    created_at timestamp without time zone DEFAULT timezone('utc'::text, now()) NOT NULL,
    updated_at timestamp without time zone DEFAULT timezone('utc'::text, now()) NOT NULL
);


--
-- Name: spools_metadata; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.spools_metadata AS
 WITH spools_geo_located_count AS (
         SELECT photographs.spool_id,
            count(*) AS geo_located_photographs_count
           FROM public.photographs
          WHERE (photographs.geo_located_at IS NOT NULL)
          GROUP BY photographs.spool_id
        )
 SELECT spools.id AS spool_id,
    COALESCE(spools_geo_located_count.geo_located_photographs_count, (0)::bigint) AS geo_located_photograph_count
   FROM (public.spools
     LEFT JOIN spools_geo_located_count ON ((spools.id = spools_geo_located_count.spool_id)))
  ORDER BY spools.id;


--
-- Name: storied_photographs_by_month; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.storied_photographs_by_month AS
 WITH range_values AS (
         SELECT date_trunc('month'::text, min(photographs.storied_at)) AS minval,
            date_trunc('month'::text, now()) AS maxval
           FROM public.photographs
        ), month_range AS (
         SELECT generate_series((range_values.minval)::timestamp with time zone, range_values.maxval, '1 mon'::interval) AS month
           FROM range_values
        ), monthly_counts AS (
         SELECT date_trunc('month'::text, photographs.storied_at) AS month,
            count(*) AS count
           FROM public.photographs
          GROUP BY (date_trunc('month'::text, photographs.storied_at))
        )
 SELECT month_range.month,
    to_char(month_range.month, 'YYYY Mon'::text) AS formatted_month,
    COALESCE(monthly_counts.count, (0)::bigint) AS count
   FROM (month_range
     LEFT JOIN monthly_counts ON ((month_range.month = monthly_counts.month)))
  ORDER BY month_range.month DESC;


--
-- Name: tagged_photographs_by_month; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.tagged_photographs_by_month AS
 WITH range_values AS (
         SELECT date_trunc('month'::text, min(photographs.human_tagged_at)) AS minval,
            date_trunc('month'::text, now()) AS maxval
           FROM public.photographs
        ), month_range AS (
         SELECT generate_series((range_values.minval)::timestamp with time zone, range_values.maxval, '1 mon'::interval) AS month
           FROM range_values
        ), monthly_counts AS (
         SELECT date_trunc('month'::text, photographs.human_tagged_at) AS month,
            count(*) AS count
           FROM public.photographs
          GROUP BY (date_trunc('month'::text, photographs.human_tagged_at))
        )
 SELECT month_range.month,
    to_char(month_range.month, 'YYYY Mon'::text) AS formatted_month,
    COALESCE(monthly_counts.count, (0)::bigint) AS count
   FROM (month_range
     LEFT JOIN monthly_counts ON ((month_range.month = monthly_counts.month)))
  ORDER BY month_range.month DESC;


--
-- Name: tags; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.tags (
    id integer NOT NULL,
    name text NOT NULL,
    type text,
    source public.tag_source DEFAULT 'human'::public.tag_source NOT NULL,
    created_at timestamp without time zone DEFAULT timezone('utc'::text, now()) NOT NULL
);


--
-- Name: users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.users (
    id integer NOT NULL,
    name text NOT NULL,
    email text NOT NULL,
    encrypted_password text,
    password_reset_token text,
    password_reset_token_expires_at timestamp without time zone,
    created_at timestamp without time zone DEFAULT timezone('utc'::text, now()) NOT NULL,
    updated_at timestamp without time zone DEFAULT timezone('utc'::text, now()) NOT NULL,
    activation_token text
);


--
-- Name: que_jobs id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.que_jobs ALTER COLUMN id SET DEFAULT nextval('public.que_jobs_id_seq'::regclass);


--
-- Name: addresses addresses_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.addresses
    ADD CONSTRAINT addresses_pkey PRIMARY KEY (id);


--
-- Name: contributor_images contributor_images_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.contributor_images
    ADD CONSTRAINT contributor_images_pkey PRIMARY KEY (id);


--
-- Name: contributors contributors_email_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.contributors
    ADD CONSTRAINT contributors_email_key UNIQUE (email);


--
-- Name: contributors contributors_password_reset_token_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.contributors
    ADD CONSTRAINT contributors_password_reset_token_key UNIQUE (password_reset_token);


--
-- Name: contributors contributors_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.contributors
    ADD CONSTRAINT contributors_pkey PRIMARY KEY (id);


--
-- Name: contributors contributors_public_token_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.contributors
    ADD CONSTRAINT contributors_public_token_key UNIQUE (public_token);


--
-- Name: favourites favourites_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.favourites
    ADD CONSTRAINT favourites_pkey PRIMARY KEY (id);


--
-- Name: localities localities_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.localities
    ADD CONSTRAINT localities_pkey PRIMARY KEY (id);


--
-- Name: localities_spools localities_spools_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.localities_spools
    ADD CONSTRAINT localities_spools_pkey PRIMARY KEY (locality_id, spool_id);


--
-- Name: locality_notification_subscriptions locality_notification_subscriptions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.locality_notification_subscriptions
    ADD CONSTRAINT locality_notification_subscriptions_pkey PRIMARY KEY (id);


--
-- Name: locality_notification_subscriptions locality_notification_subscriptions_token_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.locality_notification_subscriptions
    ADD CONSTRAINT locality_notification_subscriptions_token_key UNIQUE (token);


--
-- Name: page_cache_checks page_cache_checks_path_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.page_cache_checks
    ADD CONSTRAINT page_cache_checks_path_key UNIQUE (path);


--
-- Name: page_cache_checks page_cache_checks_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.page_cache_checks
    ADD CONSTRAINT page_cache_checks_pkey PRIMARY KEY (id);


--
-- Name: pending_cdn_actions pending_cdn_actions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.pending_cdn_actions
    ADD CONSTRAINT pending_cdn_actions_pkey PRIMARY KEY (id);


--
-- Name: photograph_notification_subscriptions photograph_notification_subscriptions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.photograph_notification_subscriptions
    ADD CONSTRAINT photograph_notification_subscriptions_pkey PRIMARY KEY (id);


--
-- Name: photograph_notification_subscriptions photograph_notification_subscriptions_token_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.photograph_notification_subscriptions
    ADD CONSTRAINT photograph_notification_subscriptions_token_key UNIQUE (token);


--
-- Name: photographs photographs_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.photographs
    ADD CONSTRAINT photographs_pkey PRIMARY KEY (id);


--
-- Name: photographs photographs_slq_identifier_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.photographs
    ADD CONSTRAINT photographs_slq_identifier_key UNIQUE (slq_identifier);


--
-- Name: photographs_tags photographs_tags_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.photographs_tags
    ADD CONSTRAINT photographs_tags_pkey PRIMARY KEY (photograph_id, tag_id);


--
-- Name: que_jobs que_jobs_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.que_jobs
    ADD CONSTRAINT que_jobs_pkey PRIMARY KEY (id);


--
-- Name: que_lockers que_lockers_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.que_lockers
    ADD CONSTRAINT que_lockers_pkey PRIMARY KEY (pid);


--
-- Name: que_values que_values_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.que_values
    ADD CONSTRAINT que_values_pkey PRIMARY KEY (key);


--
-- Name: schema_migrations schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (filename);


--
-- Name: spools spools_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.spools
    ADD CONSTRAINT spools_pkey PRIMARY KEY (id);


--
-- Name: spools spools_slq_identifier_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.spools
    ADD CONSTRAINT spools_slq_identifier_key UNIQUE (slq_identifier);


--
-- Name: stories stories_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.stories
    ADD CONSTRAINT stories_pkey PRIMARY KEY (id);


--
-- Name: tags tags_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tags
    ADD CONSTRAINT tags_pkey PRIMARY KEY (id);


--
-- Name: users users_activation_token_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_activation_token_key UNIQUE (activation_token);


--
-- Name: users users_email_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_email_key UNIQUE (email);


--
-- Name: users users_password_reset_token_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_password_reset_token_key UNIQUE (password_reset_token);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: addresses_spool_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX addresses_spool_id_index ON public.addresses USING btree (spool_id);


--
-- Name: contributor_images_contributor_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX contributor_images_contributor_id_index ON public.contributor_images USING btree (contributor_id);


--
-- Name: contributor_images_story_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX contributor_images_story_id_index ON public.contributor_images USING btree (story_id);


--
-- Name: contributors_email_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX contributors_email_index ON public.contributors USING btree (email);


--
-- Name: contributors_password_reset_token_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX contributors_password_reset_token_index ON public.contributors USING btree (password_reset_token);


--
-- Name: favourites_contributor_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX favourites_contributor_id_index ON public.favourites USING btree (contributor_id);


--
-- Name: favourites_photograph_id_contributor_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX favourites_photograph_id_contributor_id_index ON public.favourites USING btree (photograph_id, contributor_id);


--
-- Name: favourites_photograph_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX favourites_photograph_id_index ON public.favourites USING btree (photograph_id);


--
-- Name: locality_notification_subscriptions_locality_id_contributor_id_; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX locality_notification_subscriptions_locality_id_contributor_id_ ON public.locality_notification_subscriptions USING btree (locality_id, contributor_id);


--
-- Name: page_cache_checks_path_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX page_cache_checks_path_index ON public.page_cache_checks USING btree (path);


--
-- Name: page_cache_checks_updated_at_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX page_cache_checks_updated_at_index ON public.page_cache_checks USING btree (updated_at);


--
-- Name: pending_cdn_actions_type_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX pending_cdn_actions_type_index ON public.pending_cdn_actions USING btree (type);


--
-- Name: photograph_notification_subscriptions_photograph_id_contributor; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX photograph_notification_subscriptions_photograph_id_contributor ON public.photograph_notification_subscriptions USING btree (photograph_id, contributor_id);


--
-- Name: photographs_slq_identifier_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX photographs_slq_identifier_index ON public.photographs USING btree (slq_identifier);


--
-- Name: photographs_spool_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX photographs_spool_id_index ON public.photographs USING btree (spool_id);


--
-- Name: photographs_tags_photograph_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX photographs_tags_photograph_id_index ON public.photographs_tags USING btree (photograph_id);


--
-- Name: photographs_tags_tag_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX photographs_tags_tag_id_index ON public.photographs_tags USING btree (tag_id);


--
-- Name: que_jobs_args_gin_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX que_jobs_args_gin_idx ON public.que_jobs USING gin (args jsonb_path_ops);


--
-- Name: que_jobs_data_gin_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX que_jobs_data_gin_idx ON public.que_jobs USING gin (data jsonb_path_ops);


--
-- Name: que_poll_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX que_poll_idx ON public.que_jobs USING btree (queue, priority, run_at, id) WHERE ((finished_at IS NULL) AND (expired_at IS NULL));


--
-- Name: spools_slq_identifier_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX spools_slq_identifier_index ON public.spools USING btree (slq_identifier);


--
-- Name: stories_contributor_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX stories_contributor_id_index ON public.stories USING btree (contributor_id);


--
-- Name: stories_photograph_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX stories_photograph_id_index ON public.stories USING btree (photograph_id);


--
-- Name: tags_created_at_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX tags_created_at_index ON public.tags USING btree (created_at);


--
-- Name: tags_name_type_source_index; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX tags_name_type_source_index ON public.tags USING btree (name, type, source);


--
-- Name: tags_source_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX tags_source_index ON public.tags USING btree (source);


--
-- Name: tags_type_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX tags_type_index ON public.tags USING btree (type);


--
-- Name: users_email_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX users_email_index ON public.users USING btree (email);


--
-- Name: users_password_reset_token_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX users_password_reset_token_index ON public.users USING btree (password_reset_token);


--
-- Name: que_jobs que_job_notify; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER que_job_notify AFTER INSERT ON public.que_jobs FOR EACH ROW EXECUTE PROCEDURE public.que_job_notify();


--
-- Name: que_jobs que_state_notify; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER que_state_notify AFTER INSERT OR DELETE OR UPDATE ON public.que_jobs FOR EACH ROW EXECUTE PROCEDURE public.que_state_notify();


--
-- Name: page_cache_checks touch_updated_at; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER touch_updated_at BEFORE UPDATE ON public.page_cache_checks FOR EACH ROW EXECUTE PROCEDURE public.update_updated_at_column();


--
-- Name: addresses update_addresses_updated_at; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER update_addresses_updated_at BEFORE UPDATE ON public.addresses FOR EACH ROW EXECUTE PROCEDURE public.update_updated_at_column();


--
-- Name: contributor_images update_contributor_images_updated_at; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER update_contributor_images_updated_at BEFORE UPDATE ON public.contributor_images FOR EACH ROW EXECUTE PROCEDURE public.update_updated_at_column();


--
-- Name: contributors update_contributors_updated_at; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER update_contributors_updated_at BEFORE UPDATE ON public.contributors FOR EACH ROW EXECUTE PROCEDURE public.update_updated_at_column();


--
-- Name: photographs update_photographs_updated_at; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER update_photographs_updated_at BEFORE UPDATE ON public.photographs FOR EACH ROW EXECUTE PROCEDURE public.update_updated_at_column();


--
-- Name: spools update_spools_updated_at; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER update_spools_updated_at BEFORE UPDATE ON public.spools FOR EACH ROW EXECUTE PROCEDURE public.update_updated_at_column();


--
-- Name: stories update_stories_updated_at; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER update_stories_updated_at BEFORE UPDATE ON public.stories FOR EACH ROW EXECUTE PROCEDURE public.update_updated_at_column();


--
-- Name: users update_users_updated_at; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER update_users_updated_at BEFORE UPDATE ON public.users FOR EACH ROW EXECUTE PROCEDURE public.update_updated_at_column();


--
-- Name: addresses addresses_spool_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.addresses
    ADD CONSTRAINT addresses_spool_id_fkey FOREIGN KEY (spool_id) REFERENCES public.spools(id) ON DELETE CASCADE;


--
-- Name: contributor_images contributor_images_contributor_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.contributor_images
    ADD CONSTRAINT contributor_images_contributor_id_fkey FOREIGN KEY (contributor_id) REFERENCES public.contributors(id) ON DELETE CASCADE;


--
-- Name: contributor_images contributor_images_story_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.contributor_images
    ADD CONSTRAINT contributor_images_story_id_fkey FOREIGN KEY (story_id) REFERENCES public.stories(id) ON DELETE CASCADE;


--
-- Name: favourites favourites_contributor_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.favourites
    ADD CONSTRAINT favourites_contributor_id_fkey FOREIGN KEY (contributor_id) REFERENCES public.contributors(id) ON DELETE SET NULL;


--
-- Name: favourites favourites_photograph_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.favourites
    ADD CONSTRAINT favourites_photograph_id_fkey FOREIGN KEY (photograph_id) REFERENCES public.photographs(id) ON DELETE CASCADE;


--
-- Name: localities_spools localities_spools_locality_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.localities_spools
    ADD CONSTRAINT localities_spools_locality_id_fkey FOREIGN KEY (locality_id) REFERENCES public.localities(id) ON DELETE CASCADE;


--
-- Name: localities_spools localities_spools_spool_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.localities_spools
    ADD CONSTRAINT localities_spools_spool_id_fkey FOREIGN KEY (spool_id) REFERENCES public.spools(id) ON DELETE CASCADE;


--
-- Name: locality_notification_subscriptions locality_notification_subscriptions_contributor_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.locality_notification_subscriptions
    ADD CONSTRAINT locality_notification_subscriptions_contributor_id_fkey FOREIGN KEY (contributor_id) REFERENCES public.contributors(id) ON DELETE CASCADE;


--
-- Name: locality_notification_subscriptions locality_notification_subscriptions_locality_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.locality_notification_subscriptions
    ADD CONSTRAINT locality_notification_subscriptions_locality_id_fkey FOREIGN KEY (locality_id) REFERENCES public.localities(id) ON DELETE CASCADE;


--
-- Name: photograph_notification_subscriptions photograph_notification_subscriptions_contributor_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.photograph_notification_subscriptions
    ADD CONSTRAINT photograph_notification_subscriptions_contributor_id_fkey FOREIGN KEY (contributor_id) REFERENCES public.contributors(id) ON DELETE CASCADE;


--
-- Name: photograph_notification_subscriptions photograph_notification_subscriptions_photograph_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.photograph_notification_subscriptions
    ADD CONSTRAINT photograph_notification_subscriptions_photograph_id_fkey FOREIGN KEY (photograph_id) REFERENCES public.photographs(id) ON DELETE CASCADE;


--
-- Name: photographs photographs_locality_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.photographs
    ADD CONSTRAINT photographs_locality_id_fkey FOREIGN KEY (locality_id) REFERENCES public.localities(id) ON DELETE SET NULL;


--
-- Name: photographs photographs_spool_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.photographs
    ADD CONSTRAINT photographs_spool_id_fkey FOREIGN KEY (spool_id) REFERENCES public.spools(id) ON DELETE SET NULL;


--
-- Name: photographs_tags photographs_tags_photograph_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.photographs_tags
    ADD CONSTRAINT photographs_tags_photograph_id_fkey FOREIGN KEY (photograph_id) REFERENCES public.photographs(id) ON DELETE CASCADE;


--
-- Name: photographs_tags photographs_tags_tag_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.photographs_tags
    ADD CONSTRAINT photographs_tags_tag_id_fkey FOREIGN KEY (tag_id) REFERENCES public.tags(id) ON DELETE CASCADE;


--
-- Name: stories stories_contributor_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.stories
    ADD CONSTRAINT stories_contributor_id_fkey FOREIGN KEY (contributor_id) REFERENCES public.contributors(id) ON DELETE CASCADE;


--
-- Name: stories stories_photograph_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.stories
    ADD CONSTRAINT stories_photograph_id_fkey FOREIGN KEY (photograph_id) REFERENCES public.photographs(id) ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

