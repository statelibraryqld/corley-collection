require "corley_collection/operation"

module CorleyCollection
  module Authentication
    class Authenticate < CorleyCollection::Operation
      def call(attributes)
        email, password = attributes.values_at("email", "password")

        entity = fetch(email)

        if entity&.encrypted_password && encrypt_password.same?(entity.encrypted_password, password)
          Success(entity)
        else
          Failure(:user_not_found)
        end
      end

      def fetch(_email)
        raise NotImplementedError
      end
    end
  end
end
