require "securerandom"
require "time_math"

module CorleyCollection
  module Authentication
    class GenerateSecretToken
      def call(token_existence_check)
        token = nil
        retries = 3

        while retries.positive?
          candidate_token = generate_token_string

          # Regenerates a new token if the token already exists
          if token_existence_check.call(candidate_token)
            retries -= 1
            next
          else
            token = candidate_token
            break
          end
        end

        raise "Couldn't generate a unique token after three attempts" unless token

        SecretToken.new(token, token_expiry)
      end

      private

      def generate_token_string
        SecureRandom.hex
      end

      def token_expiry
        TimeMath.day.advance(Time.now, +1)
      end
    end

    class SecretToken
      attr_accessor :value, :expires_at

      def initialize(value, expires_at)
        @value = value
        @expires_at = expires_at
      end
    end
  end
end
