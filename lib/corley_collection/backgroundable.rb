# auto_register: false

require_relative "job"

module CorleyCollection
  class Backgroundable < Module
    JOB_CLASS_NAME = :Job

    ClassMethods = Class.new(Module)
    InstanceMethods = Class.new(Module)

    attr_reader :for_method
    attr_reader :job_class_name

    attr_reader :instance_mod
    attr_reader :class_mod

    def initialize(for_method:, job_class_name: JOB_CLASS_NAME)
      @job_class_name = job_class_name
      @for_method = for_method

      @class_mod = ClassMethods.new
      @instance_mod = InstanceMethods.new
    end

    def included(klass)
      define_class_methods(klass)
      define_instance_methods

      klass.extend(class_mod)
      klass.send(:include, instance_mod)
    end

    private

    def define_class_methods(klass)
      class_mod.class_exec(klass, job_class_name, for_method) do |operation_class, job_class_name, for_method|
        define_method :const_missing do |name|
          if name == job_class_name
            const_set(job_class_name, build_job_class)
          end
        end

        define_method :job_class do
          const_get(job_class_name, false)
        end

        define_method :build_job_class do
          Class.new(Job[operation_class, for_method])
        end
        private :build_job_class
      end
    end

    def define_instance_methods
      instance_mod.class_exec(job_class_name) do |_job_class_name|
        define_method :job_class do
          self.class.job_class
        end

        define_method :enqueue do |*args|
          # Extract trailing keyword arguments manually rather than with a
          # **kwargs splt param, because with that in place, any object the
          # implicitly destructures to a hash (like our ROM::Struct objects)
          # will be splatted into keyword arguments instead of being kept as a
          # single object. Detecting keyword args via `is_a(Hash)` is slightly
          # less flexible but more predictable when we're passing around struct
          # objects as `#enqueue` arguments (which we often do).
          kwargs = args.last.is_a?(Hash) ? args.pop : {}

          enqueue_kwarg_names = job_class.method(:enqueue).parameters
            .each_with_object([]) { |(type, name), params| params << name if type == :key }

          operation_kwargs = kwargs.reject { |(key, _)| enqueue_kwarg_names.include?(key) }.to_h
          enqueue_kwargs = kwargs.select { |(key, _)| enqueue_kwarg_names.include?(key) }.to_h

          # Don't pass kwargs if we have none. This improves compatibility with various params signatures.
          to_queue_args = operation_kwargs.any? ? (args << operation_kwargs) : args

          job_class.enqueue(*to_queue(*to_queue_args), **enqueue_kwargs)
        end
      end
    end
  end
end
