module CorleyCollection
  class CookieFilter
    def initialize(app)
      @app = app
      @excluded_paths = [
        %r{\A\/account},
        %r{\A\/contributors},
        %r{\A\/contributor-images},
        %r{\A\/photographs},
      ]
      @whitelisted_paths = []
    end

    def call(env)
      status, headers, body = @app.call(env)

      # We set this var for requests made by signed-in admin users. Not stripping cookies for these requests means that
      # the CDN won't send a cached response (meaning these users will always see fresh content).
      unless env["filter_cookies"] == false
        # Remove cookies from the response if path doesn't match our exclusions
        excluded = @excluded_paths.none? { |path| path =~ env["REQUEST_PATH"] }
        whitelisted = @whitelisted_paths.any? { |path| path =~ env["REQUEST_PATH"] }
        if env["REQUEST_METHOD"] == "GET" && (excluded || whitelisted)
          headers.delete "Set-Cookie"
        end
      end

      [status, headers, body]
    end
  end
end
