# auto_register: false

require "rom/struct"

module CorleyCollection
  module Entities
    class Contributor < ROM::Struct
      def full_name
        [first_name, last_name].compact.join(" ")
      end

      def public_name
        [first_name, last_initial].compact.join(" ")
      end

      def last_initial
        last_name[0] unless last_name.to_s.empty?
      end

      def active?
        !suspended?
      end

      def suspended?
        suspended
      end
    end
  end
end
