# auto_register: false

require "rom/struct"

module CorleyCollection
  module Entities
    class Locality < ROM::Struct
      def geo_location?
        !latitude.nil? && !longitude.nil?
      end
    end
  end
end
