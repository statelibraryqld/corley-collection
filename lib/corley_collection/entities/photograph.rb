# auto_register: false

require "rom/struct"

module CorleyCollection
  module Entities
    class Photograph < ROM::Struct
      def street_address?
        [street_number, street_name].compact.any?
      end

      def geo_location?
        !latitude.nil? && !longitude.nil?
      end

      def unlocated?
        !geo_location?
      end
    end
  end
end
