require "transproc"
require "dry/inflector"
require "dry/core/cache"
require "functions/text_transformations"

module CorleyCollection
  module Functions
    extend Transproc::Registry

    import Transproc::HashTransformations
    import Transproc::ArrayTransformations
    import ::Functions::TextTransformations

    class Inflector
      extend Dry::Core::Cache

      attr_reader :backend

      def initialize
        @backend = Dry::Inflector.new.extend(Transproc::Registry)
      end

      def call(input, *fns)
        composed(*fns)[input]
      end
      alias_method :[], :call

      private

      def composed(*fns)
        fetch_or_store(fns.hash) { fns.map { |fn| backend[fn] }.reduce(:>>) }
      end
    end
  end
end
