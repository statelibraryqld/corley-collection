# auto_register: false

module CorleyCollection
  class Job
    def self.[](operation_class, for_method)
      require "que"

      Class.new(Que::Job).tap do |job_class|
        job_class.class_exec(operation_class, for_method) do |operation_class, for_method| # rubocop:disable Lint/ShadowingOuterLocalVariable
          define_method :run do |*args|
            operation_class.new.public_send(for_method, *args)
          end
        end
      end
    end
  end
end
