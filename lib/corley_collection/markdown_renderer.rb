require "redcarpet"
require "functions/text_transformations"

module CorleyCollection
  class MarkdownRenderer
    attr_reader :markdown
    attr_reader :filtered_markdown

    def initialize
      @markdown = Redcarpet::Markdown.new(Redcarpet::Render::HTML)
      @filtered_markdown = Redcarpet::Markdown.new(Redcarpet::Render::HTML.new(filter_html: true))
    end

    def render(text)
      markdown.render(text)
    end

    def render_filtered_html(text)
      filtered_markdown.render(text)
    end

    def simple_format(text)
      ::Functions::TextTransformations.sanitize_html(
        render_filtered_html(text),
        allowed_tags: "p"
      ).to_s.gsub("\n\n", "").gsub("\n", "<br>")
    end
  end
end
