# auto_register: false

require "dry/monads/do"
require "dry/transaction/operation"
require_relative "backgroundable"

module CorleyCollection
  class Operation
    def self.inherited(subclass)
      super
      subclass.include Dry::Monads::Do
      subclass.include Dry::Transaction::Operation
      subclass.include Backgroundable.new(for_method: :call_from_queue)
    end
  end
end
