module CorleyCollection
  class Pager
    attr_reader :total
    attr_reader :per_page
    attr_reader :current_page

    def initialize(total:, per_page:, current_page:)
      @total = total
      @per_page = per_page
      @current_page = current_page
    end

    def total_pages
      pages = total.to_f / per_page
      if (pages % 1).zero?
        pages
      else
        pages + 1
      end.to_i
    end

    def next_page
      current_page + 1 if current_page < total_pages
    end

    def prev_page
      current_page - 1 if current_page > 1
    end
  end
end
