require "rom/relation/loaded"

module CorleyCollection
  module Plugins
    module Relations
      module Json
        class Loaded < ROM::Relation::Loaded
          def to_h
            {results: to_a, pagination: pagination}
          end

          def to_json
            JSON.generate(to_h)
          end

          def pagination
            {page: pager.current_page,
             per_page: pager.per_page,
             total_pages: pager.total_pages,
             total_results: pager.total}
          end

          def pager
            source.pager
          end
        end

        def as_json
          Loaded.new(with(auto_struct: false)).to_h
        end
      end
    end
  end
end
