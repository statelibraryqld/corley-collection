module CorleyCollection
  module Plugins
    module Relations
      class OrderableByValues < Module
        CASE_STATEMENT_TEMPLATE = "CASE %{attribute} %{conditions} END".freeze

        attr_reader :attributes

        def initialize(attributes)
          @attributes = Array(attributes)
          define_views!
        end

        def included(klass)
          super
          klass.include(InstanceMethods)
        end

        private

        def define_views!
          attributes.each do |name|
            define_method(:"order_by_#{name}_values") do |values|
              order_by_values(name, values)
            end
          end
        end

        module InstanceMethods
          # Used when fetching DB records matching ES search results.
          # By default, records are returned ordered by their ID
          # (ascending) and not the order in which their IDs were
          # passed. ES returns records ordered according to relevance,
          # so we need to maintain that order when fetching corresponding
          # records from the DB.
          def order_by_values(column, values)
            return self if values.empty?

            sql = format(CASE_STATEMENT_TEMPLATE, attribute: self[column].sql_literal(dataset), conditions: values.map { |value| "WHEN '#{value}' THEN #{values.index(value)}" }.join(" "))
            order { `#{sql}` }
          end
        end
      end
    end
  end
end
