module CorleyCollection
  module Plugins
    module Relations
      class Taggable < Module
        attr_reader :taggings

        def initialize(taggings)
          @taggings = taggings
        end

        def included(klass)
          super

          taggings = self.taggings

          klass.option :taggings, default: proc { taggings }
          klass.include(InstanceMethods)
        end

        module InstanceMethods
          def with_tag_names
            select_append { |r| array.array_remove(array.array_agg(r.tags[:name]), `NULL`).as(:tags) }.
              left_join(taggings.name, taggings.foreign_key(name) => self[:id]).
              left_join(:tags, id: :tag_id).
              group(self[:id])
          end

          def with_tag_names_and_ids
            select_append { |r| array.array_remove(array.array_agg(r.tags[:name]), `NULL`).as(:tags) }.
              select_append { |r| array.array_remove(array.array_agg(r.tags[:id]), `NULL`).as(:tag_ids) }.
              left_join(taggings.name, taggings.foreign_key(name) => self[:id]).
              left_join(:tags, id: :tag_id).
              group(self[:id])
          end

          def taggings
            __registry__[options[:taggings]]
          end
        end
      end
    end
  end
end
