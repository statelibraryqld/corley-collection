require "que"
require "que/sequel/model"

module CorleyCollection
  class QueJobManager
    def self.job_queued_with_job_class?(klass)
      QueJob.where(job_class: klass.name.to_s).any?
    end

    class QueJob < ::Que::Sequel::Model
    end
  end
end
