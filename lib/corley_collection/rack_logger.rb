# auto_register: false

require "rack/request"

module CorleyCollection
  class RackLogger
    FILTERED = "[FILTERED]".freeze

    extend Dry::Configurable

    setting :filtered_params, %w[_csrf password password_confirmation]

    attr_reader :logger

    attr_reader :config

    def initialize(logger, config = self.class.config)
      @logger = logger
      @config = config
    end

    def attach(rack_monitor)
      rack_monitor.on :stop do |env:, status:, time:|
        log_request(env, status, time)
      end

      rack_monitor.on :error do |event|
        log_exception(event[:exception])
      end
    end

    def log_request(env, status, time)
      request = Rack::Request.new(env)

      # Prepare merged params (POST _should_ be a hash, but isn't always, so
      # coerce it)
      params = request.GET.merge(Hash(request.POST))

      # Add params handled by roda's json_parser plugin
      json_params = Hash(request.get_header("roda.json_params"))
      params = params.merge(json_params)

      data = {
        method: request.request_method,
        path: request.path,
        for: request.get_header("REMOTE_ADDR"),
        status: status,
        duration: time,
        params: filter_params(params),
      }

      logger.info data.map { |k, v| "#{k}=#{v}" }.join(" ")
    end

    def log_exception(exception)
      logger.error exception.message
      logger.error exception.backtrace.join("\n")
    end

    private

    def filter_params(params)
      params.each_with_object({}) do |(k, v), h|
        if config.filtered_params.include?(k)
          h.update(k => FILTERED)
        elsif v.is_a?(Hash)
          h.update(k => filter_params(v))
        elsif v.is_a?(Array)
          h.update(k => v.map { |m| m.is_a?(Hash) ? filter_params(m) : m })
        else
          h[k] = v
        end
      end
    end
  end
end
