module CorleyCollection
  module Repos
    module AccountManagement
      def password_reset_token_exists?(password_reset_token)
        root.by_password_reset_token(password_reset_token).exist?
      end

      def by_activation_token(activation_token)
        root.by_activation_token(activation_token).one
      end

      def activation_token_exists?(activation_token)
        root.by_activation_token(activation_token).exist?
      end

      def by_current_password_reset_token(token)
        root.where {
          password_reset_token.is(token) & (password_reset_token_expires_at > Time.now)
        }.one
      end
    end
  end
end
