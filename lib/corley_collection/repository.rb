# auto_register: false

# FIXME: these requires should not be needed, this'll be fixed in rom 4.0.3
require "rom/struct"
require "rom/constants"
require "rom/container"
require "rom/repository"

require "corley_collection/container"
require "corley_collection/import"

module CorleyCollection
  class Repository < ROM::Repository::Root
    include CorleyCollection::Import.args["persistence.rom"]
  end
end
