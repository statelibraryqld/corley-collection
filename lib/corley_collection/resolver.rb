# auto_register: false

require "dry/container/error"
require "dry/container/item"
require "dry/container/resolver"

module CorleyCollection
  class Resolver < Dry::Container::Resolver
    class NotifyingProxy < SimpleDelegator
      def initialize(container_key, obj)
        @container_key = container_key
        super(obj)
      end

      def method_missing(name, *args, &block)
        super
      end

      def respond_to_missing?(name, include_private = false)
        super
      end
    end

    attr_reader :env

    def initialize(env)
      @env = env
    end

    def call(container, key)
      item = container.fetch(key.to_s) do
        raise Dry::Container::Error, "Nothing registered with the key #{key.inspect}"
      end

      if env == :production && instrument?(item)
        NotifyingProxy.new(key, item.call)
      else
        item.call
      end
    end

    def instrument?(item)
      item.is_a?(Dry::Container::Item) && item.options[:call]
    end
  end
end
