# auto_register: false

require "dry/validation"
require "dry/validation/messages/i18n"

module CorleyCollection
  class SearchQuery
    class Schema < Dry::Validation::Schema::Params
      configure do |config|
        config.messages = :i18n
      end
    end

    attr_reader :params

    attr_reader :errors

    attr_reader :validation

    attr_reader :query

    alias_method :to_s, :query

    def self.params(&block)
      @__schema__ = Dry::Validation.Schema(superschema, &block)
    end

    def self.superschema
      if superclass.instance_variable_defined?("@__schema__")
        superclass.schema
      else
        Schema
      end
    end

    def self.schema
      @__schema__
    end

    def initialize(params)
      @validation = self.class.schema.(params)
      @params = @validation.to_h
      @errors = @validation.errors
      @query = @params[:q].to_s
    end

    def with(new_params)
      self.class.new(params.merge(new_params))
    end

    def [](key)
      params[key]
    end

    def valid?
      validation.success?
    end

    def present?
      !empty?
    end

    def empty?
      query.empty?
    end
  end
end
