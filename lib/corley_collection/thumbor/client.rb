require "uri"
require "ruby-thumbor"

module CorleyCollection
  module Thumbor
    class Client
      PresetAlreadyDefinedError = Class.new(StandardError)

      attr_reader :host
      attr_reader :default_image_host
      attr_reader :client
      attr_reader :presets

      def initialize(host:, security_key:, default_image_host: nil, &block)
        @host = host
        @client = ::Thumbor::CryptoURL.new(security_key)
        @default_image_host = default_image_host

        @presets = {}
        instance_eval(&block) if block_given?
      end

      def preset(name, options)
        raise PresetAlreadyDefinedError, "+#{name}+ preset is already defined" if presets.key?(name)
        @presets[name] = options
      end

      def url(image_url, **options)
        options = options.merge(image: image_url_with_host(image_url))

        URI("#{host}#{client.generate(options)}").to_s
      end

      def original(image_url)
        url(image_url)
      end

      def direct_download_url(download_url)
        download_url = "/#{download_url}" unless download_url[0] == "/"
        "#{default_image_host}#{download_url}"
      end

      private

      def image_url_with_host(image_url)
        uri = URI(image_url)

        if uri.host
          url.to_s
        else
          image_url = "/#{image_url}" unless image_url[0] == "/"
          "#{default_image_host}#{image_url}"
        end
      end

      def method_missing(name, *args)
        if presets.key?(name)
          preset_options = presets[name]

          render_options =
            if args[1]&.has_key?(:render_options)
              args[1][:render_options]
            else
              {}
            end

          url(args[0], preset_options.merge(render_options))
        else
          super
        end
      end

      def respond_to_missing?(name, _include_private = false)
        @presets.key?(name) || super
      end
    end
  end
end
