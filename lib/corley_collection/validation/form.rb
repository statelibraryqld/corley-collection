require "dry/validation"
require "uri"

Dry::Validation.load_extensions(:monads)

Dry::Types.register("params.string") do
  Dry::Types["string"].constructor do |str|
    str ? str.strip.chomp : str
  end
end

module CorleyCollection
  module Validation
    class Form < Dry::Validation::Schema::Params
      SLUG_REGEX = /\A[a-z0-9][a-z0-9-]+\z/
      EMAIL_REGEX = /.+@.+\..+/
      PATH_REGEX = %r{\A(/[a-z0-9_\-/]+[a-z0-9]+)\z}

      configure do |config|
        option :relations, -> { Container["persistence.rom"].relations }
        option :relation

        config.type_specs = true
        config.messages = :i18n
      end

      def unique?(key, value)
        relations[relation].unique?(key => value)
      end

      def email?(input)
        !EMAIL_REGEX.match(input).nil?
      end

      def slug_format?(input)
        SLUG_REGEX.match(input)
      end

      def url?(input)
        !URI::DEFAULT_PARSER.make_regexp.match(input).nil?
      end

      def not_url?(input)
        !url?(input)
      end

      def secure_url?(input)
        url?(input) && input.match?(/^https/)
      end

      def path?(input)
        PATH_REGEX.match?(input)
      end
    end

    def self.Form(relation = nil, &block)
      Dry::Validation.Schema(Form, &block).with(relation: relation)
    end
  end
end
