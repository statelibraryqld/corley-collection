# auto_register: false

require "icy-content_buffer"

module CorleyCollection
  module View
    class Context
      attr_reader :attrs
      attr_reader :content_buffer

      def initialize(attrs = {})
        @attrs = attrs
        @content_buffer = Icy::ContentBuffer.new
      end

      def fullpath
        self[:fullpath]
      end

      def csrf_token
        self[:csrf_token].()
      end

      def csrf_metatag
        self[:csrf_metatag].()
      end

      def csrf_tag
        self[:csrf_tag].()
      end

      def flash
        self[:flash]
      end

      def flash?
        %i[notice alert].any? { |type| flash[type] }
      end

      def with(new_attrs)
        self.class.new(attrs.merge(new_attrs))
      end

      def with_flash(msg)
        with(flash: msg)
      end

      def assets
        self[:assets]
      end

      private

      def [](name)
        attrs.fetch(name)
      end
    end
  end
end
