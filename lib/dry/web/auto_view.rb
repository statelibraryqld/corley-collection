require "dry/core/cache"

module Dry
  module Web
    class AutoView
      extend Dry::Core::Cache

      attr_reader :base

      attr_reader :container

      def initialize(base, container)
        @base = base
        @container = container
      end

      def load!
        components.each do |component|
          container.register("views.#{component.identifier}", build_view(component.path))
        end
      end

      def components
        identifiers.map { |identifier| container.component(identifier) }
      end

      def identifiers
        template_paths.
          map { |p| p.gsub(template_root.to_s, "").gsub(%r{^/}, "").gsub(".html.slim", "") }.
          reject { |p| File.basename(p).start_with?("_") }.
          reject { |p| file_exists?(p) }
      end

      def template_paths
        Dir["#{template_root}/**/*.html.*"]
      end

      def template_root
        container.root.join("web/templates")
      end

      private

      def build_view(path)
        proc do |options = {}|
          view = fetch_or_store("#{base.__id__}.#{path}") do
            Class.new(base) do
              configure do |config|
                config.template = path
              end

              options.each_key { |key| expose(key) }
            end.new
          end

          view.(options)
        end
      end

      def file_exists?(identifier)
        component = container.component("#{container.config.default_namespace}.views.#{identifier}")
        component.file_exists?(container.load_paths)
      end
    end
  end
end
