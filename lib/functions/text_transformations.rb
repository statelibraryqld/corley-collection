require "sanitize"
require "html_truncator"
require "nokogiri"

module Functions
  module TextTransformations
    ALLOWED_TAGS = %w[p i em strong br].freeze

    TEXT_NODE_TRANSFORMER = lambda do |env|
      text_node_whitelist = ["h1", "h2", "h3", "h4", "h5", "h6", "ul", "ol", "li"]
      return unless text_node_whitelist.include? env[:node_name]
      env[:node].name = "p"
    end

    DIV_REMOVAL_TRANSFORMER = lambda do |env|
      return unless env[:node_name] == "div"
      env[:node].unlink
    end

    def self.sanitize_html(html, allowed_tags: ALLOWED_TAGS, remove_contents: false, include_all_text_nodes: true)
      transformers = [DIV_REMOVAL_TRANSFORMER]
      transformers << TEXT_NODE_TRANSFORMER if include_all_text_nodes

      Sanitize.fragment(
        html,
        Sanitize::Config.merge(
          Sanitize::Config::RESTRICTED,
          elements: allowed_tags ? allowed_tags.split(" ") : "",
          remove_contents: remove_contents,
          transformers: transformers
        )
      ).strip
    end

    def self.excerpt_from_html(html, word_count: 33)
      HTML_Truncator.truncate(html, word_count)
    end

    def self.strip_empty_paragraphs(html_string)
      doc = Nokogiri::HTML.fragment(html_string)
      doc.css("p").find_all.each do |p|
        p.remove if p.content.strip.empty?
      end
      doc.to_html
    end
  end
end
