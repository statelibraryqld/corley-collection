module Persistence
  module Relations
    class Addresses < ROM::Relation[:sql]
      schema(:addresses, infer: true) do
        associations do
          belongs_to :spool
        end
      end
    end
  end
end
