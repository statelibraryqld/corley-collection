module Persistence
  module Relations
    class ContributorImages < ROM::Relation[:sql]
      schema(:contributor_images, infer: true) do
        associations do
          belongs_to :story
          belongs_to :contributor
        end
      end

      use :pagination

      per_page 20

      def published
        where(published: true)
      end
    end
  end
end
