module Persistence
  module Relations
    class Contributors < ROM::Relation[:sql]
      schema(:contributors, infer: true) do
        associations do
          has_many :stories

          has_many :locality_notification_subscriptions
          has_many :localities, through: :locality_notification_subscriptions, view: :ordered_by_name
        end
      end

      use :orderable_by_values, :id
      use :pagination

      per_page 20

      def by_current_password_reset_token(token)
        where { password_reset_token.is(token) & (password_reset_token_expires_at > Time.now) }
      end

      def by_public_token(public_token)
        where(public_token: public_token)
      end
    end
  end
end
