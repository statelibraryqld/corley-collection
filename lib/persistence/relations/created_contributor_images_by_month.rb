module Persistence
  module Relations
    class CreatedContributorImagesByMonth < ROM::Relation[:sql]
      schema(:created_contributor_images_by_month, infer: true)
    end
  end
end
