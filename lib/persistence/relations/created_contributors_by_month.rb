module Persistence
  module Relations
    class CreatedContributorsByMonth < ROM::Relation[:sql]
      schema(:created_contributors_by_month, infer: true)
    end
  end
end
