module Persistence
  module Relations
    class CreatedStoriesByMonth < ROM::Relation[:sql]
      schema(:created_stories_by_month, infer: true)
    end
  end
end
