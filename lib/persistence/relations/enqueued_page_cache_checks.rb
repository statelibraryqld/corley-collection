module Persistence
  module Relations
    class EnqueuedPageCacheChecks < ROM::Relation[:sql]
      schema(:enqueued_page_cache_checks, infer: true)
    end
  end
end
