module Persistence
  module Relations
    class Favourites < ROM::Relation[:sql]
      schema(:favourites, infer: true) do
        associations do
          belongs_to :photograph
          belongs_to :contributor
        end
      end

      def by_contributor_id(id)
        where(contributor_id: id)
      end

      def by_photograph_and_contributor_id(photograph_id, contributor_id)
        where(photograph_id: photograph_id, contributor_id: contributor_id)
      end
    end
  end
end
