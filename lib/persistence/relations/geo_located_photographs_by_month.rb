module Persistence
  module Relations
    class GeoLocatedPhotographsByMonth < ROM::Relation[:sql]
      schema(:geo_located_photographs_by_month, infer: true)
    end
  end
end
