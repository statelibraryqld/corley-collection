module Persistence
  module Relations
    class Localities < ROM::Relation[:sql]
      schema(:localities, infer: true) do
        associations do
          has_many :localities_spools
          has_many :spools, through: :localities_spools

          has_many :photographs
        end
      end

      def by_name(name)
        where(name: name)
      end

      def by_lower_name(name)
        where(Sequel.lit("LOWER(localities.name) = ?", name.downcase))
      end

      def ordered_by_name
        order(:name)
      end
    end
  end
end
