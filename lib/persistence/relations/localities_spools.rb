module Persistence
  module Relations
    class LocalitiesSpools < ROM::Relation[:sql]
      schema(:localities_spools, infer: true) do
        associations do
          belongs_to :locality
          belongs_to :spool
        end
      end

      def by_spool_id(id)
        where(spool_id: id)
      end

      def by_locality_id(id)
        where(locality_id: id)
      end
    end
  end
end
