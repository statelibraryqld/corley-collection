module Persistence
  module Relations
    class LocalityNotificationSubscriptions < ROM::Relation[:sql]
      schema(:locality_notification_subscriptions, infer: true) do
        associations do
          belongs_to :locality
          belongs_to :contributor
        end
      end

      def by_locality_id(id)
        where(locality_id: id)
      end

      def by_contributor_id(id)
        where(contributor_id: id)
      end

      def by_locality_and_contributor(locality_id, contributor_id)
        where(locality_id: locality_id, contributor_id: contributor_id)
      end

      def by_token(token)
        where(token: token)
      end
    end
  end
end
