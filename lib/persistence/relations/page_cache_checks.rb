module Persistence
  module Relations
    class PageCacheChecks < ROM::Relation[:sql]
      schema(:page_cache_checks, infer: true)
    end
  end
end
