module Persistence
  module Relations
    class PendingCdnActions < ROM::Relation[:sql]
      schema(:pending_cdn_actions, infer: true)
    end
  end
end
