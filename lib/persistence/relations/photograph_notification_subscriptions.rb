module Persistence
  module Relations
    class PhotographNotificationSubscriptions < ROM::Relation[:sql]
      schema(:photograph_notification_subscriptions, infer: true) do
        associations do
          belongs_to :photograph
          belongs_to :contributor
        end
      end

      def by_photograph_id(id)
        where(photograph_id: id)
      end

      def by_contributor_id(id)
        where(contributor_id: id)
      end

      def by_photograph_and_contributor(photograph_id, contributor_id)
        where(photograph_id: photograph_id, contributor_id: contributor_id)
      end

      def by_token(token)
        where(token: token)
      end
    end
  end
end
