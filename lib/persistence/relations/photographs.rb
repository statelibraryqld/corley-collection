module Persistence
  module Relations
    class Photographs < ROM::Relation[:sql]
      schema(:photographs, infer: true) do
        associations do
          belongs_to :locality
          belongs_to :spool

          has_many :photographs_tags
          has_many :tags, through: :photographs_tags

          has_many :stories
        end
      end

      use :taggable, :photographs_tags
      use :orderable_by_values, :id
      use :pagination

      per_page 20

      def by_slq_identifier(identifier)
        where(slq_identifier: identifier)
      end

      def by_locality_id(locality_id)
        where(locality_id: locality_id)
      end

      def human_tagged
        where { human_tagged_at.not(nil) }
      end

      def human_untagged
        where { human_tagged_at.is(nil) }
      end

      def recently_human_tagged
        human_tagged.order(self[:human_tagged_at].desc)
      end

      def geo_located
        where { geo_located_at.not(nil) }
      end

      def geo_located_between(start_time, end_time)
        geo_located.where { (geo_located_at >= start_time) & (geo_located_at < end_time) }
      end

      def storied
        where { storied_at.not(nil) }
      end

      def random_untagged
        read <<-SQL
          SELECT * FROM "photographs"
          WHERE "photographs"."human_tagged_at" IS NULL
          ORDER BY RANDOM()
        SQL
      end

      def with_tag_ids
        select_append { |r| array.array_remove(array.array_agg(r.tags[:id]), `NULL`).as(:tag_ids) }.
          left_join(:photographs_tags, photograph_id: self[:id]).
          left_join(:tags, id: :tag_id).
          group(self[:id])
      end
    end
  end
end
