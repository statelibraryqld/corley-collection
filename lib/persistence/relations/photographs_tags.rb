module Persistence
  module Relations
    class PhotographsTags < ROM::Relation[:sql]
      schema(:photographs_tags, infer: true) do
        associations do
          belongs_to :photograph
          belongs_to :tag
        end
      end
    end
  end
end
