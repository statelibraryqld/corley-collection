module Persistence
  module Relations
    class RecentActiveLocalities < ROM::Relation[:sql]
      schema(:recent_active_localities, infer: true) do
        associations do
          belongs_to :locality
        end
      end
    end
  end
end
