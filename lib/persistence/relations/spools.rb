module Persistence
  module Relations
    class Spools < ROM::Relation[:sql]
      schema(:spools, infer: true) do
        associations do
          has_many :addresses

          has_many :localities_spools
          has_many :localities, through: :localities_spools

          has_many :photographs

          has_one :spools_metadata
        end
      end

      use :pagination

      per_page 20

      def by_slq_identifier(identifier)
        where(slq_identifier: identifier)
      end

      def with_photograph_slq_identifiers
        select_append { |r| array.array_remove(array.array_agg(r.photographs[:slq_identifier]), `NULL`).as(:photograph_slq_identifiers) }.
          left_join(:photographs, spool_id: :id).
          group(self[:id])
      end
    end
  end
end
