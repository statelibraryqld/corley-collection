module Persistence
  module Relations
    class SpoolsMetadata < ROM::Relation[:sql]
      schema(:spools_metadata, infer: true) do
        associations do
          belongs_to :spool
        end
      end
    end
  end
end
