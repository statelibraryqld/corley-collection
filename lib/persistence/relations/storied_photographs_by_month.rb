module Persistence
  module Relations
    class StoriedPhotographsByMonth < ROM::Relation[:sql]
      schema(:storied_photographs_by_month, infer: true)
    end
  end
end
