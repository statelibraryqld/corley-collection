module Persistence
  module Relations
    class Stories < ROM::Relation[:sql]
      schema(:stories, infer: true) do
        associations do
          has_many :contributor_images

          belongs_to :photograph
          belongs_to :contributor
        end
      end

      use :pagination
      use :orderable_by_values, :id

      per_page 20

      def published
        where(published: true)
      end

      def by_photograph_id(id)
        where(photograph_id: id)
      end

      def recently_created
        where { created_at > Time.now - 24 * 3600 }
      end

      def created_between(start_time, end_time)
        where { (created_at >= start_time) & (created_at < end_time) }
      end
    end
  end
end
