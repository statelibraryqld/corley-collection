module Persistence
  module Relations
    class TaggedPhotographsByMonth < ROM::Relation[:sql]
      schema(:tagged_photographs_by_month, infer: true)
    end
  end
end
