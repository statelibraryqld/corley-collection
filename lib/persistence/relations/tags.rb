module Persistence
  module Relations
    class Tags < ROM::Relation[:sql]
      schema(:tags, infer: true) do
        associations do
          has_many :photographs_tags
          has_many :photographs, through: :photographs_tags
        end
      end

      use :orderable_by_values, :id
      use :pagination

      per_page 20

      def human
        where(source: "human")
      end

      def machine
        where(source: "machine")
      end

      def contributor
        where(type: "Contributor")
      end

      def public
        human.where(tags[:type].not("Contributor"))
      end

      def for_tag_types
        select(:type).distinct(:type).order(:type)
      end

      def by_name_type_and_source(name, type, source)
        where(name: name, type: type, source: source)
      end
    end
  end
end
