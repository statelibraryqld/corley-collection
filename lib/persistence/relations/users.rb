module Persistence
  module Relations
    class Users < ROM::Relation[:sql]
      schema(:users, infer: true)

      use :orderable_by_values, :id
      use :pagination

      per_page 10

      def by_current_password_reset_token(token)
        where { password_reset_token.is(token) & (password_reset_token_expires_at > Time.now) }
      end
    end
  end
end
