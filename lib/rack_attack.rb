require "corley_collection/container"
require "rack/attack"

module Rack
  class Attack
    # Monkey patch an ip method
    class Request < ::Rack::Request
      def originating_ip
        env["HTTP_X_FORWARDED_FOR"].to_s.split(",").map(&:strip).first || ip
      end
    end

    redis = CorleyCollection::Container[:redis]
    cache.store = Rack::Attack::StoreProxy::RedisStoreProxy.new(redis)

    ##########################
    # ROUTE-SPECIFIC THROTTLES
    ##########################

    ### /admin/sign-in

    # Throttle POST requests by IP address
    # This limit is higher to handle legitimate scenarios where a large volume of users may attempt to sign in at the
    # same time (eg phone room staff sitting down for the start of a day of training)
    throttle("/admin/sign-in/ip", limit: 30, period: 60) do |req|
      req.originating_ip if req.path == "/admin/sign-in" && req.post?
    end

    # Throttle POST requests by email address
    throttle("/admin/sign-in/email", limit: 10, period: 60) do |req|
      if req.path == "/admin/sign-in" && req.post? && req.params["user"]
        req.params["user"]["email"]
      end
    end

    ### /admin/users/forgot-password

    # Throttle POST requests by IP address
    throttle("/admin/users/forgot-password/ip", limit: 10, period: 60) do |req|
      req.originating_ip if req.path == "/admin/users/forgot-password" && req.post?
    end

    # Throttle POST requests by email address
    throttle("/admin/users/forgot-password/email", limit: 10, period: 60) do |req|
      if req.path == "/admin/users/forgot-password" && req.post? && req.params["user"]
        req.params["user"]["email"]
      end
    end

    ### /admin/users/reset-password

    # Throttle GET requests by IP address
    throttle("/admin/users/reset-password/ip", limit: 10, period: 60) do |req|
      req.originating_ip if req.path == "/admin/users/reset-password" && req.get?
    end

    ### /admin/users/activate

    # Throttle GET requests by IP address
    throttle("/admin/users/activate/ip", limit: 10, period: 60) do |req|
      req.originating_ip if req.path == "/admin/users/activate" && req.get?
    end

    ###########
    # SAFELISTS
    ###########

    # Localhost
    safelist("allow from localhost") do |req|
      req.ip == "127.0.0.1" || req.ip == "::1"
    end

    # Admin search endpoints that we hit a lot
    safelist("allow admin search endpoints") do |req|
      !req.path.match(%r{^/admin/.+/search.json}).nil?
    end
  end
end
