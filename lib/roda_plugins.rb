require "roda"

class Roda
  module RodaPlugins
    module Auth
      module RequestMethods
        def authorize
          resolve("authentication.authorize") do |authorize|
            authorize.(session) do |m|
              m.success do |user|
                set_current_user!(user)
              end

              m.failure do |error|
                on accept: "application/json" do
                  halt 401
                end

                scope.flash[:notice] = auth_error(error)
                redirect "/admin/sign-in"
              end

              yield
            end
          end
        end

        def resolve(*keys)
          items = keys.map { |key| current_container[key] }

          if block_given?
            yield(*items)
          else
            items.first
          end
        end

        def auth_error(id)
          scope.class["core.i18n.t"]["admin.auth.errors.#{id}"]
        end

        def current_user
          scope.env["admin.current_user"]
        end

        def current_container
          scope.env["admin.current_container"] || roda_class.container
        end

        def set_current_user!(user) # rubocop:disable Naming/AccessorMethodName
          scope.env["admin.current_user"] = user
        end
      end
    end

    module ContributorAuth
      module RequestMethods
        def authorize
          resolve("authentication.authorize") do |authorize|
            authorize.(session) do |m|
              m.success do |contributor|
                set_current_contributor!(contributor)
              end

              m.failure do |error|
                on accept: "application/json" do
                  halt 401
                end

                if error == :suspended
                  session[:contributor_id] = env["main.current_contributor"] = nil
                else
                  session[:return_to] = env["REQUEST_PATH"]
                end

                scope.flash[:notice] = auth_error(error)
                redirect "/contributors/sign-in"
              end

              yield
            end
          end
        end

        def auth_error(id)
          scope.class["core.i18n.t"]["main.auth.errors.#{id}"]
        end

        def current_contributor
          scope.env["main.current_contributor"]
        end

        def set_current_contributor!(contributor) # rubocop:disable Naming/AccessorMethodName
          scope.env["main.current_contributor"] = contributor
        end
      end
    end

    module MainView
      module RequestMethods
        def view(name, overrides = {})
          options = {context: scope.current_view_context}.merge(overrides)
          is to: "views.#{name}", call_with: [options]
        end

        def enqueue_cache_check
          return unless roda_class.container.env == :production

          check_path = [
            scope.request.path,
            (scope.request.query_string unless scope.request.query_string == ""),
          ].compact.join("?")

          roda_class.container["cdn.operations.enqueue_check"].(check_path)
        end
      end

      module InstanceMethods
        def current_view_context(with = {})
          view_context.with_flash(flash).with(with)
        end

        def view_context
          self.class["view.context"].with(
            fullpath: request.fullpath,
            assets: self.class["core.assets"],
            current_contributor: env["main.current_contributor"],
            csrf_metatag: -> { Rack::Csrf.metatag(request.env) },
            csrf_tag: -> { Rack::Csrf.tag(request.env) },
            csrf_token: -> { Rack::Csrf.token(request.env) },
          )
        end
      end
    end

    module View
      module RequestMethods
        def view(name, overrides = {})
          resolve("views.#{name}").({context: scope.current_view_context}.merge(overrides))
        end
      end

      module InstanceMethods
        def current_view_context
          view_context.with_flash(flash)
        end

        def view_context
          self.class["view.context"].with(
            fullpath: request.fullpath,
            assets: self.class["core.assets"],
            current_user: env["admin.current_user"],
            csrf_metatag: -> { Rack::Csrf.metatag(request.env) },
            csrf_tag: -> { Rack::Csrf.tag(request.env) },
            csrf_token: -> { Rack::Csrf.token(request.env) },
          )
        end
      end
    end

    register_plugin(:view, View)
    register_plugin(:auth, Auth)
    register_plugin(:contributor_auth, ContributorAuth)
    register_plugin(:main_view, MainView)
  end
end
