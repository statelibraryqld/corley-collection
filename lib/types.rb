require "dry/struct"
require "dry/types"
require "uri"
require "corley_collection/functions"

module Types
  include Dry::Types.module

  ## App settings

  def self.DatabaseURL(env)
    Types::Strict::String.constructor do |url|
      if env == :development && (branch = `git rev-parse --abbrev-ref HEAD`.strip) != "master"
        uri = URI(url)
        uri.path = "#{uri.path}_#{branch}"
        uri.to_s
      else
        url
      end
    end
  end

  ## For rom relations

  SymbolizedHash = Types::Hash.constructor { |value| CorleyCollection::Functions[:deep_symbolize_keys][value.to_h] }

  CommaSeparatedArray = Types::Strict::Array.constructor { |value| value.to_s.split(",").map(&:strip).reject { |v| v.empty? } }

  CapitalizedLocalityName = Types::Strict::String.constructor { |value| value.to_s.downcase.split.map(&:capitalize).join(" ") }
end
