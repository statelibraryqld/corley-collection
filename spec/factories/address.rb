Factory.define :address do |f|
  f.latitude { fake(:address, :latitude) }
  f.longitude { fake(:address, :longitude) }
  f.street_address { fake(:address, :full_address) }
  f.association(:spool)
end
