require "securerandom"
require "time_math"

Factory.define :contributor do |f|
  f.first_name { fake(:name, :name) }
  f.last_name { fake(:name, :name) }
  f.email { fake(:internet, :email) }
  f.suspended { false }
  f.encrypted_password ""
  f.password_reset_token { SecureRandom.hex }
  f.password_reset_token_expires_at { TimeMath.day.advance(Time.now) }
  f.public_token { SecureRandom.hex }

  f.trait :suspended do |t|
    t.suspended { true }
  end
end
