Factory.define :contributor_image do |f|
  f.association(:story)
  f.association(:contributor)

  f.caption { fake(:hitchhikers_guide_to_the_galaxy, :quote) }
  f.path { "path/to/the/asset.jpg" }
  f.content_type { "image/jpg" }
  f.size { 189_427 }
  f.published { true }
  f.created_at { Time.now }
  f.updated_at { Time.now }
end
