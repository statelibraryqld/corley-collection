Factory.define :favourite do |f|
  f.association(:photograph)

  f.trait :with_contributor do |t|
    t.association(:contributor)
  end
end
