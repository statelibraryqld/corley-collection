Factory.define :locality do |f|
  f.name { fake(:address, :community) }
  f.latitude { fake(:address, :latitude) }
  f.longitude { fake(:address, :longitude) }
end
