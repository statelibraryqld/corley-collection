require "securerandom"

Factory.define :locality_notification_subscription do |f|
  f.association(:locality)
  f.association(:contributor)
  f.token { SecureRandom.hex }
end
