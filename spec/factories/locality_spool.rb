Factory.define :locality_spool, relation: :localities_spools do |f|
  f.association(:locality)
  f.association(:spool)
end
