Factory.define :photograph do |f|
  f.sequence(:slq_identifier) { |n| "1000-1000-#{n}" }
  f.street_number { (1..100).to_a.sample.to_s }
  f.street_name { fake(:address, :street_name) }
  f.latitude { fake(:address, :latitude) }
  f.longitude { fake(:address, :longitude) }
  f.human_tagged_at { nil }
  f.geo_located_at { nil }
  f.street_address { fake(:address, :full_address) }
  f.postcode { (2000..2300).to_a.sample.to_s }
  f.house_name { nil }
  f.annotations { nil }
  f.damaged { false }
  f.shop { false }
  f.similar_photographs { [] }

  f.trait :with_spool do |t|
    t.association(:spool)
  end

  f.trait :with_locality do |t|
    t.association(:locality)
  end

  f.trait :unlocated do |t|
    t.street_number { nil }
    t.street_name { nil }
    t.street_address { nil }
    t.geo_located_at { nil }
    t.latitude { nil }
    t.longitude { nil }
    t.postcode { nil }
  end
end
