require "securerandom"

Factory.define :photograph_notification_subscription do |f|
  f.association(:photograph)
  f.association(:contributor)
  f.token { SecureRandom.hex }
end
