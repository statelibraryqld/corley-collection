Factory.define :photograph_tag, relation: :photographs_tags do |f|
  f.association(:photograph)
  f.association(:tag)
end
