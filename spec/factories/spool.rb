Factory.define :spool do |f|
  f.sequence(:slq_identifier) { |n| "1000-1000-#{n}" }
  f.content_description { fake(:lorem, :sentence) }
  f.photograph_count { (1..20).to_a.sample }
end
