Factory.define :story do |f|
  f.association(:photograph)
  f.association(:contributor)
  f.body { fake(:hitchhikers_guide_to_the_galaxy, :quote) }
  f.published { true }
end
