Factory.define(:tag) do |f|
  f.sequence(:name) { |n| "tag_#{n}" }
  f.type { fake(:artist, :name) }
  f.source { ["human", "machine"].sample }
end
