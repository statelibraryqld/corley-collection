require "securerandom"
require "time_math"

Factory.define :user do |f|
  f.name { fake(:name, :name) }
  f.email { fake(:internet, :email) }
  f.encrypted_password ""
  f.password_reset_token { SecureRandom.hex }
  f.password_reset_token_expires_at { TimeMath.day.advance(Time.now) }
  f.activation_token { SecureRandom.hex }
end
