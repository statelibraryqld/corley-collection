ENV["RACK_ENV"] = "test"
require "webmock/rspec"

# Allow external connections, otherwise requests to Elasticsearch are blocked.
WebMock.allow_net_connect!

require_relative "./support/suite"

SPEC_ROOT = Pathname(__FILE__).dirname
FIXTURES_PATH = SPEC_ROOT.join("fixtures").freeze

suite = CorleyCollection::TestSuite.instance

suite.start_coverage
suite.require_containers
