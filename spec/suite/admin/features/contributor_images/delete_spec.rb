require "support/web"

RSpec.feature "Admin / Contributor images / Delete", :js do
  let!(:admin) do
    factory[:user, email: "corley-collection-admin@corley-collection.test"]
  end

  let!(:contributor_image) do
    factory[:contributor_image]
  end

  scenario "I can delete a contributor image", :authorized do
    click_on "Contributor images"

    click_on contributor_image.id

    accept_confirm do
      find("a", text: "Delete").click
    end

    expect(page).to have_flash_message("Contributor image has been deleted")
  end
end
