require "support/web"

RSpec.feature "Admin / Contributor images / Edit", :js do
  let!(:admin) do
    factory[:user, email: "corley-collection-admin@corley-collection.test"]
  end

  let!(:contributor_image) do
    factory[:contributor_image]
  end

  scenario "I can edit a contributor image", :authorized do
    click_on "Contributor images"

    click_on contributor_image.id

    find("[data-field-name=caption] [data-field-input]").set("Fixing the caption")
    find("button", text: "Save changes").click

    expect(page).to have_flash_message("Contributor image has been updated")
  end
end
