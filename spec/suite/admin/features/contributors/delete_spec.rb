require "support/web"

RSpec.feature "Admin / Contributors / Delete", :js do
  let!(:admin) do
    factory[:user, email: "corley-collection-admin@corley-collection.test"]
  end

  let!(:contributor) do
    factory[:contributor, email: "andrew@example.com"]
  end

  scenario "I can delete a contributor", :authorized do
    click_on "Contributors"

    click_on "andrew@example.com"

    accept_confirm do
      find("a", text: "Delete").click
    end

    expect(page).to have_flash_message("Contributor has been deleted")
  end
end
