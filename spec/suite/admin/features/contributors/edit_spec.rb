require "support/web"

RSpec.feature "Admin / Contributors / Edit", :js do
  let!(:admin) do
    factory[:user, email: "corley-collection-admin@corley-collection.test"]
  end

  let!(:contributor) do
    factory[:contributor, email: "andrew@example.com"]
  end

  scenario "I can edit a constributor", :authorized do
    click_on "Contributors"

    click_on "andrew@example.com"

    find("[data-field-name=email] [data-field-input]").set("andrew-corrected@example.com")
    find("button", text: "Save changes").click

    expect(page).to have_flash_message("Contributor has been updated")
  end
end
