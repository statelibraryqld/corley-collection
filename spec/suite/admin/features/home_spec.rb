require "support/web"

RSpec.feature "Admin / Home" do
  scenario "not signed in" do
    visit "/admin"

    expect(page).to have_content("Sign in")
  end
end
