require "support/web"

RSpec.feature "Admin / Photographs", :js do
  let!(:admin) do
    factory[:user, email: "corley-collection-admin@corley-collection.test"]
  end

  scenario "I can create a photograph", :authorized do
    click_on "Photographs"
    click_on "Add a photograph"

    find("[data-field-name=slq_identifier] [data-field-input]").set("1000-1000-1000")
    find("button", text: "Create photograph").click

    expect(page).to have_flash_message("Photograph has been created")
  end
end
