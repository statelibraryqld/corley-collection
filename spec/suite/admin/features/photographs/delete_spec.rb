require "support/web"

RSpec.feature "Admin / Photographs / Delete", :js do
  let!(:admin) do
    factory[:user, email: "corley-collection-admin@corley-collection.test"]
  end

  let!(:photograph) do
    factory[:photograph, slq_identifier: "1000-1991-9121"]
  end

  scenario "I can delete a photograph", :authorized do
    click_on "Photographs"

    click_on "1000-1991-9121"

    accept_confirm do
      find("a", text: "Delete").click
    end

    expect(page).to have_flash_message("Photograph has been deleted")
  end
end
