require "support/web"

RSpec.feature "Admin / Photographs / Edit", :js do
  let!(:admin) do
    factory[:user, email: "corley-collection-admin@corley-collection.test"]
  end

  let!(:photograph) do
    factory[:photograph]
  end

  scenario "I can edit a photograph", :authorized do
    click_on "Photographs"

    click_on photograph.slq_identifier

    find("[data-field-name=slq_identifier] [data-field-input]").set("1000-1000-1000")
    find("button", text: "Save changes").click

    expect(page).to have_flash_message("Photograph has been updated")
  end

  scenario "I can't update a photograph with invalid data", :authorized do
    click_on "Photographs"

    click_on photograph.slq_identifier

    find("[data-field-name=slq_identifier] [data-field-input]").set("", clear: :backspace)
    find("button", text: "Save changes").click

    within("[data-field-name=slq_identifier]") do
      expect(page).to have_content("must be filled")
    end
  end
end
