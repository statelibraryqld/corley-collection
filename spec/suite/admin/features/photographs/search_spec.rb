require "support/web"
require "support/search"

RSpec.feature "Admin / Photographs / Search", :js, :search do
  let!(:admin) do
    factory[:user, email: "corley-collection-admin@corley-collection.test"]
  end

  index!(:photograph_1) do
    factory[:photograph, slq_identifier: "1001"]
  end

  index!(:photograph_2) do
    factory[:photograph, slq_identifier: "1002"]
  end

  scenario "I can search for a photograph", :authorized do
    click_on "Photographs"

    find(:css, "input[type='search']").set("1002")
    find("button", text: "Search").click

    within(".table") do
      expect(page).to have_content("1002")
      expect(page).to_not have_content("1001")
    end

    expect(page).to_not have_content("Page 1 of")
  end
end
