require "support/web"

RSpec.feature "Admin / Sign in", :js do
  given!(:user) do
    factory[:user, email: "test@gmail.com"]
  end

  scenario "I can sign in and sign out" do
    visit "/admin/sign-in"

    expect(page).to have_content("Sign in")

    fill_in("user-email", with: "test@gmail.com")
    fill_in("user-password", with: "password")

    click_on "Sign in"

    expect(page).to have_flash_message("You are now signed in")
    expect(page).to have_content("You’re logged in as: test@gmail.com")

    accept_confirm do
      click_on "Log out"
    end

    expect(page).to have_flash_message("You are now signed out")
    expect(page).to have_content("Sign in")
  end

  scenario "I see error message when invalid credentials are used" do
    visit "/admin/sign-in"

    fill_in("user-email", with: "no-such-user@corley-collection.test")
    fill_in("user-password", with: "password")

    click_on "Sign in"

    expect(page).to have_flash_message("Email or password is not correct")
  end
end
