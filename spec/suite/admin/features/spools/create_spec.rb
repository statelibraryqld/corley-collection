require "support/web"

RSpec.feature "Admin / Spools / Create", :js do
  let!(:admin) do
    factory[:user, email: "corley-collection-admin@corley-collection.test"]
  end

  scenario "I can create a spool", :authorized do
    click_on "Spools"
    click_on "Add a spool"

    find("[data-field-name=slq_identifier] [data-field-input]").set("1000-1000")
    find("button", text: "Create spool").click

    expect(page).to have_flash_message("Spool has been created")
  end
end
