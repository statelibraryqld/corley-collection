require "support/web"

RSpec.feature "Admin / Spools / Delete", :js do
  let!(:admin) do
    factory[:user, email: "corley-collection-admin@corley-collection.test"]
  end

  let!(:spool) do
    factory[:spool, slq_identifier: "1000-1991"]
  end

  scenario "I can delete a spool", :authorized do
    click_on "Spools"

    click_on "1000-1991"

    accept_confirm do
      find("a", text: "Delete").click
    end

    expect(page).to have_flash_message("Spool has been deleted")
  end
end
