require "support/web"

RSpec.feature "Admin / Spools / Edit", :js do
  let!(:admin) do
    factory[:user, email: "corley-collection-admin@corley-collection.test"]
  end

  let!(:spool) do
    factory[:spool]
  end

  scenario "I can edit a spool", :authorized do
    click_on "Spools"

    click_on spool.slq_identifier

    find("[data-field-name=slq_identifier] [data-field-input]").set("1000-1000")
    find("button", text: "Save changes").click

    expect(page).to have_flash_message("Spool has been updated")
  end

  scenario "I can't update a spool with invalid data", :authorized do
    click_on "Spools"

    click_on spool.slq_identifier

    find("[data-field-name=slq_identifier] [data-field-input]").set("", clear: :backspace)
    find("button", text: "Save changes").click

    within("[data-field-name=slq_identifier]") do
      expect(page).to have_content("must be filled")
    end
  end
end
