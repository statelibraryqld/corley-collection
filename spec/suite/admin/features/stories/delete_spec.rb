require "support/web"

RSpec.feature "Admin / Stories / Delete", :js do
  let!(:admin) do
    factory[:user, email: "corley-collection-admin@corley-collection.test"]
  end

  let!(:story) do
    factory[:story]
  end

  scenario "I can delete a story", :authorized do
    click_on "Stories"

    click_on story.id

    accept_confirm do
      find("a", text: "Delete").click
    end

    expect(page).to have_flash_message("Story has been deleted")
  end
end
