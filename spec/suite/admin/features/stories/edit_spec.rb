require "support/web"

RSpec.feature "Admin / Stories / Edit", :js do
  let!(:admin) do
    factory[:user, email: "corley-collection-admin@corley-collection.test"]
  end

  let!(:story) do
    factory[:story]
  end

  scenario "I can edit a story", :authorized do
    click_on "Stories"

    click_on story.id

    find("[data-field-name=body] [data-field-input]").set("Fixing the body")
    find("button", text: "Save changes").click

    expect(page).to have_flash_message("Story has been updated")
  end
end
