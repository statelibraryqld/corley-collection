require "support/web"

RSpec.feature "Admin / User / Dashboard", :js do
  let!(:admin) do
    factory[:user, name: "Jane"]
  end

  scenario "I can view my dashboard", :authorized do
    click_on "Admin"

    expect(page).to have_content("Dashboard")
  end
end
