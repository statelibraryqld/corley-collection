require "support/web"

RSpec.feature "Admin / User / Edit", :js do
  let!(:admin) do
    factory[:user, name: "Jane"]
  end

  scenario "I can edit my account with valid data", :authorized do
    click_on "Account"

    find("[data-field-name=password] [data-field-input]").set("newpassword")
    find("[data-field-name=password_confirmation] [data-field-input]").set("", clear: :backspace)

    find("button", text: "Save changes").click

    within("[data-field-name=password_confirmation]") do
      expect(page).to have_content("must match password")
    end

    find("[data-field-name=name] [data-field-input]").set("", clear: :backspace)
    find("[data-field-name=password_confirmation] [data-field-input]").set("newpassword")

    find("button", text: "Save changes").click

    within("[data-field-name=name]") do
      expect(page).to have_content("must be filled")
    end

    find("[data-field-name=name] [data-field-input]").set("Jane Doe")
    find("button", text: "Save changes").click

    expect(page).to have_flash_message("Your account has been updated")
  end
end
