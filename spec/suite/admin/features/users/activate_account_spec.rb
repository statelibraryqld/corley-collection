require "support/web"

RSpec.feature "Admin / Users / Activate account", :js do
  let(:user) { factory[:user] }

  scenario "I can activate my account by setting a password, and be signed in automatically" do
    visit "/admin/users/activate/#{user.activation_token}"

    expect(page).to have_content("Activate your account by setting a password")

    find("[data-field-name=password] [data-field-input]").set("password")
    find("[data-field-name=password_confirmation] [data-field-input]").set("password")

    click_on "Save"

    expect(page).to have_content("Your password has been set")
    expect(page.current_path).to eq("/admin")
  end

  scenario "I see an error message when I don't provide valid input" do
    visit "/admin/users/activate/#{user.activation_token}"

    expect(page).to have_content("Activate your account by setting a password")

    # First we don't fill either field
    find("[data-field-name=password] [data-field-input]").set("", clear: :backspace)
    find("[data-field-name=password_confirmation] [data-field-input]").set("", clear: :backspace)

    click_on "Save"

    expect(page).to have_content("must be filled")

    # Then we fill password and password_confirmation with different values
    find("[data-field-name=password] [data-field-input]").set("password")
    find("[data-field-name=password_confirmation] [data-field-input]").set("notpassword")

    click_on "Save"

    expect(page).to have_content("must match password")
  end

  scenario "I see an error message when the activation token is invalid" do
    visit "/admin/users/activate/notatoken"

    expect(page).to have_content("Activation token is invalid")
  end
end
