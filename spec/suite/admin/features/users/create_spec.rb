require "support/web"
require "support/bg"

RSpec.feature "Admin / Users", :js do
  let!(:admin) do
    factory[:user, email: "corley-collection-admin@corley-collection.test"]
  end

  scenario "I can create a user with valid data", :authorized do
    click_on "Users"
    click_on "Add a user"

    find("[data-field-name=name] [data-field-input]").set("", clear: :backspace)

    find("button", text: "Create user").click

    within("[data-field-name=name]") do
      expect(page).to have_content("must be filled")
    end

    find("[data-field-name=name] [data-field-input]").set("Jane")
    find("[data-field-name=email] [data-field-input]").set("jane@corley-collection.test")

    find("button", text: "Create user").click

    expect(page).to have_flash_message("User has been created")

    expect(Mail::Container[:mailer].messages.last).to include(
      to: "jane@corley-collection.test",
      subject: "Corley Collection account activation",
      text_body: /A Corley Collection account has been created for you/
    )
  end

  scenario "I can't create a user with the email of an existing user", :authorized do
    click_on "Users"
    click_on "Add a user"

    find("[data-field-name=name] [data-field-input]").set("Jane")
    find("[data-field-name=email] [data-field-input]").set("corley-collection-admin@corley-collection.test")

    find("button", text: "Create user").click

    expect(page).to have_content("email address is already taken")
  end
end
