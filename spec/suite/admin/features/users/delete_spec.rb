require "support/web"

RSpec.feature "Admin / Users / Delete", :js do
  let!(:admin) do
    factory[:user]
  end

  let!(:user) do
    factory[:user, email: "jane@corley-collection.test"]
  end

  scenario "I can edit a user", :authorized do
    click_on "Users"

    click_on "jane@corley-collection.test"

    accept_confirm do
      find("a", text: "Delete").click
    end

    expect(page).to have_flash_message("User has been deleted")
  end
end
