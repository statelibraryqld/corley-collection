require "support/web"

RSpec.feature "Admin / Users / Edit", :js do
  let!(:admin) do
    factory[:user]
  end

  let!(:user) do
    factory[:user, name: "Jane"]
  end

  scenario "I can edit a user with valid data", :authorized do
    click_on "Users"

    click_on user.email

    find("[data-field-name=name] [data-field-input]").set("", clear: :backspace)

    within("[data-field-name=name]") do
      expect(page).to have_content("must be filled")
    end

    find("[data-field-name=name] [data-field-input]").set("Jane Doe")

    find("button", text: "Save changes").click

    expect(page).to have_flash_message("User has been updated")
    expect(page).to have_content("Jane Doe")
  end
end
