require "support/web"
require "support/bg"
require "support/mail"

RSpec.feature "Admin / Users / Forgot password", :js do
  let(:user) { factory[:user] }

  scenario "A password reset success email is sent when I enter a valid email address" do
    visit "/admin/sign-in"
    click_on "Forgot password"

    expect(page).to have_content("Forgotten your password?")

    find("[data-field-name=email] [data-field-input]").set(user.email)
    click_on "Submit"

    expect(page).to have_content("Check your email for password reset instructions")
    expect(Mail::Container[:mailer].messages.last).to include(
      to: user.email,
      subject: "Corley Collection password reset request",
      text_body: /You can reset your password by visiting this URL/
    )
  end

  scenario "A password reset failure email is sent when I enter an invalid email address" do
    visit "/admin/sign-in"
    click_on "Forgot password"

    expect(page).to have_content("Forgotten your password?")

    find("[data-field-name=email] [data-field-input]").set("notauser@example.com")
    click_on "Submit"

    expect(page).to have_content("Check your email for password reset instructions")
    expect(Mail::Container[:mailer].messages.last).to include(
      to: "notauser@example.com",
      subject: "Corley Collection password reset request",
      text_body: /Unfortunately no account was found matching this address/
    )
  end

  scenario "I see an error message when I don't provide a properly formatted email address" do
    visit "/admin/sign-in"
    click_on "Forgot password"

    expect(page).to have_content("Forgotten your password?")

    find("[data-field-name=email] [data-field-input]").set("notanemailaddress")
    click_on "Submit"

    expect(page).to have_content("is not an email address")
  end
end
