require "support/web"

RSpec.feature "Admin / Users / Reset password", :js do
  let(:user_with_current_token) { factory[:user] }
  let(:user_with_expired_token) { factory[:user, password_reset_token_expires_at: time.yesterday] }

  scenario "I can reset my password, and be signed in automatically" do
    visit "/admin/users/reset-password/#{user_with_current_token.password_reset_token}"

    expect(page).to have_content("Set your password")

    find("[data-field-name=password] [data-field-input]").set("password")
    find("[data-field-name=password_confirmation] [data-field-input]").set("password")

    click_on "Save"

    expect(page).to have_content("Your password has been set")
    expect(page.current_path).to eq("/admin")
  end

  scenario "I see an error message when I don't provide valid input" do
    visit "/admin/users/reset-password/#{user_with_current_token.password_reset_token}"

    expect(page).to have_content("Set your password")

    # First we don't fill either field
    find("[data-field-name=password] [data-field-input]").set("", clear: :backspace)
    find("[data-field-name=password_confirmation] [data-field-input]").set("", clear: :backspace)

    click_on "Save"

    expect(page).to have_content("must be filled")

    # Then we fill password and password_confirmation with different values
    find("[data-field-name=password] [data-field-input]").set("password")
    find("[data-field-name=password_confirmation] [data-field-input]").set("notpassword")

    click_on "Save"

    expect(page).to have_content("must match password")
  end

  scenario "I see an error message when the password reset token is invalid or expired" do
    # First we provide an expired token
    visit "/admin/users/reset-password/#{user_with_expired_token.password_reset_token}"

    expect(page).to have_content("Password reset token is invalid or expired")

    # Then we provide a non-existent token
    visit "/admin/users/reset-password/notatoken"

    expect(page).to have_content("Password reset token is invalid or expired")
  end
end
