require "support/db"
require "admin/user_repo"

RSpec.describe Admin::UserRepo do
  subject(:user_repo) { described_class.new }

  let(:user_with_current_token) { factory[:user, password_reset_token: "123"] }
  let(:user_with_expired_token) { factory[:user, password_reset_token: "456", password_reset_token_expires_at: time.yesterday] }

  describe "#by_current_password_reset_token" do
    it "returns a user by their password reset token if the token is current" do
      expect(user_repo.by_current_password_reset_token(user_with_current_token.password_reset_token).id).to eql(user_with_current_token.id)
    end

    it "doesn't return a user by their password reset token if the token is expired" do
      expect(user_repo.by_current_password_reset_token(user_with_expired_token.password_reset_token)).to eql(nil)
    end
  end

  describe "#password_reset_token_exists?" do
    it "returns true if a user exists with the given password reset token" do
      expect(user_repo.password_reset_token_exists?(user_with_current_token.password_reset_token)).to eql(true)
    end

    it "returns false if a user doesn't exist with the given password reset token" do
      expect(user_repo.password_reset_token_exists?("789")).to eql(false)
    end
  end
end
