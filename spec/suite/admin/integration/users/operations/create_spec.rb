require "support/db"
require "admin/users/operations/create"

RSpec.describe Admin::Users::Operations::Create, "#call" do
  subject(:create) { described_class.new }

  let(:users) { relations[:users].with(auto_struct: true) }

  let!(:existing_user) { factory[:user] }

  it "creates a new user" do
    result = create.(
      name: "Jane Doe",
      email: "jane@corley-collection.test"
    )

    expect(result).to be_success

    user = result.value!

    expect(user.id).to_not be(nil)
    expect(user.name).to eql("Jane Doe")
    expect(user.email).to eql("jane@corley-collection.test")
    expect(user.activation_token).to_not be(nil)
  end

  it "doesn't create a new user with an email address that's already in use" do
    result = create.(
      name: "Jane Doe",
      email: existing_user.email
    )

    expect(result).to be_failure
  end
end
