require "support/db"
require "admin/users/operations/delete"

RSpec.describe Admin::Users::Operations::Delete, "#call" do
  subject(:delete) { described_class.new }

  let!(:user) { factory[:user, name: "Jane"] }

  let(:user_repo) { Admin::Container[:user_repo] }

  it "deletes an existing user" do
    delete.(user.id)

    expect(user_repo[user.id]).to be(nil)
  end
end
