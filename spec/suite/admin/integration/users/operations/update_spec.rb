require "support/db"
require "admin/users/operations/update"

RSpec.describe Admin::Users::Operations::Update, "#call" do
  subject(:update) { described_class.new }

  let!(:user) { factory[:user, name: "Jane"] }
  let!(:another_user) { factory[:user] }

  let(:users) { relations[:users].with(auto_struct: true) }

  let(:params) do
    user.to_h
  end

  it "updates an existing user" do
    result = update.(user.id, params.merge(name: "Jane Doe"))

    expect(result).to be_success

    user = result.value!

    expect(user.name).to eql("Jane Doe")
  end

  it "doesn't update an existing user with an email address that's already in use" do
    result = update.(user.id, params.merge(name: "Jane Doe", email: another_user.email))

    expect(result).to be_failure
  end
end
