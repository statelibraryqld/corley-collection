require "support/view"

require "admin/user_repo"
require "admin/views/users/index"

RSpec.describe Admin::Views::Users::Index, "#call" do
  subject(:view) do
    described_class.new(user_repo: user_repo)
  end

  let(:user_repo) do
    instance_double(Admin::UserRepo)
  end

  context "rendering index" do
    let(:users) do
      paged_collection([user_1, user_2])
    end

    let(:user_1) do
      double(:user, id: 1, name: "Jane", email: "jane@doe.org")
    end

    let(:user_2) do
      double(:user, id: 2, name: "John", email: "john@doe.org")
    end

    let(:output) { render(view, page: 1, params: {}) }

    it "includes a list of users" do
      expect(user_repo).to receive(:index).with(1).and_return(users)

      expect(output).to have_selector("tbody tr", count: 2)

      expect(output).to have_selector("td", text: "Jane")
      expect(output).to have_selector("td", text: "jane@doe.org")

      expect(output).to have_selector("td", text: "John")
      expect(output).to have_selector("td", text: "john@doe.org")

      expect(output).to have_selector('a[href="?page=2"]', text: "Next")
    end
  end
end
