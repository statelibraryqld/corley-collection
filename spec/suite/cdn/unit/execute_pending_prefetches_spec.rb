require "cdn/operations/execute_pending_prefetches"
require "cdn/pending_cdn_action_repo"
require "cdn/cdn77/client"

RSpec.describe Cdn::Operations::ExecutePendingPrefetches do
  subject(:execute_pending_prefetches) {
    described_class.new(
      pending_cdn_action_repo: pending_cdn_action_repo,
      cdn: cdn_client
    )
  }

  let(:pending_cdn_action_repo) do
    instance_double(Cdn::PendingCdnActionRepo)
  end

  let(:cdn_client) do
    instance_double(Cdn::Cdn77::Client)
  end

  let(:pending_prefetch_1) do
    double(
      :pending_cdn_action,
      id: 1,
      type: "prefetch",
      path: "/pending-prefetch-1"
    )
  end

  let(:pending_prefetch_2) do
    double(
      :pending_cdn_action,
      id: 2,
      type: "prefetch",
      path: "/pending-prefetch-2"
    )
  end

  before do
    allow(pending_cdn_action_repo).to receive(:prefetches).and_return([pending_prefetch_1, pending_prefetch_2])
    allow(cdn_client).to receive(:prefetch_paths)
    allow(pending_cdn_action_repo).to receive(:delete)
  end

  it "actions pending prefetch requests" do
    execute_pending_prefetches.()

    expect(cdn_client).to have_received(:prefetch_paths).with(["/pending-prefetch-1", "/pending-prefetch-2"])
    expect(pending_cdn_action_repo).to have_received(:delete).with([1, 2])
  end
end
