require "cdn/operations/execute_pending_purges"
require "cdn/pending_cdn_action_repo"
require "cdn/operations/execute_pending_prefetches"
require "cdn/cdn77/client"
require "dry/monads"

RSpec.describe Cdn::Operations::ExecutePendingPurges do
  subject(:execute_pending_purges) {
    described_class.new(
      pending_cdn_action_repo: pending_cdn_action_repo,
      execute_pending_prefetches: execute_pending_prefetches,
      cdn: cdn_client
    )
  }

  let(:pending_cdn_action_repo) do
    instance_double(Cdn::PendingCdnActionRepo)
  end

  let(:execute_pending_prefetches) do
    instance_double(Cdn::Operations::ExecutePendingPrefetches)
  end

  let(:cdn_client) do
    instance_double(Cdn::Cdn77::Client)
  end

  let(:pending_purge_1) do
    double(
      :pending_cdn_action,
      id: 1,
      type: "purge",
      path: "/pending-purge-1"
    )
  end

  let(:pending_purge_2) do
    double(
      :pending_cdn_action,
      id: 2,
      type: "purge",
      path: "/pending-purge-2"
    )
  end

  before do
    allow(pending_cdn_action_repo).to receive(:purges).and_return([pending_purge_1, pending_purge_2])
    allow(cdn_client).to receive(:purge_paths).and_return(Dry::Monads::Success(double(:response)))
    allow(pending_cdn_action_repo).to receive(:delete)
    allow(execute_pending_prefetches).to receive(:enqueue)
  end

  it "actions pending purge requests, and enqueues the execute pending prefetches job" do
    execute_pending_purges.()

    expect(cdn_client).to have_received(:purge_paths).with(["/pending-purge-1", "/pending-purge-2"])
    expect(pending_cdn_action_repo).to have_received(:delete).with([1, 2])
    expect(execute_pending_prefetches).to have_received(:enqueue)
  end
end
