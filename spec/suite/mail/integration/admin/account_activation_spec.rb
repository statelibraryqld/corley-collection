require "support/db"
require "mail/admin/account_activation"

RSpec.describe Mail::Admin::AccountActivation do
  subject(:account_activation) { described_class.new }

  describe "#delivery_hash" do
    let(:user) { factory[:user, name: "Jane Doe"] }
    let(:mail) { account_activation.delivery_hash(user: user) }

    it "renders an email containing an account activation link" do
      expect(mail[:to]).to eq user.email

      expect(mail[:subject]).to eq "Corley Collection account activation"

      expect(mail[:html_body]).to include "Hello Jane Doe"
      expect(mail[:html_body]).to include "A Corley Collection account has been created for you."
      expect(mail[:html_body]).to include "You must activate your account before you can sign in; please do so by following <a href=\"http://localhost:3000/admin/users/activate/#{user.activation_token}\">this link</a>."
    end
  end
end
