require "support/db"
require "mail/admin/password_reset_success"

RSpec.describe Mail::Admin::PasswordResetSuccess do
  subject(:password_reset_success) { described_class.new }

  describe "#delivery_hash" do
    let(:user) { factory[:user, name: "Jane Doe"] }
    let(:mail) { password_reset_success.delivery_hash(user: user) }

    it "renders an email containing a password reset link" do
      expect(mail[:to]).to eq user.email

      expect(mail[:subject]).to eq "Corley Collection password reset request"

      expect(mail[:html_body]).to include "Hello Jane Doe"
      expect(mail[:html_body]).to include "Recently you (or perhaps someone else) requested a password reset for a Corley Collection account with this email address."
      expect(mail[:html_body]).to include "You can reset your password by following <a href=\"http://localhost:3000/admin/users/reset-password/#{user.password_reset_token}\">this link</a>, which is valid for only 24 hours."
      expect(mail[:html_body]).to include "If you didn't request a password reset please disgregard this email. You can continue to use your existing password to login."
    end
  end
end
