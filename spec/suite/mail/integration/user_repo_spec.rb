require "support/db"
require "mail/user_repo"

RSpec.describe Mail::UserRepo do
  subject(:user_repo) { described_class.new }

  let(:user) { factory[:user] }

  describe "#[]" do
    it "returns a user by their ID" do
      expect(user_repo[user.id.to_s].id).to eql(user.id)
    end
  end
end
