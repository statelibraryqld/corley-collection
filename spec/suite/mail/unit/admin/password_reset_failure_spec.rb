require "mail/admin/password_reset_failure"

RSpec.describe Mail::Admin::PasswordResetFailure do
  subject(:password_reset_failure) { described_class.new }

  describe "#delivery_hash" do
    let(:email) { "notauser@example.com" }
    let(:mail) { password_reset_failure.delivery_hash(email: email) }

    it "renders an email not containing a password reset link" do
      expect(mail[:to]).to eq "notauser@example.com"

      expect(mail[:subject]).to eq "Corley Collection password reset request"

      expect(mail[:html_body]).to include "Recently you (or perhaps someone else) requested a password reset for a Corley Collection account with this email address."
      expect(mail[:html_body]).to include "Unfortunately no account was found matching this address."
    end
  end
end
