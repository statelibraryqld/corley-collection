require "mail/view/controller"

RSpec.describe Mail::View::Controller do
  subject(:mail) { test_mail.new }

  let(:test_mail) {
    Class.new(described_class) do
      configure do |config|
        config.paths = [FIXTURES_PATH.join("templates/mail")]
        config.layout = false
        config.template = "test"
      end

      expose(:from) { "welcome@corley-collection.dev" }
      expose(:to) { "jane@doe.org" }
      expose(:subject) { "Welcome to Corley Collection!" }

      expose :user_name
    end
  }

  let(:input) { {user_name: "Jane Doe"} }

  describe "#delivery_hash" do
    subject(:delivery_hash) { mail.delivery_hash(input) }

    it "includes from:" do
      expect(delivery_hash).to include(from: "welcome@corley-collection.dev")
    end

    it "includes to:" do
      expect(delivery_hash).to include(to: "jane@doe.org")
    end

    it "includes subject:" do
      expect(delivery_hash).to include(subject: "Welcome to Corley Collection!")
    end

    it "includes html_body:" do
      expect(delivery_hash).to include(html_body: "<p>Welcome to Corley Collection Jane Doe!</p>")
    end

    it "includes text_body:" do
      expect(delivery_hash).to include(text_body: "Welcome to Corley Collection Jane Doe!\n")
    end
  end
end
