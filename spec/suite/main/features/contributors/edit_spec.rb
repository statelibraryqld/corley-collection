require "support/web"

RSpec.feature "Main / Contributors / Edit", :js do
  given!(:contributor) do
    factory[:contributor, email: "contributor@example.com"]
  end

  given!(:another_contributor) do
    factory[:contributor, email: "another-contributor@example.com"]
  end

  scenario "A contributor edits their account" do
    visit "/contributors/sign-in"

    expect(page).to have_content("Sign in")

    fill_in("contributor-email", with: "contributor@example.com")
    fill_in("contributor-password", with: "password")
    click_on "Sign in"

    visit "/account/edit"

    expect(page).to have_content("Edit your account")

    fill_in("contributor-first-name", with: "Claire")
    fill_in("contributor-last-name", with: "Spotswood")
    fill_in("contributor-email", with: "contributor-adjusted@example.com")

    click_on "Update"

    expect(page).to have_flash_message("Your account has been updated")
    expect(page.current_path).to eql("/account")

    expect(page).to have_content("Claire Spotswood")
    expect(page).to have_content("contributor-adjusted@example.com")
  end

  scenario "A contributor sets their email to that of another contributor" do
    visit "/contributors/sign-in"

    expect(page).to have_content("Sign in")

    fill_in("contributor-email", with: "contributor@example.com")
    fill_in("contributor-password", with: "password")
    click_on "Sign in"

    visit "/account/edit"

    expect(page).to have_content("Edit your account")

    fill_in("contributor-first-name", with: "Claire")
    fill_in("contributor-last-name", with: "Spotswood")
    fill_in("contributor-email", with: "another-contributor@example.com")

    click_on "Update"

    expect(page).to have_content("email address is already taken")
  end
end
