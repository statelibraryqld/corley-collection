require "support/web"
require "support/bg"
require "support/mail"

RSpec.feature "Main / Contributors / Forgot password", :js do
  let!(:contributor) { factory[:contributor, email: "jane@example.com"] }

  scenario "A password reset success email is sent when I enter a valid email address" do
    visit "/contributors/sign-in"
    click_on "Forgot password"

    expect(page).to have_content("Forgotten your password?")

    fill_in("contributor-email", with: "jane@example.com")
    click_on "Reset password"

    expect(page).to have_content("Check your email for password reset instructions")
    expect(Mail::Container[:mailer].messages.last).to include(
      to: "jane@example.com",
      subject: "Corley Collection password reset request",
      text_body: /You can reset your password by visiting this URL/
    )
  end

  scenario "A password reset failure email is sent when I enter an invalid email address" do
    visit "/contributors/sign-in"
    click_on "Forgot password"

    expect(page).to have_content("Forgotten your password?")

    fill_in("contributor-email", with: "notauser@example.com")
    click_on "Reset password"

    expect(page).to have_content("Check your email for password reset instructions")
    expect(Mail::Container[:mailer].messages.last).to include(
      to: "notauser@example.com",
      subject: "Corley Collection password reset request",
      text_body: /Unfortunately no contributor account was found matching this address/
    )
  end
end
