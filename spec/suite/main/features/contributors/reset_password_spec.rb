require "support/web"

RSpec.feature "Main / Contributors / Reset password", :js do
  let!(:contributor_with_current_token) { factory[:contributor] }
  let!(:contributor_with_expired_token) { factory[:contributor, password_reset_token_expires_at: time.yesterday] }

  scenario "I can reset my password, and be signed in automatically" do
    visit "/contributors/reset-password/#{contributor_with_current_token.password_reset_token}"

    expect(page).to have_content("Reset your password")

    fill_in("contributor-password", with: "password")
    fill_in("contributor-password-confirmation", with: "password")

    click_on "Reset password"

    expect(page).to have_content("Your password has been set")
    expect(page.current_path).to eq("/")
  end

  scenario "I see an error message when I don't provide valid input" do
    visit "/contributors/reset-password/#{contributor_with_current_token.password_reset_token}"

    expect(page).to have_content("Reset your password")

    click_on "Reset password"

    expect(page).to have_content("must be filled")

    fill_in("contributor-password", with: "password")
    fill_in("contributor-password-confirmation", with: "not the password")

    click_on "Reset password"

    expect(page).to have_content("must match password")
  end

  scenario "I see an error message when the password reset token is expired" do
    visit "/contributors/reset-password/#{contributor_with_expired_token.password_reset_token}"

    expect(page).to have_content("Password reset token is invalid or expired")
  end

  scenario "I see an error message when the password reset token is invalid" do
    visit "/contributors/reset-password/notatoken"

    expect(page).to have_content("Password reset token is invalid or expired")
  end
end
