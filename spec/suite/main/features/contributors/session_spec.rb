require "support/web"

RSpec.feature "Main / Contributors / Session", :js do
  given!(:contributor) do
    factory[:contributor, email: "contributor@gmail.com"]
  end

  given!(:suspended_contributor) do
    factory[:contributor, :suspended, email: "suspended@gmail.com"]
  end

  scenario "I can sign in and sign out" do
    visit "/contributors/sign-in"

    expect(page).to have_content("Sign in")

    fill_in("contributor-email", with: "contributor@gmail.com")
    fill_in("contributor-password", with: "password")

    click_on "Sign in"

    expect(page).to have_flash_message("Welcome. You are now signed in")

    visit "/account"

    expect(page).to have_content("Your account")
    expect(page).to have_content("contributor@gmail.com")

    click_on "Sign out"

    expect(page).to have_flash_message("You are now signed out")
    expect(page).to have_content("Sign in")
  end

  scenario "I visit the sign in page while signed in" do
    visit "/contributors/sign-in"

    expect(page).to have_content("Sign in")

    fill_in("contributor-email", with: "contributor@gmail.com")
    fill_in("contributor-password", with: "password")

    click_on "Sign in"

    expect(page).to have_flash_message("Welcome. You are now signed in")

    visit "/contributors/sign-in"

    expect(page).to have_flash_message("You are already signed in")
    expect(page.current_path).to eql("/account")
  end

  scenario "I see an error message when invalid credentials are used" do
    visit "/contributors/sign-in"

    fill_in("contributor-email", with: "no-such-user@corley-collection.test")
    fill_in("contributor-password", with: "password")

    click_on "Sign in"

    expect(page).to have_flash_message("Email or password is not correct")
  end

  scenario "I cannot sign in with suspended credentials" do
    visit "/contributors/sign-in"

    fill_in("contributor-email", with: "suspended@gmail.com")
    fill_in("contributor-password", with: "password")

    click_on "Sign in"

    expect(page).to have_flash_message("Your account has been suspended")
  end

  scenario "I am signed out if suspended while signed in" do
    visit "/contributors/sign-in"

    fill_in("contributor-email", with: "contributor@gmail.com")
    fill_in("contributor-password", with: "password")

    click_on "Sign in"

    expect(page).to have_flash_message("Welcome. You are now signed in")

    relations[:contributors].by_pk(contributor.id).update(suspended: true)

    visit "/account"

    expect(page).to have_flash_message("Your account has been suspended")
    expect(page.current_path).to eql("/contributors/sign-in")
  end
end
