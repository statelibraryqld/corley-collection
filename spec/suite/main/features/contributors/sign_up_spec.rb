require "support/web"
require "support/bg"

RSpec.feature "Main / Contributors / Sign up", :js do
  given!(:contributor) do
    factory[:contributor, email: "existing-contributor@example.com"]
  end

  scenario "A contributor can sign up" do
    visit "/contributors/sign-up"

    expect(page).to have_content("Sign up")

    fill_in("contributor-first-name", with: "Claire")
    fill_in("contributor-last-name", with: "Spotswood")
    fill_in("contributor-email", with: "contributor@example.com")
    fill_in("contributor-password", with: "password")
    fill_in("contributor-password-confirmation", with: "password")
    check("contributor[accept_terms]")

    click_on "Sign up"

    expect(page).to have_flash_message("Welcome. You are now a Corley Collection contributor")
    expect(page.current_path).to eql("/")

    visit "/account"

    expect(page).to have_content("Your account")
    expect(page).to have_content("contributor@example.com")

    expect(Mail::Container[:mailer].messages.last).to include(
      to: "contributor@example.com",
      subject: "Welcome to the Corley Explorer"
    )
  end

  scenario "A contributor can't sign up without accepting the terms" do
    visit "/contributors/sign-up"

    expect(page).to have_content("Sign up")

    fill_in("contributor-first-name", with: "Claire")
    fill_in("contributor-last-name", with: "Spotswood")
    fill_in("contributor-email", with: "contributor@example.com")
    fill_in("contributor-password", with: "password")
    fill_in("contributor-password-confirmation", with: "password")

    click_on "Sign up"

    expect(page).to have_flash_message("There was a problem creating your account")
    expect(page).to have_content("You must accept these terms to continue")
  end

  scenario "A contributor can't sign using an existing address" do
    visit "/contributors/sign-up"

    expect(page).to have_content("Sign up")

    fill_in("contributor-first-name", with: "Claire")
    fill_in("contributor-last-name", with: "Spotswood")
    fill_in("contributor-email", with: "existing-contributor@example.com")
    fill_in("contributor-password", with: "password")
    fill_in("contributor-password-confirmation", with: "password")
    check("contributor[accept_terms]")

    click_on "Sign up"

    expect(page).to have_flash_message("There was a problem creating your account")
    expect(page).to have_content("email address is already taken")
  end

  scenario "After sign up a contributor is redirected to their requested resource" do
    visit "/account"

    expect(page).to have_flash_message("You need to sign in to access this page")
    expect(page.current_path).to eql("/contributors/sign-in")
    click_on "No account? Create one"

    fill_in("contributor-first-name", with: "Claire")
    fill_in("contributor-last-name", with: "Spotswood")
    fill_in("contributor-email", with: "contributor@example.com")
    fill_in("contributor-password", with: "password")
    fill_in("contributor-password-confirmation", with: "password")
    check("contributor[accept_terms]")

    click_on "Sign up"

    expect(page).to have_flash_message("Welcome. You are now a Corley Collection contributor")
    expect(page.current_path).to eql("/account")
  end

  scenario "After sign in a contributor is redirected to their requested resource" do
    visit "/account"

    expect(page).to have_flash_message("You need to sign in to access this page")
    expect(page.current_path).to eql("/contributors/sign-in")

    fill_in("contributor-email", with: "existing-contributor@example.com")
    fill_in("contributor-password", with: "password")

    click_on "Sign in"

    expect(page).to have_flash_message("Welcome. You are now signed in")
    expect(page.current_path).to eql("/account")
  end
end
