require "support/web"

RSpec.feature "Main / Locality Notification Subscriptions / Subscribe", :js do
  given!(:contributor) do
    factory[:contributor, email: "contributor@example.com"]
  end

  given!(:locality) do
    factory[:locality, name: "Brisbane"]
  end

  scenario "A contributor can subscribe to the locality", :authorized_contributor do
    visit "/localities/#{locality.id}/notification-subscriptions/new"

    expect(page).to have_content("Email notifications")

    find("button", text: "Subscribe").click

    expect(page).to have_content("Please tick the checkbox to subscribe")

    page.check("locality_notifications")

    find("button", text: "Subscribe").click

    expect(page).to have_content("You are now subscribed.")
  end
end
