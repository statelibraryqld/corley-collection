require "support/web"

RSpec.feature "Main / Locality Notification Subscriptions / Unsubscribe" do
  given!(:contributor) do
    factory[:contributor, public_token: "my_public_access_token"]
  end

  given!(:subscription) do
    factory[:locality_notification_subscription, contributor: contributor, token: "my_subscription_token"]
  end

  describe "GET /locality-notification-subscriptions/:token/unsubscribe" do
    context "when the notification subscription exists" do
      it "deletes the subscription" do
        expect { visit("/locality-notification-subscriptions/my_subscription_token/unsubscribe") }.to change {
          relations[:locality_notification_subscriptions].exist?(id: subscription.id)
        }.from(true).to(false)
      end

      it "displays a message" do
        visit "/locality-notification-subscriptions/my_subscription_token/unsubscribe"

        expect(page).to have_content("You have been unsubscribed.")
      end
    end

    context "when the notification subscription doesn't exist" do
      it "display a message" do
        visit "/locality-notification-subscriptions/foobar/unsubscribe"

        expect(page).to have_content("This notification subscription was not found. It is likely you already unsubscribed.")
      end
    end
  end

  describe "GET /locality-notification-subscriptions/contributor/:public_token/unsubscribe" do
    given!(:second_subscription) do
      factory[:locality_notification_subscription, contributor: contributor]
    end

    given!(:third_subscription) do
      factory[:locality_notification_subscription]
    end

    context "when the public token identifies a contributor" do
      it "deletes all locality subscriptions belonging to the contributor" do
        expect { visit("/locality-notification-subscriptions/contributor/my_public_access_token/unsubscribe") }.to change {
          relations[:locality_notification_subscriptions].count
        }.from(3).to(1)
      end

      it "displays a message" do
        visit "/locality-notification-subscriptions/contributor/my_public_access_token/unsubscribe"

        expect(page).to have_content("You have been unsubscribed from all place notifications.")
      end
    end

    context "when the public token doesn't identify a contributor" do
      it "displays the same message as if it did" do
        visit "/locality-notification-subscriptions/contributor/a_random_token/unsubscribe"

        expect(page).to have_content("You have been unsubscribed from all place notifications.")
      end
    end
  end
end
