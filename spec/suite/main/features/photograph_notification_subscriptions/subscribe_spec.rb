require "support/web"

RSpec.feature "Main / Photograph Notification Subscriptions / Create", :js do
  given!(:contributor) do
    factory[:contributor, email: "contributor@example.com"]
  end

  given!(:photograph) do
    factory[:photograph, slq_identifier: "1000-1000-0001"]
  end

  scenario "A contributor can subscribe to the photograph", :authorized_contributor do
    visit "/photographs/1000-1000-0001/notification-subscriptions/new"

    expect(page).to have_content("Email notifications")

    find("button", text: "Subscribe").click

    expect(page).to have_content("Please subscribe to either the photograph or its places")

    page.check("photograph_notifications")

    find("button", text: "Subscribe").click

    expect(page).to have_content("You are now subscribed.")
  end
end
