require "support/web"

RSpec.feature "Main / Stories / Create", :js do
  given!(:contributor) do
    factory[:contributor, email: "contributor@example.com"]
  end

  given!(:photograph) do
    factory[:photograph, slq_identifier: "1000-1000-0001"]
  end

  scenario "A contributor can create a story", :authorized_contributor do
    visit "/photographs/1000-1000-0001/stories/new"

    expect(page).to have_content("Add your story")

    find("#story-body").set("I once lived here")

    find("button", text: "Submit").click

    expect(page).to have_content("Thank you")
  end
end
