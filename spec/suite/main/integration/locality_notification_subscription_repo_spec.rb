require "support/db"
require "main/locality_notification_subscription_repo"

RSpec.describe Main::LocalityNotificationSubscriptionRepo do
  let(:repo) { described_class.new }

  let!(:locality) { factory[:locality] }

  let!(:contributor) { factory[:contributor] }

  describe "#find_or_create" do
    context "when the contributor does not have a subscription for the locality" do
      it "creates the subscription" do
        expect { repo.find_or_create(locality.id, contributor.id) }.to change {
          relations.locality_notification_subscriptions.exist?(locality_id: locality.id, contributor_id: contributor.id)
        }.from(false).to(true)
      end
    end

    context "when the contributor already has a subscription for the locality" do
      let!(:subscription) { factory[:locality_notification_subscription, locality: locality, contributor: contributor] }

      it "returns the existing subscription" do
        expect(repo.find_or_create(locality.id, contributor.id).id).to eql(subscription.id)
      end
    end
  end

  describe "#create" do
    it "creates a subscription with a token" do
      subscription = repo.create(locality_id: locality.id, contributor_id: contributor.id)

      expect(subscription.token).not_to be nil
    end
  end
end
