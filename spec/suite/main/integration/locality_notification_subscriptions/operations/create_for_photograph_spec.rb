require "support/db"
require "main/locality_notification_subscriptions/operations/create_for_photograph"

RSpec.describe Main::LocalityNotificationSubscriptions::Operations::CreateForPhotograph do
  subject(:create_for_photograph) { described_class.new }

  let!(:contributor) { factory[:contributor] }

  describe "#call" do
    context "when the photograph has a locality" do
      let!(:photograph) { factory[:photograph, :with_locality, locality: locality] }

      let!(:locality) { factory[:locality] }

      context "when the user has no subscription to the locality" do
        it "creates a subscription to the locality" do
          expect { create_for_photograph.(contributor.id, photograph.id) }.to change {
            relations[:locality_notification_subscriptions].exist?(contributor_id: contributor.id, locality_id: locality.id)
          }.from(false).to(true)
        end
      end

      context "when the user already has a subscription to the locality" do
        let!(:subscription) { factory[:locality_notification_subscription, contributor: contributor, locality: locality] }

        it "returns the existing subscription to the locality" do
          result = create_for_photograph.(contributor.id, photograph.id)
          expect(result).to be_success
          expect(result.value![0].id).to eql(subscription.id)
        end
      end
    end
  end
end
