require "support/db"
require "main/locality_repo"

RSpec.describe Main::LocalityRepo do
  subject(:locality_repo) { described_class.new }

  describe "#find_or_create_by_name" do
    let!(:taylors_lakes) { factory[:locality, name: "Taylors Lakes"] }

    context "when a matching locality exists" do
      it "performs a case insensitive find of localities by name" do
        locality = locality_repo.find_or_create_by_name("TAYLORS LAKES")

        expect(locality.id).to eql(taylors_lakes.id)
      end
    end

    context "when a matching locality does not exist" do
      it "creates the locality" do
        locality = locality_repo.find_or_create_by_name("Green Acres")

        expect(locality.name).to eql("Green Acres")
        expect(locality.id).not_to be nil
      end
    end
  end
end
