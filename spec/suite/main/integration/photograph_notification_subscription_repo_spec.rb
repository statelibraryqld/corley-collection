require "support/db"
require "main/photograph_notification_subscription_repo"

RSpec.describe Main::PhotographNotificationSubscriptionRepo do
  let(:repo) { described_class.new }

  let!(:photograph) { factory[:photograph] }

  let!(:contributor) { factory[:contributor] }

  describe "#find_or_create" do
    context "when the contributor does not have a subscription for the photograph" do
      it "creates the subscription" do
        expect { repo.find_or_create(photograph.id, contributor.id) }.to change {
          relations.photograph_notification_subscriptions.exist?(photograph_id: photograph.id, contributor_id: contributor.id)
        }.from(false).to(true)
      end
    end

    context "when the contributor already has a subscription for the photograph" do
      let!(:subscription) { factory[:photograph_notification_subscription, photograph: photograph, contributor: contributor] }

      it "returns the existing subscription" do
        expect(repo.find_or_create(photograph.id, contributor.id).id).to eql(subscription.id)
      end
    end
  end

  describe "#create" do
    it "creates a subscription with a token" do
      subscription = repo.create(photograph_id: photograph.id, contributor_id: contributor.id)

      expect(subscription.token).not_to be nil
    end
  end
end
