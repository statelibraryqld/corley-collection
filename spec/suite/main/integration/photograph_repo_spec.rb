require "support/db"
require "main/photograph_repo"

RSpec.describe Main::PhotographRepo do
  subject(:photograph_repo) { described_class.new }

  let!(:photograph) { factory[:photograph, human_tagged_at: nil, geo_located_at: nil] }

  let!(:first_tag) { factory[:tag, name: "Brick", type: "wallMaterial"] }
  let!(:second_tag) { factory[:tag, name: "Pitched", type: "roofShape"] }
  let!(:third_tag) { factory[:tag, name: "Ford", type: "car"] }

  let!(:first_tagging) { factory[:photograph_tag, photograph: photograph, tag: first_tag] }
  let!(:second_tagging) { factory[:photograph_tag, photograph: photograph, tag: second_tag] }

  describe "#append_tag_ids" do
    context "when a photograph already has taggings" do
      it "appends the additional tags" do
        expect { photograph_repo.append_tag_ids(photograph.id, [third_tag.id]) }
          .to change { relations[:photographs_tags].where(photograph_id: photograph.id).pluck(:tag_id) }
          .from([first_tag.id, second_tag.id])
          .to([first_tag.id, second_tag.id, third_tag.id])
      end
    end
  end

  describe "#mark_as_human_tagged" do
    it "marks the photograph human tagged by setting the human_tagged_at column" do
      expect { photograph_repo.mark_as_human_tagged(photograph.id) }
        .to change { relations[:photographs].by_pk(photograph.id).one[:human_tagged_at].nil? }
        .from(true)
        .to(false)
    end
  end

  describe "#mark_as_geo_located" do
    it "marks the photograph geo located by setting the geo_located_at column" do
      expect { photograph_repo.mark_as_geo_located(photograph.id) }
        .to change { relations[:photographs].by_pk(photograph.id).one[:geo_located_at].nil? }
        .from(true)
        .to(false)
    end
  end

  describe "#mark_as_storied" do
    it "marks the photograph storied by setting the storied_at column" do
      expect { photograph_repo.mark_as_storied(photograph.id) }
        .to change { relations[:photographs].by_pk(photograph.id).one[:storied_at].nil? }
        .from(true)
        .to(false)
    end
  end
end
