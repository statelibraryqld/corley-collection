require "support/web"

RSpec.describe "/api/v1/current-contributor", :db do
  let!(:contributor) { factory[:contributor, first_name: "Andrew", last_name: "James", email: "andrew@example.com"] }

  def json
    JSON.parse(last_response.body)
  end

  context "when a contributor is signed in" do
    before do
      env("rack.session", contributor_id: contributor.id)
    end

    it "returns the details of the signed in contributor" do
      get "/api/v1/current-contributor.json"

      expect(last_response.status).to be 200

      expect(json).to eql(
        "contributor" => {
          "name" => "Andrew James",
          "email" => "andrew@example.com"
        }
      )
    end
  end

  context "when a contributor is not signed in" do
    it "returns a null contributor" do
      get "/api/v1/current-contributor.json"

      expect(last_response.status).to be 200

      expect(json).to eql(
        "contributor" => nil
      )
    end
  end
end
