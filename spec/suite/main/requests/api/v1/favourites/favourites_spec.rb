require "support/web"

RSpec.describe "/api/v1/favourites", :db do
  let!(:contributor) { factory[:contributor, first_name: "Andrew", email: "andrew@example.com"] }

  let!(:first_photograph) { factory[:photograph, slq_identifier: "1000-1000-0001"] }
  let!(:second_photograph) { factory[:photograph, slq_identifier: "1000-1000-0002"] }
  let!(:third_photograph) { factory[:photograph, slq_identifier: "1000-1000-0003"] }
  let!(:fourth_photograph) { factory[:photograph, slq_identifier: "1000-1000-0004"] }

  let!(:first_favourite) { factory[:favourite, :with_contributor, photograph: first_photograph, contributor: contributor, set_names: %w(foo bar baz)] }
  let!(:second_favourite) { factory[:favourite, :with_contributor, photograph: second_photograph, contributor: contributor] }
  let!(:anonymous_favourite) { factory[:favourite, photograph: third_photograph] }

  let(:favourites) { relations[:favourites].with(auto_struct: true) }

  def json
    JSON.parse(last_response.body)
  end

  describe "GET /api/v1/favourites" do
    context "when no contributor is signed in" do
      it "returns an empty array" do
        get "/api/v1/favourites.json"

        expect(last_response.status).to eql 200

        expect(json).to eql(
          "photographs" => []
        )
      end
    end

    context "when a contributor is signed in" do
      before do
        env("rack.session", contributor_id: contributor.id)
      end

      it "returns their favourite photographs" do
        get "/api/v1/favourites.json"

        expect(last_response.status).to eql 200

        slq_identifiers = json["photographs"].map { |p| p["slqIdentifier"] }

        expect(slq_identifiers).to include("1000-1000-0001")
        expect(slq_identifiers).to include("1000-1000-0002")
        expect(slq_identifiers).not_to include("1000-1000-0003")

        first_favourite = json["photographs"].detect { |p| p["slqIdentifier"] == "1000-1000-0001" }
        second_favourite = json["photographs"].detect { |p| p["slqIdentifier"] == "1000-1000-0002" }

        expect(first_favourite["setNames"]).to eql(["foo", "bar", "baz"])
        expect(second_favourite["setNames"]).to eql([])
      end
    end
  end

  describe "POST /api/v1/favourites" do
    context "when no contributor is signed in" do
      it "creates an anonymous favourite for each request" do
        post "/api/v1/favourites.json?slq_photograph_identifier=1000-1000-0004"
        post "/api/v1/favourites.json?slq_photograph_identifier=1000-1000-0004"

        expect(last_response.status).to eql 200

        expect(favourites.where(photograph_id: fourth_photograph.id, contributor_id: nil).count).to eql(2)
      end
    end

    context "when a contributor is signed in" do
      before do
        env("rack.session", contributor_id: contributor.id)
      end

      context "with set names" do
        it "creates a favourite for that contributor" do
          post "/api/v1/favourites.json?slq_photograph_identifier=1000-1000-0004&set_names[]=foo&set_names[]=bar&set_names[]=baz"

          expect(last_response.status).to eql 200

          expect(favourites.where(photograph_id: fourth_photograph.id, contributor_id: contributor.id).count).to eql(1)
          expect(favourites.where(photograph_id: fourth_photograph.id, contributor_id: contributor.id).one.set_names).to eql(["foo", "bar", "baz"])
        end

        it "creates only one favourite for a given photograph per contributor" do
          post "/api/v1/favourites.json?slq_photograph_identifier=1000-1000-0004&set_names[]=foo&set_names[]=bar&set_names[]=baz"
          post "/api/v1/favourites.json?slq_photograph_identifier=1000-1000-0004&set_names[]=foo&set_names[]=bar&set_names[]=baz"

          expect(last_response.status).to eql 200

          expect(favourites.where(photograph_id: fourth_photograph.id, contributor_id: contributor.id).count).to eql(1)
        end
      end

      context "without set names" do
        it "creates a favourite for that contributor" do
          post "/api/v1/favourites.json?slq_photograph_identifier=1000-1000-0004"

          expect(last_response.status).to eql 200

          expect(favourites.where(photograph_id: fourth_photograph.id, contributor_id: contributor.id).count).to eql(1)
        end

        it "creates only one favourite for a given photograph per contributor" do
          post "/api/v1/favourites.json?slq_photograph_identifier=1000-1000-0004"
          post "/api/v1/favourites.json?slq_photograph_identifier=1000-1000-0004"

          expect(last_response.status).to eql 200

          expect(favourites.where(photograph_id: fourth_photograph.id, contributor_id: contributor.id).count).to eql(1)
        end
      end
    end

    context "with invalid data" do
      context "with an invalid photo identifier" do
        it "returns 422 with an error" do
          post "/api/v1/favourites.json?slq_photograph_identifier=1000322"

          expect(last_response.status).to eql 422

          expect(json["errors"]).to eql(
            "slq_photograph_identifier" => ["photograph must exist"]
          )
        end
      end

      context "with no photo identifer" do
        it "returns 422 with an error" do
          post "/api/v1/favourites.json"

          expect(last_response.status).to eql 422

          expect(json["errors"]).to eql(
            "slq_photograph_identifier" => ["is missing"]
          )
        end
      end
    end
  end

  describe "DELETE /api/v1/favourites" do
    context "when no contributor is signed in" do
      it "returns unprocessable response" do
        delete "/api/v1/favourites.json?slq_photograph_identifier=1000-1000-0004"

        expect(last_response.status).to eql 422
      end
    end

    context "when a contributor is signed in" do
      before do
        env("rack.session", contributor_id: contributor.id)
      end

      it "deletes the favourite for that contributor" do
        delete "/api/v1/favourites.json?slq_photograph_identifier=1000-1000-0001"

        expect(last_response.status).to eql 200

        expect(favourites.where(photograph_id: first_photograph.id, contributor_id: contributor.id).count).to eql(0)
      end
    end
  end
end
