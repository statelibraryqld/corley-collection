require "support/web"

RSpec.describe "/api/v1/localities", :db do
  def json
    JSON.parse(last_response.body)
  end

  let!(:first_locality) do
    factory[:locality, name: "Walkervale", latitude: -27.470125, longitude: 153.021072]
  end

  let!(:second_locality) do
    factory[:locality, name: "Norvile", latitude: -28.470125, longitude: 154.021072]
  end

  let!(:first_spool) do
    factory[
      :spool,
      slq_identifier: "1001-1001",
      content_description: "A spool containing photographs of downtown",
      photograph_count: 1
    ]
  end

  let!(:second_spool) do
    factory[
      :spool,
      slq_identifier: "1001-1002",
      content_description: "A spool containing photographs of a horse",
      photograph_count: 1
    ]
  end

  let!(:third_spool) do
    factory[
      :spool,
      slq_identifier: "1001-1003",
      content_description: "A spool containing photographs of a horse",
      photograph_count: 1
    ]
  end

  let!(:first_locality_spool) do
    factory[:locality_spool, locality: first_locality, spool: first_spool]
  end

  let!(:second_locality_spool) do
    factory[:locality_spool, locality: first_locality, spool: second_spool]
  end

  let!(:third_locality_spool) do
    factory[:locality_spool, locality: second_locality, spool: third_spool]
  end

  describe "GET /api/v1/localities" do
    it "returns all localities ordered by name" do
      get "/api/v1/localities.json"

      expect(last_response.status).to be 200

      expect(json["localities"]).to eql(
        [
          {
            "id" => second_locality.id,
            "name" => "Norvile",
            "geoLocation" => {"latitude" => -28.470125, "longitude" => 154.021072},
            "spoolCount" => 1,
            "spools" => [
              {
                "slqIdentifier" => "1001-1003",
                "photographCount" => 1,
                "geoCount" => 0
              }
            ]
          },
          {
            "id" => first_locality.id,
            "name" => "Walkervale",
            "geoLocation" => {"latitude" => -27.470125, "longitude" => 153.021072},
            "spoolCount" => 2,
            "spools" => [
              {
                "slqIdentifier" => "1001-1001",
                "photographCount" => 1,
                "geoCount" => 0
              },
              {
                "slqIdentifier" => "1001-1002",
                "photographCount" => 1,
                "geoCount" => 0
              }
            ]
          }
        ]
      )
    end
  end
end
