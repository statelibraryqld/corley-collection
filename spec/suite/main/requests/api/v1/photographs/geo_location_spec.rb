require "support/web"

RSpec.describe "/api/v1/photographs/:slq_identifier/geo-location", :db do
  def json
    JSON.parse(last_response.body)
  end

  let!(:photograph) do
    factory[:photograph, :unlocated, slq_identifier: "1001-1003-1001"]
  end

  let!(:located_photograph) do
    factory[:photograph, slq_identifier: "1001-1003-1002", latitude: -27.5093469, longitude: 153.0321211]
  end

  let!(:annerly) do
    factory[:locality, name: "Annerly"]
  end

  describe "PUT /api/v1/photographs/:slq_identifier/geo-location" do
    context "with valid data" do
      it "updates the geo location of the photograph" do
        payload = {
          street_number: "18",
          street_name: "Barkly",
          street_type: "Road",
          postcode: "4130",
          latitude: -27.5093469,
          longitude: 153.0321211,
          locality_name: "ANNERLY",
          state: "QLD",
          street_address: "18 Barkly Road Annerly QLD 4130"
        }

        put "/api/v1/photographs/1001-1003-1001/geo-location.json", payload

        expect(last_response.status).to be 200

        photo = relations[:photographs].with(auto_struct: true).by_pk(photograph.id).one

        expect(photo.street_number).to eql("18")
        expect(photo.street_name).to eql("Barkly")
        expect(photo.postcode).to eql("4130")
        expect(photo.street_type).to eql("Road")
        expect(photo.street_address).to eql("18 Barkly Road Annerly QLD 4130")
        expect(photo.latitude).to eql(-27.5093469)
        expect(photo.longitude).to eql(153.0321211)
        expect(photo.locality_id).to eql(annerly.id)
        expect(photo.geo_located_at).not_to be nil
      end

      it "creates a locality where none exists" do
        payload = {
          street_number: "18",
          street_name: "Barkly",
          street_type: "Road",
          postcode: "4130",
          latitude: -27.5093469,
          longitude: 153.0321211,
          locality_name: "WHITEHORSE LAKES",
          state: "QLD",
          street_address: "18 Barkly Road Whitehorse Lakes QLD 4130"
        }

        put "/api/v1/photographs/1001-1003-1001/geo-location.json", payload

        expect(last_response.status).to be 200

        locality = relations[:localities].with(auto_struct: true).where(name: "Whitehorse Lakes").one
        photo = relations[:photographs].with(auto_struct: true).by_pk(photograph.id).one

        expect(locality).not_to be nil
        expect(photo.locality_id).to eql locality.id
      end
    end

    context "with invalid data" do
      it "returns a validation error" do
        payload = {
          street_number: "18",
          street_name: "Barkly",
          street_type: "Road",
          postcode: "4130",
          latitude: nil,
          longitude: nil,
          locality_name: "ANNERLY",
          state: "VIC",
          street_address: "18 Barkly Road Annerly QLD 4130"
        }

        put "/api/v1/photographs/1001-1003-1001/geo-location.json", payload

        expect(last_response.status).to be 422

        expect(json["errors"]).to eq(
          "latitude" => ["must be a float"],
          "longitude" => ["must be a float"],
          "state" => ["must be equal to QLD"]
        )
      end
    end

    context "when the photograph already has a geo location" do
      it "returns an error" do
        payload = {
          street_number: "18",
          street_name: "Barkly",
          street_type: "Road",
          postcode: "4130",
          latitude: -27.5093469,
          longitude: 153.0321211,
          locality_name: "ANNERLY",
          state: "QLD",
          street_address: "18 Barkly Road Annerly QLD 4130"
        }

        put "/api/v1/photographs/1001-1003-1002/geo-location.json", payload

        expect(last_response.status).to be 422

        expect(json["errors"]).to eq(
          "photograph_slq_identifier" => ["Photograph already has a geo location"]
        )
      end
    end
  end
end
