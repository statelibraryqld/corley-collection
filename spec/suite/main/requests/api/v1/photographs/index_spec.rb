require "support/web"
require "support/search"

RSpec.describe "/api/v1/photographs", :db, :search do
  def json
    JSON.parse(last_response.body)
  end

  let!(:spool) do
    factory[
      :spool,
      slq_identifier: "1001-1003",
      content_description: "A spool",
      photograph_count: 1
    ]
  end

  let!(:wavell_heights) do
    factory[:locality, name: "Wavell Heights"]
  end

  let!(:chatswood) do
    factory[:locality, name: "Chatswood"]
  end

  let(:update_time) do
    Time.now
  end

  index!(:wavell_heights_photograph) do
    factory[
      :photograph,
      :with_spool,
      :with_locality,
      damaged: true,
      house_name: "Alice's Cottage",
      human_tagged_at: update_time,
      geo_located_at: update_time,
      latitude: -27.470125,
      locality: wavell_heights,
      longitude: 153.021072,
      postcode: "3444",
      shop: false,
      slq_identifier: "1001-1003-1001",
      spool: spool,
      street_address: "24 Lauriston Street Wavell Heights 3444",
      street_name: "Lauriston",
      street_number: "24",
      street_type: "Street",
    ]
  end

  index!(:chatswood_photograph) do
    factory[
      :photograph,
      :with_spool,
      :with_locality,
      annotations: nil,
      damaged: false,
      house_name: nil,
      latitude: nil,
      locality: chatswood,
      longitude: nil,
      postcode: "3676",
      shop: true,
      slq_identifier: "1001-1003-1002",
      spool: spool,
      street_address: "68 High Street Chatswood QLD 3676",
      street_name: "High",
      street_number: "68",
      street_type: "Street",
    ]
  end

  index!(:photograph_from_another_spool) do
    factory[
      :photograph,
      :with_spool,
      annotations: "Something on the back",
      damaged: true,
      house_name: "Cloud street",
      latitude: nil,
      longitude: nil,
      postcode: "3231",
      shop: true,
      slq_identifier: "1001-1003-1003",
      street_address: "50 Zed Street",
      street_name: "Zed",
      street_number: "50",
      street_type: "Street",
    ]
  end

  describe "GET /api/v1/photographs" do
    context "when provided no parameters" do
      it "returns aggregrations over the entire photograph collection" do
        get "/api/v1/photographs.json"

        expect(last_response.status).to be 200

        expect(json["pagination"]).to eql(
          "page" => 1,
          "nextPage" => nil,
          "prevPage" => nil,
          "perPage" => 20,
          "totalPages" => 1,
          "totalResults" => 3
        )

        expect(json["aggregations"]["spools"]).to eql(
          [
            {"spoolSlqIdentifier" => "1001-1003", "count" => 2},
            {"spoolSlqIdentifier" => photograph_from_another_spool.spool.slq_identifier, "count" => 1}
          ]
        )

        expect(json["aggregations"]["localities"]).to eql(
          [
            {
              "locality" => {
                "id" => wavell_heights.id,
                "name" => "Wavell Heights"
              },
              "count" => 1
            },
            {
              "locality" => {
                "id" => chatswood.id,
                "name" => "Chatswood"
              },
              "count" => 1
            }
          ]
        )
      end
    end

    context "when provided a query" do
      it "returns photographs and aggregations matching that query" do
        get "/api/v1/photographs.json?query=wavell"

        expect(last_response.status).to be 200

        expect(json["photographs"]).to include(
          hash_including(
            "slqIdentifier" => "1001-1003-1001",
            "spoolSlqIdentifier" => "1001-1003",
            "address" => {
              "streetNumber" => "24",
              "streetName" => "Lauriston",
              "streetType" => "Street",
              "postcode" => "3444",
              "streetAddress" => "24 Lauriston Street Wavell Heights 3444",
              "localityName" => "Wavell Heights"
            },
            "geoLocation" => {"latitude" => -27.470125, "longitude" => 153.021072},
            "shop" => false,
            "damaged" => true,
            "houseName" => "Alice's Cottage",
            "stories" => [],
          )
        )

        expect(json["pagination"]).to match hash_including(
          "page" => 1,
          "nextPage" => nil,
          "prevPage" => nil,
          "perPage" => 20
        )

        expect(json["aggregations"]["spools"]).to include(
          "spoolSlqIdentifier" => "1001-1003",
          "count" => 1
        )

        expect(json["aggregations"]["localities"]).to eql(
          [
            "locality" => {
              "id" => wavell_heights.id,
              "name" => "Wavell Heights"
            },
            "count" => 1
          ]
        )
      end
    end

    context "when asked for photographs for one locality" do
      it "returns photographs for that locality" do
        get "/api/v1/photographs.json?locality_ids[]=#{chatswood.id}"

        expect(last_response.status).to be 200

        expect(json["photographs"].map { |p| p["slqIdentifier"] }).to eql(["1001-1003-1002"])
      end
    end

    context "when asked for photographs for two localities" do
      it "returns photographs for both localities" do
        get "/api/v1/photographs.json?locality_ids[]=#{chatswood.id}&locality_ids[]=#{wavell_heights.id}"

        expect(last_response.status).to be 200

        slq_identifiers = json["photographs"].map { |p| p["slqIdentifier"] }

        expect(slq_identifiers).to include("1001-1003-1001")
        expect(slq_identifiers).to include("1001-1003-1002")
      end
    end

    context "when asked for photographs for one spool" do
      it "returns photographs for that spool" do
        get "/api/v1/photographs.json?spool_slq_identifiers[]=#{spool.slq_identifier}"

        expect(last_response.status).to be 200

        slq_identifiers = json["photographs"].map { |p| p["slqIdentifier"] }

        expect(slq_identifiers).to include("1001-1003-1001")
        expect(slq_identifiers).to include("1001-1003-1002")
        expect(slq_identifiers).not_to include("1001-1003-1003")
      end
    end

    context "when asked for human tagged photographs" do
      it "returns photographs that are human tagged" do
        get "/api/v1/photographs.json?human_tagged=true"

        expect(last_response.status).to be 200

        expect(json["photographs"].map { |p| p["slqIdentifier"] }).to eql(["1001-1003-1001"])
      end
    end

    context "when asked for non-human tagged photographs" do
      it "returns photographs that are human tagged" do
        get "/api/v1/photographs.json?human_tagged=false"

        expect(last_response.status).to be 200

        slq_identifiers = json["photographs"].map { |p| p["slqIdentifier"] }

        expect(slq_identifiers).to include("1001-1003-1002")
        expect(slq_identifiers).to include("1001-1003-1003")
        expect(slq_identifiers).not_to include("1001-1003-1001")
      end
    end

    context "when asked for photographs with geo location" do
      it "returns photographs with geo locations" do
        get "/api/v1/photographs.json?has_geo_location=true"

        expect(last_response.status).to be 200

        expect(json["photographs"].map { |p| p["slqIdentifier"] }).to eql(["1001-1003-1001"])
      end
    end

    context "when asked for photographs without geo location" do
      it "returns photographs without geo locations" do
        get "/api/v1/photographs.json?has_geo_location=false"

        expect(last_response.status).to be 200

        slq_identifiers = json["photographs"].map { |p| p["slqIdentifier"] }

        expect(slq_identifiers).to include("1001-1003-1002")
        expect(slq_identifiers).to include("1001-1003-1003")
        expect(slq_identifiers).not_to include("1001-1003-1001")
      end
    end

    context "when asked for photographs of shops" do
      it "returns photographs that are shops" do
        get "/api/v1/photographs.json?is_shop=true"

        expect(last_response.status).to be 200

        slq_identifiers = json["photographs"].map { |p| p["slqIdentifier"] }

        expect(slq_identifiers).to include("1001-1003-1002")
        expect(slq_identifiers).to include("1001-1003-1003")
        expect(slq_identifiers).not_to include("1001-1003-1001")
      end
    end

    context "when asked for damaged photographs" do
      it "returns photographs that are damaged" do
        get "/api/v1/photographs.json?is_damaged=true"

        expect(last_response.status).to be 200

        slq_identifiers = json["photographs"].map { |p| p["slqIdentifier"] }

        expect(slq_identifiers).to include("1001-1003-1001")
        expect(slq_identifiers).to include("1001-1003-1003")
        expect(slq_identifiers).not_to include("1001-1003-1002")
      end
    end

    context "when asked for photographs with house names" do
      it "returns photographs that have house names" do
        get "/api/v1/photographs.json?has_house_name=true"

        expect(last_response.status).to be 200

        slq_identifiers = json["photographs"].map { |p| p["slqIdentifier"] }

        expect(slq_identifiers).to include("1001-1003-1001")
        expect(slq_identifiers).to include("1001-1003-1003")
        expect(slq_identifiers).not_to include("1001-1003-1002")
      end
    end

    context "when asked for photographs with annotations" do
      it "returns photographs that have annotations" do
        get "/api/v1/photographs.json?has_annotations=true"

        expect(last_response.status).to be 200

        slq_identifiers = json["photographs"].map { |p| p["slqIdentifier"] }

        expect(slq_identifiers).to include("1001-1003-1003")
        expect(slq_identifiers).not_to include("1001-1003-1002")
        expect(slq_identifiers).not_to include("1001-1003-1001")
      end
    end

    context "when given invalid parameters" do
      it "returns an error" do
        get "/api/v1/photographs.json?has_geo_location=foo&per_page=200&page=ten"

        expect(last_response.status).to be 422

        expect(json["errors"]["has_geo_location"]).to eql(["must be boolean"])
        expect(json["errors"]["page"]).to eql(["must be a whole number"])
        expect(json["errors"]["per_page"]).to eql(["must be less than or equal to 100"])
      end
    end
  end
end
