require "support/web"

RSpec.describe "/api/v1/photographs/next-untagged", :db do
  def json
    JSON.parse(last_response.body)
  end

  let!(:tagged_photograph) do
    factory[:photograph, slq_identifier: "1001-1003-1001", human_tagged_at: Time.now]
  end

  describe "GET /api/v1/photographs/next-untagged" do
    context "when there are no more photgraphs to tag" do
      it "returns null" do
        get "/api/v1/photographs/next-untagged.json"

        expect(last_response.status).to be 200

        expect(json).to eql(
          "photograph" => nil,
          "untaggedCount" => 0
        )
      end
    end

    context "when there are photgraphs needing to be tagged" do
      let!(:untagged_photograph) do
        factory[:photograph,
          slq_identifier: "1001-1003-1002",
          street_number: nil,
          street_name: nil,
          latitude: nil,
          longitude: nil
        ]
      end

      it "returns a random untagged photograph" do
        get "/api/v1/photographs/next-untagged.json"

        expect(last_response.status).to be 200

        expect(json).to eql(
          "photograph" => {
            "slqIdentifier" => "1001-1003-1002",
            "spoolSlqIdentifier" => nil,
            "address" => nil,
            "geoLocation" => nil,
            "tags" => [],
            "tagCollection" => {},
          },
          "untaggedCount" => 1
        )
      end
    end
  end
end
