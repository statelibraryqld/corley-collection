require "support/web"

RSpec.describe "/api/v1/photographs/:slq_identifier", :db do
  def json
    JSON.parse(last_response.body)
  end

  let!(:photograph_with_address) do
    factory[
      :photograph,
      :with_spool,
      :with_locality,
      latitude: -27.470125,
      locality: locality,
      longitude: 153.021072,
      postcode: "3444",
      slq_identifier: "1001-1003-1001",
      spool: spool,
      street_address: "24 Lauriston Street Wavell Heights 3444",
      street_name: "Lauriston",
      street_number: "24",
      street_type: "Street",
      house_name: "Alice's Cottage",
      damaged: true,
      shop: false,
      annotations: "Illegible writing on the back.",
      similar_photographs: ["1001-1053", "1999-1614", "1878-1817"]
    ]
  end

  let!(:photograph_without_address) do
    factory[
      :photograph,
      :with_spool,
      latitude: nil,
      longitude: nil,
      slq_identifier: "1001-1003-1002",
      spool: spool,
      street_name: nil,
      street_number: nil,
      house_name: nil
    ]
  end

  let!(:spool) do
    factory[
      :spool,
      slq_identifier: "1001-1003",
      content_description: "A spool containing photographs of Wavell Heights",
      photograph_count: 2
    ]
  end

  let!(:locality) do
    factory[:locality, name: "Wavell Heights"]
  end

  let!(:roof_material_tag) do
    factory[:tag, type: "roofMaterial", source: "human", name: "Tile"]
  end

  let!(:first_wall_material_tag) do
    factory[:tag, type: "wallMaterial", source: "human", name: "Weatherboards"]
  end

  let!(:second_wall_material_tag) do
    factory[:tag, type: "wallMaterial", source: "human", name: "Rendered Brick"]
  end

  let!(:first_photograph_tag) do
    factory[:photograph_tag, photograph: photograph_with_address, tag: roof_material_tag]
  end

  let!(:second_photograph_tag) do
    factory[:photograph_tag, photograph: photograph_with_address, tag: first_wall_material_tag]
  end

  let!(:third_photograph_tag) do
    factory[:photograph_tag, photograph: photograph_with_address, tag: second_wall_material_tag]
  end

  let!(:machine_tag) do
    factory[:tag, type: "roofMaterial", source: "machine", name: "Tin"]
  end

  let!(:fourth_photograph_tag) do
    factory[:photograph_tag, photograph: photograph_with_address, tag: machine_tag]
  end

  let!(:contributor_tag) do
    factory[:tag, type: "Contributor", source: "human", name: "Horse"]
  end

  let!(:fifth_photograph_tag) do
    factory[:photograph_tag, photograph: photograph_with_address, tag: contributor_tag]
  end

  let!(:contributor) do
    factory[:contributor, first_name: "George", last_name: "Smiley"]
  end

  let!(:story) do
    factory[:story, photograph: photograph_with_address, contributor: contributor, body: "This was my safehouse.\r\n\r\nIt was on Albert Street<script>alert('foo');</script>"]
  end

  describe "GET /api/v1/photographs/:slq_identifier" do
    context "when the photograph has address and geolocation details" do
      it "returns the photograph" do
        get "/api/v1/photographs/1001-1003-1001.json"

        expect(last_response.status).to be 200

        expect(json).to eql(
          "slqIdentifier" => "1001-1003-1001",
          "spoolSlqIdentifier" => "1001-1003",
          "address" => {
            "streetNumber" => "24",
            "streetName" => "Lauriston",
            "streetType" => "Street",
            "postcode" => "3444",
            "streetAddress" => "24 Lauriston Street Wavell Heights 3444",
            "localityName" => "Wavell Heights"
          },
          "geoLocation" => {"latitude" => -27.470125, "longitude" => 153.021072},
          "geoLocatedAt" => nil,
          "shop" => false,
          "damaged" => true,
          "houseName" => "Alice's Cottage",
          "annotations" => "Illegible writing on the back.",
          "tags" => [
            {"id" => roof_material_tag.id, "name" => "Tile", "type" => "roofMaterial"},
            {"id" => first_wall_material_tag.id, "name" => "Weatherboards", "type" => "wallMaterial"},
            {"id" => second_wall_material_tag.id, "name" => "Rendered Brick", "type" => "wallMaterial"},
            {"id" => contributor_tag.id, "name" => "Horse", "type" => "Contributor"}
          ],
          "tagCollection" => {
            "roofMaterial" => [
              {"id" => roof_material_tag.id, "name" => "Tile"}
            ],
            "wallMaterial" => [
              {"id" => first_wall_material_tag.id, "name" => "Weatherboards"},
              {"id" => second_wall_material_tag.id, "name" => "Rendered Brick"}
            ],
            "Contributor" => [
              {"id" => contributor_tag.id, "name" => "Horse"}
            ],
          },
          "humanTaggedAt" => nil,
          "stories" => [
            {
              "contributorName" => "George S",
              "bodyHtml" => "<p>This was my safehouse.</p><p>It was on Albert Streetalert('foo');</p>",
              "displayDate" => story.created_at.strftime("%-e %B %Y"),
              "createdAt" => story.created_at.iso8601,
              "contributorImages" => []
            }
          ],
          "storiedAt" => nil,
          "similarPhotographs" => ["1001-1053", "1999-1614", "1878-1817"]
        )
      end
    end

    context "when the photograph has no address or geolocation details" do
      it "returns the photograph" do
        get "/api/v1/photographs/1001-1003-1002.json"

        expect(last_response.status).to be 200

        expect(json).to eql(
          "slqIdentifier" => "1001-1003-1002",
          "spoolSlqIdentifier" => "1001-1003",
          "address" => nil,
          "geoLocation" => nil,
          "geoLocatedAt" => nil,
          "shop" => false,
          "damaged" => false,
          "houseName" => nil,
          "annotations" => nil,
          "tags" => [],
          "tagCollection" => {},
          "humanTaggedAt" => nil,
          "stories" => [],
          "storiedAt" => nil,
          "similarPhotographs" => []
        )
      end
    end

    context "when the photograph doesn't exist" do
      it "returns a 404 response" do
        get "/api/v1/photographs/1001-1006-1002.json"

        expect(last_response.status).to be 404
      end
    end
  end
end
