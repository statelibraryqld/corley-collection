require "support/web"

RSpec.describe "/api/v1/photographs/:slq_identifier/taggings", :db do
  def json
    JSON.parse(last_response.body)
  end

  let!(:photograph) do
    factory[
      :photograph,
      slq_identifier: "1001-1003-1001",
      street_number: "24",
      street_name: "Lauriston Street"
    ]
  end

  let!(:roof_material_tag) do
    factory[:tag, type: "roofMaterial", source: "human", name: "Tile"]
  end

  let!(:fence_tag) do
    factory[:tag, type: "fence", source: "human", name: "Brick"]
  end

  let!(:details_tag) do
    factory[:tag, type: "roofShape", source: "human", name: "Pitched"]
  end

  let!(:contributor_tag) do
    factory[:tag, type: "Contributor", source: "human", name: "Cat"]
  end

  let!(:first_photograph_tag) do
    factory[:photograph_tag, photograph: photograph, tag: roof_material_tag]
  end

  describe "POST /api/v1/photographs/:slq_identifier/taggings" do
    context "with valid taggings for existing tags" do
      it "appends the taggings" do
        expect { post "/api/v1/photographs/1001-1003-1001/taggings.json", tag_ids: [fence_tag.id, details_tag.id] }
          .to change { relations[:photographs_tags].where(photograph_id: photograph.id).count }
          .from(1)
          .to(3)
      end
    end

    context "with valid taggings and new tags" do
      it "creates the taggings and creates new 'contributor' tags" do
        payload = {
          tag_ids: [fence_tag.id, details_tag.id],
          contributor_tags: " Dog, Lawn mower, Cat ", # tag user input gets cleaned up
        }

        post "/api/v1/photographs/1001-1003-1001/taggings.json", payload

        expect(last_response.status).to be 200

        expect(relations[:photographs_tags].where(photograph_id: photograph.id).count).to eql(6)
        expect(relations[:tags].where(type: "Contributor").count).to eql(3)

        expect(relations[:tags].where(type: "Contributor").to_a).to match [
          hash_including(
            name: "Cat",
            type: "Contributor"
          ),
          hash_including(
            name: "Dog",
            type: "Contributor",
          ),
          hash_including(
            name: "Lawn mower",
            type: "Contributor"
          )
        ]
      end
    end

    context "with invalid input" do
      it "returns an error with invalid taggings" do
        post "/api/v1/photographs/1001-1003-1001/taggings.json", tag_ids: ["a"]

        expect(last_response.status).to be 422

        expect(json["errors"]).to eq(
          "tag_ids" => {"0" => ["must be a whole number"]}
        )
      end

      it "returns an error with no input" do
        post "/api/v1/photographs/1001-1003-1001/taggings.json"

        expect(last_response.status).to be 422

        expect(json["errors"]).to eq(
          "tag_ids_or_contributor_tags_presence" => ["Either tag ids or contributor tags must be present"]
        )
      end

      it "returns an error when a tag doesn't exist" do
        post "/api/v1/photographs/1001-1003-1001/taggings.json", tag_ids: [100_001]

        expect(last_response.status).to be 422

        expect(json["errors"]).to eq(
          "tags_exist" => ["tags must exist"]
        )
      end
    end
  end
end
