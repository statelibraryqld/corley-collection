require "support/web"

RSpec.describe "PUT /api/v1/photographs/:slq_identifier", :db do
  def json
    JSON.parse(last_response.body)
  end

  let!(:photograph_with_address) do
    factory[
      :photograph,
      slq_identifier: "1001-1003-1001",
      house_name: nil,
      damaged: false,
      shop: false,
      annotations: nil
    ]
  end

  describe "PUT /api/v1/photographs/:slq_identifier" do
    context "with valid data" do
      it "updates the photograph's house_name, annotations and shop and damaged status" do
        payload = {
          damaged: true,
          shop: true,
          annotations: "Something on the back of the photo.",
          house_name: "Kirribilli",
        }

        put "/api/v1/photographs/1001-1003-1001.json", payload

        expect(last_response.status).to be 200

        photograph = relations[:photographs].with(auto_struct: true).by_pk(photograph_with_address.id).one!

        expect(photograph.damaged).to be true
        expect(photograph.shop).to be true
        expect(photograph.annotations).to eql("Something on the back of the photo.")
        expect(photograph.house_name).to eql("Kirribilli")
      end
    end

    context "with invalid data" do
      it "returns validation errors" do
        payload = {
          damaged: "foobar"
        }

        put "/api/v1/photographs/1001-1003-1001.json", payload

        expect(last_response.status).to be 422

        expect(json["errors"]).to eql(
          "damaged" => ["must be boolean"]
        )
      end
    end
  end
end
