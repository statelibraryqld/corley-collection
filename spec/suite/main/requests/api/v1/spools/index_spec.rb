require "support/web"

RSpec.describe "/api/v1/spools", :db do
  def json
    JSON.parse(last_response.body)
  end

  let!(:first_spool) do
    factory[
      :spool,
      slq_identifier: "1001-1003",
      content_description: "A spool containing photographs of downtown",
      photograph_count: 2
    ]
  end

  let!(:second_spool) do
    factory[
      :spool,
      slq_identifier: "1001-1006",
      content_description: "A spool containing photographs of a horse",
      photograph_count: 1
    ]
  end

  let!(:first_locality) do
    factory[:locality, name: "Brisbane CBD", latitude: -27.470125, longitude: 153.021072]
  end

  let!(:second_locality) do
    factory[:locality, name: "Suburb X", latitude: nil, longitude: nil]
  end

  let!(:first_locality_spool) do
    factory[:locality_spool, locality: first_locality, spool: first_spool]
  end

  let!(:second_locality_spool) do
    factory[:locality_spool, locality: second_locality, spool: second_spool]
  end

  let!(:first_photograph) do
    factory[:photograph, :with_spool, slq_identifier: "1001-1003-0001", spool: first_spool]
  end

  let!(:second_photograph) do
    factory[:photograph, :with_spool, slq_identifier: "1001-1003-0002", spool: first_spool]
  end

  let!(:third_photograph) do
    factory[:photograph, :with_spool, slq_identifier: "1001-1006-0001", spool: second_spool]
  end

  describe "GET /api/v1/spools" do
    it "returns all spools" do
      get "/api/v1/spools.json"

      expect(last_response.status).to be 200

      expect(json["spools"]).to eql(
        [
          {
            "slqIdentifier" => "1001-1003",
            "photographCount" => 2,
            "photographSlqIdentifiers" => ["1001-1003-0001", "1001-1003-0002"],
            "localities" => [
              {"id" => first_locality.id, "name" => "Brisbane CBD", "geoLocation" => {"latitude" => -27.470125, "longitude" => 153.021072}}
            ]
          },
          {
            "slqIdentifier" => "1001-1006",
            "photographCount" => 1,
            "photographSlqIdentifiers" => ["1001-1006-0001"],
            "localities" => [
              {"id" => second_locality.id, "name" => "Suburb X", "geoLocation" => nil}
            ]
          }
        ]
      )
    end
  end
end
