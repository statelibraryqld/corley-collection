require "support/web"

RSpec.describe "/api/v1/spools/:slq_identifier", :db do
  def json
    JSON.parse(last_response.body)
  end

  let!(:spool) do
    factory[
      :spool,
      slq_identifier: "1001-1003",
      content_description: "A spool containing photographs of downtown",
      photograph_count: 2
    ]
  end

  let!(:first_locality) do
    factory[:locality, name: "Brisbane CBD", latitude: -27.470125, longitude: 153.021072]
  end

  let!(:second_locality) do
    factory[:locality, name: "Suburb X", latitude: nil, longitude: nil]
  end

  let!(:first_locality_spool) do
    factory[:locality_spool, locality: first_locality, spool: spool]
  end

  let!(:second_locality_spool) do
    factory[:locality_spool, locality: second_locality, spool: spool]
  end

  let!(:first_photograph) do
    factory[:photograph, :with_spool, slq_identifier: "1001-1003-0001", spool: spool]
  end

  let!(:second_photograph) do
    factory[:photograph, :with_spool, slq_identifier: "1001-1003-0002", spool: spool]
  end

  let!(:first_address) do
    factory[:address, spool: spool, street_address: "123 High St", latitude: -27.470125, longitude: 153.021072]
  end

  let!(:second_address) do
    factory[:address, spool: spool, street_address: "57 East St", latitude: -27.770125, longitude: 151.021072]
  end

  describe "GET /api/v1/spools/:slq_identifier" do
    context "when the spool exists" do
      it "returns the spool" do
        get "/api/v1/spools/1001-1003.json"

        expect(last_response.status).to be 200

        expect(json).to match(
          hash_including(
            "slqIdentifier" => "1001-1003",
            "contentDescription" => "A spool containing photographs of downtown",
            "photographCount" => 2,
            "photographs" => array_including(
              hash_including(
                "slqIdentifier" => "1001-1003-0001",
                "hasHumanTags" => false,
                "hasStories" => false
              )
            ),
            "photographSlqIdentifiers" => ["1001-1003-0001", "1001-1003-0002"],
            "localities" => [
              {"id" => first_locality.id, "name" => "Brisbane CBD", "geoLocation" => {"latitude" => -27.470125, "longitude" => 153.021072}},
              {"id" => second_locality.id, "name" => "Suburb X", "geoLocation" => nil}
            ],
            "addresses" => [
              {"streetAddress" => "123 High St", "geoLocation" => {"latitude" => -27.470125, "longitude" => 153.021072}},
              {"streetAddress" => "57 East St", "geoLocation" => {"latitude" => -27.770125, "longitude" => 151.021072}}
            ]
          )
        )
      end
    end

    context "when the spool doesn't exist" do
      it "returns a 404 response" do
        get "/api/v1/spools/1001-1006.json"

        expect(last_response.status).to be 404
      end
    end
  end
end
