require "support/web"

RSpec.describe "/api/v1/tags", :db do
  def json
    JSON.parse(last_response.body)
  end

  let!(:roof_material_tag) do
    factory[:tag, type: "roofMaterial", source: "human", name: "Tile"]
  end

  let!(:first_wall_material_tag) do
    factory[:tag, type: "wallMaterial", source: "human", name: "Weatherboards"]
  end

  let!(:second_wall_material_tag) do
    factory[:tag, type: "wallMaterial", source: "human", name: "Rendered Brick"]
  end

  let!(:machine_tag) do
    factory[:tag, type: "roofMaterial", source: "machine", name: "Tin"]
  end

  describe "GET /api/v1/tags" do
    it "returns all tags available for selection" do
      get "/api/v1/tags.json"

      expect(last_response.status).to be 200

      expect(json["tagTypes"]).to eql(["roofMaterial", "wallMaterial"])

      expect(json["tagOptions"]).to eql(
        "roofMaterial" => [
          {"id" => roof_material_tag.id, "name" => "Tile"}
        ],
        "wallMaterial" => [
          {"id" => first_wall_material_tag.id, "name" => "Weatherboards"},
          {"id" => second_wall_material_tag.id, "name" => "Rendered Brick"}
        ]
      )
    end
  end
end
