require "main/photographs/search_query"

RSpec.describe Main::Photographs::SearchQuery do
  let(:search_query) { described_class.new }

  describe "#valid?" do
    it "returns false if per_page is greater than 100" do
      query = described_class.new(per_page: 101)

      expect(query.valid?).to be false
      expect(query.errors[:per_page]).to include("must be less than or equal to 100")
    end
  end
end
