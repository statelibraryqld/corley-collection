CorleyCollection::Container.start :que

RSpec.configure do |config|
  config.before :suite do
    # Run Que jobs inline during tests
    CorleyCollection::Container[:que].run_synchronously = true
  end
end
