CorleyCollection::Container.start(:persistence)

Dir[SPEC_ROOT.join("support/db/*.rb").to_s].each(&method(:require))

require "database_cleaner"
DatabaseCleaner[:sequel, connection: Test::DatabaseHelpers.db].strategy = :truncation

RSpec.configure do |config|
  config.include Test::DatabaseHelpers

  config.define_derived_metadata(file_path: /admin/) do |metadata|
    metadata[:factory] = :admin
  end

  config.define_derived_metadata(file_path: /main/) do |metadata|
    metadata[:factory] = :main
  end

  config.define_derived_metadata(file_path: %r[suite/integration|mail]) do |metadata|
    metadata[:factory] = nil
  end

  config.before :suite do
    DatabaseCleaner.clean_with :truncation
  end

  config.around(:example) do |example|
    if suite.clean_db?(example) || example.metadata[:db]
      DatabaseCleaner.cleaning do
        example.run
      end
    else
      example.run
    end
  end

  config.include(Test::FactoryHelper.new(:admin), factory: :admin)
  config.include(Test::FactoryHelper.new(:main), factory: :main)
  config.include(Test::FactoryHelper.new, factory: nil)
end
