module Test
  module DatabaseHelpers
    module_function

    def relations
      rom.relations
    end

    def rom
      CorleyCollection::Container["persistence.rom"]
    end

    def db
      CorleyCollection::Container["persistence.db"]
    end
  end

  class FactoryHelper < Module
    attr_reader :type

    def initialize(type = nil)
      @type = type

      factory = entity_namespace ? Factory.struct_namespace(entity_namespace) : Factory

      define_method(:factory) do
        factory
      end
    end

    def entity_namespace
      @entity_namespace ||=
        begin
          case type
          when :admin
            require "admin/entities"
            Admin::Entities
          when :main
            require "main/entities"
            Main::Entities
          end
        end
    end
  end
end
