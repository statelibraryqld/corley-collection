require "time_math"
require "timeout"

module Test
  module Helpers
    TimeOutWaitingForElementVisibility = Class.new(StandardError)
    TimeOutWaitingForElementInvisibility = Class.new(StandardError)

    class TimeHelper
      attr_reader :now

      def initialize(now)
        @now = TimeMath(now)
      end

      def yesterday
        now.decrease(:day, 1).()
      end

      def tomorrow
        now.advance(:day, 1).()
      end

      def one_week_ago
        now.decrease(:week, 1).()
      end

      def one_week_from_now
        now.advance(:week, 1).()
      end

      def two_weeks_ago
        now.decrease(:week, 2).()
      end

      def one_month_ago
        now.decrease(:month, 1).()
      end

      def two_months_ago
        now.decrease(:month, 2).()
      end

      def one_hour_ago
        now.decrease(:hour, 1).()
      end

      def one_hour_from_now
        now.advance(:hour, 1).()
      end

      def one_year_from_now
        now.advance(:year, 1).()
      end
    end

    def time(now = Time.now)
      TimeHelper.new(now)
    end

    def have_flash_message(text)
      wait_for_visibility ".flash__message"
      have_selector(".flash__message", text: text)
    end

    def wait_for(*selector_args, timeout: Capybara.default_max_wait_time, **selector_kwargs)
      Capybara.using_wait_time timeout do
        has_selector?(*selector_args, **selector_kwargs)
      end
    end

    def wait_for_visibility(*selector_args, timeout: Capybara.default_max_wait_time, **selector_kwargs)
      Timeout.timeout timeout, TimeOutWaitingForElementVisibility do
        Capybara.using_wait_time 0 do
          sleep 0.05 until has_selector?(*selector_args, visible: true, **selector_kwargs)
        end
      end
    end
  end
end
