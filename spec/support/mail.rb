RSpec.configure do |config|
  config.after(:each) do
    if Mail::Container.key?(:mailer)
      Mail::Container[:mailer].messages.clear
    end
  end
end
