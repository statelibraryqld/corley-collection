Dir[SPEC_ROOT.join("support/redis/*.rb").to_s].each(&method(:require))

CorleyCollection::Container.start(:redis)

RSpec.configure do |config|
  config.include Test::RedisHelpers
end
