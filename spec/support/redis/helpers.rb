module Test
  module RedisHelpers
    module_function

    def redis
      @redis ||= CorleyCollection::Container[:redis]
    end

    def self.included(rspec)
      rspec.around(:each) do |example|
        with_clean_redis do
          example.run
        end
      end
    end

    def with_clean_redis
      redis.flushall
      begin
        yield
      ensure
        redis.flushall
      end
    end
  end
end
