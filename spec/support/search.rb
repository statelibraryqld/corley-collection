require_relative "bg"

Dir[SPEC_ROOT.join("support/search/*.rb").to_s].each(&method(:require))

RSpec.configure do |config|
  config.include Test::SearchHelpers

  config.before :suite do
    Admin::Container[:indexer].extend(Test::SearchHelpers::IndexerTestMode)

    es = Test::SearchHelpers.es

    es.relations.each do |name, relation|
      relation.delete_index if es.gateways[:default].dataset?(name)
      relation.create_index unless relation.class.multi_index_types
    end
  end

  config.before(:all, :search) do
    admin_indexer.start
  end

  config.after(:all, :search) do
    admin_indexer.stop
  end

  config.before(:example, :search) do |example|
    indexed_entities = example.metadata.fetch(:indexed_entities, [])

    rel_map = indexed_entities.each_with_object({}) do |k, h|
      entity = public_send(k)
      rel_name = entity.class.schema[:id].meta[:source]
      (h[rel_name] ||= []) << entity
    end

    rel_map.each do |rel_name, entities|
      index_entities(rel_name, entities)
    end
  end

  config.around(:example, :search) do |example|
    admin_indexer.cleaning { example.run }
  end
end
