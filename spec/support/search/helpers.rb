require "set"

module Test
  module SearchHelpers
    module IndexerTestMode
      def cleaning
        reset!
        yield
      ensure
        indexed_relations.each do |name|
          relation = es.relations[name]
          relation.delete
          relation.refresh
        end
        reset!
      end

      def index(relation_name, entity)
        indexed_relations << relation_name
        super(relation_name, entity)
      end

      def indexed_relations
        @__indexed_relations__
      end

      def reset!
        @__indexed_relations__ = Set.new
      end
    end

    module_function

    def self.included(klass)
      super
      klass.extend(ExampleGroupMethods)
    end

    module ExampleGroupMethods
      def index!(name, &block)
        let!(name, &block)
        indexed_entities << name
      end

      def indexed_entities
        metadata[:indexed_entities] ||= []
      end
    end

    def index_entities(relation_name, entities)
      Array(entities).each { |entity| admin_indexer.index(relation_name, entity) }
      es.relations[relation_name].refresh
    end

    def admin_indexer
      Admin::Container[:indexer]
    end

    def es
      Search::Container["es.rom"]
    end
  end
end
