module Test
  module ViewHelpers
    class PagedCollection < SimpleDelegator
      Pager = Struct.new(:current_page, :total_pages, :per_page) do
        def prev_page
          nil
        end

        def next_page
          2
        end
      end

      attr_reader :opts

      def initialize(coll, opts)
        super(coll)
        @opts = opts
      end

      def pager
        @pager ||= Pager.new(opts[:current_page] || 1, opts[:total_pages] || 2, opts[:per_page] || 10)
      end
    end

    def paged_collection(coll, opts = {})
      PagedCollection.new(coll, opts)
    end

    def render(view, opts = {})
      view.(opts.merge(context: view_context))
    end

    def render_main(view, opts = {})
      view.(opts.merge(context: main_view_context))
    end

    def current_user
      double(:current_user, email: "jane@doe.org")
    end

    def view_context
      Admin::Container["view.context"].with(
        assets: Admin::Container["core.assets"],
        current_user: current_user,
        csrf_metatag: -> { '' },
        csrf_tag: -> { '' },
        csrf_token: -> { '' },
        fullpath: "",
        flash: {}
      )
    end

    def main_view_context
      Main::Container["view.context"].with(
        assets: Main::Container["core.assets"]
      )
    end
  end
end
