require_relative "db"

require "capybara/chromedriver/logger"
require "capybara/rspec"
require "capybara-screenshot/rspec"
require "rack/test"
require "selenium/webdriver"

Dir[SPEC_ROOT.join("support/web/*.rb").to_s].each(&method(:require))

require SPEC_ROOT.join("../system/boot").realpath

suite = CorleyCollection::TestSuite.instance

# Stop ES indexer in specs
Admin::Container[:indexer].stop

Capybara.app = Test::WebHelpers.app
Capybara.server = :puma, {Silent: true}
Capybara.server_port = suite.capybara_server_port
Capybara.default_max_wait_time = 5
Capybara.save_path = suite.tmp_dir.join("capybara-screenshot").to_s
Capybara.javascript_driver = :selenium_chrome_headless
Capybara::Screenshot.prune_strategy = {keep: 10}

# Uncomment these to print Chrome console log messages to output

# Capybara::Chromedriver::Logger.filters = [
#   /Download the React DevTools/i,
# ]

# RSpec.configure do |config|
#   config.after :each, type: :feature, js: true do
#     Capybara::Chromedriver::Logger::TestHooks.after_example!
#   end
# end

Capybara.register_driver :selenium_chrome_headless do |app|
  capabilities = Selenium::WebDriver::Remote::Capabilities.chrome(
    chromeOptions: {
      args: %w[
        headless
        disable-gpu
        window-size=1600,768
      ],
    },
    loggingPrefs: {
      browser: "ALL",
    }
  )

  Capybara::Selenium::Driver.new app,
    browser: :chrome,
    desired_capabilities: capabilities
end

RSpec.configure do |config|
  config.define_derived_metadata(file_path: /requests/) do |metadata|
    metadata[:type] = :request
  end

  config.include Rack::Test::Methods, type: :request
  config.include Rack::Test::Methods, Capybara::DSL, type: :feature
  config.include Test::WebHelpers

  config.before :suite do
    Test::WebHelpers.app.freeze
  end

  config.before(:each, authorized: true) do
    sign_in(admin)
  end

  config.before(:each, authorized_contributor: true) do
    sign_in_contributor(contributor)
  end

  config.after(:each) do
    Capybara.reset_sessions!
  end
end

RSpec::Matchers.define_negated_matcher :array_excluding, :include
