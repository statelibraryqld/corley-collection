module Test
  module WebHelpers
    module_function

    def sign_in(user)
      visit "/admin/sign-in"
      fill_in("user-email", with: user.email)
      fill_in("user-password", with: "password")
      click_on "Sign in"
    end

    def sign_in_contributor(contributor)
      visit "/contributors/sign-in"
      fill_in("contributor-email", with: contributor.email)
      fill_in("contributor-password", with: "password")
      click_on "Sign in"
    end

    def app
      CorleyCollection::Application.app
    end

    def fill_in_rich_text_editor(selector, with:)
      text = with

      page.driver.browser.action
        .move_to(find(selector).native)
        .click
        .send_keys(text)
        .perform
    end
  end
end
