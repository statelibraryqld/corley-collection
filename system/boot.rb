require_relative "corley_collection/container"

container = CorleyCollection::Container

container.finalize!

# Load backend systems
Dir[container.root.join("backend").realpath.join("*")].each do |f|
  require "#{f}/system/boot"
end

# Load sub-apps
Dir[container.root.join("apps").realpath.join("*")].each do |f|
  require "#{f}/system/boot"
end

require "corley_collection/application"
