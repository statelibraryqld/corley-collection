CorleyCollection::Container.boot(:assets) do |container|
  init do
    require "corley_collection/assets"
  end

  start do
    use :settings

    precompiled = container.config.env == :production || container.settings.precompiled_assets.to_s == "true"

    assets = CorleyCollection::Assets.new(
      root: container.root,
      precompiled: precompiled,
      server_url: container.settings.assets_server_url
    )

    register :assets, assets
  end
end
