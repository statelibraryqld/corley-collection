CorleyCollection::Container.boot(:exception_notifier) do |container|
  init do
    require "bugsnag"

    # Required for notifying exceptions in rake tasks
    require "rake"
    require "bugsnag/integrations/rake"
  end

  start do
    env = container.config.env

    rack_monitor.on(:error) do |payload|
      container[:exception_notifier].notify(payload[:exception])
    end

    if env == :production
      Bugsnag.configure do |config|
        config.project_root = container.root
        config.release_stage = env
        config.notify_release_stages = [:production]
        config.api_key = container.settings.bugsnag_api_key
        config.logger.level = Logger::INFO
      end

      register(:exception_notifier, Bugsnag)
    else
      class SilencedNotifier
        def self.notify(*); end
      end

      register(:exception_notifier, SilencedNotifier)
    end
  end
end
