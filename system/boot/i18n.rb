CorleyCollection::Container.boot(:i18n) do |container|
  init do
    require "i18n"
    require "dry/validation"
  end

  start do
    load_paths = Dir["#{container.root}/apps/**/config/locales/**/*.yml"]

    I18n.load_path += load_paths
    I18n.backend.load_translations

    register "i18n.t", I18n.method(:t)
  end
end
