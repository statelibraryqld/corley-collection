CorleyCollection::Container.boot(:persistence, namespace: true, from: :snowflakes) do
  before(:init) do
    # dry-types compatibility shims for rom-sql
    #
    # TODO: remove once rom-sql has an update compatible with dry-types >= 0.13
    require "dry/types"
    require "dry/types/compat/int"
    require "dry/types/compat/form_types"
  end

  after(:init) do
    require "sequel_pg"

    require "corley_collection/plugins/relations/taggable"
    require "corley_collection/plugins/relations/json"
    require "corley_collection/plugins/relations/orderable_by_values"

    ROM.plugins do
      register :taggable, CorleyCollection::Plugins::Relations::Taggable, type: :relation
      register :json, CorleyCollection::Plugins::Relations::Json, type: :relation
      register :orderable_by_values, CorleyCollection::Plugins::Relations::OrderableByValues, type: :relation
    end
  end

  configure do |config|
    config.database_url = container.settings.database_url
    config.max_connections = container.settings.web_max_threads
    config.global_extensions = [:postgres]
    config.connection_extensions = %i[error_sql pg_array pg_json pg_enum]
  end
end
