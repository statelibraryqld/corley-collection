CorleyCollection::Container.boot :que do |container|
  init do
    require "que"

    # Alias method to make AppSignal's Que integration compatible with the
    # new version of Que.
    Que::Job.class_eval do
      alias_method :attrs, :que_attrs
    end

    Que.log_formatter = -> data {
      return nil if data[:event] == :job_unavailable
      JSON.dump(data)
    }
  end

  start do
    use :exception_notifier

    use :logger
    Que.logger = logger

    Que.connection = container["persistence.db"]

    register :que, Que
  end
end
