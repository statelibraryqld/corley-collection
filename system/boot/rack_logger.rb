CorleyCollection::Container.boot :rack_logger do
  init do
    require "corley_collection/rack_logger"
  end

  start do
    use :logger

    rack_logger_class = Class.new(CorleyCollection::RackLogger) do
      configure do |config|
        config.filtered_params = %w[
          _csrf
          password
          password_confirmation
          card_token
        ]
      end
    end

    rack_logger = rack_logger_class.new(logger)
    rack_logger.attach(rack_monitor)
  end
end
