CorleyCollection::Container.boot :redis do |container|
  init do
    require "redis"
  end

  start do
    use :settings

    redis = Redis.new(url: container.settings.redis_url)

    container.register "redis", redis
  end
end
