CorleyCollection::Container.boot :s3, namespace: true do |container|
  init do
    require "aws-sdk-s3"
  end

  start do
    use :settings

    assets_client = Aws::S3::Client.new(
      access_key_id: container.settings.s3_assets_access_key_id,
      secret_access_key: container.settings.s3_assets_secret_access_key,
      region: container.settings.s3_region,
    )

    assets_resource = Aws::S3::Resource.new(client: assets_client)

    register "buckets.assets", assets_resource.bucket(container.settings.s3_assets_bucket)
  end
end
