CorleyCollection::Container.boot(:settings, from: :system) do
  before(:init) do
    require "types"
  end

  settings do
    # db
    key :database_url, Types::DatabaseURL(CorleyCollection::Container.env)
    key :session_secret, Types::Strict::String.constrained(filled: true)

    # assets
    key :assets_server_url, Types::Strict::String.constrained(filled: true).optional.default(nil)
    key :precompiled_assets, Types::Params::Bool

    # app
    key :app_domain, Types::Strict::String.constrained(filled: true)
    key :canonical_url, Types::Strict::String.constrained(filled: true)
    key :cdn_purge_delay_in_seconds, Types::Coercible::Integer.constrained(filled: true)
    key :ga_tracking_id, Types::Strict::String.constrained(filled: true).optional.default(nil)
    key :log_to_stdout, Types::Params::Bool
    key :origin_url, Types::Strict::String.constrained(filled: true)
    key :redis_url, Types::Strict::String.constrained(filled: true)
    key :bugsnag_api_key, Types::Strict::String.constrained(filled: true)
    key :postmark_api_key, Types::Strict::String.constrained(filled: true)
    key :immediate_story_publication, Types::Params::Bool
    key :web_max_threads, Types::Coercible::Integer.constrained(filled: true).default(5)

    # basic http auth
    key :basic_auth_username, Types::Strict::String.constrained(filled: true).optional.default(nil)
    key :basic_auth_password, Types::Strict::String.constrained(filled: true).optional.default(nil)

    # uploaded assets
    key :s3_region, Types::Strict::String.constrained(filled: true)
    key :s3_assets_bucket, Types::Strict::String.constrained(filled: true)
    key :s3_assets_prefix, Types::Strict::String.constrained(filled: true)
    key :s3_assets_access_key_id, Types::Strict::String.constrained(filled: true)
    key :s3_assets_secret_access_key, Types::Strict::String.constrained(filled: true)

    # Thumbor
    key :thumbor_security_key, Types::Strict::String.constrained(filled: true)
    key :thumbor_host, Types::Strict::String.constrained(filled: true)
  end
end
