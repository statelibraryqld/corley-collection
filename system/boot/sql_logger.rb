CorleyCollection::Container.boot :sql_logger do |container|
  init do
    require "dry/monitor/sql/logger"
  end

  start do
    if container.config.env != :production
      sql_logger_class = Class.new(Dry::Monitor::SQL::Logger) do
        configure do |config|
          config.colorize = (container.config.env == :development)
        end
      end

      sql_logger_class.new(logger).subscribe(notifications)
    end
  end
end
