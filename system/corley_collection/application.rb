require "dry/web/roda/application"
require "rack/auth/basic"

require "roda_plugins"
require_relative "container"
require "bugsnag"

module CorleyCollection
  class Application < Dry::Web::Roda::Application
    configure do |config|
      config.container = Container
    end

    use self[:rack_monitor]
    use Bugsnag::Rack if ENV["RACK_ENV"] == "production"

    PUBLIC_FILES = %w(
      /assets
      /api-docs
      /robots.txt
      /favicon.ico
    ).freeze

    plugin :static, PUBLIC_FILES, header_rules: [
      [:all, {"Cache-Control" => "public, max-age=86400"}],
      ["/assets", {"Cache-Control" => "public, max-age=31536000"}]
    ]
    plugin :error_handler

    if self["settings"].basic_auth_username && self["settings"].basic_auth_password
      use Rack::Auth::Basic do |username, password|
        username == self["settings"].basic_auth_username && password == self["settings"].basic_auth_password
      end
    end

    route do |r|
      r.on "admin" do
        r.run ::Admin::Application.freeze.app
      end

      r.run ::Main::Application.freeze.app
    end

    error do |e|
      self.class[:rack_monitor].instrument(:error, exception: e)
      raise e
    end
  end
end
