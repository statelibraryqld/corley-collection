require "logger"
require "dry/web/container"
require "dry/system/components"
require "snowflakes/components"

module CorleyCollection
  class Container < Dry::Web::Container
    # use :bootsnap

    load_paths! "lib"

    require "corley_collection/resolver"

    configure do |config|
      config.name = :corley_collection
      config.resolver = CorleyCollection::Resolver.new(config.env)
      config.default_namespace = "corley_collection"

      config.log_levels = %i[test development production].map { |e| [e, Logger::DEBUG] }.to_h

      config.auto_register = %w[
        lib/corley_collection
        lib/authentication
      ]
    end

    require "corley_collection/functions"
    register :inflector, CorleyCollection::Functions::Inflector.new

    def self.settings
      self[:settings]
    end
  end
end
