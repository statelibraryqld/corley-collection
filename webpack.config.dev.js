var path = require("path");

module.exports = {
  resolve: {
    alias: {
      hyperapp: path.resolve(__dirname, "apps/main/assets/public/hyperapp-monkeypatched/index.js")
    }
  },
  module: {
    rules: [
      {
        test: require.resolve("turbolinks"),
        use: ["imports-loader?this=>window"]
      },
      {
        test: require.resolve("fg-loadcss"),
        use: ["imports-loader?this=>window&exports=>undefined"]
      },
      {
        test: require.resolve("fg-loadcss/src/cssrelpreload.js"),
        use: ["imports-loader?this=>window"]
      },
      {
        test: require.resolve("url-polyfill"),
        use: ["imports-loader?this=>window"]
      }
    ]
  }
};
