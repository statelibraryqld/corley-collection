var fs = require("fs");
var path = require("path");
const appDirectory = fs.realpathSync(process.cwd());

const modulesForCompilation = [
  "query-string",
  "strict-uri-encode",
  "expired",
  "autotrack",
  "dom-utils"
];

module.exports = {
  resolve: {
    alias: {
      hyperapp: path.resolve(__dirname, "apps/main/assets/public/hyperapp-monkeypatched/index.js")
    }
  },
  module: {
    rules: [
      // Override icelab-assets configuration to remove the specific
      // `includes:` block in this app for now. This is necessary because
      // some node_modules (specifically query-string and its deps) need to be
      // compiled
      {
        test: /\.(js|jsx)$/,
        include: [
          path.resolve(appDirectory, "./apps"),
        ].concat(modulesForCompilation.map(m => (
          path.resolve(appDirectory, "./node_modules", m)
        ))),
        use: [{
          loader: 'babel-loader',
          options: {
            babelrc: false,
            presets: [require.resolve('babel-preset-react-app')],
            plugins: [require.resolve('babel-plugin-syntax-dynamic-import')],
          },
        }],
      },
      {
        test: require.resolve("turbolinks"),
        use: ["imports-loader?this=>window"]
      },
      {
        test: require.resolve("fg-loadcss"),
        use: ["imports-loader?this=>window&exports=>undefined"]
      },
      {
        test: require.resolve("fg-loadcss/src/cssrelpreload.js"),
        use: ["imports-loader?this=>window"]
      }
    ]
  }
};
